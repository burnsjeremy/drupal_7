<?php
  $rows = $themedata['rows'];
?>

<table>
  <thead>
    <tr>
      <td></td>
      <td>Page Name</td>
      <td>Page URL</td>
      <td>Activity Id</td>
      <td>Vars Sent for page</td>
    </tr>
  </thead>
  <tbody>
<?php
    for ( $i=0 ; $i < count($rows) ; $i++)  {
      print "<tr>";
      $link = l(t('edit'),
                "admin/settings/pdl2_mediamind/edit/".$rows[$i]->mm_id,
                array(
                  'attributes'=> array(
                    'class' => 'button_edit_tag',
                  )
                )
              );
      print "  <td>".$link."</td>";
      print "  <td>".$rows[$i]->page_name."</td>";
      print "  <td>".$rows[$i]->page_url."</td>";
      print "  <td>".$rows[$i]->activity_id."</td>";
      print "  <td>";
      $vars = unserialize($rows[$i]->serialized_vars);
      if (is_array($vars)) {
        foreach ($vars as $key => $value) {
          print $key.", ";
        }
      }
      print "  </td>";
      print "</tr>";
    }
?>
  </tbody>
</table>

<?php
  $link = l(t('Add a new tagging page'),
            "admin/settings/pdl2_mediamind/edit/-1",
            array(
              'attributes'=> array(
                'class' => 'button_new_tag',
              )
            )
          );
  print $link;
?>
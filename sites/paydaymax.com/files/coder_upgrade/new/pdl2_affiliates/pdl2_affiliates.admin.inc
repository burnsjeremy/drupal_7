<?php

function pdl2_affilates_admin() {
  $form = array();

  $form["pdl2_affiliate_linkshare_merchant_id"] = array(
    '#type' => 'textfield',
    '#title' => t('Linkshare Merchant ID'),
    '#description' => t('Merchant ID used in pixel.'),
    '#default_value' => variable_get('pdl2_affiliate_linkshare_merchant_id', ""),
    '#size' => 50,
    '#required' => TRUE,
  );

  $form["pdl2_affiliate_linkshare_ref"] = array(
    '#type' => 'textfield',
    '#title' => t('Linkshare'),
    '#description' => t('Default Linkshare refcode.'),
    '#default_value' => variable_get('pdl2_affiliate_linkshare_ref', ""),
    '#size' => 50,
    '#required' => TRUE,
  );

  $form["pdl2_affiliate_commission_junction_enterprise_id"] = array(
    '#type' => 'textfield',
    '#title' => t('Commission Junction Enterprise ID'),
    '#description' => t('Enterprise ID used in pixel.'),
    '#default_value' => variable_get('pdl2_affiliate_commission_junction_enterprise_id', ""),
    '#size' => 50,
    '#required' => TRUE,
  );

  $form["pdl2_affiliate_efficientfrontier_ref"] = array(
    '#type' => 'textfield',
    '#title' => t('Efficient Frontier'),
    '#description' => t('Default Efficient Frontier refcode. AKA Adobe AdLens.'),
    '#default_value' => variable_get('pdl2_affiliate_efficientfrontier_ref', ""),
    '#size' => 50,
    '#required' => TRUE,
  );

  $form["pdl2_affiliate_commissionjunction_ref"] = array(
    '#type' => 'textfield',
    '#title' => t('Commission Junction'),
    '#description' => t('Default Commission Junction refcode.'),
    '#default_value' => variable_get('pdl2_affiliate_commissionjunction_ref', ""),
    '#size' => 50,
    '#required' => TRUE,
  );

  $form["pdl2_affiliate_mypoints_ref"] = array(
    '#type' => 'textfield',
    '#title' => t('My Points'),
    '#description' => t('Default Commission Junction refcode.'),
    '#default_value' => variable_get('pdl2_affiliate_mypoints_ref', ""),
    '#size' => 50,
    '#required' => TRUE,
  );

  $form["pdl2_affiliate_efficientfrontier_company_abbreviation"] = array(
    '#type' => 'textfield',
    '#title' => t('Efficient Frontier Comapny Abbreviation'),
    '#description' => t('Efficient Frontier pixel abbreviation for this website.'),
    '#default_value' => variable_get('pdl2_affiliate_efficientfrontier_company_abbreviation', ""),
    '#size' => 50,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

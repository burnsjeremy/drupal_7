<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_dynamic_phone_admin($form, &$form_state) {
  $form = array();

  $form["pdl2_dynamic_phone_default"] = array(
    '#type' => 'textfield',
    '#title' => t('Default phone number'),
    '#default_value' => variable_get('pdl2_dynamic_phone_default', ""),
    '#size' => 100,
    '#description' => t('Default phone number to be used when no other number is accessible (eg DB failure)'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

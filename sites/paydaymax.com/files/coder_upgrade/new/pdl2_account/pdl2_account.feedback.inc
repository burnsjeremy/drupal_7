<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_feedback_page() {
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $output = drupal_get_form("pdl2_account_feedback_form");
  $output .= '<div class="return-to-account">' . l("<< Return to Account", "account") . '</div>';
  return $output;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_feedback_form($form) {
  $form = array();


  $pdl2 = pdl2_core_get_api();
  $companyinfo = $pdl2->getCompanyInfo();
  $company_number = $companyinfo->company->customer_service_number->phone->number;

  $form['1'] = array("#value" => "<h1>Customer Feedback</h1>");
  $form['2'] = array("#value" => "<p>On this page, you have the opportunity to submit a Testimonial regarding our services, or leave a Suggestion to help us serve you better! Please give a testimonial online or call $company_number to leave your testimonial.</p>");
  $form['3'] = array("#value" => "<p><strong>Please enter your testimonial or suggestion below</strong></p>");

  $form['feedback'] = array(
    "#type" => "textarea",
    "#title" => t("Testimonial/Suggestion"),
    "#required" => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Testimonial/Suggestion'),
  );

  $form['4'] = array("#value" => "<p></p><p>Please note this is <strong>NOT AN EMAIL</strong>. You will not receive a response. If you have any questions, please contact Customer Service at <strong>$company_number</strong>. Thank You!</p>");

  return $form;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_feedback_form_submit($form, &$form_state) {
  $pdl2 = pdl2_core_get_api();

  $values = $form_state['values'];
  $result = $pdl2->saveTestimonial($values['feedback']);

  if ($result) {
    drupal_set_message(t('Your feedback has been submitted.'));
  }
  else {
    drupal_set_message(t('An error occurred and we were unable to submit your feedback.'), 'error');
  }
}

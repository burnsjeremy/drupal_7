<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_howitworks_page($page = 'payday') {
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  if ($page == 'payday') {
    drupal_goto("node/" . variable_get('pdl2_account_howitworks_payday_nid', "1"));
  }
  else {
    drupal_goto("node/" . variable_get('pdl2_account_howitworks_personal_nid', "1"));
  }
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_howitworks_admin() {
  return drupal_get_form('pdl2_account_howitworks_form');
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_howitworks_form($form) {
  $form = array();

  $form["payday_page"] = array(
    '#title' => t('Page for How It Works - Payday'),
    "#type" => "textfield",
    "#maxlength" => 10,
    "#size" => 10,
    "#required" => TRUE,
    "#description" => "The node ID to display when 'How It Works' is clicked for Personal. Should be something like '23'.",
    "#default_value" => variable_get('pdl2_account_howitworks_payday_nid', ''),
  );
  $form["personal_page"] = array(
    '#title' => t('Page for How It Works - Personal'),
    "#type" => "textfield",
    "#maxlength" => 10,
    "#size" => 10,
    "#description" => "The node ID to display when 'How It Works' is clicked for Personal. Should be something like '24'.",
    "#required" => TRUE,
    "#default_value" => variable_get('pdl2_account_howitworks_personal_nid', ''),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_howitworks_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $payday_nid = $values['payday_page'];
  $personal_nid = $values['personal_page'];
  if (!is_numeric($personal_nid)) {
    form_set_error("personal_page", t("Personal Node ID entered must be a digit"));
  }
  if (!is_numeric($payday_nid)) {
    form_set_error("payday_page", t("Payday Node ID entered must be a digit"));
  }
  $per = node_load($personal_nid);
  if ($per == null) {
    form_set_error("personal_page", t("Personal Node ID entered doesn't exist. Find the page you want to use and enter the node ID."));
  }
  $pay = node_load($payday_nid);
  if ($pay == null) {
    form_set_error("payday_page", t("Payday Node ID entered doesn't exist. Find the page you want to use and enter the node ID."));
  }
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_howitworks_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  variable_set('pdl2_account_howitworks_payday_nid', $values['payday_page']);
  variable_set('pdl2_account_howitworks_personal_nid', $values['personal_page']);
  drupal_set_message('How it works pages updated.');
}

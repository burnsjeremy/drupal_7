<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_coupons_page() {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $pdl2 = pdl2_core_get_api();
  $company = $pdl2->getCompanyInfo();
  $siteName = pdl2_core_get_site_name();
  $themedata = array(
    "company" => $company,
    "siteName" => $siteName,
  );

  return theme('account/coupons', array('themedata' => $themedata));
}

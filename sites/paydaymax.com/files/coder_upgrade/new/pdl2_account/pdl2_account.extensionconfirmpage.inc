<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_extension_confirm_page() {

  $pdl2 = pdl2_core_get_api();

  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  //is opt out?
  else if ($pdl2->currUser->optOut) {
    drupal_goto('account/logout');
  }

  $loanId = $pdl2->getOpenLoan();

  if (empty($loanId)) {
    drupal_set_message('There was an error processing your extension.', 'error');
    drupal_goto('account');
  }

  $loanStatusObject = $pdl2->getLoanStatus($loanId);
  $loanStatus = $loanStatusObject->loan_status;

  $slugs = array(
    'approved' => array(4, 5),
    'denied' => array(9),
  );

  $slug = multidimensional_search($loanStatus->code, $slugs);
  $slug = !$slug ? 'accepted' : $slug;

  drupal_goto( 'account/extend/confirm/' . $slug );

}

function multidimensional_search($needle, $haystack) {

  if (empty($haystack) || !isset($needle)) {
    return false;
  }

  foreach ($haystack as $slug => $values) {
    if (array_search($needle, $values) === false) {
      continue;
    }
    else {
      return $slug;
    }
  }

  return;

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_extension_confirmstatus_page($loanStatus) {

  $pdl2 = pdl2_core_get_api();
  $loanId = $pdl2->getOpenLoan();

  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  //is opt out?
  else if ($pdl2->currUser->optOut) {
    drupal_goto('account/logout');
  }

  $themedata['loanStatus'] = $loanStatus;

  switch ($loanStatus) {
    case 'denied':
      $output = theme('extensiondenied', array('themedata' => $themedata));
      break;
    case 'approved':
      $themedata['transactions'] = $pdl2->getLoanTransactionsById($loanId);
      $themedata['originalLoanDueDate'] = $_SESSION['originalLoanDueDate'];
      $themedata['currentPaymentTotal'] = $_SESSION['currentPaymentTotal'];
      unset($_SESSION['originalLoanDueDate']);
      unset($_SESSION['currentPaymentTotal']);
      $themedata['loanDueDate'] = $pdl2->getLoanDueDate($loanId)->date;
      $themedata['loanConfirmation'] = $pdl2->getExtensionConfirmation($loanId);
      $output = theme('extensionapproved', array('themedata' => $themedata));
      break;
    default:
      $output = theme('extensionaccepted', array('themedata' => $themedata));
      break;
  }

  $output .= drupal_get_form('pdl2_account_extension_confirmstatus_form');

  return $output;

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_extension_confirmstatus_form($form) {

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );
  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_extension_confirmstatus_form_submit($form, &$form_state) {
  drupal_goto('account');
}

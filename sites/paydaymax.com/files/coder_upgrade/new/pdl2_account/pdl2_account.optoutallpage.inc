<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_optoutall_page() {
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
  else {
    $pdl2 = pdl2_core_get_api();
    $companyinfo = $pdl2->getCompanyInfo();
    $company_number = $companyinfo->company->customer_service_number->phone->number;

    $siteName = pdl2_core_get_site_name();
    $themedata = array(
      'siteName' => $siteName,
      "phone" => $company_number,
    );
    $output = theme('account/optoutall', array('themedata' => $themedata));
    return $output;
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_optoutall_form() {
  $form = array();

  $form["optout"] = array(
    "#type" => "checkbox",
    "#title" => t("Check here if you wish to opt out of all email communications and no longer wish to do any new business with us."),
    "#default_value" => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_optoutall_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $optout = $values["optout"] == 1;

  if ($optout) {
    $pdl2 = pdl2_core_get_api();
    $success = $pdl2->privacyOptOutAll(true);

    if ($success) {
      drupal_goto('account/optedout');
    }
    else {
      $message = t("We were unable to process your request at this time.");
      drupal_set_error($message);
    }
  }
  else {
    $message = t("You have elected not to opt out of electronic communications.");
    drupal_set_message($message);
  }
}

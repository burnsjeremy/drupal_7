<?php
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_login_page() {
  $output = drupal_get_form("pdl2_account_login_form");
  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_login_form($form) {

  $siteName = pdl2_core_get_site_name();

  $form = array();
  $form["headermsg"] = array(
    "#value" => t("Please enter your email address and password below to log in to " . $siteName),
  );

  $form["entered_email"] = array(
    "#type" => "textfield",
    "#title" => t("Email Address"),
    "#maxlength" => 255,
    "#required" => TRUE,
  );

  $form["entered_password"] = array(
    "#type" => "password",
    "#title" => t("Password"),
    "#maxlength" => 255,
    "#required" => TRUE,
  );

  $form["createaccount"] = array(
    "#value" => l("Create an account here!", "newaccount"),
  );

  $form["spc"] = array(
    "#value" => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
  );

  $form["forgotpwd"] = array(
    "#value" => l("Forgot your password?", "account/forgotpassword"),
  );

  $form["brk"] = array(
    "#value" => '<br/>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Login'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_login_form_validate($form, &$form_state) { }

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_login_form_submit($form, &$form_state) {

  $values = $form_state['values'];
  $email = $values["entered_email"];
  $password = $values["entered_password"];

  // In case we came a weird way in from the quick start form
  unset($_SESSION['email']);
  unset($_SESSION['password']);

  // Let's try and login!!!
  $pdl2 = pdl2_core_get_api();
  $login = $pdl2->login($email, $password, 'client');

  if (empty($login)) {
    drupal_set_message(t("An error occurred during login."), 'error');
    return;
  }

  if ($login->userId > 0) {

    $isEligible = $pdl2->customerLoanPaydayIsEligible();

    if (!is_bool($isEligible) && $isEligible === PDL2Controller::NON_BUSINESS_STATE) {
      $company = $pdl2->getCompanyInfo();
      $address = $pdl2->getHomeAddress();
      $state = $pdl2->getState($address->address->state);
      $pdl2->logout();
      drupal_set_message(t("At this time, %company is not offering loans in the state of %state", array('%company' => $company->company->name, '%state' => $state)), 'error');
    }

    if (isset($_SESSION["jumpto"])) {
      $jumpurl = $_SESSION["jumpto"];
      unset($_SESSION["jumpto"]);
      $form_state["redirect"] = $jumpurl;
    }
    else {
      $form_state["redirect"] = "account";
    }
  }
  else {
    if ($login->status->code == 901) {
      $chatinfo = $pdl2->getCompanyChatInfo($email);
      $livechatlink = $chatinfo->chat->url;
      drupal_set_message(t($login->status->detail . "<br/><br/>If you would like to chat to one of our friendly account representatives, please click " . l("here", $livechatlink)), 'error');
    }
    else {
      drupal_set_message(t("Incorrect username/password, try again!"), 'error');
    }
  }
}

<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_privacypref_page() {
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $siteName = pdl2_core_get_site_name();
  $themedata = array('siteName' => $siteName);
  $output = theme('account/privacy-preferences', array('themedata' => $themedata));
  return $output;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_privacypref_form() {

  $pdl2 = pdl2_core_get_api();
  $prefs = $pdl2->getPrivacyPreferences();

  $form = array();

  $form["birthdayoffers"] = array(
    "#type" => "checkbox",
    "#title" => t("I do not want to receive birthday emails or offers."),
    "#default_value" => $prefs->birthdayOptOut,
  );
  $form["holidayoffers"] = array(
    "#type" => "checkbox",
    "#title" => t("I do not want to receive holiday emails or offers."),
    "#default_value" => $prefs->holidayOptOut,
  );
  $form["specialoffers"] = array(
    "#type" => "checkbox",
    "#title" => t("I do not want to receive special emails or offers."),
    "#default_value" => $prefs->specialOptOut,
  );
  $form["affiliateinfo"] = array(
    "#type" => "checkbox",
    "#title" => t("I do not want information concerning my creditworthiness shared with your affiliates (Note: This does not prevent us from sharing your transaction and experience information with our affiliates.)"),
    "#default_value" => $prefs->affiliateOptOut,
  );
  $form["financialinfo"] = array(
    "#type" => "checkbox",
    "#title" => t("I do not want my personal financial information shared with third parties (other than service providers, joint marketers, and affiliates) without my express consent."),
    "#default_value" => $prefs->thirdPartyOptOut,
  );
  if ($prefs->smsOptOut === TRUE || $prefs->smsOptOut === FALSE) {
    $form["smsinfo"] = array(
      "#type" => "checkbox",
      "#title" => t("I do not want to receive SMS notifications."),
      "#default_value" => $prefs->smsOptOut,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save My Preferences'),
  );

  return $form;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_privacypref_form_submit($form, &$form_state) {

  $values = $form_state['values'];

  $birthdayOffers = $values["birthdayoffers"];
  $holidayOffers = $values["holidayoffers"];
  $specialOffers = $values["specialoffers"];
  $affiliateInfo = $values["affiliateinfo"];
  $financialInfo = $values["financialinfo"];
  $smsInfo = $values["smsinfo"];

  $prefsModel = new PrivacyPreferencesModel(null /*StatusModel*/,
                                            $birthdayOffers,
                                            $holidayOffers,
                                            $specialOffers,
                                            $affiliateInfo,
                                            $financialInfo,
                                            $smsInfo);

  $pdl2 = pdl2_core_get_api();
  $success = $pdl2->setPrivacyPreferences($prefsModel);

  if ($success) {
    drupal_set_message(t("Your privacy preferences have been saved."));
  }
  else {
    drupal_set_message(t("An error occurred and we were unable to save your privacy preferences."));
  }
}

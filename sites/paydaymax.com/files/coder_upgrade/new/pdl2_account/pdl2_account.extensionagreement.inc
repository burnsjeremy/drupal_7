<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_extensionagreement_page() {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');

  }
  else {

    $pdl2 = pdl2_core_get_api();

    $loanId = $pdl2->getOpenLoan();

    if (empty($loanId)) {
      drupal_set_message('There was an error processing your extension.', 'error');
      drupal_goto('account');
    }
    else {
      $financeCharge = $pdl2->getExtensionFinanceCharge($loanId);

      $agreement = $pdl2->getDraftExtensionAgreement($loanId);
      $borrower = $pdl2->getUserInfo();
      $apr = $pdl2->getExtensionApr($loanId);
      $ssn = $pdl2->getSsn();

      $_SESSION['originalLoanDueDate'] = $agreement->loan_agreement->dueDatePrevious->date;

      $themedata = array(
        'agreement' => $agreement,
        'date' => $agreement->loan_agreement->dueDatePrevious->date,
        'borrower' => $borrower,
        'apr' => $apr,
        'ssn' => $ssn,
      );

      $output = drupal_get_form('pdl2_account_extensionagreement_form', $themedata);

      return $output;
    }
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_extensionagreement_form($form, $form = array(), $themedata = array()) {
  $form = array();

  $form['pageHeader'] = array('#value' => '<h1>Loan Agreement</h1>');
  $form['agreementIntro'] = array(
    '#value' => '<p class="agreementintro"><strong>Before a loan can be completed, you must check "I Agree" at the bottom of this agreement.</strong></p>',
  );
  $form['lenderborrower'] = array('#value' => theme('lenderborrower', array('themedata' => $themedata)));

  $form['dashboard'] = array(
    '#prefix' => '<div class="dashboard">',
    '#suffix' => '</div>',
  );


  $form['dashboard']['agreementHeader'] = array('#value' => '<h4>Loan Agreement</h4>');
  $form['dashboard']['agreementText'] = array('#value' => $themedata['agreement']->loan_agreement->agreementText);

  $form['extend_agree'] = array(
    '#type' => 'checkbox',
    '#title' => 'I agree <span class="form-required">*</span>',
  );

  $form['buttons'] = array(
    '#prefix' => '<div class="buttons">',
    '#suffix' => '</div>',
  );
  $form['buttons']['next'] = array(
    '#type' => 'submit',
    '#value' => t('Extend My Loan'),
    '#attributes' => array('class' => 'rright'),
  );
  /*$form['submit'] = array('#type' => 'submit',
   '#value' => t('Extend My Loan'));*/

  $form['buttons']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('CANCEL'),
    '#validate' => array('pdl2_application_loanagreement_cancel_validate'),
    '#submit' => array('pdl2_application_loanagreement_cancel_submit'),
    '#attributes' => array('class' => 'lleft'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_extensionagreement_form_validate($form, &$form_state) {

  $extend_agree = $form_state['values']['extend_agree'];

  if (!$extend_agree) {
    form_set_error('loan_agree', t('You must agree to the terms.'));
  }

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_extensionagreement_form_submit($form, &$form_state) {
  $pdl2 = pdl2_core_get_api();

  $loanId = $pdl2->getOpenLoan();

  if (empty($loanId)) {
    drupal_goto('account');
  }
  else {
    $_SESSION['currentPaymentTotal'] = $pdl2->getCurrentPaymentTotal($loanId);
    $result = $pdl2->extensionSave($loanId);

    drupal_goto('account/extend/confirm');
  }
}
function pdl2_application_loanagreement_cancel_submit($form, &$form_state) {
  drupal_goto('account');
}


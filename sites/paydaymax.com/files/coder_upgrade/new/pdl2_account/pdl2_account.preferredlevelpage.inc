<?php
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_preferredlevel_page() {
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
  else {
    $pdl2 = pdl2_core_get_api();
    $siteName = pdl2_core_get_site_name();
    $imagePath = base_path() . drupal_get_path("module", "pdl2_account") . "/images/";

    $memberStatus = $pdl2->getPreferredMemberStatus();

    //Figure out the next level
    $current = null;
    foreach ($memberStatus->membership_potential->result as $level) {
      if ($current != null) {
        $nextLevel = $level->goal->membership->name;
        $nextLevelId = $level->goal->membership->id;
        $untilNextCount = $level->remaining_loans;
        break;
      }
      if ($level->goal->membership->id == $memberStatus->membership->id) {
        $current = $level;
      }
    }

    $themedata = array(
      'siteName' => $siteName,
      'imagePath' => $imagePath,
      'memberStatus' => $memberStatus->membership->name,
      'nextLevel' => $nextLevel,
      'nextLevelId' => $nextLevelId,
      'untilNextCount' => $untilNextCount,
      'atPlatinum' => ($memberStatus->membership->id == 3),
    );

    $output = theme('account/preferred_level', array('themedata' => $themedata));

    return $output;
  }
}

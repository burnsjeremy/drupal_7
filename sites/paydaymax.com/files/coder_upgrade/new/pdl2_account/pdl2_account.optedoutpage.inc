<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_optedout_page() {
  //not not opt out
  if ( !pdl2_account_not_opt_out() ) {
    //log out
    if (!pdl2_account_is_logged_in()) {
      drupal_goto("account/login");
    }
  }
  else {
    $pdl2 = pdl2_core_get_api();
    $companyinfo = $pdl2->getCompanyInfo();
    $pdl2->logout();
    $company_number = $companyinfo->company->customer_service_number->phone->number;
    $themedata['number'] = $company_number;
    $output = theme('account/optedout', array('themedata' => $themedata));
    return $output;
  }
}

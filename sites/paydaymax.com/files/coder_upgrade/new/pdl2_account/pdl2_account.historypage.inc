<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_history_page($loan_type) {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $pdl2 = pdl2_core_get_api();
  $loanHistory = $pdl2->getCustomerLoanHistory($loan_type);
  $themedata = array(
    "loanHistory" => $loanHistory,
    "loan_type" => $loan_type,
  );
  $output = theme('account/history', array('themedata' => $themedata));
  return $output;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_history_page_loan_status($loan_type, $loan_id) {
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $pdl2 = pdl2_core_get_api();
  $loan = $pdl2->getLoanById($loan_id, $loan_type);
  $agreement = $pdl2->getLoanAgreement($loan_id, $loan_type);
  $agreements = $pdl2->getLoanAgreements($loan_id, $loan_type);
  $history = $pdl2->getLoanHistoryById($loan_id, $loan_type);
  $trans = $pdl2->getLoanTransactionsById($loan_id, $loan_type);
  $status = $pdl2->getLoanStatus($loan_id, $loan_type);
  $balance = $pdl2->getLoanBalance($loan_id, $loan_type);
  $user = $pdl2->getUserInfo();
  $referral = $pdl2->getReferralsList();
  $referralAmount = $pdl2->getReferralAmountReceived();
  $referralBalance = $pdl2->getReferralBalance();


  $referralTransactions = $pdl2->customerGetTransactions('referral-credit');

  // Let's see if we have an "override" message here
  $msg = $history->loanHistory->loan->loan->messages->customer_status_message;
  if ($msg) {
    $overrideMsg = $msg;
  }

  $msg = $history->loanHistory->loan->loan->messages->customer_loan_summary;
  if ($msg) {
    $overrideBalanceMsg = $msg;
  }

  $payments = array();
  $amt_deposited = 0;
  $amt_charges = 0;
  $i = 0;
  if ($trans->loanTransactions) {
    foreach ($trans->loanTransactions->result as $key => $value) {
      if ( ($value->transaction->type == 'deposit') && ($value->transaction->status == 'completed')) {
        $amt_deposited = $amt_deposited + $value->transaction->payment->payment->amount;
      }
      if ( (($value->transaction->type == 'extension-fee') && ($value->transaction->status == 'completed')) ||
           (($value->transaction->type == 'finance-charge') && ($value->transaction->status == 'completed')) ||
           (($value->transaction->type == 'return-fee') && ($value->transaction->status == 'completed')) ) {
        $amt_charges = $amt_charges + $value->transaction->payment->payment->amount;
      }
      if (($value->transaction->status != 'canceled') && ($value->transaction->type != "deposit")) {
        $payments[$i] = array(
          'action' => $value->transaction->type,
          'date' => $value->transaction->date->date,
          'amount' => $value->transaction->payment->payment->amount,
          'status' => $value->transaction->status,
        );
      }
      if ($value->transaction->type == 'deposit' && $value->transaction->status != 'canceled') {
        $deposits[$i] = array(
          'date' => $value->transaction->date->date,
          'amount' => $value->transaction->payment->payment->amount,
          'status' => $value->transaction->status,
        );
      }
      $i++;
    }
  }

  /**
   For payments, as per Chris McCord and PDLNEW-2838 and PDLNEW-2886:
   1) show ALL completed transactions; THEN
   2) show any pending transactions on the next due date
   */

  usort($payments, "pdl2_account_cmp_payment_date_action");

  $newpayments = array();
  $pendingDate = null;
  foreach ($payments as $payment) {
    if ($payment["status"] == "completed") {
      array_push($newpayments, $payment);
    }
    else if ($payment["status"] == "pending") {
      if ($pendingDate != null && $pendingDate != $payment["date"]) {
        break;
      }
      else {
        $pendingDate = $payment["date"];
        array_push($newpayments, $payment);
      }
    }
  }

  $payments = $newpayments;
  // Sort by status then date PDLNEW-2838
  //usort($payments, "pdl2_account_cmp_payment_status");

  // Grab the first 2 - PDLNEW-2886
  //$payments = array_slice($payments, 0, 2);

  $themedata = array(
    "loan" => $loan->loan,
    "customer" => $user->customer,
    "agreement" => $agreement->loan_agreement,
    "agreements" => $agreements,
    "loan_history" => $history->loanHistory,
    "loan_transactions" => $trans->loanTransactions,
    "loan_status" => $status->loan_status,
    "loan_type" => $loan_type,
    "loan_balance" => isset($overrideBalanceMsg) ? $overrideBalanceMsg : $balance,
    "referral_amount" => $referralAmount,
    "referral_balance" => $referralBalance,
    "fees_finance_chanrges" => $amt_charges,
    "amt_deposited" => $amt_deposited,
    "referral_list" => $referral,
    "payments" => $payments,
    "deposits" => $deposits,
    "overrideMsg" => $overrideMsg,
    "referralTransactions" => $referralTransactions,
  );

  // PDMNEW-2899 - don't show payments for loans in open return or closed return status
  $status = $loan->loan->status->loan_status->code;
  if ($status == 13 /* Open Return */ || $status == 12 /* Closed Return*/) {
    unset($themedata["payments"]);
  }


  $output = theme('account/loanstatus', array('themedata' => $themedata));
  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_cmp_payment_date_action($a, $b) {
  if ($a["date"] > $b["date"]) {
    return 1;
  }
  else if ($a["date"] < $b["date"]) {
    return -1;
  }
  else {
    if ($a["action"] == "payment") {
      return -1;
    }
    else {
      return 1;
    }
  }
}
// Order by status (pending first) then by date
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_cmp_payment_status($a, $b) {
  if ($a["status"] == $b["status"]) {
    if ($a["date"] > $b["date"]) { // Oldest first PDLNEW-2886
      return 1;
    }
    else if ($a["date"] < $b["date"]) {
      return -1;
    }
    else {
      return 0;
    }
  }
  else if ($a["status"] == "pending") {
    return -1;
  }
  else {
    return 1;
  }
}

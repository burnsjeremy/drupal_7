<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_bankinginfo_page() {
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $pdl2 = pdl2_core_get_api();

  $imagePath = base_path() . drupal_get_path("module", "pdl2_account") . "/images/";

  $themedata = array("imagePath" => $imagePath);

  $output = theme('account/bankinginfo', array('themedata' => $themedata));
  return $output;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_bankinginfo_form() {
  $form = array();

  $form["routing_number"] = array(
    '#title' => t('Routing Number'),
    "#type" => "textfield",
    "#maxlength" => 9,
    "#size" => 20,
    "#required" => TRUE,
  );
  $form["account_number"] = array(
    '#title' => t('Bank Account Number'),
    "#type" => "textfield",
    "#maxlength" => 14,
    "#size" => 20,
    "#required" => TRUE,
  );
  $form["account_number_2"] = array(
    '#title' => t('Confirm Bank Account Number'),
    "#type" => "textfield",
    "#maxlength" => 14,
    "#size" => 20,
    "#required" => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Bank Details'),
  );

  return $form;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_bankinginfo_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if (strlen($values['routing_number']) != 9) {
    form_set_error("routing_number", t("Routing number is 9 digits"));
  }
  if ($values['account_number'] != $values['account_number_2']) {
    form_set_error("account_number", t('Account numbers do not match'));
    form_set_error("account_number_2", t(''));
  }
  if (strlen($values['account_number']) < 4) {
    form_set_error("account_number", t('Bank Account Number is at least 4 digits.'));
  }
}


/**
 * This currently ignores all information from the form, but the email address.
 * Note - the referral code doesn't seem to be here either.....
 */
function pdl2_account_bankinginfo_form_submit($form, &$form_state) {
  $pdl2 = pdl2_core_get_api();
  $values = $form_state['values'];

  $routing_number = $values['routing_number'];
  $account_number = $values['account_number'];

  try {
    $pdl2->setBankAccount($routing_number, $account_number);
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
    drupal_set_message("Banking Details not changed. Please try again.", 'error');
  }

}

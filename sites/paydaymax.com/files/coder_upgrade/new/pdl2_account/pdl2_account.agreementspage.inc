<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_account_agreements_page() {
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $pdl2 = pdl2_core_get_api();
  $loanId = $pdl2->getOpenLoan();

  // Check we were given a loan ID
  if (!isset($loanId) || $loanId == "") {
    return theme('account/agreements', array('themedata' => null));
  }

  $loan_type = $pdl2->getLoanType($loanId);
  // Try and get the agreements for this loan ID and client ID
  $agreements = $pdl2->getLoanAgreements($loanId, $loan_type);
  if (!$agreements) {
    return theme('account/agreements', array('themedata' => null));
  }

  usort($agreements, "agreements_cmp");

  $themedata = array(
    'loanAgreements' => $agreements,
    'loanId' => $loanId,
  );

  $output = theme('account/agreements', array('themedata' => $themedata));
  return $output;
}

function agreements_cmp($a, $b) {
  if ($a == $b) {
    return 0;
  }
  return ($a->agreementDate->date < $b->agreementDate->date) ? 1 : -1;
}

<div>
  <h1>Sorry!</h1>
  <p>
    We're sorry, your payment has been declined.  
    To process your payment using a different card, click <a href="<?php echo url('account/card-payment'); ?>">here</a>.   
    You also have the option of wiring your payment in via 
    Moneygram or Western Union using the wire instructions listed below. 
  </p> 
  <p>
    You can use the wire information included below to submit a payment, 
    however, it is important that you immediately contact <strong><?php echo $themedata['collection_co']->full_name; ?></strong> 
    at <strong><?php echo $themedata['collection_co']->phone; ?></strong> to set up a payment arrangement. 
  </p>
  <p>
    <strong>
    If you have an active payment arrangement it is imperative that you maintain the 
    payment arrangement to avoid any negative ramifications. 
    </strong>
  </p>
  <p>
    You need to go to Western Union and get the BLUE QUICK COLLECT FORM (or go to 
    <a href="http://www.westernunion.com">www.westernunion.com</a> and choose the quick collection option).
  </p>
  <ol>
    <li>
      COMPANY NAME: <?php echo strtoupper($themedata['collection_co']->full_name); ?>    </li>
    <li>
      CODE CITY: <?php echo 'OFFER'; ?> (ONE WORD NO SPACES)
    </li>
    <li>
      STATE: <?php echo 'NEVADA'; ?>
    </li>
    <li>
      ACCOUNT #: <?php echo $themedata['loanId']; ?>
    </li>
    <li>
      REFERENCE #: Your Telephone Number
    </li>
  </ol> 
  <p>
    <strong>
      You can also use your debit card or various charge cards at www.westernunion.com.
    </strong>
  </p>
  <h3>OR</h3>
  <p>
    You can go to Moneygram and get the BLUE EXPRESSPAYMENT FORM.
  </p>
  <ol>
    <li>
      The Company name is: <?php echo 'CPD-OFFER'; ?>
    </li>
    <li>
      The Receive Code is: <?php echo '4243'; ?>
    </li>
    <li>
      The City is: <?php echo 'Las Vegas'; ?>
    </li>
    <li>
      The State is: <?php echo 'Nevada'; ?>
    </li>
  </ol>
</div>
<?php
  $siteName = $themedata['siteName'];
?>
<div id="logout_page" class="log-out">
  <h3>Thank You</h3>
  <p>Thank you for choosing <?= $siteName ?> to handle your payday loan needs. We value your business and we look forward to providing you with the top-notch service that you deserve.</p>
  <p>If you have questions, call us at (877) 937-3729. Our customer service agents are here to help 24 hours a day, 6 days a week.</p>
  <p><a href="login">Click to log back in</a></p>
</div>
<?php
  $d = $themedata["userDetails"]->customer;
  $bd = $themedata["bankDetails"]->bank_account;
  $canedit = $themedata["hasEditRights"];
?>
<div id="width-stretch">
<div id="ApplicationArea">
  <div class="my-details">
    <h1 class="clean">Personal Information</h1>
  
    <?php if (!$canedit) { ?>
    <p><strong>Changes?</strong> Contact customer service for assistance.</p>
    <?php } ?>
  
      <h2 class="clean">General</h2>
      <div class="borders"></div>
      <div class="no-fields">
        <span class="label">Name:</span> 
        <span class="data"><?php print $d->first_name . ($d->middle_name ? " " . $d->middle_name . " " : "") . $d->last_name; ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">  
        <span class="label">Date of Birth:</span>
        <span class="data"><?php print $themedata['dob']->date;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">
        <span class="label">Address 1:</span>
        <span class="data"><?= $d->home_address->address->street_primary; ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">  
        <span class="label">Address 2:</span>
        <span class="data"><?= $d->home_address->address->street_secondary; ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">  
        <span class="label">City:</span>
        <span class="data"><?= $d->home_address->address->city; ?></span>
      </div>
      <div class="borders"></div>  
      <div class="no-fields">  
        <span class="label">State:</span>
        <span class="data"><?= $d->home_address->address->state;?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">  
        <span class="label">Zip Code:</span>
        <span class="data"><?= $d->home_address->address->zip; ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">  
        <span class="label">E-Mail:</span>
        <span class="data"><?= $d->email->emailaddress->result; ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">  
        <span class="label">SSN:</span>
        <span class="data"><?php print $d->ssn;  ?></span>
      </div><!--end General-->
      
      <h2 class="clean">Drivers License</h2>
      <div class="borders"></div>
      <div class="no-fields">
        <span class="label">Number:</span>
        <span class="data"><?php print $themedata['dlicense']->number;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">  
        <span class="label">State Issued:</span>
        <span class="data"><?php print $themedata['dlicense']->state;  ?></span>
      </div><!--end Drivers License-->
      
      <h2 class="clean">Personal Phones</h2>
      <div class="borders"></div>
      <div class="no-fields"> 
        <span class="label">Home:</span>
        <span class="data"><?= $d->phonenumbers->phone_numbers->homephone; ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Cell:</span>
        <span class="data"><?= $d->phonenumbers->phone_numbers->cellphone; ?></span>
      </div><!--end Personal Phones-->
  
      <h2 class="clean">Residency</h2>
      <div class="borders"></div>
      <div class="no-fields"> 
        <span class="label">Rent or Own?:</span>
        <span class="data"><?= $themedata['residency']; ?></span>
      </div><!--end Residency-->  
      
      <h2 class="clean">Income/Employment</h2>
      <div class="borders"></div>
      <div class="no-fields"> 
        <span class="label">Income Source:</span>
        <span class="data"><?php print $themedata['income_source'];  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Employer:</span>
        <span class="data"><?php print $themedata['employer_name']; ;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Department:</span>
        <span class="data"><?php print $themedata['employment_department'];  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Address 1:</span>
        <span class="data"><?php print $themedata['emp_address']->street_primary;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Address 2:</span>
        <span class="data"><?php print  $themedata['emp_address']->street_secondary;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">City:</span>
        <span class="data"><?php print  $themedata['emp_address']->city;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">State:</span>
        <span class="data"><?php print  $themedata['emp_address']->state;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Zip Code:</span>
        <span class="data"><?php print  $themedata['emp_address']->zip;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Started:</span>
        <span class="data"><?php print $themedata['employment_startdate'];  ?></span>
      </div><!--end Income/Employment-->  
      
      <h2 class="clean">Work Phones</h2>
      <div class="borders"></div>
      <div class="no-fields"> 
        <span class="label">Main:</span>
        <span class="data"><?php print $themedata['employment_phones']->workphone;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Verify:</span>
        <span class="data"><?php print $themedata['employment_phones']->workpager;  ?></span>
      </div><!--end Work Phones--> 
       
      <h2 class="clean">Pay Info</h2>
      <div class="borders"></div>
      <div class="no-fields"> 
        <span class="label">Monthly Pay:</span>
        <span class="data">$<?php print $themedata['pay_frequency']->PayFrequency->montly_pay_amount;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Direct Deposit:</span>
        <span class="data"><?= ($themedata['pay_frequency']->PayFrequency->is_direct_deposit == 1) ? 'Yes' : 'No' ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Pay Day:</span>
        <span class="data"><?php print $themedata['pay_frequency']->PayFrequency->payday_name;  ?></span>
      </div><!--end Pay Info-->
         
      <h2 class="clean">Bank Info</h2>
      <div class="borders"></div>
      <div class="no-fields"> 
        <span class="label">Name:</span>
        <span class="data"><?php print $bd->bank_name;  ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Phone:</span>
        <span class="data"><?php  print $bd->bank_phone; ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Routing #:</span>
        <span class="data"><?= $bd->routing_number ?></span>
      </div>
      <div class="borders"></div>
      <div class="no-fields">   
        <span class="label">Account #:</span>
        <span class="data"><?= $bd->account_number ?></span>
      </div><!--end Bank Info-->
      
  <div class="clear"></div>
  <div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>
  </div><!--end .mydetails-->
</div><!--end #ApplicationArea-->
</div><!--end #width-stretch-->

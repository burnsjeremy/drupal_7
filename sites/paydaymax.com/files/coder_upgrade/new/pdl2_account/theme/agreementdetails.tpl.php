<?php
  $agreement = $themedata["agreement"]->loan_agreement;
?>
    
<div id="myaccount_agreement_container">
    
  <h1>View Loan Agreement</h1>
    
  <div id="fullAgreement">
  <?php
     if ($agreement->parentLoanId){
        //dd('has parent loan id');
        print theme('personal_lenderborrower', $themedata);
     } else {
        //dd('no has parent loan id');
        print theme('lenderborrower', $themedata);
     }
  ?>
    
  <div id="loanagreement">
    <h4>Loan Agreement</h4>
    <?= $agreement->agreementText; ?>
    <p></p>
    <div id="efttext">
      <?= $agreement->eftTextYes; ?>
    </div>
    <div id="signature">
      <p><?= $agreement->fullName ?></p>
    </div>
  </div>
    
  </div>
</div>

<div class="return-to-account"><?php print l("View Another Agreement", "account/agreements"); ?></div>
<div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>

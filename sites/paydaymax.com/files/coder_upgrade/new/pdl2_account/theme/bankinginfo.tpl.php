<div id="Whole">
    <div id="banking" class="Wrapper Rounded">
  <h1>Your Banking Info</h1>
    <p>This section must contain traditional checking account information only. No savings account,
    debit card, credit cards or payroll cards will be accepted.</p>
  <?php $path = drupal_get_path('module', 'pdl2_account'); ?>
  <img src="<?php print $themedata['imagePath']; ?>/check.gif" alt="example check">

  <?= drupal_get_form("pdl2_account_bankinginfo_form"); ?>

</div>

<div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>

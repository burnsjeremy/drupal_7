<div id="Whole">

  <h1>MoneyGram Information</h1>

  <div id="money_gram_info" class="Wrapper Rounded">


    <div class="moneygram-info">
      <?php
        if ($themedata['allowed'] == 1) {
          print "<p>The MoneyGram option is available to you</p>";
        }
        else {
          print "<p>The MoneyGram s not Available</p>";
        }
      ?>
    </div>

    <p class="info">
      We are pleased to announce another choice our customers have to receive
      money.  Customers that have had at least one paid-in-full closed loan
      with us will be eligible to receive their next loan via MoneyGram.
      For a low $10 fee <strong>an approved loan will be immediately
      available at any of MoneyGram&quot;s U.S. based agent locations, such as Wal-Mart</strong>.
      MoneyGram is the largest processor of money orders in the U.S.
    </p>

    <p class="info">
      You can search <strong>for a MoneyGram location near you from the MoneyGram website:
      <a href="http://www.moneygram.com">http://www.moneygram.com</a></strong>
    </p>

  </div>

  <div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>

</div>

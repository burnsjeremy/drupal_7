<?php
    $siteName = $themedata["siteName"];
    $imagePath = $themedata["imagePath"];

    $silverImagePath = $imagePath . 'silver_member_sm.gif';
    $goldImagePath = $imagePath . 'gold_member_sm.gif';
    $platinumImagePath = $imagePath . 'platinum_member_sm.gif';

    if($themedata['isPlatinum']) {
        $nextImagePath = $platinumImagePath;
    } else {
    	if ($themedata['nextLevelId'] == 1)
	    	$nextImagePath = $silverImagePath;
	    else if ($themedata['nextLevelId'] == 2)
	    	$nextImagePath = $goldImagePath;
	    else if ($themedata['nextLevelId'] == 3)
	    	$nextImagePath = $platinumImagePath;

    }
?>

<br/>

<h1>The PayDayMax Preferred Member Rewards Program</h1>
<div id="member-lvl">
  <h2>You are a <strong><?php echo $themedata['memberStatus'];?>!</strong></h2>
  <div id="next-lvl">
    <p>
      <?php if ($themedata['atPlatinum']): ?>
      <?php else: ?>
        You will be a <strong><?php echo $themedata['nextLevel'];?> </strong> after <strong><?php echo $themedata['untilNextCount'];?></strong> more successful loan<?php if ($themedata['untilNextCount'] != 1) echo "s"; ?>.
	    </p>
	    <?//<img width="71" height="52" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw%3D%3D" />?>
	    <img width="71" height="52" src="<?php echo $nextImagePath?>"/>
      <?php endif; ?>
  </div>
</div>
<p><?php echo $siteName; ?> is now offering you yet another great way to save money on your loans!</p>
<p>Our <strong>Preferred Member Rewards Program</strong> has been created to show our appreciation to you by making <?php echo $siteName; ?> your payday loan source.</p>
<p><?php echo $siteName; ?> would like to thank all of our loyal customers by offering you the chance to receive additional discounts on our already low interest rates.&nbsp; 
Depending on how many loans you successfully close with <?php echo $siteName; ?>, you can receive discounts of <strong>up to 30% off initial fees</strong>.</p>
<p>Here&rsquo;s how the <strong>Preferred Member Rewards Program</strong>* works:</p>
<div class="silver">
	<h3>SILVER Member</h3>
	<div class="body">
		<p>Once you have successfully closed <strong>5 loans</strong> with <?php echo $siteName; ?>, you&rsquo;ll qualify as a <strong>Silver member</strong> and be eligible to receive a 10% discount off initial fees starting with your 6th loan!</p>
		<h4><span>10%</span> discount</h4>
	</div>
</div>
<div class="gold">
	<h3>GOLD Member</h3>
	<div class="body">
		<p>Once you have successfully closed <strong>10 loans</strong> with <?php echo $siteName; ?>, you&rsquo;ll qualify as a <strong>Gold member</strong> and be eligible to receive a 20% discount off initial fees starting with your 11th loan!</p>
		<h4><span>20%</span> discount</h4>
	</div>
</div>
<div class="platinum">
	<h3>PLATINUM Member</h3>
	<div class="body">
		<p>Once you have successfully closed <strong>15 loans</strong> with <?php echo $siteName; ?>, you&rsquo;ll qualify as a <strong>Platinum member</strong> and be eligible to receive a 30% discount off initial fees starting with your 16th loan!</p>
		<h4><span>30%</span> discount</h4>
	</div>
</div>
<p>*Offer does not apply to extensions. Offer only good on loans applied for after the start of the promotion. No other offers or discounts may be used with this offer. This promotion may expire at any time without notice.</p>

<div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>

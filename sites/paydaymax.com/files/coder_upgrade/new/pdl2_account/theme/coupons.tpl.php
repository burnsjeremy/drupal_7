<div id="Whole">
  <div id="cash_advances_discount_coupon" class="Wrapper Rounded">
    <h1>Welcome To <?php print $themedata["siteName"]; ?> NEW Discount Coupons! </h1>
    <p><?php print $themedata["siteName"]; ?> is always looking for ways to give you the best service possible. With our New Discount Coupons,
      returning customers can earn up to 30% off their next payday loan. Your discount coupon is so simple to use, you
      can't lose when it comes to getting the cash you need.</p>

    <h2>How do I get my discount coupon?</h2>
    <p>Customers who successfully close their payday loan with <?php print $themedata["siteName"]; ?> may be eligible for a discount coupon.
      Those who qualify will receive their discount coupon code by e-mail.* Just make sure we have your correct e-mail
      address, so that you can get the money saving coupon you've earned.</p>

    <h2>How do I use my discount coupon?**</h2>
    <p>Simply login to your account and start the loan application process. Enter the discount coupon code when you see
      the coupon discount box. Once your payday loan is approved, you'll get your money in no time. It really is that
      easy! If you already have your coupon,
      <?php print l('apply here and start saving today!', 'apply/payday'); ?>
    </p>

    <p class="outro"><?php print $themedata["siteName"]; ?></p>

    <p><small>*Certain restrictions apply to all discount coupon offers.</small></p>
    <p><small>**Discount coupons cannot be used with any other offer. This discount coupon is intended to be used by the
      recipient and cannot be transferred or used by anyone else.</small></p>
  </div>

  <div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>
</div>

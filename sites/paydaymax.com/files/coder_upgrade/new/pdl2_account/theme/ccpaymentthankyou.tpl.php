<div>
	<h1>Thank You!</h1>
	<p>Thank you for your payment! Your payment of $<?php echo number_format(floatval($themedata['amount']), 2); ?> has been successfully processed. This transaction will show up on your statement as <strong><?php echo strtoupper($themedata['collection_co']->full_name); ?></strong> and will post to your account today.</p>
	<?php if($themedata['balance'] > 0): ?>
		<p>You still have a remaining balance due on your loan. It is very important that you contact <strong><?php echo strtoupper($themedata['collection_co']->full_name); ?></strong> at <strong><?php echo strtoupper($themedata['collection_co']->phone); ?></strong> so that they can help you to establish a payment arrangement that fits your needs or to confirm that your payment arrangement is still on schedule.</p>
		<p>Submitting this payment online does not guarantee that any currently scheduled debits to your bank account will be ceased if you do not contact <strong><?php echo strtoupper($themedata['collection_co']->full_name); ?></strong> to confirm that your payment posted successfully and that your loan is in good standing.</p>
	<?php else: ?>
		<p><strong>With this payment, you have satisfied the outstanding balance due on your loan and may be eligible to apply for a new loan at this time.</strong></p>
		<p>If you have any questions, please contact <strong><?php echo strtoupper($themedata['collection_co']->full_name); ?></strong> at <strong><?php echo strtoupper($themedata['collection_co']->phone); ?></strong></p>
	<?php endif; ?>
</div>
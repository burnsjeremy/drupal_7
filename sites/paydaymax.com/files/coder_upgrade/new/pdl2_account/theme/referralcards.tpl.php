<?php
  function printReferralCard($themedata) {
    print "<div class='company-name'>";
    print $themedata['siteName'];
    print "</div>";
    print "<div class='company-phone'>";
    print $themedata['company']->company->customer_service_number->phone->number;
    print "</div>";
    print "<div class='company-url'>";
    print $themedata['company']->company->name;
    print "</div>";
    print "<div class='referral-code'>Referral Code: ";
    print "<span class='ref-code'>".$themedata['referralCode']."</span>";
    print "</div>";
    print "<div class='referral-notes'>Use this card to apply and your friend will receive a bonus</div>";
  }
?>

  <table id="referral-card" width="100%" cellspacing="0" cellpadding="0">
    <tbody>
      <tr>
        <td width="50%" class="bottom_dotted_line right_dotted_line top_dotted_line left_dotted_line">
          <div>
          <?php printReferralCard($themedata); ?>
          </div>
        </td>
        <td class="bottom_dotted_line right_dotted_line top_dotted_line rright">
          <?php printReferralCard($themedata); ?>
        </td>
      </tr>
      <tr>
        <td class="bottom_dotted_line right_dotted_line left_dotted_line">
          <?php printReferralCard($themedata); ?>
        </td>
        <td class="bottom_dotted_line right_dotted_line top_dotted_line rright">
          <?php printReferralCard($themedata); ?>
        </td>
      </tr>
      <tr>
        <td class="bottom_dotted_line right_dotted_line left_dotted_line">
          <?php printReferralCard($themedata); ?>
        </td>
        <td class="bottom_dotted_line right_dotted_line top_dotted_line rright">
          <?php printReferralCard($themedata); ?>
        </td>
      </tr>
      <tr>
        <td class="bottom_dotted_line right_dotted_line left_dotted_line">
          <?php printReferralCard($themedata); ?>
        </td>
        <td class="bottom_dotted_line right_dotted_line top_dotted_line rright">
          <?php printReferralCard($themedata); ?>
        </td>
      </tr>
    </tbody>
  </table>

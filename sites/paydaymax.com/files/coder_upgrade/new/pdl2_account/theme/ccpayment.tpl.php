
<div>
	<h2>Make Credit/Debit Card Payment</h2>
	<p>
	  We've made it even easier for you to make payments on your loan with 
	  <?php echo $themedata['siteName']; ?>. You can pay your balance in full 
	  or make a partial payment. If you need to extend your loan, please 
	  click <a href="extend">here</a>.
	</p>
	
	<strong>Payment Options</strong>
	<ul>
		<?php 
			$options = array_reverse($themedata['ccOptions']->options->option);
			
			foreach($options as $option){
				if($option->percent == 100){
					print "<li>Pay Off the Loan: $".number_format($option->amount,2)."</li>";
				}else{
					print "<li>Pay ".$option->percent."% today ($".number_format($option->amount,2).")</li>";
				}
			} 
		?>
		<li>Other</li>
	</ul>
	<p>
	<strong>An Important Note About Making a Payment Via Credit Card</strong>
	
	We prefer that you use a debit card to make your loan payment, 
	however, we understand that this is not always an option. You 
	should be aware that using a credit card to make a payment on an 
	existing line of credit could result in additional fees or 
	interest being charged to you by your financial institution.
	</p>
	<br />
	<table width=100% border="0" bgcolor="#000000" cellpadding="4" cellspacing="1">
		<tr bgcolor="#80D8FD" align="left">
<td width=33%><font face="arial,verdana" size="1">DEPOSIT</font>: <span>$<b><?php print number_format($themedata['deposit'],2); ?></b></span></td>
<td width=33%><font face="arial,verdana" size="1">PAID</font>: <span>$<b><?php print number_format($themedata['paid'],2); ?></b></span></td>
<td><font face="arial,verdana" size="1">DUE</font>: <span>$<b><?php print number_format($themedata['due'],2); ?></b></span></td>
		</tr>
	</table>
  <p></p>
	<p><strong>Please Note:</strong> If you cannot make one of the above payment amounts,
  we are able to take a lower amount today. However, this may not bring your account current
  and may cause you additional fees. Please contact <?php echo $themedata['collection_co']->collection_company->full_name; ?>
  at <?php echo $themedata['collection_co']->collection_company->phone; ?> to discuss suitable payment arrangements to keep your account in good standing.</p>
</div>
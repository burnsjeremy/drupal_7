<h1>Extend Your Loan</h1>
<h1>CONFIRMATION INFORMATION</h1>
<h2>Your new due date is <? echo date('l, F jS, Y', strtotime($themedata['loanDueDate'])) ?></h2>
<div>
  <span class='Label'>Payment Date:</span>
  <span><? echo date('l, F jS, Y', strtotime($themedata['originalLoanDueDate'])) ?></span>
</div>
<div>
  <span class='Label'>Amount:</span>
  <span>$<?php echo number_format($themedata['currentPaymentTotal'], 2);?></span>
</div>
<div>
  <span class='Label'>Confirmation Code:</span>
  <span><?php echo $themedata['loanConfirmation']; ?></span>
</div>
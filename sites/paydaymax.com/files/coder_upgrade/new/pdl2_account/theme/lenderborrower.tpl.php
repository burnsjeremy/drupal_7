<?php
$borrower = $themedata['borrower']->customer;
$address = $borrower->home_address->address;
$agreement = $themedata['agreement']->loan_agreement;
$date = $themedata['date'];
$ssn = $themedata['ssn'];
?>
<div class="dashboard">
	<table>
		<tr class="small">
			<th colspan="2">Lender:</th>
			<th colspan="2">Borrower:</th>
		</tr>
		<tr class="small">
			<td class="center" colspan="2">
				<p><?=$agreement->lender?></p>
				<p>Address: <?=$agreement->lenderAddressPrimary?></p>
				<p><?=$agreement->lenderAddressSecondary?></p>
				<p>Date: <?=date('M jS, Y', strtotime($date))?></p>
				<p>Due: <?=date('M jS, Y', strtotime($agreement->dueDate->date))?></p>
			</td>
			<td class="center" colspan="2">
				<p><?=$borrower->first_name?> <?=$borrower->middle_name?> <?=$borrower->last_name?></p>
				<p><?=$address->street_primary?></p>
				<?php if($address->street_secondary){ echo '<p>'.$address->street_secondary.'</p>'; } ?>
				<p><?=$address->city?>, <?=$address->state?> <?=$address->zip?></p>
				<p>SSN: <?=$ssn?></p>
			</td>
		</tr>
		<tr class="small">
			<th width="25%">Annual Percentage Rate</th>
			<th width="25%">Finance Charge</th>
			<th width="25%">Amount Financed</th>
			<th width="25%">Total Payments</th>
		</tr>
		<tr class="small">
			<td class="center">
				<p>Cost of your loan as a yearly rate</p>
				<h4><?=$themedata['apr']?>%</h4>
			</td>
			<td class="center">
				<p>Cost of Credit</p>
				<h4>$<?=number_format($agreement->financeCharge, 2)?></h4>
			</td>
			<td class="center">
				<p>The amount of credit provided to you or on your behalf</p>
				<h4>$<?=number_format($agreement->amountFinanced, 2)?></h4>
			</td>
			<td class="center">
				<p>Amount you will have paid after making all payments as scheduled</p>
				<h4>$<?=number_format($agreement->totalPayments, 2)?></h4>
			</td>
		</tr>
		<tr class="small">
			<th colspan="4" class="items small">Itemization of the Amount Financed</th>
		</tr>
		<tr class="small">
			<td colspan="2" style="text-align:right;">1. Amount Paid Directly to the Borrower:</td>
			<td>$<?=number_format($agreement->amountPaidBorrower, 2)?></td>
		</tr>
		<tr class="small">
			<td colspan="2" style="text-align:right;">2. Amount Paid to Creditor:</td>
			<td>$<?=number_format($agreement->amountPaidCreditor, 2)?></td>
		</tr>
		<tr class="small">
			<td colspan="2" style="text-align:right;">3. Total Amount to pay Lender (Payback):</td>
			<td>$<?=number_format($agreement->totalPayments, 2)?></td>
		</tr>
	</table>
</div>
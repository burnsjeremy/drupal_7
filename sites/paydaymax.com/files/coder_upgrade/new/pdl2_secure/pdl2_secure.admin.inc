<?php
//this is the admin for the secure redirects
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_secure_admin($form, &$form_state) {
  $form = array();

  $form["pdl2_secure_enable"] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Secure Redirects'),
    '#default_value' => variable_get('pdl2_secure_enable', false),
    '#description' => t('Enable the use of secure/non-secure redirects. (If you are on a dev environment you will probably want this off)'),
  );

  return system_settings_form($form);
}

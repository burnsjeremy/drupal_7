<?php


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_loanconfirm_status($loan_type, $loanStatus, $userType) {

  $pagename = 'confirm-' . $loan_type . '-' . $loanStatus;
  $catid = '104CONF';


  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_application", "pdl2_application_loanconfirm_status");
  }

  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  //is opt out?
  else if ($pdl2->currUser->optOut) {
    drupal_goto('account/logout');
  }

  $loanId = $pdl2->getOpenLoan();

  // ## why did we opt to use session for loan id, and not getOpenLoan?
//  $loanId = $_SESSION['loanId'];
  //	unset($_SESSION['loanId']);
  //	
  //	if (!isset($loanId))
  //	{
  //	 $loanId = $pdl2->getLastDeniedLoan();
  //	}

  watchdog("pdl2_application", "Decision:%status loantype:%loantype usertype:%usertype loanid:%loanid clientid:%clientid", array("%status" => $loanStatus, "%loantype" => $loan_type, "%usertype" => $userType, "%loanid" => $loanId, "%clientid" => $pdl2->currUser->userId), WATCHDOG_NOTICE);

  /* lead sell handling
   /**/
  if ($loanStatus == 'denied' && empty($_GET['rstatus'])) {
    $status = $pdl2->affiliateLeadsellGetStatus();
    if ($status->status == 'sold') {
      if (isset($status->redirect)) {
        drupal_goto($status->redirect);
      }
      else {
        watchdog("pdl2_leadsell_fail", "Status Sold, but no redirect url supplied. client id: %clientid", array("%clientid" => $pdl2->currUser->userId), WATCHDOG_NOTICE);
      }
    }
    elseif ($status->status == 'pending' || $status->status == 'unknown') {
      drupal_goto('confirm' . '/' . $loan_type . '/awaiting/' . $userType);
    }
  }

  $agreement = $pdl2->getLoanAgreement($loanId, $loan_type);
  $referralCode = $pdl2->getReferralCode();
  $companyinfo = $pdl2->getCompanyInfo();
  $transactions = $pdl2->getLoanTransactionsById($loanId, $loan_type, 'deposit');

  $funds_release_date = $transactions->loanTransactions->result->transaction->date->date;
  $funds_expected_completion_time = $transactions->loanTransactions->result->transaction->expected_completion_time->date;
  $company_number = $companyinfo->company->customer_service_number->phone->number;
  $company_name = $companyinfo->company->name;
  $pay_freq = $pdl2->customerGetPayFrequency();

  $themedata = array();

  $themedata['loanType'] = $loan_type;
  $themedata['loanStatus'] = $loanStatus;
  $themedata['agreement'] = $agreement;
  $themedata['userType'] = $userType;
  $themedata['referral_code'] = $referralCode;
  $themedata['company_name'] = $company_name;
  $themedata['number'] = $company_number;
  $themedata['funds_release_date'] = $funds_release_date;
  $themedata['funds_expected_completion_time'] = $funds_expected_completion_time;
  $themedata['payFreq'] = $pay_freq->PayFrequency->payday_name;


  if ($loan_type == 'extend' && $loanStatus == 'approved') {
    $confirmPage = theme('extendconfirmapproved', array('themedata' => $themedata));
  }
  else {

    switch ($loanStatus) {
      case 'awaiting':
        drupal_add_js(array('arg' => arg()), array('type' => 'setting', 'scope' => JS_DEFAULT));
        drupal_add_js(drupal_get_path('module', 'pdl2_application') . '/js/leadsell.js');
        $confirmPage = theme('loanconfirmawaiting', array('themedata' => $themedata));
        break;
      case 'denied':
        $confirmPage = theme('loanconfirmdenied', array('themedata' => $themedata));
        break;
      case 'approved':
        $confirmPage = theme('loanconfirmapproved', array('themedata' => $themedata));
        break;
      default:
        $confirmPage = theme('loanconfirmaccepted', array('themedata' => $themedata));
        break;
    }
  }

  $output = $confirmPage;

  if ($loanStatus != 'awaiting') {
    $output .= drupal_get_form('pdl2_application_loanconfirm_status_form', $themedata);
    $output .= pdl2_application_write_pageview($pagename, $catid);

    $loanAmount = $agreement->loan_agreement->amountFinanced;
    $clientId = $pdl2->currUser->userId;

    // PDLNEW-3025 EF pixels should be fired in all circumstances
    if ($_COOKIE['ef_id']) {
      $efud = ($userType == "new") ? "new" : "previous";
      watchdog("pdl2_aff_ef", "Firing pixel type:%type amount:%amount loanid:%loanid clientid:%clientid", array("%type" => $efud, "%amount" => $loanAmount, "%loanid" => $loanId, "%clientid" => $clientId), WATCHDOG_NOTICE);

      $pdl2->affiliatePartnersEfrontierCreateLoanTransaction($_COOKIE['ef_id'], $loanId);

      $company_abbreviation = variable_get('pdl2_affiliate_efficientfrontier_company_abbreviation', "");

      $segment = "";
      switch ($loanStatus) {
        case 'denied':
          $segment = '5603';
          $ef_loanStatus = 'denied';
          break;
        case 'approved':
          $segment = '278';
          $ef_loanStatus = 'approved';
          break;
        case 'accepted':
          $segment = '278';
          $ef_loanStatus = 'approved';
      }

      $output .= '<script language="javascript" src="http://www.everestjs.net/static/st.v2.js"></script>
      <script language="javascript">
        var ef_event_type="transaction";
        var ef_transaction_properties = "ev_' . $userType . '_' . $ef_loanStatus . '_' . $company_abbreviation . '=1&ev_' . $userType . '_' . $ef_loanStatus . '_loan_value_' . $company_abbreviation . '=' . $loanAmount . '&ev_transid=' . $loanId . '";
        /*
         * Do not modify below this line
         */
        var ef_segment = "' . $segment . '";
        var ef_search_segment = "";
        var ef_userid="1065";
        var ef_pixel_host="pixel.everesttech.net";
        var ef_fb_is_app = 0;
        var ef_allow_3rd_party_pixels = 1;
        effp();
      </script>
      <noscript>
        <img src="http://pixel.everesttech.net/1065/t?ev_' . $userType . '_' . $ef_loanStatus . '_' . $company_abbreviation . '=1&ev_' . $userType . '_' . $ef_loanStatus . '_loan_value_' . $company_abbreviation . '=' . $loanAmount . '&ev_transid=' . $loanId . '" width="1" height="1"/>
      </noscript>';
    }


    // PDLNEW-3036: CJ & MP pixels should fire on all pages, not just approval


    // According to Michael Kinyanjui:
    /*
     Thanks Chris, Matt,

     Tha AMOUNT should always = 0 (please hardcode). That functionality is only for retail based setups.

     Your test <img src="https://www.emjcd.com/u?AMOUNT=250&CID=4406026&OID=1435396090&TYPE=334636&CURRENCY=USD&METHOD=IMG" height="1" width="20" />
     Live Test <img src="https://www.emjcd.com/u?AMOUNT=0&CID=1515179&OID=1438524225&TYPE=334636&CURRENCY=USD&METHOD=IMG" height="1" width="20" />
     */
    if ($_COOKIE['CJ_AID'] && $_SESSION['CJ_Action_id']) {
      $cjEnterpriseId = variable_get('pdl2_affiliate_commission_junction_enterprise_id', "");
      watchdog("pdl2_aff_cj", "Firing pixel type: %type loanid: %loanid, clientid: %clientid, AID: %aid, PID: %pid, SID: %sid",
             array("%type" => $_SESSION['CJ_Action_id'], "%loanid" => $loanId, "%clientid" => $clientId, "%aid" => $_COOKIE['CJ_AID'], "%pid" => $_COOKIE['CJ_PID'], "%sid" => $_COOKIE['CJ_SID']), WATCHDOG_NOTICE);
      $output .= "<!-- Commission Junction Pixel -->\n";
      $output .= '<img src="https://www.emjcd.com/u?AMOUNT=0&CID=' . $cjEnterpriseId . '&OID=' . $loanId . '&TYPE=' . $_SESSION['CJ_Action_id'] . '&CURRENCY=USD&METHOD=IMG" height="1" width="20" />';
      unset($_SESSION['CJ_Action_id']);
    }

    if ($_COOKIE['MP_VisitID']) {
      watchdog("pdl2_aff_mp", "Firing pixel visitid:%visitid clientid:%clientid loanid:%loanid", array("%visitid" => $_COOKIE['MP_VisitID'], "%clientid" => $clientId, "%loanid" => $loanId), WATCHDOG_NOTICE);
      $output .= "<!-- MyPoints Tracking Pixel -->\n";
      $output .= '<img src="http://www.mypoints.com/emp/u/' . $_COOKIE['MP_VisitID'] . '/A/ctr.gif" width="1" height="1" >';
    }

    if ($_COOKIE['LS_SiteID']) {
      $mid = variable_get('pdl2_affiliate_linkshare_merchant_id', "");
      if ( variable_get('pdl2_application_enable_real_time_linkshare', '') ) {
        if ($loanStatus == 'approved') {
          if ($userType == 'new') {
            $skulist = 'New Loan Approved';
          }
          else {
            $skulist = 'Exist Loan Approved';
          }
        }
        else {
          $skulist = 'Loan Application';
        }
      }
      else {
        $skulist = 'valid';
      }
      watchdog("pdl2_aff_ls", "Firing pixel LS_SiteID: %ls_siteid clientid:%clientid loanid:%loanid", array("%ls_siteid" => $_COOKIE['LS_SiteID'], "%clientid" => $clientId, "%loanid" => $loanId), WATCHDOG_NOTICE);
      $output .= "<!-- Linkshare Tracking Pixel -->\n";
      $output .= '<img src="https://track.linksynergy.com/ep?mid=' . $mid . '&ord=' . $loanId . '&skulist=' . $skulist . '&qlist=1&amtlist=0&cur=USD&namelist=Valid%20Lead&mail=default@linkshare.com"> ';
    }
  }

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->endProcess();
  }

  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_loanconfirm_status_form($form, $formArray, $themedata) {

  $form['buttons'] = array(
    '#type' => 'fieldset',
    '#attributes' => array('class' => 'btns'),
  );

  $form['buttons']['next'] = array(
    '#type' => 'submit',
    '#value' => t('VIEW ACCOUNT'),
    '#attributes' => array('class' => 'image view-account center'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_loanconfirm_status_form_validate($form, &$form_state) { }


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_loanconfirm_status_form_submit($form, &$form_state) {
  drupal_goto('account');
}

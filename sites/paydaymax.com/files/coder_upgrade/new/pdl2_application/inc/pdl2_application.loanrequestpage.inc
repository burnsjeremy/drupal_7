<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_loanrequest_page($loan_type) {
  $pagename = 'apply-loanrequest';
  $catid = '102APPL';

  $isPersonal = ($loan_type == "personal");

  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_application", "pdl2_application_loanrequest_page");
  }

  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  //is opt out?
  else if ($pdl2->currUser->optOut) {
    drupal_goto('account/logout');
  }
  else if (!$isPersonal && !isset($_SESSION['loanDraftHash']) && !$_SESSION['loanDraftHash']) {
    drupal_goto('account');
  }
  else if ($isPersonal && !isset($_SESSION['personalLoanDraftHash']) && !$_SESSION['personalLoanDraftHash']) {
    drupal_goto('account');
  }

  $userType = ($pdl2->pdl2_account_isNewUser()) ? "new" : "previous";

  drupal_add_js(drupal_get_path('module', 'pdl2_application') . '/js/calendar_lite/ng_lite.js');
  drupal_add_js(drupal_get_path('module', 'pdl2_application') . '/js/calendar_lite/components/calendar_lite.js');

  $class = ($_SESSION["existinguser"] == 1 ? 'Step2of3' : 'fiveof5');

  /******************* PERSONAL */
  if ($isPersonal) {

    $draftLoanId = $_SESSION['personalLoanDraftHash'];
    $clarity = $pdl2->loanPersonalDraftAgreementGetClarity($draftLoanId);
    if (empty($clarity)) {
      drupal_goto('confirm/personal/denied/' . $userType);
    }

    drupal_add_js(drupal_get_path('module', 'pdl2_application') . '/js/personalloanschedule.js');

    $maxLoanObject = $pdl2->customerLoanPersonalGetMaxOffer($pdl2->currUser->userId);
    if ($maxLoanObject->maxLoan == 0) {
      drupal_goto('confirm/personal/denied/' . $userType);
    }

    $loanIncrements = listsHelper::incrementalList(
  		$maxLoanObject->minLoan,
  		$maxLoanObject->maxLoan,
  		$maxLoanObject->increment,
  		true
  	);

    $loanDueDates = $pdl2->customerLoanPersonalGetDueDates($pdl2->currUser->userId);

    foreach ($loanDueDates as $keyDue => $valDue) {
      $dueDates[$valDue->date] = $valDue->date;
    }

    $dueDate = $pdl2->loanPersonalDraftGetDueDate($draftLoanId)->date;
    if (!$dueDate) {
      $dueDate = reset($dueDates);
      $pdl2->loanPersonalDraftSetDueDate($draftLoanId, $dueDate);
    }

    $loanAmount = $pdl2->loanPersonalDraftGetPrincipal($draftLoanId);
    if (!$loanAmount) {
      $loanAmount = $maxLoanObject->maxLoan;
      $pdl2->loanPersonalDraftSetPrincipal($draftLoanId, $loanAmount);
    }

    if (is_array($dueDate->result)) {
      $loanDueDate = $dueDate->result[0]->date;
    }
    else {
      $loanDueDate = $dueDate->result->date;
    }

    $schedule = $pdl2->loanPersonalDraftGetSchedule($draftLoanId);

    $hasCoupons = $pdl2->getCoupons();

    $themedata['loan'] = $loanAmount;
    $themedata['principal_select'] = $loanAmount;
    $themedata['dueDate'] = $loanDueDate;
    $themedata['schedule'] = $schedule;
    $themedata['total'] = $loanAmount + $financeCharge;

    $themedata['amounts'] = $loanIncrements;
    $themedata['dueDates'] = $dueDates;
    $themedata['hasCoupons'] = $hasCoupons;

    $output = "<div id='ApplicationArea'><div class='loan_req'><h1 class='step $class'>Loan Request</h1>" . drupal_get_form('pdl2_application_personalrequest_page_form', $themedata) . pdl2_application_write_pageview($pagename, $catid) . "</div></div>";

  }
  else {

    /******************* PAYDAY */

    $draftLoanId = $_SESSION['loanDraftHash'];
    $clarity = $pdl2->getClarity($draftLoanId);
    if (empty($clarity)) {
      drupal_goto('confirm/payday/denied/' . $userType);
    }
    drupal_add_js(drupal_get_path('module', 'pdl2_application') . '/js/loanfeestotal.js');

    $maxLoanObject = $pdl2->getMaxLoanOffer();
    if ($maxLoanObject->maxLoan == 0) {
      drupal_goto('confirm/payday/denied/' . $userType);
    }

    $loanIncrements = listsHelper::incrementalList(
  		$maxLoanObject->minLoan,
  		$maxLoanObject->maxLoan,
  		$maxLoanObject->increment,
  		true
  	);

    $loanDueDates = $pdl2->getDueDates();

    if (is_array($loanDueDates->result)) {
      foreach ($loanDueDates->result as $keyDue => $valDue) {
        $dueDates[$valDue->date] = $valDue->date;
      }
    }
    else {
      $dueDates[$loanDueDates->result->date] = $loanDueDates->result->date;
    }

    $dueDate = $pdl2->getLoanDraftDueDate($draftLoanId)->date;
    if (!$dueDate) {
      $dueDate = reset($dueDates);
      $pdl2->setLoanDueDate($draftLoanId, $dueDate);
    }

    $loanAmount = $pdl2->getPrincipal($draftLoanId);
    if (!$loanAmount) {
      $loanAmount = $maxLoanObject->maxLoan;
      $pdl2->setPrincipal($draftLoanId, $loanAmount);
    }

    $financeCharge = $pdl2->getFinanceCharge($draftLoanId);
    $hasCoupons = $pdl2->getCoupons();

    $themedata['loan'] = $loanAmount;
    $themedata['principal_select'] = $loanAmount;
    $themedata['dueDate'] = $dueDate;
    $themedata['fees'] = $financeCharge;
    $themedata['total'] = $loanAmount + $financeCharge;

    $themedata['amounts'] = $loanIncrements;
    $themedata['dueDates'] = $dueDates;
    $themedata['hasCoupons'] = $hasCoupons;

    $output = "<div id='ApplicationArea'><div class='loan_req'><h1 class='step $class'>Loan Request</h1>" . drupal_get_form('pdl2_application_paydayrequest_page_form', $themedata) . pdl2_application_write_pageview($pagename, $catid) . "</div></div>";
  }

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->endProcess();
  }

  return $output;

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_personalrequest_page_form($form, $formArray, $themedata) {
  return pdl2_application_gen_loanrequest_form("personal", $themedata);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_paydayrequest_page_form($form, $formArray, $themedata) {
  return pdl2_application_gen_loanrequest_form("payday", $themedata);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_gen_loanrequest_form($loan_type, $themedata) {

  $isPersonal = ($loan_type == "personal");
  $pdl2 = pdl2_core_get_api();
  $maxLoanObject = $isPersonal ? $pdl2->customerLoanPersonalGetMaxOffer($pdl2->currUser->userId) : $pdl2->getMaxLoanOffer();
  $maxLoanOffer = $maxLoanObject->maxLoan;

  $form["disablesubmit"] = array("#value" => '<script type="text/javascript">$("#pdl2-application-' . $loan_type . 'request-page-form").submit(function(){$("#edit-next", this).attr("disabled", "disabled");});</script>');

  $form['loanreqfieldset'] = array(
    '#type' => 'fieldset',
  );

  $form['loanreqfieldset']['loanheader'] = array(
    "#value" => '<div class="font12 padding10">' . t("On this step we'll ask you to choose how much you need to borrow, and the date you can pay it back on. Once you click \"Continue\", you will have one final review before your request is submitted.") . '</div>',
  );

  $form['loanreqfieldset']['loanfeestotal'] = array(
    '#value' => '<div class="font12 padding10">' . t("Congratulations, you have qualified for up to $") . $maxLoanOffer . '</div>',
  );

  $form['loanreqfieldset']['loan_change_wrapper'] = array(
    '#prefix' => '<div class="padding10-nobottom loan-change">',
    '#suffix' => '</div>',
  );

  $form['loanreqfieldset']['loan_change_wrapper']['q-info'] = array(
    "#value" => t('<div class="font12 l-info lleft">Would you like to submit your application for this amount</div>'),
  );

  $form['loanreqfieldset']['loan_change_wrapper']['q-radio'] = array(
    '#type' => 'radios',
    '#required' => true,
    '#default_value' => 0,
    '#options' => array(
      0 => 'Yes',
      1 => 'No',
    ),
    '#prefix' => '<div class="l-q-align rright">',
    '#suffix' => '</div><div class="clear"></div>',
  );

  $form['loanreqfieldset']['loan_change_wrapper']['loan-q-info'] = array(
    '#value' => '<div class="font10 lleft padding10-notop-noleft">' . t('By clicking "No", you will be able to select a lower loan amount.') . '</div>',
  );

  $form['loanreqfieldset']['principal'] = array(
    '#type' => 'select',
    '#required' => true,
    '#title' => t('How much would you like to borrow?'),
    '#options' => $themedata['amounts'],
    '#default_value' => $themedata['principal_select'],
    '#prefix' => '<div class="clear"></div><div id="show-loan-change" class="padding10 lleft hidden">',
    '#suffix' => '</div>',
  );

  if ($themedata['hasCoupons']) {

    $form['loanreqfieldset']['coupon'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter a coupon code if you have one.'),
    );

  }

  if ($isPersonal) {
    $form['loanreqfieldset']['loanschedule'] = array(
      '#prefix' => '<div id="personalschedule">',
      '#suffix' => '</div><div class="clear"></div>',
      '#value' => theme('personalschedule', array('themedata' => $themedata)),
    );
  }

  $newDate = array();
  foreach ($themedata['dueDates'] as $date) {
    $newDate += array($date => date("D., M. d, Y", strtotime($date)));
  }
  $form['loanreqfieldset']['loanduedate'] = array(
    '#type' => 'radios',
    '#required' => true,
    '#title' => t('Please select ' . ($isPersonal ? 'your first' : 'a') . ' due date (We have provided a calendar below for reference.)'),
    '#options' => $newDate,
    '#default_value' => $themedata['dueDate'],
    '#prefix' => '<div class="l-due-dates">',
    '#suffix' => '</div><div class="clear"></div>',
  );
  //dsm($themedata['dueDates']);		
  //dsm($newDate);

  if (!$isPersonal) {
    $financeFee = $pdl2->getFinanceCharge($_SESSION['loanDraftHash']);
    $form['loanreqfieldset']['financefee'] = array(
      "#prefix" => "<span id='financefee'>",
      "#value" => "Finance Fee: $" . number_format($financeFee, 2),
      "#suffix" => "</span>",
    );
  }

  $form['loanreqfieldset']['today'] = array(
    "#value" => '<div id="print_today" class="font12 padding10"></div>',
  );
  $form['loanreqfieldset']['calendar'] = array(
    "#value" => '<div id="calendar"></div>',
  );



  //SUBMIT AND CANCEL BUTTONS -- TODO//- MAKE BACK BUTTON BELOW A CANCEL BUTTON
  $form['buttons'] = array(
    '#prefix' => '<div class="buttons">',
    '#suffix' => '</div>',
  );

  $form['buttons']['next'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
    '#attributes' => array('class' => 'image continue rright'),
  );

  $form['buttons']['back'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#validate' => array('pdl2_application_loanrequest_cancel_validate'),
    '#submit' => array('pdl2_application_loanrequest_cancel_submit'),
    '#attributes' => array('class' => 'image cancel lleft'),
  );

  return $form;

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_paydayrequest_page_form_validate($form, &$form_state) {

  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_application", "pdl2_application_paydayrequest_page_form_validate");
  }

  $principal = $form_state['values']['principal'];

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->endProcess();
  }

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_paydayrequest_page_form_submit($form, &$form_state) {

  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_application", "pdl2_application_paydayrequest_page_form_submit");
  }

  $draftLoanId = $_SESSION['loanDraftHash'];

  if ($form_state['values']['coupon_code']) {
    $pdl2->setCoupon($draftLoanId, $form_state['values']['coupon_code']);
  }

  $pdl2->setLoanDueDate($draftLoanId, $form_state['values']['loanduedate']);

  // If the user selects YES use the max, we need to manually set THE MAX YO!
  if ($form_state["values"]["q-radio"] == 0) {
    $maxLoanObject = $pdl2->getMaxLoanOffer();
    $maxLoanAmount = $maxLoanObject->maxLoan;
    $pdl2->setPrincipal($draftLoanId, $maxLoanAmount);
  }
  else {
    $pdl2->setPrincipal($draftLoanId, $form_state['values']['principal']);
  }

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->endProcess();
  }

  drupal_goto('apply/payday/agreement');
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_personalrequest_page_form_validate($form, &$form_state) {

  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_application", "pdl2_application_personalrequest_page_form_validate");
  }

  $principal = $form_state['values']['principal'];
  $state = $pdl2->getUserInfo()->customer->home_address->address->state;

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->endProcess();
  }

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_personalrequest_page_form_submit($form, &$form_state) {

  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_application", "pdl2_application_personalrequest_page_form_submit");
  }

  $draftLoanId = $_SESSION['personalLoanDraftHash'];

  if ($form_state['values']['coupon_code']) {
    $pdl2->loanPersonalDraftSetCoupon($draftLoanId, $form_state['values']['coupon_code']);
  }

  $pdl2->loanPersonalDraftSetDueDate($draftLoanId, $form_state['values']['loanduedate']);

  // If the user selects YES use the max, we need to manually set THE MAX YO!
  if ($form_state["values"]["q-radio"] == 0) {
    $maxLoanObject = $pdl2->customerLoanPersonalGetMaxOffer($pdl2->currUser->userId);
    $maxLoanAmount = $maxLoanObject->maxLoan;
    $pdl2->loanPersonalDraftSetPrincipal($draftLoanId, $maxLoanAmount);
  }
  else {
    $pdl2->loanPersonalDraftSetPrincipal($draftLoanId, $form_state['values']['principal']);
  }


  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->endProcess();
  }

  drupal_goto('apply/personal/agreement');
}



/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_loanrequest_cancel_validate($form, &$form_state) { }

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_loanrequest_cancel_submit($form, &$form_state) {
  drupal_goto('cancel');
}


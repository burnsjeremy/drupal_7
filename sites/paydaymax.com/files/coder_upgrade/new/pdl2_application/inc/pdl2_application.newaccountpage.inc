<?php
//Set the number of pages for the new account process.
//To handle the PDW before the client is saved, this should be set to 4
//To handle PDW only after the client is saved, set this to 3.
define("PDL2_NEWACCOUNT_MAXPAGES", 3);

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_page() {
  // if we get here with a logged in user - rediretc to /apply.
  if (pdl2_account_is_logged_in()) {
    drupal_goto("apply/payday");
  }

  $output = drupal_get_form('pdl2_application_newaccount_form');
  return '<div id="ApplicationArea">' . $output . '</div>';
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_confirm_page() {
  $output = drupal_get_form('pdl2_application_newaccount_confirm_form');
  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_confirm_form($form) {
  $form = array();
  $fieldArray = array();
  $pdl2 = pdl2_core_get_api();
  $draftId = $pdl2->getHash();

  for ($i = 1; $i <= PDL2_NEWACCOUNT_MAXPAGES; $i++) {
    pdl2_application_newaccount_confirm_get_field_array($i, $fieldArray);
  }

  // TODO: Make this more reliable. Hardcoding incoming source and Employment Information string is not good.
  $incomeSource = $_SESSION['data']['newaccount'][1]["incomesource"];
  if ($incomeSource == "retirement") {
    unset($fieldArray["Employment Information"]);
  }

  $themedata = array('fieldArray' => $fieldArray);
  $output = theme('newaccount/confirm', array('themedata' => $themedata));

  $form["disablesubmit"] = array("#value" => '<script type="text/javascript">$("#pdl2-application-newaccount-confirm-form").submit(function(){$("#edit-submit", this).attr("disabled", "disabled");});</script>');

  $form['confirmTable'] = array('#value' => $output);

  $form['buttons'] = array(
    '#prefix' => '<div class="buttons">',
    '#suffix' => '</div>',
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
    '#attributes' => array('class' => 'image continue rright'),
  );
  return $form;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_confirm_form_submit($form, &$form_state) {
  $pdl2 = pdl2_core_get_api();
  $draftId = $pdl2->getHash();

  try {
    //dd("before customer save");
    $customer = $pdl2->customerSave($draftId);
    //dd("After customer save");
  }
  catch (Exception $e) {
    drupal_set_message($e->getMessage(), 'error');
  }
  if ($customer) {

    if ($_COOKIE['ef_id']) {
      $pdl2->affiliatePartnersEfrontierCreateClientTransaction($_COOKIE['ef_id'], $customer);
    }

    // Let's try and login!!!
    $login = $pdl2->login($_SESSION['data']['email'], $_SESSION['data']['password'], 'client');

    if (empty($login)) {
      drupal_set_message(t("An error occurred during login."), 'error');
    }

    $pdl2->setHash('');

    if ($_SESSION['sms_opt_in']) {
      $pdl2->setOptOutSms(false);
    }
    else {
      $pdl2->setOptOutSms(true);
    }

    // set if the customer is opt in for 3rd party lender
    if ($_SESSION['data']['otherlenders']) {
      $pdl2->customerPrivacySetOptOutThirdParty(false);
    }
    else {
      $pdl2->customerPrivacySetOptOutThirdParty(true);
    }
    unset($_SESSION['data']['otherlenders']);

    unset($_SESSION['data']);
    $form_state['redirect'] = 'apply/payday';
  }
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_form($form) {
  $pagereq = arg(1);

  // If we're not going to a specific page, reset all data and start from scratch.
  if (!isset($pagereq)) {
    unset($_SESSION['data']);
    unset($_SESSION["personalLoanDraftHash"]);
    unset($_SESSION["loanDraftHash"]);
    unset($_SESSION['loanid']);
    unset($_SESSION['loanType']);
  }

  $edit = arg(2);

  if ($edit == 'edit') {
    $_SESSION['data']['edit'] = true;
  }
  else {
    $_SESSION['data']['edit'] = false;
  }
  $sessionData = $_SESSION['data'];

  $formObj = pdl2_application_get_page($sessionData);

  $formData = $sessionData['newaccount'][PDL2_NEWACCOUNT_PAGE];

  return $formObj->getForm($formData);
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_form_validate($form, &$form_state) {
  //$pdl2 = pdl2_core_get_api();

  $sessionData = $_SESSION['data'];
  $formObj = pdl2_application_get_page($sessionData);
  $formObj->validateForm($form, $form_state);
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_form_submit($form, &$form_state) {
  $sessionData = $_SESSION['data'];
  $formObj = pdl2_application_get_page($sessionData);
  $formObj->submitForm($form, $form_state, PDL2_NEWACCOUNT_PAGE);

  $nextPage = pdl2_application_get_next_page(PDL2_NEWACCOUNT_PAGE, PDL2_NEWACCOUNT_MAXPAGES);

  if ($nextPage && !$_SESSION['data']['edit']) {
    $_SESSION['data']['page'] = $nextPage;
    drupal_redirect_form($form, 'newaccount/' . $nextPage);
  }
  else {
    drupal_redirect_form($form, 'newaccount/confirm');
  }
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_get_page($sessionData) {
  $page = arg(1);
  if ($page != "1" && $page != "2" && $page != "3" && $page != "4") {
    $page = 1;
  }

  if (empty($page)) {
    $page = 1;
  }
  elseif (!empty($sessionData['page']) && $sessionData['page'] < $page) {
    drupal_goto('newaccount/' . $sessionData['page']);
  }
  elseif (empty($sessionData['page']) && $page > 1) {
    drupal_goto('newaccount/1');
  }

  define('PDL2_NEWACCOUNT_PAGE', $page);
  $formName = 'NewAccountPage' . $page;
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'pdl2_application') . '/forms/' . $formName . '.form.php';
  $form = new $formName();
  return $form;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_get_next_page($page, $maxPages) {
  $nextPage = $page + 1;
  return ($nextPage <= $maxPages) ? $nextPage : false;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_back_submit($form, &$form_state) {
  $backPage = $_SESSION['data']['page'] - 1;
  $_SESSION['data']['page'] = ($backPage > 1) ? $backPage : 1;
  // How did this ever work without the redrect set up ?
  $form_state['redirect'] = 'newaccount/' . $_SESSION['data']['page'];
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_form_after_build($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == 'Back') {
    _pdl2_application_disable_validation($form);
  }
  return $form;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_get_incomesource() {
  $formData = $_SESSION['data']['newaccount'][1];
  return $formData['incomesource'];
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_confirm_get_field_array($page, &$fieldArray) {
  $pageData = $_SESSION['data']['newaccount'][$page];

  $formName = 'NewAccountPage' . $page;
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'pdl2_application') . '/forms/' . $formName . '.form.php';
  $form = new $formName();
  $confirmFields = $form->getConfirmFields();

  foreach ($confirmFields as $keyGroup => $valGroup) {
    foreach ($valGroup as $keyField => $valField) {
      $fieldArray[$keyGroup][] = array(
        'title' => $valField,
        'value' => _pdl2_application_newaccount_get_whole_value($keyField, $pageData),
        'url' => "newaccount/$page/edit",
      );
    }
  }
}


function _pdl2_application_newaccount_get_whole_value($keyField, $pageData) {
  switch ($keyField) {
    case 'homephone':
    case 'cellphone':
    case 'workphone':
    case 'workverify':
    case 'refrelphone':
    case 'ref1phone':
      $value = $pageData[$keyField . '1'] . '-' . $pageData[$keyField . '2'] . '-' . $pageData[$keyField . '3'];
      if ($value == '--') {
        $value = '';
      }
      break;
    case 'socsec':
      $value = $pageData[$keyField . '1'] . $pageData[$keyField . '2'] . $pageData[$keyField . '3'];
      break;
    case 'name':
      $value = $pageData['firstname'] . ' ';
      $value .= ($pageData['middlename']) ? $pageData['middlename'] . ' ' : '';
      $value .= $pageData['lastname'];
      break;
    case 'address':
      $value = $pageData['homestreet1'] . '<br/>';
      $value .= ($pageData['homestreet2'])
              ? $pageData['homestreet2']  . '<br/>' : '';
      $value .= $pageData['homecity'] . ', ' . $pageData['homestate'] . ' ' . $pageData['homezip'];
      break;
    case 'workaddress':
      $value = $pageData['workstreet1'] . '<br/>';
      $value .= ($pageData['workstreet2'])
              ? $pageData['workstreet2']  . '<br/>' : '';
      $value .= $pageData['workcity'] . ', ' . $pageData['workstate'] . ' ' . $pageData['workzip'];
      break;
    case 'bankaddress':
      $bankaddress = $pageData['bankaddress'];
      $value = $bankaddress->bank_name . '<br/>';
      $value .= $bankaddress->address->street_primary . '<br/>';
      $value .= ($bankaddress->address->street_secondary) ? $bankaddress->address->street_secondary  . '<br/>' : '';
      $value .= $bankaddress->address->city . ', ' . $bankaddress->address->state . ' ' . $bankaddress->address->zip;
      break;
    case 'mailaddress':
      if (!empty($pageData['homemailaddr'])) {
        $value = $pageData['homemailaddr'] . '<br/>' . $pageData['homemailcity'] . ','
                . $pageData['homemailstate'] . ' ' . $pageData['homemailzip'];
      }
      else {
        $value = '';
      }
      break;
    case 'directdeposit':
      switch ($pageData['directdeposit']) {
        case 1:
          $value = 'Yes';
          break;
        case 2:
          $value = 'No';
          break;
        default:
          $value = '';
          break;
      }
      break;
    case 'incomesource':
      switch ($pageData['incomesource']) {
        case 'employment':
          $value = 'Employment';
          break;
        case 'retirement':
          $value = 'Retirement';
          break;
        case 'disability':
          $value = 'Disability';
          break;
        case 'other':
          $value = 'Other';
          break;
        case 'unemployed':
          $value = 'Unemployment';
          break;
      }
      break;
    case 'dob':
      $value = date('F j, Y', strtotime($pageData['dob_year'] . '-' . $pageData['dob_month'] . '-' . $pageData['dob_day']));
      break;
    case 'workbegin':
      if ($pageData['incomesource'] == "retirement") {
        $value = "";
      }
      else {
        $value = date('F Y', strtotime($pageData['workbeginyr'] . '-' . $pageData['workbeginmo'] . '-01'));
      }
      break;
    case 'pay_frequency':
      switch ($pageData['pay_frequency']) {
        case 'paid_once_a_week':
          $value = 'Weekly';
          break;
        case 'paid_every_other_week':
          $value = 'Every other Week';
          break;
        case 'paid_twice_a_month':
          $value = 'Twice Monthly';
          break;
        case 'paid_once_a_month':
          $value = 'Monthly';
          break;
      }
      break;
    case 'paycheckAmount':
      $value = '$' . number_format($pageData['paycheckAmount'], 2);
      break;
    default:
      $value = $pageData[$keyField];
      break;
  }

  return $value;
}

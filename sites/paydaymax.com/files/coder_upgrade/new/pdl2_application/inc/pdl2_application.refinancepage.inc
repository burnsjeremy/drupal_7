<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_refinance_page() {

  $pdl2 = pdl2_core_get_api();

  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  //is opt out?
  else if ($pdl2->currUser->optOut) {
    drupal_goto('account/logout');
  }

  $openLoan = $pdl2->getOpenLoan();

  $isPersonalRefinanceEligible = false; // set default
  if (!empty($openLoan)) {

    $isPersonalRefinanceEligible = $pdl2->loanPersonalRefinanceIsLoanEligible($openLoan);

    if (!$isPersonalRefinanceEligible) {
      drupal_set_message(t('You are not eligible to refinance your personal loan at this time.'));
      drupal_goto('account');
    }

  }

  $_SESSION["loanType"] = 'personal';

  $pdl2 = pdl2_core_get_api();

  $siteName = pdl2_core_get_site_name();
  $schedule = $pdl2->loanPersonalGetSchedule($openLoan);

  $themedata = array(
    'siteName' => $siteName,
    'schedule' => $schedule->result,
  );

  $output = "<h1 class='step Step1of3'>Refinance Your Personal Loan</h1>";
  $output .= theme('refinance', array('themedata' => $themedata));
  $output .= theme('personal_payment_schedule', array('themedata' => $themedata));
  $output .= theme('prequalagreement', array('themedata' => $themedata));
  $output .= drupal_get_form('pdl2_application_personal_refinance_form');
  return "<div id='ApplicationArea'><div class='payday_apply'>" . $output . "</div></div>";
}

// At this point the personal & payday loan apply forms are identical apart from the form name
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_personal_refinance_form($form) {

  $form['buttons'] = array(
    '#prefix' => '<div id="apply_button" class="buttons">',
    '#suffix' => '</div>',
  );

  //submit button exsists in 2 places (NewAccountPage4.form.php) to keep in correct order
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
    '#attributes' => array('class' => 'image continue rright'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_personal_refinance_form_submit($form, &$form_state) {

  $pdl2 = pdl2_core_get_api();

  $openLoan = $pdl2->getOpenLoan();
  $userType = ($pdl2->pdl2_account_isNewUser()) ? "new" : "existing";

  try {
    $res = $pdl2->loanPersonalRefinanceDraftCreateRefinanceDraft($openLoan);
  }
  catch (Exception $e) {
    watchdog("pdl2_application", "Could not create personal loan draft: %err", array("%err" => $e->getMessage()), WATCHDOG_ERROR);
    drupal_goto('confirm/personal/denied/' . $userType);
  }

  if (!empty($res)) {
    $_SESSION['personalLoanDraftHash'] = $res;
    unset($_SESSION['loanDraftHash']);
    $prequal = $pdl2->loanPersonalDraftAgreementSetClarity($_SESSION['personalLoanDraftHash']);

    if (!$prequal) {
      drupal_goto('confirm/personal/denied/' . $userType);
    }
    else {
      drupal_goto('apply/personal/loanrequest');
    }
  }
  else { // res was empty, but not caught
    watchdog("pdl2_application", "Could not create personal loan draft: result was empty", array(), WATCHDOG_ERROR);
    drupal_goto('confirm/personal/denied/' . $userType);
  }
}

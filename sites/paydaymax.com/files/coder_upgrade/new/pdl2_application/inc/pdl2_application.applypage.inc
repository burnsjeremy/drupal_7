<?php


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_apply_page($loan_type) {

  $pagename = ($loan_type == "payday" ? 'apply' : 'personal');
  $catid = '102APPL';

  $pdl2 = pdl2_core_get_api();
  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_application", "pdl2_application_apply_page");
  }

  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  //is opt out?
  else if ($pdl2->currUser->optOut) {
    drupal_goto('account/logout');
  }

  $openLoan = $pdl2->getOpenLoan();
  if ($loan_type == "personal") {

    $isPersonalEligible = $pdl2->customerLoanPersonalIsEligible($pdl2->currUser->userId);
    $isPersonalRefinanceEligible = false;
    if (!empty($openLoan)) {
      $isPersonalRefinanceEligible = $pdl2->loanPersonalRefinanceIsLoanEligible($openLoan);
    }

    if (!empty($openLoan) && !$isPersonalRefinanceEligible) {
      drupal_set_message(t('You are not eligible to refinance your personal loan at this time.'));
      drupal_goto('account');
    }
    else if (empty($openLoan) && !$isPersonalEligible) {
      drupal_set_message(t('You are not eligible to apply for a personal loan at this time.'));
      drupal_goto('account');
    }
  }

  if (!empty($openLoan) && $loan_type != "personal") {
    drupal_set_message(t('You have an open loan, and cannot apply for another at this time.'));
    drupal_goto('account');
  }

  $_SESSION["loanType"] = $loan_type;

  if ($loan_type == "personal") {
    $output = drupal_get_form('pdl2_application_personal_page_form') . pdl2_application_write_pageview($pagename, $catid);
  }
  else {
    $output = drupal_get_form('pdl2_application_payday_page_form') . pdl2_application_write_pageview($pagename, $catid);
  }

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->endProcess();
  }
  return "<div id='ApplicationArea'><div class='payday_apply'>" . $output . "</div></div>";
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_payday_page_form($form) {
  return pdl2_application_gen_apply_page_form("payday");
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_personal_page_form($form) {
  return pdl2_application_gen_apply_page_form("personal");
}

// At this point the personal & payday loan apply forms are identical apart from the form name
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_gen_apply_page_form($loan_type) {
  $form = array();

  $pdl2 = pdl2_core_get_api();

  $form["disablesubmit"] = array("#value" => '<script type="text/javascript">$("#pdl2-application-' . $loan_type . '-page-form").submit(function(){$("#edit-submit", this).attr("disabled", "disabled");});</script>');

  $form['pageheader'] = array(
    '#value' => "<h1 class='step Step1of3'>Let's Get Started</h1>",
  );

  $requestedAmountOptions = listsHelper::incrementalList(100, 1500, 10, true);
  if (isset($_SESSION['qs_amount'])) {
    $qs_amount = $_SESSION['qs_amount'];
  }
  $form['requested_amount'] = array(
    '#type' => 'select',
    '#title' => 'How much would you like to apply for?',
    '#options' => array('0' => '-Select-') + $requestedAmountOptions,
    '#default_value' => ($formData['requested_amount']) ? $formData['requested_amount'] : '' . $qs_amount . '',
    //'#required' => true
  );

  $payFrequency = $pdl2->customerGetPayFrequency($pdl2->currUser->userId);

  //if customer has no Payday information saved show paydaywizard
  if (!$payFrequency->PayFrequency->payday_type) {
    $form['pageheader'] = array(
      '#value' => "<h1 class='step fourof5'>Payday Schedule</h1>",
    );

    //to load these scripts in the footer of the page the param - footer was added
    drupal_add_js(drupal_get_path('module', 'pdl2_application') . '/js/calendar_lite/ng_lite.js', array('scope' => 'footer'));
    drupal_add_js(drupal_get_path('module', 'pdl2_application') . '/js/calendar_lite/components/calendar_lite.js', array('scope' => 'footer'));
    drupal_add_js(drupal_get_path('module', 'pdl2_application') . '/js/paydaywizard.js', array('scope' => 'footer'));

    require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'pdl2_application') . '/forms/PaydayWizard.form.php';

    $pdw = new PaydayWizard();
    $form = array_merge($form, $pdw->getForm(array()));

    $form['#after_build'][] = 'pdl2_application_payday_form_after_build';

    //unset submit b/c it will show after fields are added from below
    unset($form['submit']);
  }

  //site name added - I had the service name wrong JB
  $siteName = pdl2_core_get_site_name();
  $themedata = array('siteName' => $siteName);

  //prequalAgreement exsists in 2 places (NewAccountPage4.form.php) to keep in correct order	
  $form['prequalAgreement'] = array(
    '#value' => theme('prequalagreement', array('themedata' => $themedata)),
  );

  $form['buttons'] = array(
    '#prefix' => '<div id="apply_button" class="buttons">',
    '#suffix' => '</div>',
  );

  //submit button exsists in 2 places (NewAccountPage4.form.php) to keep in correct order
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
    '#attributes' => array('class' => 'image continue rright'),
  );

  return $form;
}




/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_payday_form_after_build($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == 'Back') {
    _pdl2_application_disable_validation($form);
  }
  return $form;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_payday_page_form_validate($form, &$form_state) {
  if (!$form_state['values']['requested_amount']) {
    form_set_error('requested_amount', t('You must select an amount you would like to apply for.'));
  }
  if ($form_state['values']['pay_frequency']) {
    $paydayWizard = new PaydayWizard();
    $paydayWizard->validateForm($form, $form_state);
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_personal_page_form_after_build($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == 'Back') {
    _pdl2_application_disable_validation($form);
  }
  return $form;
}


/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_personal_page_form_validate($form, &$form_state) {
  if ($form_state['values']['pay_frequency']) {
    $paydayWizard = new PaydayWizard();
    $paydayWizard->validateForm($form, $form_state);
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_payday_page_form_submit($form, &$form_state) {
  //undet amount b/c it has already been used
  unset($_SESSION['qs_amount']);
  //call api
  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_application", "pdl2_application_apply_page_form_submit");
  }

  $clientId = $pdl2->currUser->userId;
  $userType = ($pdl2->pdl2_account_isNewUser()) ? "new" : "existing";

  //First, check to see if we have payday wizard info to save
  if ($form_state['values']['pay_frequency']) {
    $paydayWizard = new PaydayWizard();
    try {
      $paydayWizard->submitForm($form, $form_state, $clientId);
    }
    catch (Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
    }
  }

  $res = $pdl2->createLoanDraft();

  if (!empty($res)) {

    $_SESSION['loanDraftHash'] = $res;
    unset($_SESSION['personalLoanDraftHash']);
    $pdl2->loanDraftSetRequestedAmount($_SESSION['loanDraftHash'], $form_state['values']['requested_amount']);

    $prequal = $pdl2->setClarity($_SESSION['loanDraftHash']);

    if ($pdl2->isProfilingCalls()) {
      $pdl2->logger()->endProcess();
    }

    if (!$prequal) {
      drupal_goto('confirm/apply/denied/' . $userType);
    }
    else {
      drupal_goto('apply/payday/loanrequest');
    }
  }
  else {
    drupal_goto('confirm/apply/denied/' . $userType);
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_personal_page_form_submit($form, &$form_state) {

  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_application", "pdl2_application_personal_page_form_submit");
  }

  $openLoan = $pdl2->getOpenLoan();
  $userType = ($pdl2->pdl2_account_isNewUser()) ? "new" : "existing";

  try {
    if (!empty($openLoan)) {
      $res = $pdl2->loanPersonalRefinanceDraftCreateRefinanceDraft($openLoan);
    }
    else {
      $res = $pdl2->createPersonalLoanDraft();
    }
  }
  catch (Exception $e) {
    watchdog("pdl2_application", "Could not create personal loan draft: %err", array("%err" => $e->getMessage()), WATCHDOG_ERROR);
    drupal_goto('confirm/personal/denied/' . $userType);
  }

  if (!empty($res)) {
    $_SESSION['personalLoanDraftHash'] = $res;
    unset($_SESSION['loanDraftHash']);
    $pdl2->loanPersonalDraftSetRequestedAmount($_SESSION['personalLoanDraftHash'], $form_state['values']['requested_amount']);
    $prequal = $pdl2->loanPersonalDraftAgreementSetClarity($_SESSION['personalLoanDraftHash']);

    if ($pdl2->isProfilingCalls()) {
      $pdl2->logger()->endProcess();
    }

    if (!$prequal) {
      drupal_goto('confirm/personal/denied/' . $userType);
    }
    else {
      drupal_goto('apply/personal/loanrequest');
    }
  }
}

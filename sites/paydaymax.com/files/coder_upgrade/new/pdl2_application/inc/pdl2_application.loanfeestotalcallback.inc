<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_loanfeestotalcallback_function($loan_type, $key, $val) {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    echo '';
    exit;
  }
  else if (pdl2_account_not_opt_out()) {
    echo '';
    exit;
  }

  $pdl2 = pdl2_core_get_api();
  $draftLoanId = $_SESSION['loanDraftHash'];

  switch ($key) {
    case 'coupon':
      $_SESSION['couponCode'] = $val;
      $res = $pdl2->setCoupon($draftLoanId, $val, $loan_type);
      break;
    case 'loanduedate':
      $res = $pdl2->setLoanDueDate($draftLoanId, $val, $loan_type);
      break;
    case 'principal':
      $res = $pdl2->setPrincipal($draftLoanId, $val, $loan_type);
      break;
  }

  if (!isset($loan_amount)) {
    $loan_amount = $res->loan->principal;
  }

  if (!isset($financeCharge)) {
    $financeCharge = $res->loan->finance_charges;
  }

  $total = $loan_amount + $financeCharge;

  $res = array(
    'loan' => $loan_amount,
    'fees' => $financeCharge,
    'total' => $total,
  );
  $json = json_encode($res);
  echo $json;
}

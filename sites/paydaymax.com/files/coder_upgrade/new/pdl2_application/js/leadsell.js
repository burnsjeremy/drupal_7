$(document).ready(function ()
{
  //interval 10 secs
  //get status
  //if pending/unknown retry
  //if rejected redirect to denied
  //if sold, redirect url
  
  var loan_type = Drupal.settings.arg[1];
  var user_type = Drupal.settings.arg[3];
  
  var denied_url = Drupal.settings.basePath + "confirm/" + loan_type + "/denied/" + user_type + "/?rstatus=1"; //rstatus = rejected status
  
  setTimeout( function(){ window.location.href = denied_url; }, 1000*60*2);  
  setInterval( function(){ getSellStatus(); }, 1000*10 );

  function getSellStatus()
  {
		var ajax_obj = $.getJSON(Drupal.settings.basePath + "affiliate/leadsell/getstatus", function( data ){
      if (data.status == 'rejected'){
        window.location.href = denied_url;
      } else if ( data.status == 'sold' ){
        if (data.redirect){
          window.location.href = data.redirect;
        } else {
          window.location.href = denied_url;
        }
      }
    });
    
    // do nothing if pending or unknown
    // interval will continue
  }
});
  
  
$(document).ready(function ()
{
  var loan_type = "payday";// Personal never loads this page getUrlVars()["loan_type"];
	
	function updateLoanInfo(key,val){
		// set text to loading...
	  if ($('#financefee'))
    	$('#financefee').html("Finance Fee: calculating...");

		var loading = '<span><img src="' + Drupal.settings.basePath + Drupal.settings.pdl2_core.basepath + '/theme/ajax-loader.gif" /> Loading...</span>';
		$('#loan').html(loading);
		$('#fees').html(loading);
		$('#total').html(loading);
			
		var ajax_obj = $.getJSON(Drupal.settings.basePath + "apply/" + loan_type + "/loanrequest/" + key + "/" + val,function(data){
		  if ($('#financefee'))
    		$('#financefee').html("Finance Fee: $" + data.fees.toFixed(2)); // loan req page
    		
			$('#loan').html("$" + data.loan.toFixed(2));
			$('#fees').html("$" + data.fees.toFixed(2));
			$('#total').html("$" + data.total.toFixed(2));
		});
	}

	$('#edit-principal').change(function(e){
		updateLoanInfo('principal',$(this).val());
	});
	
	$('#edit-coupon').change(function(e){
		if( $(this).val() ){
			updateLoanInfo('coupon',$(this).val());
		}
	});
	
	 // loan req page
	$('input:radio[name=loanduedate]').click(function (e)
	{
		updateLoanInfo('loanduedate',$(this).val());
	});

	
	$('#edit-loanduedate').change(function(e){
		updateLoanInfo('loanduedate',$(this).val());
	});
		
	var timeout;
	$('#edit-coupon').keyup(function() {
		if( $('#edit-coupon').val().length >= 1 ){
			if(timeout) {
				clearTimeout(timeout);
				timeout = null;
			}
			timeout = setTimeout(myFunction, 2000);
		}else{
			if(timeout) {
				clearTimeout(timeout);
				timeout = null;
			}
		}
	});
	
	function myFunction() { 
		if( $('#edit-coupon').val().length >= 1 ){
			updateLoanInfo('coupon', $('#edit-coupon').val());
		}
		clearTimeout(timeout); // this way will not run infinitely
	}
	
//	updateLoanInfo("x", "x");


//LOAN REQUEST PAGE - Show today's Date
	var today = new Date();
	var day = today.getDay();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();

//Days and Months array
	var m = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var d = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
//Choose days and months to display
	day = d[today.getDay()];
	mm = m[today.getMonth()];

//add zeros to appr fields
	if (dd < 10)
	{
		dd = '0' + dd;
	}
	if (mm < 10)
	{
		mm = '0' + mm;
	}
	
//format today string	
	today_format = day + ' ' + mm + ' ' + dd + ', ' + yyyy;

//print date
	$("#print_today").text('Today is: ' + today_format);

//CALENDAR
	var today2 = new Date();
	var day2 = today2.getDay();
	var dd2 = today2.getDate();
	var mm2 = today2.getMonth(); //January is 0!
	var yyyy2 = today2.getFullYear();
	
ng.ready( function() {
    var my_cal = new ng.Calendar({
        object:'calendar',
        days_text:'short',
        visible: true,
        close_on_select: false,
        num_months: 4,
        num_col: 4,
        allow_selection: false,
        weekend: [0, 6],
        start_date: new Date(yyyy2, mm2, dd2).from_string('today'),
        display_date: new Date(yyyy2, mm2, dd2).from_string('today'),
        //range_off: [
        //	['Sep 5th, 2012', 'Sep 10th, 2012'],
        //	['Jan 1st, 2013', 'Jan 12th, 2013']
        //],
        buttons_color: 'none'
    });
});

$('input:radio[name=q-radio]').click(function (e)
	{
		$("#show-loan-change").slideToggle();
	});
});

/*
*/
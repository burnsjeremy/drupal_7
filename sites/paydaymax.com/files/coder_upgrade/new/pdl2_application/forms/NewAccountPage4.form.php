<?php
class NewAccountPage4 extends NewAccountPageAbstract
{
  protected $_isValid = true;
  protected $_confirmFields = array(
    'Pay Frequency' => array(
                        'pay_frequency' => 'How often are you paid?',
                        'paycheckAmount' => 'Paycheck Amount',
                        ),
    );
  
  public function getForm($formData) {
    $pdl2 = pdl2_core_get_api();

    $form = array();

    $payDates = $pdl2->getEveryOtherWeekPayDates();

    $payFrequencyOptions = array(
      '' => '-Select-',
      'paid_once_a_week' => 'Every Week',
      'paid_every_other_week' => 'Every Other Week',
      'paid_twice_a_month' => 'Twice per Month',
      'paid_once_a_month' => 'Once per Month',
    );

    $weekdays = array(
      '' => '-Select-',
      'monday' => 'Monday',
      'tuesday' => 'Tuesday',
      'wednesday' => 'Wednesday',
      'thursday' => 'Thursday',
      'friday' => 'Friday',
    );

    $weeks = array(
      '' => '-Select-',
      1 => 'First',
      2 => 'Second',
      3 => 'Third',
      4 => 'Fourth',
      5 => 'Last',
    );

    $monthdates = array(
      '' => '-Select-',
      1  => '1st',
      2  => '2nd',
      3  => '3rd',
      4  => '4th',
      5  => '5th',
      6  => '6th',
      7  => '7th',
      8  => '8th',
      9  => '9th',
      10 => '10th',
      11 => '11th',
      12 => '12th',
      13 => '13th',
      14 => '14th',
      15 => '15th',
      16 => '16th',
      17 => '17th',
      18 => '18th',
      19 => '19th',
      20 => '20th',
      21 => '21st',
      22 => '22nd',
      23 => '23rd',
      24 => '24th',
      25 => '25th',
      26 => '26th',
      27 => '27th',
      28 => '28th',
      31 => 'Last Day',
    );

  $form["disablesubmit"] = array("#value" => '<script type="text/javascript">$("#pdl2-application-apply-page-form").submit(function(){$("#edit-submit", this).attr("disabled", "disabled");});</script>');

  $form['calheader'] = array(
    '#value' => '<h3>Please Select Your Payday Schedule</h3>'
  );
    
  $form['calendarfieldset'] = array(
    '#prefix' => '<div class="fieldset">',
    '#suffix' => '</div>', 
  );

  $form['calendarfieldset']['calendarheader'] = array(
    "#value" => '<div class="font12 padding10">'.t('We need to know the schedule of your pay days. Our helpful Pay Day Wizard below will guide you through a few simple questions. We\'ve also provided a calendar for reference.').'</div>'
    );

  $form['calendarfieldset']['today'] = array(
    "#value" => '<div id="print_today" class="font12 padding10-notop"></div>'
  );
    
  $form['calendarfieldset']['calendar'] = array(
    "#value" => '<div id="calendar"></div>'
  );

  $form['pdwheader'] = array(
    '#value' => '<h3>Payday Wizard</h3>'
  );

  $form['paywizardfieldset']['class'] = array(
    '#prefix' => '<div id="pay_day_wizard" class="pay-day-wizard padding10 clearfix">',
    '#suffix' => '</div>',
  );

  $form['paywizardfieldset']['class']['pay_frequency'] = array(
    '#type' => 'select',
    '#required' => true,
    '#title' => t('How often are you paid?'),
    '#default_value' => ($formData['pay_frequency']) ? $formData['pay_frequency'] : '-Select-',
    '#options' => $payFrequencyOptions,
  );

  $form['paywizardfieldset']['class']['weekly_day'] = array(
    '#type' => 'select',
    '#title' => t('Which day?'),
    '#default_value' => ($formData['weekly_day']) ? $formData['weekly_day'] : '-Select-',
    '#options' => $weekdays,
    '#suffix' => '<div class="padding10-onlyleft hidden clearfix" id="weekly-1-date-options-2"></div>',
  );

  $form['paywizardfieldset']['class']['bi_weekly_day'] = array(
    '#type' => 'select',
    '#title' => t('I get paid on'),
    '#default_value' => ($formData['bi_weekly_day']) ? $formData['bi_weekly_day'] : '-Select-',
    '#options' => $weekdays,
    '#suffix' => '<div class="padding10-onlyleft hidden" id="bi-weekly-1-date-options-2"></div><div class="clear"></div><div class="padding10-onlyleft hidden" id="bi-weekly-pay-amount-input"></div>',
  );

  foreach($payDates as $keyDate => $valDate) {
    $form['paywizardfieldset']['class']["bi_weekly_date_{$keyDate}_wrapper"] = array(
      '#prefix' => '<div id="bi-weekly-date-' . $keyDate . '-wrapper">',
      '#suffix' => '</div>'
    );
    $form['paywizardfieldset']['class']["bi_weekly_date_{$keyDate}_wrapper"]['inline_radios_1'] = array(
      '#prefix' => '<div class="inline_radios">',
      '#suffix' => '</div>'
    );

    $bi_weekly_date_array = array(
      $valDate->result[0]->date => date('m/d/Y', strtotime($valDate->result[0]->date)),
      $valDate->result[1]->date => date('m/d/Y', strtotime($valDate->result[1]->date)),
    );
    $form['paywizardfieldset']['class']["bi_weekly_date_{$keyDate}_wrapper"]['inline_radios_1']["bi_weekly_date_$keyDate"] = array(
      '#type' => 'radios',
      '#title' => ' My last paydate was: (see your last paycheck for this date)',
      '#default_value' => $formData["bi_weekly_date_$keyDate"],
      '#options' => $bi_weekly_date_array,
    );
  }

  $form['paywizardfieldset']['class']['inline_options_4'] = array(
    //'#prefix' => '<div class="">',
    //'#suffix' => '</div>'
  );
  
  $form['paywizardfieldset']['class']['inline_options_4']['monthly_week'] = array(
    '#type' => 'select',
    '#title' => t('I get paid on the'),
    '#default_value' => ($formData['monthly_week']) ? $formData['monthly_week'] : '-Select-',
    '#options' => $weeks,
  );

  $form['paywizardfieldset']['class']['inline_options_4']['monthly_day'] = array(
    '#type' => 'select',
    '#default_value' => ($formData['monthly_day']) ? $formData['monthly_day'] : '-Select-',
    '#options' => $weekdays,
    '#description' => t('of every month'),
  );

  $form['paywizardfieldset']['class']['monthly_date'] = array(
    '#type' => 'select',
    '#title' => t('I get paid on the'),
    '#default_value' => ($formData['monthly_date']) ? $formData['monthly_date'] : '-Select-',
    '#options' => $monthdates,
    '#description' => t('of every month'),
  );

  $form['paywizardfieldset']['class']['inline_options_3'] = array(
    //'#prefix' => '<div class="">',
    //'#suffix' => '</div>'
  );

  $form['paywizardfieldset']['class']['inline_options_3']['monthly_after_day'] = array(
    '#type' => 'select',
    '#title' => t('I get paid on the first'),
    '#default_value' => ($formData['monthly_after_day']) ? $formData['monthly_after_day'] : '-Select-',
    '#options' => $weekdays,
  );

  $monthAfterDates = array_chunk($monthdates, 22, true);

  $form['paywizardfieldset']['class']['inline_options_3']['monthly_after_date'] = array(
    '#type' => 'select',
    '#title' => t('after the'),
    '#default_value' => ($formData['monthly_after_date']) ? $formData['monthly_after_date'] : '-Select-',
    '#options' => $monthAfterDates[0],
    '#description' => t('of the month'),
  );

  $twiceMonthlyDate1 = array_slice($monthdates, 1, 15, true);
  $twiceMonthlyDate2 = array_slice($monthdates, 15, NULL, true);

  $form['paywizardfieldset']['class']['inline_options_1'] = array(
    //'#prefix' => '<div class="">',
    //'#suffix' => '</div>'
  );

  $form['paywizardfieldset']['class']['inline_options_1']['twice_monthly_date_1'] = array(
    '#type' => 'select',
    '#title' => t('I get paid on the'),
    '#default_value' => ($formData['twice_monthly_date_1']) ? $formData['twice_monthly_date_2'] : '-Select-',
    '#options' => array( '' => '-Select-') + $twiceMonthlyDate1,
  );

  $form['paywizardfieldset']['class']['inline_options_1']['twice_monthly_date_2'] = array(
    '#type' => 'select',
    '#title' => t('and'),
    '#default_value' => ($formData['twice_monthly_date_2']) ? $formData['twice_monthly_date_2'] : '-Select-',
    '#options' => array( '' => '-Select-') + $twiceMonthlyDate2,
    '#description' => t('of every month'),
  );

  $form['paywizardfieldset']['class']['inline_options_2'] = array(
    //'#prefix' => '<div class="">',
    //'#suffix' => '</div>'
  );

  $form['paywizardfieldset']['class']['inline_options_2']['twice_monthly_day_select_1'] = array(
    '#type' => 'select',
    '#title' => t('I get paid on'),
    '#default_value' => ($formData['twice_monthly_day_select_1']) ? $formData['twice_monthly_day_select_1'] : '-Select-',
    '#options' => array('' => '-Select-', 1 => 'First and Third', 2 => 'Second and Fourth'),
  );

  $form['paywizardfieldset']['class']['inline_options_2']['twice_monthly_day_select_2'] = array(
    '#type' => 'select',
    '#default_value' => ($formData['twice_monthly_day_select_2']) ? $formData['twice_monthly_day_select_2'] : '-Select-',
    '#options' => $weekdays,
  );

  $form['paywizardfieldset']['class']['twice_monthly_radios_wrapper'] = array(
    '#prefix' => '<div class="hidden" id="twice-monthly-radios-wrapper">',
    '#suffix' => '</div><div class="clear"></div>',
  );

  $form['paywizardfieldset']['class']['twice_monthly_radios_wrapper']['twice_monthly_radios'] = array(
    '#type' => 'radios',
    '#title' => t(' Please select and complete one option that describes your pay schedule'),
    '#default_value' => ($formData['twice_monthly_radios']) ? $formData['twice_monthly_radios'] : '',
    '#options' => array(
                    'bi-weekly' => 'I get paid every two weeks on the same day of the week.<br /><span class="ex-radio-text no-left-padding">Example: I get paid every other Wednesday.</span><br /><div class="padding10-onlyleft"  id="bi-weekly-date-options-1"></div><div class="padding10-onlyleft"  id="bi-weekly-2-date-options-2"></div><div id="edit-pay-bi-weekly"></div><br /><span class="or-text-radio">Or....</span>',
                    'date' => 'I am paid based on the date.<br /><span class="ex-radio-text">Example: I get paid on the 15th and last day of every month.</span><br /><div class="padding10-onlyleft"  id="date-date-options-1"></div><div id="date-date-options-2"></div><div id="edit-pay-date-weekly"></div><br /><span class="or-text-radio">Or....</span>',
                    'week' => 'I am paid on the same day of the week, but only twice per month.<br /><span class="ex-radio-text">Example: I get paid on the first and third Friday of every month.</span><div class="clear"></div><div class="padding10-onlyleft"  id="week-week-options-1"></div><div id="week-week-options-2"></div><div class="clear"></div><div id="edit-pay-week-weekly"></div>'
                  ),
  );

  $form['paywizardfieldset']['class']['monthly_radios_wrapper'] = array(
    '#prefix' => '<div class="hidden" id="monthly-radios-wrapper">',
    '#suffix' => '</div><div class="clear"></div>'
  );

  $form['paywizardfieldset']['class']['monthly_radios_wrapper']['monthly_radios'] = array(
    '#type' => 'radios',
    '#title' => t(' Please select and complete one option that describes your pay schedule'),
    '#default_value' => ($formData['monthly_radios']) ? $formData['otherlenders'] : '',
    '#options' => array(
                    'date' => 'I am paid on a specific date.<br /><span class="ex-radio-text">Example: I get paid on the 1st of every month.</span><br /><div class="padding10-onlyleft"  id="monthly-date-options-1"></div><div id="edit-pay-date-monthly"></div><br /><span class="or-text-radio">Or....</span>',
                    'day' => 'I am paid on a certain day of a certain week.<br /><span class="ex-radio-text">Example: I get paid on the 2nd Tuesday of every month.</span><br /><div class="padding10-onlyleft"  id="day-day-options-1"></div><div id="day-day-options-2"></div><div id="edit-pay-day-monthly"></div><br /><span class="or-text-radio">Or....</span>',
                    'after' => 'I am paid after a certain date.<br /><span class="ex-radio-text">Example: I get paid on the first Monday after the 15th of every month.</span><br /><div class="padding10-onlyleft"  id="after-week-options-1"></div><div id="after-week-options-2"></div><div id="edit-pay-after-monthly"></div>'
                  ),
  );
  
  $form['paywizardfieldset']['class']['paycheckAmount'] = array(
    '#type' => 'textfield',
    //'#required' => true,
    '#title' => 'My last paycheck was for $',
    '#maxlength' => 4,
    '#size' => 6,
    '#default_value' => ($formData['paycheckAmount']) ? $formData['paycheckAmount']: '',
    '#description' => t('.00'),
  );

  //site name
  $siteName = pdl2_core_get_site_name();
  $themedata = array('siteName'=>$siteName);

  $form['prequalAgreement'] = array(
    '#value' => theme('prequalagreement', $themedata)
  );

  $form['buttons'] = array(
    '#prefix' => '<div class="buttons">',
    '#suffix' => '</div>',
  );
  
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
	  '#attributes' => array('class' => 'image continue rright'),
  );

  $form['buttons']['back'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#attributes' => array('class' => 'image back lleft'),
    '#validate' => array('pdl2_application_newaccount_back_validate'),
    '#submit' => array('pdl2_application_newaccount_back_submit'),
  );
  $form['buttons']['notice'] = array(
    '#value' => '<div class="infoText">'.t('Note: Hitting the back button will erase all the fields on the current page.').'</div>',
  );

  $form['#after_build'][] = 'pdl2_application_payday_form_after_build';

  return $form;
  
  }
  
  public function validateForm($form, &$form_state) {
    $pdl2 = pdl2_core_get_api();
    //$pdl2->debugMode = true;
    $valFloat = new Zend_Validate_Float();
    $notEmpty = new Zend_Validate_NotEmpty();
    
    //error message handling for Requested Loan Amount
    $requested_amount = $form_state['values']['requested_amount']; 
    if (!$notEmpty->isValid( $requested_amount ) || $requested_amount == '0') {
      form_set_error('requested_amount', t('Please Select an amount you would like to apply for'));
    }
    //error message handling for Pay Check Amount
    $paycheckAmount = $form_state['values']['paycheckAmount']; 
    if (!$notEmpty->isValid( $paycheckAmount )) {
      form_set_error('paycheckAmount', t('Please enter an amount for your last paycheck!'));
    }else if (!$valFloat->isValid( $paycheckAmount )) {
      form_set_error('paycheckAmount', t('Your Paycheck Amount should be a dollar amount!'));
    }
    
    //get error messages for choices    
    switch($form_state['values']['pay_frequency']) {
      case 'paid_once_a_week':
        if (!$notEmpty->isValid($form_state['values']['weekly_day'])) {
          form_set_error('weekly_day', t('You must select a day of the week for your payday.'));
        }
        break;
      case 'paid_every_other_week':
        if (!$notEmpty->isValid($form_state['values']['bi_weekly_day'])) {
          form_set_error('bi_weekly_day', t('You must select a day of the week for your payday.'));
        }
    
        $biWeeklyRadioName = 'bi_weekly_date_' . $form_state['values']['bi_weekly_day'];
        
        if (!$notEmpty->isValid($form_state['values'][$biWeeklyRadioName])) {
          form_set_error($biWeeklyRadioName, t('You must select your last pay date.'));
        }
        break;
      case 'paid_twice_a_month':
        switch ($form_state['values']['twice_monthly_radios']) {
          case 'date':
            if (!$notEmpty->isValid($form_state['values']['twice_monthly_date_1'])) {
              form_set_error($form_state['values']['twice_monthly_date_1'], t('You must select your first twice monthly pay date.'));
            }
            
            if (!$notEmpty->isValid($form_state['values']['twice_monthly_date_2'])) {
              form_set_error($form_state['values']['twice_monthly_date_2'], t('You must select your first twice monthly pay date.'));
            }            
            break;
          case 'week':
            if (!$notEmpty->isValid($form_state['values']['twice_monthly_day_select_1'])) {
              form_set_error($form_state['values']['twice_monthly_day_select_1'], t('You must select the weeks you are paid.'));
            }

            if (!$notEmpty->isValid($form_state['values']['twice_monthly_day_select_2'])) {
              form_set_error($form_state['values']['twice_monthly_day_select_2'], t('You must select the day of the week you are paid.'));
            }              
            break;
          case 'bi-weekly':
            if (!$notEmpty->isValid($form_state['values']['bi_weekly_day'])) {
              form_set_error('bi_weekly_day', t('You must select a day of the week for your payday.'));
            }
        
            $biWeeklyRadioName = 'bi_weekly_date_' . $form_state['values']['bi_weekly_day'];
            
            if (!$notEmpty->isValid($form_state['values'][$biWeeklyRadioName])) {
              form_set_error($biWeeklyRadioName, t('You must select your last pay date.'));
            }
            break;
            
          default:
            form_set_error($form_state['values']['twice_monthly_radios'], t('Please select a option for being paid twice monthly.'));
            break;
        }        
        break;
        
        
        
      case 'paid_once_a_month':
        switch ($form_state['values']['monthly_radios']) {
        
          case 'day':
            if (!$notEmpty->isValid($form_state['values']['monthly_day'])) {
              form_set_error('monthly_date', t('You must select the weekday you are paid every month.'));
            }
            
            if (!$notEmpty->isValid($form_state['values']['monthly_week'])) {
              form_set_error('monthly_week', t('You must select the week you are paid every month.'));
            }            
            break;
                
          case 'date':
            if (!$notEmpty->isValid($form_state['values']['monthly_date'])) {
              form_set_error('monthly_date', t('You must select the day you are paid every month.'));
            }            
            break;
            
          case 'after':
            if (!$notEmpty->isValid($form_state['values']['monthly_after_date'])) {
              form_set_error('monthly_after_date', t('You must select the date after which you are paid every month.'));
            }
 
            if (!$notEmpty->isValid($form_state['values']['monthly_after_day'])) {
              form_set_error('monthly_after_day', t('You must select the first weekday you are paid monthly on after a certain date.'));
            }            
            break;
            
          default:
            form_set_error('monthly_radios', 'You must select an option for being paid monthly.');
            break;
        }        
        break;
        
    }
    
    // clean up any bad error messages //TODO - I dont think this is actually needed here
    $this->pdl2_cleanup_form_error_messages();
  }
  
  public function submitForm($form, &$form_state, $page) {
    $pdl2 = pdl2_core_get_api();
    $draftId = $pdl2->getHash();
    
    switch ($form_state['values']['pay_frequency']) {
      case 'paid_once_a_week':
        $pdl2->customerDraftPaymentSetScheduleWeekly($draftId, $form_state['values']['paycheckAmount'], 
          $form_state['values']['weekly_day']);
        break;
      case 'paid_every_other_week':
        $biWeeklyRadioName = 'bi_weekly_date_' . $form_state['values']['bi_weekly_day'];
        $pdl2->customerDraftPaymentSetScheduleEveryOtherWeek($draftId, $form_state['values']['paycheckAmount'], 
          $form_state['values'][$biWeeklyRadioName], $form_state['values']['bi_weekly_day']);
        break;
      case 'paid_twice_a_month':
        switch ($form_state['values']['twice_monthly_radios']) {
          case 'bi-weekly':
            $biWeeklyRadioName = 'bi_weekly_date_' . $form_state['values']['bi_weekly_day'];
            $pdl2->customerDraftPaymentSetScheduleEveryOtherWeek($draftId, $form_state['values']['paycheckAmount'], 
              $form_state['values'][$biWeeklyRadioName], $form_state['values']['bi_weekly_day']);
            break;
          case 'date':
            $pdl2->customerDraftPaymentSetScheduleTwiceAMonthSpecificDays($draftId, 
              $form_state['values']['paycheckAmount'], $form_state['values']['twice_monthly_date_1'], $form_state['values']['twice_monthly_date_2']);
            break;
          case 'week':
            switch ($form_state['values']['twice_monthly_day_select_1']) {
              case 1:
                $weekOne = 1;
                $weekTwo = 3;
                break;
              case 2:
                $weekOne = 2;
                $weekTwo = 4;
                break;
            }
            $pdl2->customerDraftPaymentSetScheduleTwiceAMonthSpecificWeeks($draftId, 
              $form_state['values']['paycheckAmount'], $form_state['values']['twice_monthly_day_select_2'], $weekOne, $weekTwo);
            break;
        }
        break;
      case 'paid_once_a_month':
        switch ($form_state['values']['monthly_radios']) {
          case 'day':
            $pdl2->customerDraftPaymentSetScheduleMonthlySpecificDay($draftId, 
              $form_state['values']['paycheckAmount'], $form_state['values']['monthly_date']);
            break;
          case 'date':
            $pdl2->customerDraftPaymentSetScheduleMonthlyWeekdayInSpecificWeek($draftId,
              $form_state['values']['paycheckAmount'], $form_state['values']['monthly_week'], 
              $form_state['values']['monthly_day']);
            break;
          case 'after':
            $pdl2->customerDraftPaymentSetScheduleMonthlyWeekdayAfterSpecificDay($draftId, 
              $form_state['values']['paycheckAmount'], $form_state['values']['monthly_after_date'],
              $form_state['values']['monthly_after_day']);
            break;
        }
        break;
    }
    
    $_SESSION['data']['newaccount'][$page] = $form_state['values'];    
  }
  
  public function isValid() {
    return $this->_isValid;
  }

}
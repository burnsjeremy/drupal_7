<?php
require_once 'NewAccountPage4.form.php';

class PaydayWizard extends NewAccountPage4
{
  public function submitForm($form, &$form_state, $clientId) {
    $pdl2 = pdl2_core_get_api();
    
    switch ($form_state['values']['pay_frequency']) {
      case 'paid_once_a_week':
        $pdl2->customerPaymentSetScheduleWeekly($clientId, 
						$form_state['values']['paycheckAmount'],
						$form_state['values']['weekly_day']);
        break;
      case 'paid_every_other_week':
        $biWeeklyRadioName = 'bi_weekly_date_' . $form_state['values']['bi_weekly_day'];
        $pdl2->customerPaymentSetScheduleEveryOtherWeek($clientId, 
							$form_state['values']['paycheckAmount'],
							$form_state['values'][$biWeeklyRadioName], 
							$form_state['values']['bi_weekly_day']);
        break;
      case 'paid_twice_a_month':
        switch ($form_state['values']['twice_monthly_radios']) {
          case 'bi-weekly':
            $biWeeklyRadioName = 'bi_weekly_date_' . $form_state['values']['bi_weekly_day'];
            $pdl2->customerPaymentSetScheduleEveryOtherWeek($clientId, 
							    $form_state['values']['paycheckAmount'],
							    $form_state['values'][$biWeeklyRadioName], 
							    $form_state['values']['bi_weekly_day']);
            break;
          case 'date':
            $pdl2->customerPaymentSetScheduleTwiceAMonthSpecificDays($clientId, 
								    $form_state['values']['paycheckAmount'], 
								    $form_state['values']['twice_monthly_date_1'], 
								    $form_state['values']['twice_monthly_date_2']);
            break;
          case 'week':
            switch ($form_state['values']['twice_monthly_day_select_1']) {
              case 1:
                $weekOne = 1;
                $weekTwo = 3;
                break;
              case 2:
                $weekOne = 2;
                $weekTwo = 4;
                break;
            }
            $pdl2->customerPaymentSetScheduleTwiceAMonthSpecificWeeks($clientId, 
								      $form_state['values']['paycheckAmount'], 
								      $form_state['values']['twice_monthly_day_select_2'], 
								      $weekOne,
								      $weekTwo);
            break;
        }
        break;
      case 'paid_once_a_month':
        switch ($form_state['values']['monthly_radios']) {
          case 'date':
            $pdl2->customerPaymentSetScheduleMonthlySpecificDay($clientId, 
								$form_state['values']['paycheckAmount'], 
								$form_state['values']['monthly_date']);
            break;
          case 'day':
            $pdl2->customerPaymentSetScheduleMonthlyWeekdayInSpecificWeek($clientId,
									  $form_state['values']['paycheckAmount'], 
									  $form_state['values']['monthly_week'], 
									  $form_state['values']['monthly_day']);
            break;
          case 'after':
            $pdl2->customerPaymentSetScheduleMonthlyWeekdayAfterSpecificDay($clientId, 
									    $form_state['values']['paycheckAmount'], 
									    $form_state['values']['monthly_after_date'],
									    $form_state['values']['monthly_after_day']);
            break;
        }
        break;
    }    
  } 
}
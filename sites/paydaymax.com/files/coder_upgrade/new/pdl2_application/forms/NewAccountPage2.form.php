<?php
class NewAccountPage2 extends NewAccountPageAbstract {
  protected $_isValid = true;
  protected $_nonRelativeIds = array(1,8);
  protected $_relativeIds = array(3,4,5,9,10,8,6,7);
  protected $_confirmFields = array(
      'Employment Information' => array('employer' => 'Employer',
                                        'position' => 'Department',
                                        'workaddress' => 'Address',
                                        'workverify' => 'Verifcation Number',
                                        'workverifyext' => 'Ext',
                                        'workphone' => 'Work Phone Number',
                                        'workphoneext' => 'Ext',
                                        'workbegin' => 'Began working'),
      'Pay Information' => array('directdeposit' => 'Direct Deposit?'),
      'References' => array('refrelname' => 'Relative\'s Name',
                            'refrelphone' => 'Phone',
                            'refrelrelationship' => 'Relationship',
                            'ref1name' => 'Non-relative\'s Name',
                            'ref1phone' => 'Phone',
                            'ref1relationship' => 'Relationship'),
  );


public function getForm($formData) {
  $form = array();
  $employment = array();
  $pdl2 = pdl2_core_get_api();
  $incomeSource = pdl2_application_newaccount_get_incomesource();
  if($incomeSource == 'employment') {

    $topHeader = 'Employment & References';

    $employment["disablesubmit"] = array("#value" => '<script type="text/javascript">$("#pdl2-application-newaccount-form").submit(function(){$("#edit-submit", this).attr("disabled", "disabled");});</script>');

    $employment["headermsg"] = array(
      "#value" => '<h3>'.t('Now we need to know about your place of employment').'</h3>'
    );

      $employment['employer'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#title' => 'Place of Employment',
          '#maxlength' => 35,
          '#default_value' => ($formData['employer']) ? $formData['employer'] : '',
          '#description' => '<div class="tooltip" title="'.t('Please tell us the name of the place you work.').'"></div>'
      );
      $employment['position'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#title' => 'Department',
          '#maxlength' => 20,
          '#default_value' => ($formData['position']) ? $formData['position'] : '',
          '#description' => '<div class="tooltip" title="'.t('Please tell us which department you work in.').'"></div>'
        );
      $employment['workstreet1'] = array(
          '#type' => 'textfield',
          '#title' => t('Address'). '<br />('.t('Do not enter a PO Box').')',
          '#required' => true,
          '#default_value' => ($formData['workstreet1']) ? $formData['workstreet1'] : '',
          '#description' => '<div class="tooltip" title="'.t('Please tell us the address of where you are currently employed.').'"></div>'
        );
      $employment['workstreet2'] = array(
          '#type' => 'textfield',
          '#title' => '&nbsp;',
          '#default_value' => ($formData['workstreet2']) ? $formData['workstreet2'] : '',
        );
      $employment['workcity'] = array(
          '#type' => 'textfield',
          '#title' => t('City'),
          '#maxlength' => 30,
          '#required' => true,
          '#default_value' => ($formData['workcity']) ? $formData['workcity'] : '',
          '#description' => '<div class="tooltip" title="'.t('Please tell us the city of your employment location.').'"></div>'
        );
      $employment['workzip'] = array(
            '#type' => 'textfield',
            '#title' => t('Zip Code'),
            '#required' => true,
            '#size' => 6,
            '#maxlength' => 5,
            '#default_value' => ($formData['workzip']) ? $formData['workzip'] : '',
            '#description' => '<div class="tooltip" title="'.t('Please tell us the zip code of your employment location.').'"></div>'
      );

      $employment['workverify'] = array(
            '#prefix' => '<div class="inline phone autotabs">',
            '#suffix' => '</div>'
        );

      $employment['workverify']['workverify1'] = array(
            '#type' => 'textfield',
            '#title' => t('Verification Phone Number'),
            '#required' => true,
            '#default_value' => ($formData['workverify1']) ? $formData['workverify1'] : '',
            '#size' => 3,
            '#maxlength' => 3,
          );
      $employment['workverify']['workverify2'] = array(
            '#type' => 'textfield',
            '#required' => true,
            '#default_value' => ($formData['workverify2']) ? $formData['workverify2'] : '',
            '#size' => 3,
            '#maxlength' => 3,
          );
      $employment['workverify']['workverify3'] = array(
            '#type' => 'textfield',
            '#required' => true,
            '#default_value' => ($formData['workverify3']) ? $formData['workverify3'] : '',
            '#size' => 4,
            '#maxlength' => 4,
          );
      $employment['workverify']['workverifyext'] = array(
            '#type' => 'textfield',
            '#title' => 'Ext.',
            '#default_value' => ($formData['workverifyext']) ? $formData['workverifyext'] : '',
            '#size' => 7,
            '#maxlength' => 6,
            '#prefix' => '<div class="short-label">',
            '#suffix' => '</div>'
          );
      $employment['workverify']['tooltip'] = array(
            '#value' => '<div class="tooltip" title="'.t("Please tell us your work supervisor's phone number. This must be included in order for your application to be processed.").'"></div><div class="clear"></div>',
      );
      $employment['workverifymsg'] = array(
          '#value' => '<div class="padding10 font12 italic lleft">'.t('This number will be used to verify your employment. Your loan request depends on our ability to quickly and accurately verify your place of employment through this number. This number must be a business land line. No cell phones will be accepted. We cannot verify employment if your company requires us to fax a request or requires us to call a toll pay number.').'</div>',
      );
      $employment['workphone'] = array(
          '#prefix' => '<div class="inline phone autotabs">',
          '#suffix' => '</div>'
        );

      $employment['workphone']['workphone1'] = array(
            '#type' => 'textfield',
            '#title' => t('Work Phone Number'),
            '#required' => true,
            '#default_value' => ($formData['workphone1']) ? $formData['workphone1'] : '',
            '#size' => 3,
            '#maxlength' => 3,
          );
      $employment['workphone']['workphone2'] = array(
            '#type' => 'textfield',
            '#required' => true,
            '#default_value' => ($formData['workphone2']) ? $formData['workphone2'] : '',
            '#size' => 3,
            '#maxlength' => 3,
          );
      $employment['workphone']['workphone3'] = array(
            '#type' => 'textfield',
            '#required' => true,
            '#default_value' => ($formData['workphone3']) ? $formData['workphone3'] : '',
            '#size' => 4,
            '#maxlength' => 4,
          );
      $employment['workphone']['workphoneext'] = array(
            '#type' => 'textfield',
            '#title' => 'Ext.',
            '#default_value' => ($formData['workphoneext']) ? $formData['workphoneext'] : '',
            '#size' => 7,
            '#maxlength' => 6,
            '#prefix' => '<div class="short-label">',
            '#suffix' => '</div>'
          );
      $employment['workphone']['tooltip'] = array(
            '#value' => '<div class="tooltip" title="'.t("Please tell us your work phone number where we can reach you.").'"></div><div class="clear"></div>',
      );
      $employment['workphonemsg'] = array(
            '#value' => '<div class="padding10 font12 italic lleft">'.t('Please fill in your work phone number at which we can reach you. This must be a landline number, not a mobile phone number.').'</div>',
      );

      $employment['workbegin'] = array(
            '#prefix' => '<div class="inline">',
            '#suffix' => '</div>'
      );

      $months = DateHelper::$months;
      $employment['workbegin']['workbeginmo'] = array(
          '#type' => 'select',
          '#title' => 'When did you start',
          '#required' => true,
          '#default_value' => ($formData['workbeginmo']) ? $formData['workbeginmo'] : '',
          '#options' => $months
      );

      $yearsArray = array('' => Year);
      $years = DateHelper::getYearArray(date('Y'), 20);
      $yearsArray += $years;

      $employment['workbegin']['workbeginyr'] = array(
          '#type' => 'select',
          '#required' => true,
          '#default_value' => ($formData['workbeginyr']) ? $formData['workbeginyr'] : '',
          '#options' => $years,
      );
      $employment['workbegin']['tooltip'] = array(
        '#value' => '<div class="tooltip" title="'.t("Please tell us the start date of your current job.").'"></div>',
      );

    }
    else {
      $topHeader = 'References';
      $headerMsg = '';
    }

    $form["topheader"] = array(
      "#value" => '<h1 class="step twoof5">'.t("$topHeader").'</h1>'
    );

    $form['employmentfieldset'] = array(
      '#prefix' => '<div class="fieldset">',
      '#suffix' => '</div>',
    );

    $form['employmentfieldset']["isrequired"] = array(
      "#value" => '<p><span class="form-required" title="This field is required.">*</span> '.t('Required Field.').'</p>'
    );

    $form['employmentfieldset'] = array_merge($form['employmentfieldset'], $employment);

    $form['employmentfieldset']['directdeposit'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Are you paid by direct deposit?',
        '#default_value' => ($formData['directdeposit']) ? $formData['directdeposit'] : '',
        '#options' => array('' => 'Select', 1 => 'Yes', 2 => 'No'),
        '#description' => '<div class="tooltip" title="'.t('Please tell us if you have your paycheck direct deposited to your bank account.').'"></div>'
    );

    $form['reffieldset'] = array(
      '#prefix' => '<div class="fieldset">',
      '#suffix' => '</div>',
    );

    $form['reffieldset']["refreltitle"] = array(
      "#value" => '<h3>'.t('Personal References').'</h3>');

    $form['reffieldset']["refrelsubtitle"] = array(
      '#value' => '<h4 class="padding10-notop">'.t('Relative').'</h4>',
    );

    $form['reffieldset']['refrelname'] = array(
        '#type' => 'textfield',
        '#title' => t('Full Name'),
        '#maxlength' => 35,
        '#required' => true,
        '#default_value' => ($formData['refrelname']) ? $formData['refrelname'] : '',
        '#description' => '<div class="tooltip" title="'.t('Please tell us the full name of your relative (family member).').'"></div>'
    );

    $form['reffieldset']['refrelphone'] = array(
      '#prefix' => '<div class="inline phone autotabs">',
      '#suffix' => '</div>'
    );

    $form['reffieldset']['refrelphone']['refrelphone1'] = array(
          '#type' => 'textfield',
          '#title' => t('Phone'),
          '#required' => true,
          '#default_value' => ($formData['refrelphone1']) ? $formData['refrelphone1'] : '',
          '#size' => 3,
          '#maxlength' => 3,
    );

    $form['reffieldset']['refrelphone']['refrelphone2'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#default_value' => ($formData['refrelphone2']) ? $formData['refrelphone2'] : '',
          '#size' => 3,
          '#maxlength' => 3,
    );
    $form['reffieldset']['refrelphone']['refrelphone3'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#default_value' => ($formData['refrelphone3']) ? $formData['refrelphone3'] : '',
          '#size' => 4,
          '#maxlength' => 4,
    );
    $form['reffieldset']['tooltip'] = array(
        '#value' => '<div class="tooltip" title="'.t("Please tell us your relative\'s home phone number.").'"></div>',
    );

    $relationships = $this->_getRelationships($pdl2->getRelationships()->result);

    $form['reffieldset']['refrelrelationship'] = array(
        '#type' => 'select',
        '#required' => true,
        '#maxlength' => 35,
        '#title' => 'Relationship',
        '#default_value' => ($formData['refrelrelationship']) ? $formData['refrelrelationship'] : '',
        '#options' => array( '' => 'Relationship') + $relationships['relativeRef'],
        '#description' => '<div class="tooltip" title="'.t('Please tell us if you have your paycheck direct deposited to your bank account.').'"></div>'
    );

    $form['reffieldset']["ref1subtitle"] = array(
        '#value' => '<h4 class="padding10">'.t('Non-Relative').'</h4>',
    );

    $form['reffieldset']['ref1name'] = array(
        '#type' => 'textfield',
        '#title' => t('Full Name'),
        '#required' => true,
        '#default_value' => ($formData['ref1name']) ? $formData['ref1name'] : '',
        '#description' => '<div class="tooltip" title="'.t('Please tell us the full name of your non-relative.').'"></div>'
    );

    $form['reffieldset']['ref1phone'] = array(
        '#prefix' => '<div class="inline phone autotabs">',
        '#suffix' => '</div>'
    );

    $form['reffieldset']['ref1phone']['ref1phone1'] = array(
          '#type' => 'textfield',
          '#title' => t('Phone'),
          '#required' => true,
          '#default_value' => ($formData['ref1phone1']) ? $formData['ref1phone1'] : '',
          '#size' => 3,
          '#maxlength' => 3,
    );

    $form['reffieldset']['ref1phone']['ref1phone2'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#default_value' => ($formData['ref1phone2']) ? $formData['ref1phone2'] : '',
          '#size' => 3,
          '#maxlength' => 3,
    );

    $form['reffieldset']['ref1phone']['ref1phone3'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#default_value' => ($formData['ref1phone3']) ? $formData['ref1phone3'] : '',
          '#size' => 4,
          '#maxlength' => 4,
    );

    $form['reffieldset']['ref1phone']['tooltip'] = array(
        '#value' => '<div class="tooltip" title="'.t("Please tell us your non-relative\'s home phone number.").'"></div>',
    );

    $form['reffieldset']['ref1relationship'] = array(
        '#type' => 'select',
        '#required' => true,
        '#title' => 'Relationship',
        '#default_value' => ($formData['ref1relationship']) ? $formData['ref1relationship'] : '',
        '#options' => array( '' => 'Relationship') + $relationships['nonRelativeRef'],
        '#description' => '<div class="tooltip" title="'.t('How is this person related to you (friend, co-worker, etc...).').'"></div>'
    );

    $form['buttons'] = array(
      '#prefix' => '<div class="buttons">',
      '#suffix' => '</div>'
    );
    $form['buttons']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Next'),
          '#attributes' => array('class' => 'image continue rright'),
    );
    $form['buttons']['back'] = array(
          '#type' => 'submit',
          '#value' => t('Back'),
          '#attributes' => array('class' => 'image back lleft'),
          '#validate' => array('pdl2_application_newaccount_back_validate'),
          '#submit' => array('pdl2_application_newaccount_back_submit'),
    );
    $form['buttons']['notice'] = array(
          '#value' => '<div class="infoText">'.t('Note: Hitting the back button will erase all the fields on the current page.').'</div>',
    );

    $form['#after_build'][] = 'pdl2_application_newaccount_form_after_build';

    $_SESSION["existinguser"] = 0;

    return $form;
  }


  public function validateForm($form, &$form_state) {
    $incomeSource = pdl2_application_newaccount_get_incomesource();
    $valInt = new Zend_Validate_Int();
    $valPhone = new Pdl2_Validate_PhoneNumber();
    
    $pdl2 = pdl2_core_get_api();
    $draftId = $pdl2->getHash();

    // Personal References are always on this page so check those first.
    $refrelName = $form_state['values']['refrelname'];
    if (!preg_match('/^[A-Za-z-\' .]+$/', $refrelName)) {
      form_set_error('refrelname', "Relative Full Name : Please enter a valid name (letters, spaces, dots, dashes and apostrophes only)");
    }

    $refrelPhone = $form_state['values']['refrelphone1'] . $form_state['values']['refrelphone2'] . $form_state['values']['refrelphone3'];
    if (!$valPhone->isValid($refrelPhone)) {
      $message = implode(',', $valPhone->getMessages());
      form_set_error('refrelphone1', "Relative Phone Number : ".$message);
    }

    $ref1Name = $form_state['values']['ref1name'];
    if (!preg_match('/^[A-Za-z-\' .]+$/', $ref1Name)) {
      form_set_error('ref1name', "Non Relative Name : Please enter a valid name (letters, spaces, dots, dashes and apostrophes only)");
    }

    $ref1Phone = $form_state['values']['ref1phone1'] . $form_state['values']['ref1phone2'] . $form_state['values']['ref1phone3'];
    if (!$valPhone->isValid($ref1Phone)) {
      $message = implode(',', $valPhone->getMessages());
      form_set_error('ref1phone1', "Non Relative Phone Number : ".$message);
    }

    // Employment type validation.
    if ($incomeSource == 'employment') {

      $employer = $form_state['values']['employer'];
      if (!preg_match('/^[0-9A-Za-z- .]+$/', $employer)) {
        form_set_error('employer', "Place of employment : Please enter a valid employer (letters, numbers, spaces, dots, and dashes only)");
      }

      $position = $form_state['values']['position'];
      if (!preg_match('/^[0-9A-Za-z- .]+$/', $position)) {
        form_set_error('position', "Department : Please enter a valid department (letters, spaces, dots, and dashes only)");
      }

      $workstreet1 = $form_state['values']['workstreet1'];
      if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $workstreet1)) {
        form_set_error('workstreet1', "Address : Please enter a valid address (letters, numbers, spaces, dots, dashes and apostrophes only)");
      }

      $workstreet2 = $form_state['values']['workstreet2'];
      if ($workstreet2 != "") {
        if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $workstreet2)) {
          form_set_error('workstreet2', "Address line 2 : Please enter a valid address (letters, numbers, spaces, dots, dashes and apostrophes only)");
        }
      }

      $city = $form_state['values']['workcity'];
      if (!preg_match('/^[A-Za-z .]+$/', $city)) {
        form_set_error('workcity', "City : Please enter a valid city (letters, spaces, and dots only)");
      }

      $zip = $form_state['values']['workzip'];
      if (!preg_match('/[0-9]{5}/', $zip)) {
        form_set_error('workzip', "ZIP : Please enter a valid 5 digit ZIP code");
      }


      $verifyPhone = $form_state['values']['workverify1'] . $form_state['values']['workverify2'] . $form_state['values']['workverify3'];
      if (!$valPhone->isValid($verifyPhone)) {
        $message = implode(',', $valPhone->getMessages());
        form_set_error('workverify1', "Verification Phone Number : ".$message);
      }

      $verifyExt = $form_state['values']['workverifyext'];
      if (!empty($verifyExt) && !$valInt->isValid($verifyExt)) {
        form_set_error('workverifyext', implode(',', $valInt->getMessages()));
      }

      $workPhone = $form_state['values']['workphone1'] . $form_state['values']['workphone2'] . $form_state['values']['workphone3'];
      if (!$valPhone->isValid($workPhone)) {
        $message = implode(',', $valPhone->getMessages());
        form_set_error('workphone1', "Work Phone Number : ".$message);
      }

      $workExt = $form_state['values']['workphoneext'];
      if (!empty($workExt) && !$valInt->isValid($workExt)) {
        form_set_error('workphoneext', implode(',', $valInt->getMessages()));
      }
    }


    // Rules for phone numbers
    // (1) Home/Cell can match Work/Verification Phone
    // (2) Home/Cell cannot match Relative/Non-relative Phone
    // (3) Relative/nonrelative cannot match
    // (4) Work/Verification cannot match Relative/Non-relative Phone

    // always exists
    $home_phone = $_SESSION['apply_homephone'];
    // may exist
    $cell_phone = $_SESSION['apply_cellphone'];

    // (1) check work/verify with home phone
    if ($workPhone == $home_phone) {
      form_set_error('workphone1', "Work phone # cannot match your home phone # from the previous page. Home Phone=[".$home_phone."]");
    }
    if ($verifyPhone == $home_phone) {
      form_set_error('workverify1', "Verify phone # cannot match your home phone # from the previous page. Home Phone=[".$home_phone."]");
    }
    // (2) relative / non-realative != home
    if ($ref1Phone == $home_phone) {
      form_set_error('refphone1', "Non Relative phone # cannot match your home phone # from the previous page. Home Phone=[".$home_phone."]");
    }
    if ($refrelPhone == $home_phone) {
      form_set_error('refrelphone1', "Relative phone # cannot match your home phone # from the previous page. Home Phone=[".$home_phone."]");
    }
    // (3) check relative != non relative
    if ($ref1Phone == $refrelPhone) {
      form_set_error('refrelphone1', "Relative phone # cannot match your Non-Relative phone # [".$ref1Phone."]");
    }
    // (4) check work with realtive & non-relative
    if ($workPhone == $refrelPhone) {
      form_set_error('refrelphone1', "Relative phone # cannot match your work phone # [".$workPhone."]");
    }
    if ($workPhone == $ref1Phone) {
      form_set_error('refphone1', "Non-relative phone # cannot match your work phone # [".$workPhone."]");
    }

    // Same checks now for Cell phone
    if ($cell_phone) {
      if ($workPhone == $cell_phone) {
        form_set_error('workphone1', "Work phone # cannot match your cell phone # from the previous page. Cell Phone=[".$cell_phone."]");
      }
      if ($verifyPhone == $cell_phone) {
        form_set_error('workverify1', "Verify phone # cannot match your cell phone # from the previous page. Cell Phone=[".$cell_phone."]");
      }
      // (2) relative / non-realative != home
      if ($ref1Phone == $cell_phone) {
        form_set_error('refphone1', "Non Relative phone # cannot match your cell phone # from the previous page. Cell Phone=[".$cell_phone."]");
      }
      if ($refrelPhone == $cell_phone) {
        form_set_error('refrelphone1', "Relative phone # cannot match your cell phone # from the previous page. Cell Phone=[".$cell_phone."]");
      }
    }

    // PDLNEW-2687 - only check work ZIP if income type is employment
    if ($incomeSource == "employment")
    {
      try {
        $pdl2->customerDraftEmploymentSetAddress($draftId,
                                                 $form_state['values']['workzip'],
                                                 $form_state['values']['workcity'],
                                                 $form_state['values']['workstreet1'],
                                                 $form_state['values']['workstreet2']);
      }
      catch (Exception $e) {
        form_set_error('workzip', "ZIP: The ZIP code you entered does not exist. Please check.");
      }

      try {
        $startDate = $this->_combineDates($form_state['values']['workbeginyr'],
                                          $form_state['values']['workbeginmo'],
                                          1);
        $pdl2->customerDraftEmploymentSetStartDate($draftId, $startDate);
      }
      catch (Exception $e) {
        form_set_error('workbeginmo', "Invalid employment start date, please check.");
      }
    }
  }


  public function submitForm($form, &$form_state, $page) {
    $pdl2 = pdl2_core_get_api();

    $draftId = $pdl2->getHash();

    $incomeSource = pdl2_application_newaccount_get_incomesource();

    if($incomeSource == 'employment') {
      $pdl2->customerDraftEmploymentSetEmployerName($draftId,
                                                    $form_state['values']['employer']);
      $pdl2->customerDraftEmploymentSetDepartment($draftId,
                                                  $form_state['values']['position']);
      try {
        $pdl2->customerDraftEmploymentSetAddress($draftId,
                                                 $form_state['values']['workzip'],
                                                 $form_state['values']['workcity'],
                                                 $form_state['values']['workstreet1'],
                                                 $form_state['values']['workstreet2']);
      }
      catch (Exception $e) {
        form_set_error('workzip', "Zip Code does not validate to a location. Enter a valid Zip Code");
        drupal_goto("newaccount/2");
      }
      $mainNumber = $this->_combineNumbers($form_state['values']['workphone1'],
                                           $form_state['values']['workphone2'],
                                           $form_state['values']['workphone3']);

      if ($form_state['values']['workphoneext']) {
        $mainNumber .= 'x' . $form_state['values']['workphoneext'];
      }

      $verificationNumber = $this->_combineNumbers($form_state['values']['workverify1'],
                                                   $form_state['values']['workverify2'],
                                                   $form_state['values']['workverify3']);

      if ($form_state['values']['workverifyext']) {
        $verificationNumber .= 'x' . $form_state['values']['workverifyext'];
      }

      $pdl2->customerDraftEmploymentSetPhones($draftId,
                                              $mainNumber,
                                              $verificationNumber);


      //Get work state from work zip code
      $res = $pdl2->customerDraftEmploymentGetAddress($draftId);
      $form_state['values']['workstate'] = $res->result->address->state;
    }

    $directDeposit = $form_state['values']['directdeposit'];
    // yes = 1, no = 2 from the form
    if ($directDeposit == "1") {
      $directDeposit = true;
    }
    else {
      $directDeposit = false;
    }
    $pdl2->customerDraftPaymentSetWhetherDirectDeposit($draftId,
                                                       $directDeposit);

    $pdl2->customerDraftSetRelativeReference($draftId,
                                             $form_state['values']['refrelname'],
                                             $this->_combineNumbers($form_state['values']['refrelphone1'],
                                                                    $form_state['values']['refrelphone2'],
                                                                    $form_state['values']['refrelphone3']),
                                             $form_state['values']['refrelrelationship']);

    $pdl2->customerDraftSetNonRelativeReference($draftId,
                                                $form_state['values']['ref1name'],
                                                $this->_combineNumbers($form_state['values']['ref1phone1'],
                                                                       $form_state['values']['ref1phone2'],
                                                                       $form_state['values']['ref1phone3']),
                                                $form_state['values']['ref1relationship']);

    $_SESSION['data']['newaccount'][$page] = $form_state['values'];
  }


  public function getConfirmFields() {
    $confirmFields = $this->_confirmFields;
    $incomeSource = pdl2_application_newaccount_get_incomesource();

    if ($incomeSource != 0) {
      unset($confirmFields['Employment Information']);
    }

    return $confirmFields;
  }


  protected function _getRelationships($relationshipsResult) {
    $relativeArray = array();
    $nonRelativeArray = array();

    foreach ($relationshipsResult as $keyRel => $valRel) {
      if (in_array($valRel->id, $this->_relativeIds)) {
        $relativeArray[$valRel->relationship] = $valRel->relationship;
      }

      if (in_array($valRel->id, $this->_nonRelativeIds)) {
        $nonRelativeArray[$valRel->relationship] = $valRel->relationship;
      }
    }

    return array('relativeRef' => $relativeArray, 'nonRelativeRef' => $nonRelativeArray);
  }
}
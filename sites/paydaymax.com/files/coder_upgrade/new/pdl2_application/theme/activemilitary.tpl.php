<div id="active-military">
  <p>While we are grateful for the dedication of U.S. armed forces members, we are unable to process loan applications for U.S. military personnel and their dependents. We encourage you to take advantage of alternative forms of financial assistance that are available to you from your branch of the U.S. armed forces.</p>
  <div id='military_flags'>
  <div><a href="http://www.afas.org/" target="_blank"><img src="<?=$themedata['imagePath']?>/image003.jpg" alt=""></a>Air Force</div>
    <div><a href="http://www.aerhq.org/" target="_blank"><img src="<?=$themedata['imagePath']?>/image004.jpg" alt=""></a>Army</div>
    <div><a href="http://www.nmcrs.org/" target="_blank"><img src="<?=$themedata['imagePath']?>/image005.jpg" alt=""></a>Marines</div>
    <div><a href="http://www.cgmahq.org/" target="_blank"><img src="<?=$themedata['imagePath']?>/image006.jpg"></a>Coast Guard</div>
    <div><a href="http://www.nmcrs.org/" target="_blank"><img src="<?=$themedata['imagePath']?>/image007.jpg"></a>Navy</div>
  </div>

</div> 
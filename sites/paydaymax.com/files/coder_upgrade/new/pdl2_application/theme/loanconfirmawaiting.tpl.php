<div class="dashboard center">
	<h2>Please wait while we process your loan request</h2>
  <p>This can take up to two minutes. Please do not hit the back button or exit the browser.</p>
  <img src="<?php print base_path() . drupal_get_path('module', 'pdl2_application') . '/images/loading_anim.gif'; ?>" />
</div>
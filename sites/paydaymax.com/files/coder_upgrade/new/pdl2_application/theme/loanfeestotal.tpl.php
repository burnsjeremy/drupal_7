<div class="dashboard">
  <table id="loanfees-total">
    <tbody>
      <tr>
        <th>Loan:</th>
        <th>Fees:</td>
        <th>Total:</td>
      </tr>
      <tr>
        <td class="center" id="loan">$<?php echo number_format($themedata['loan'], 2);?></td>
        <td class="center" id="fees">$<?php echo number_format($themedata['fees'], 2);?></td>
        <td class="center" id="total">$<?php echo number_format($themedata['total'], 2);?></td>
      </tr>
    </tbody>
  </table>
</div>
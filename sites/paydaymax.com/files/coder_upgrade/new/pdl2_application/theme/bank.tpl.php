<div id="bank-section">
	<div class="dashboard">
		<p>When choosing your loan to be deposited to your checking account, please be aware that the availability of your money is dependent on your loans approval. Loan approvals will occur at 4:00 p.m. P.S.T.</p>
	</div>
</div>
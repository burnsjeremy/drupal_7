<?php /** MB: changed text as per PDMNEW-2663. If you're here changing the text, we may have different copy per type of user */ ?>
<div class="dashboard">
	<h3>Application Accepted</h3>
	<p>We will contact you as soon as we have reached a decision!</p>
	<p>We have a referral program set up for our customers in which you can earn $100.00 for each successful referral! Your referral code is <?php echo $themedata["referral_code"]; ?>. Please use this code to refer your friends and family.</p>
</div>
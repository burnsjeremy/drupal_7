<div>
  <p>How it works: if your application is approved, we'll roll your existing loan into a new one. The refinancing application is very similar to the loan application you're already familiar with. You may refinance your loan up to three times.</p>
  <p>As a reminder, here is the information regarding your current personal loan:</p>
</div>
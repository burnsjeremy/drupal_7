<?php
//Build a mock array of test data to use for testing the saving of a client.
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_application_newaccount_savetest_page() {
  $pdl2 = pdl2_core_get_api();

  $client = array(
    'interestId' => 1,
    'salutation' => 1,
    'firstName' => 'Test',
    'middleName' => 'The',
    'lastName' => 'Save',
    'homeAddress1' => '742 Evergreen Terrace',
    'homeAddress2' => '',
    'homeCity' => 'Springfield',
    'homeState' => 'IL',
    'homeZip' => 61870,
    'rentOrOwn' => 1,
    'incomeSource' => 0,
    'homePhone' => '5554558582',
    'birthdate' => '1981-08-15',
    'driversLicenseNum' => 'thx1138a113',
    'driversLicenseState' => 'IL',
    'emailAddress' => 'actate81+8712@gmail.com',
    'hasReferralCode' => 0,
    'password' => 'test1234',
    'referenceRelativeName' => 'Ben Wyatt',
    'referenceRelativePhone' => '5554998185',
    'referenceRelativeRelationship' => 6,
    'referenceNonRelativeName' => 'Ron Fing Swanson',
    'referenceNonRelativePhone' => '5554892018',
    'referenceNonRelativeRelationship' => 8,
    'routingNumber' => '064000017',
    'bankAccount' => '125355325',
    'routingVerified' => 1,
    'bankName' => 'REGIONS BANK',
    'bankPhone' => '8007344667',
    'bankAddress' => 'P.O. BOX 681<br />BIRMINGHAM AL',
    'socSec' => '400008712',
    'securityAnswer1' => 'Lawrence',
    'securityAnswer2' => 'Wilmington',
    'militaryMember' => 1,
    'militarySpouseOrDependent' => 1,
    'directDeposit' => 1,
    'agreeToTerms' => 1,
    'placeOfEmployment' => 'Los Pollos Hermanos',
    'workDepartment' => 'Special Projects',
    'workAddress1' => '1600 Pennsylvania Avenue',
    'workAddress2' => '',
    'workCity' => 'Washington',
    'workState' => 'IL',
    'workZip' => 61870,
    'workVerificationPhone' => '5554259281',
    'workVerificationExt' => '123456',
    'workPhoneNumber' => '5554882928',
    'workPhoneExt' => '345678',
    'workBeginYear' => 1995,
    'workBeginMonth' => '09',
  );
  $pdl2->saveNewClient($client);
}

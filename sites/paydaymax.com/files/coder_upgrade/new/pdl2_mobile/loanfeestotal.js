$(function() {

	
	function updateLoanInfo(key,val){
		// set text to loading...
		var loading = '<span><img src="' + Drupal.settings.basePath + Drupal.settings.pdl2_mobile.basepath + '/theme/ajax-loader.gif" /> Loading...</span>';
		$('#loan').html(loading);
		$('#fees').html(loading);
		$('#total').html(loading);
			
		var ajax_obj = $.getJSON(Drupal.settings.basePath + "apply/loanrequest/" + key + "/" + val,function(data){
			$('#loan').html("$" + data.loan.toFixed(2));
			$('#fees').html("$" + data.fees.toFixed(2));
			$('#total').html("$" + data.total.toFixed(2));
		});
	}
	
	$('#edit-principal').change(function(e){
		updateLoanInfo('principal',$(this).val());
	});
	
	$('#edit-coupon').change(function(e){
		if( $(this).val() ){
			updateLoanInfo('coupon',$(this).val());
		}
	});
	
	$('#edit-loanduedate').change(function(e){
		updateLoanInfo('loanduedate',$(this).val());
	});
	
	
	// dynamically update for coupon after done typing
	var timeout;
	$('#edit-coupon').keyup(function() {
		if( $('#edit-coupon').val().length >= 1 ){
			if(timeout) {
				clearTimeout(timeout);
				timeout = null;
			}
			timeout = setTimeout(myFunction, 2000);
		}else{
			if(timeout) {
				clearTimeout(timeout);
				timeout = null;
			}
		}
	});
	
	function myFunction() { 
		if( $('#edit-coupon').val().length >= 1 ){
			updateLoanInfo('coupon', $('#edit-coupon').val());
		}
		clearTimeout(timeout); // this way will not run infinitely
	}
		

});

/*
*/
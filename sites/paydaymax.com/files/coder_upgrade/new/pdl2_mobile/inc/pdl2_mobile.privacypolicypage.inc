<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_privacypolicy_page() {

  $pagename = 'privacy-policy';
  $catid = '101ACCT';

  // TODO Please change this theme call to use an associative array for the $variables parameter.
  $output = theme('privacypolicy') . drupal_get_form('pdl2_mobile_privacypolicy_form') . pdl2_mobile_write_pageview($pagename, $catid);

  return $output;

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_privacypolicy_form($form) {

  $form['buttons'] = array(
    '#type' => 'fieldset',
    '#attributes' => array('class' => 'btns'),
  );

  $form['buttons']['back'] = array(
    '#type' => 'submit',
    '#value' => t('BACK'),
    '#attributes' => array('class' => 'btn back'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_privacypolicy_form_submit($form, &$form_state) {
  drupal_goto('apply');
}

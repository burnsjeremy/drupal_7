<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_logout_page() {

  $before = pdl2_core_millisecs();

  $pagename = 'logout';
  $catid = '101ACCT';

  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_logout_page");
  }

  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  else {
    $pdl2->logout();

    drupal_set_message(t('Thanks for visiting the site!'));

    if ($pdl2->isProfilingCalls()) {
      $pdl2->logger()->endProcess();
    }

    drupal_goto('account/login');
  }
}

<?php
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_password_sent_page() {

  $pagename = 'password-sent';
  $catid = '101ACCT';

  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_password_sent_page");
  }

  $email = $_SESSION['forgotemail'];
  unset($_SESSION['forgotemail']);

  $companyinfo = $pdl2->getCompanyInfo();
  $company_name = $companyinfo->company->name;
  $company_number = $companyinfo->company->customer_service_number->phone->number;

  $themedata['email'] = $email;
  $themedata['company'] = $company_name;
  $themedata['number'] = $company_number;

  $output = theme('passwordsent', array('themedata' => $themedata)) . pdl2_mobile_write_pageview($pagename, $catid);

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->endProcess();
  }

  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_password_sent_form() {

  $form["forgotpwd"] = array(
    "#value" => "<p>" . l("BACK TO LOGIN", "account") . "</p>",
  );

  return $form;
}

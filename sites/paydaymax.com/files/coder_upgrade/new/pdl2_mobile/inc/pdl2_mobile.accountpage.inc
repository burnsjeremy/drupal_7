<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_account_page() {

  $pagename = 'account';
  $catid = '101ACCT';

  $pdl2 = pdl2_core_get_api();

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_account_page");
  }

  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  //is opt out?
  else if ($pdl2->currUser->optOut) {
    drupal_goto('account/logout');
  }

  $openLoan = false;
  $loanId = $pdl2->getOpenLoan();

  if ($pdl2->isAsyncCalls()) {
    if ($loanId) {
      $async_requests['loanStatus'] = $pdl2->getLoanStatusAsync($loanId);
      $async_requests['loanBalance'] = $pdl2->getLoanBalanceAsync($loanId);
      $async_requests['loanDueDate'] = $pdl2->getLoanDueDateAsync($loanId);
      $async_requests['allowExtension'] = $pdl2->allowExtensionAsync();
    }

    $async_requests['autoApproved'] = $pdl2->isAutoApprovedAsync();
    $async_requests['maxLoanObject'] = $pdl2->getMaxLoanOfferAsync();
    $async_requests['companyinfo'] = $pdl2->getCompanyInfoAsync();
    $async_requests['referralCode'] = $pdl2->getReferralCodeAsync();

    foreach ($async_requests as $key => $request) {

      $full = false;
      if ($key == 'loanStatus') {
        $full = true;
      }

      $$key = $pdl2->getResult($request, $full);
      unset($async_requests[$key]);
    }

  }
  else { //if not async
    if ($loanId) {
      $loanStatus = $pdl2->getLoanStatus($loanId);
      $loanType = $pdl2->getLoanType($loanId);
      $loanBalance = $pdl2->getLoanBalance($loanId);
      $loanDueDate = $pdl2->getLoanDueDate($loanId);
      $allowExtension = $pdl2->allowExtension();
    }
    $autoApproved = $pdl2->isAutoApproved();
    $maxLoanObject = $pdl2->getMaxLoanOffer();
    $companyinfo = $pdl2->getCompanyInfo();
    $referralCode = $pdl2->getReferralCode();
  }

  if ($loanId) {
    $openLoan = true;
    $pendingStatuses = array(0, 1, 2);

    if (in_array($loanStatus->code, $pendingStatuses)) {
      $loanStatus->name = 'Pending';
    }
  }

  $autoApproved = $autoApproved ? ($maxLoanObject->maxLoan >= $maxLoanObject->minLoan) : 0;
  $companyName = $companyinfo->company->name;
  $companyNumber = $companyinfo->company->customer_service_number->phone->number;

  $themedata = array(
    'openLoan' => $openLoan,
    'loantype' => $loanType,
    'loanStatus' => $loanStatus->loan_status,
    'loanDueDate' => $loanDueDate,
    'loanBalance' => $loanBalance,
    'allowExtension' => $allowExtension,
    'autoApproved' => $autoApproved,
    'maxLoanOffer' => $maxLoanObject->maxLoan,
    'companyName' => $companyName,
    'companyNumber' => $companyNumber,
    'referralCode' => $referralCode,
  );

  $output = theme('account', array('themedata' => $themedata)) . pdl2_mobile_write_pageview($pagename, $catid);

  if ($pdl2->isProfilingCalls()) {
    $pdl2->logger()->endProcess();
  }

  return $output;

}

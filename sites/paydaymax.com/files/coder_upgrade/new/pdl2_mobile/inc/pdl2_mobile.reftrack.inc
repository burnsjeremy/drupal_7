<?php
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_reftrack() {
  watchdog("pdl2_mobile_rt", "Incoming RT refname:%ref", array("%ref" => $_GET['ref']), WATCHDOG_NOTICE);

  $refcode = $_GET["ref"];
  pdl2_mobile_reftrack_do_reftrack($refcode);

  $url = pdl2_mobile_reftrack_get_redirect_url($refcode);
  drupal_goto($url);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_reftrack_do_reftrack($refcode) {

  $pdl2 = pdl2_core_get_api();
  $refSessionId = $pdl2->affiliateTrackingSessionInitialize($refcode);
  watchdog("pdl2_aff_rt", "Incoming ref:%name refsessionid:%refsessionid", array("%name" => $refcode, "%refsessionid" => $refSessionId), WATCHDOG_NOTICE);
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_mobile_reftrack_get_redirect_url($refcode = '') {

  $pdl2 = pdl2_core_get_api();
  unset($_GET['q']);
  $options = array(
    'absolute' => TRUE,
    'query' => $_GET,
  );

  // Assume we're always going to login here. Note that this was <front>, but Drupal's <front> redirection was wiping out our URL params causing no GA tracking of utm params.
  $page = "account/login";

  if ( !empty($_REQUEST["redirect"]) ) {
    $redirect_id = $_REQUEST['redirect'];
    $redirect_url = $pdl2->getReftrackRedirectPage($redirect_id);
    $page = $redirect_url != '' ? $redirect_url : $page;
  }

  if ($refcode) {
    $options['query']['ref'] = $refcode;
  }
  // TODO The second parameter to this function call should be an array.
  $url = url($page, $options);
  return $url;
}

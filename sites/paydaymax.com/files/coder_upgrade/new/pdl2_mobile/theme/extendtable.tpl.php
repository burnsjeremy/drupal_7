<table>
  <tr>
    <th class="left">Charge</th>
    <th class="left">Amount</th>
    <th class="left">Minimum Due</th>
  </tr>
  <tr>
    <td>Principal</td>
    <td>$<?php echo number_format($themedata['currentPrincipal'], 2);?></td>
    <td>$<?php echo number_format($themedata['minimumPrincipalDue'], 2);?></td>  
  </tr>
  <tr>
    <td>Fees</td>
    <td>$<?php echo number_format($themedata['currentFees'], 2);?></td>
    <td>$<?php echo number_format($themedata['minimumFees'], 2);?></td>  
  </tr>
  <tr>
    <td>Total</td>
    <td>$<?php echo number_format($themedata['currentTotal'], 2);?></td>
    <td>$<?php echo number_format($themedata['minimumTotal'], 2);?></td>  
  </tr>  
</table>

<div class="dashboard">
	<h3>Application Approved.</h3>
	<p class="center">We have approved your loan. Please check your email for complete details.</p>
	<p class="center">You may view the loan agreement by logging in to your <?php echo $themedata['company_name']; ?> account from a desktop computer.</p>
</div>
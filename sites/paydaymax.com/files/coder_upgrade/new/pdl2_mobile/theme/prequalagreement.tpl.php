<div class="dashboard">
	<p>I understand that MyCashNow will not perform credit checks with traditional credit bureaus, but I agree to allow verification of the information given in my application with national consumer reporting agencies  which provide access to non-traditional consumer credit data, including, but not limited to, Teletrack and Clarity.</p>
</div>

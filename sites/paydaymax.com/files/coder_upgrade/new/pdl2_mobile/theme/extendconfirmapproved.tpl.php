<div class="dashboard">
	<h3>Extension Granted.</h3>
	<p class="center">We have granted your loan extension request. Please check your email for complete details.</p>
	<p class="center">You may view the loan agreement by logging in to your <?php echo $themedata['company_name']; ?> account from a desktop computer.</p>
</div>
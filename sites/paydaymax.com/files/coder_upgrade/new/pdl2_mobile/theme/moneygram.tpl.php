<div id="moneygram-section">
	<div class="dashboard">
		<p>Thank you for choosing the MoneyGram option. We are sure you will be pleased with this new service. With a minimal $10.00 processing fee added to your loan, you can now have your money the same day your loan is approved, without having to wait on overnight deposits to your bank account.</p>
	</div>
</div>
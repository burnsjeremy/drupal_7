$(function() {
	$(document).ready(function() {

		var minAmountRadio = $('#edit-paymentType-0');
		var customAmountRadio = $('#edit-paymentType-1');
		var customAmountText = $('#edit-paymentAmount');
		
		customAmountText.attr('disabled', 'disabled');
		
		if(customAmountRadio.is(':checked')){
			customAmountText.removeAttr('disabled');
		}
		
		minAmountRadio.click(function(){
			customAmountText.attr('disabled', 'disabled');
		});
		
		customAmountRadio.click(function(){
			customAmountText.removeAttr('disabled');
		});
		
	});
});
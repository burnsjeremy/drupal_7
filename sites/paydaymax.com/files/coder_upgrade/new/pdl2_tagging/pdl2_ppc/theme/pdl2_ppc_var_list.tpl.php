<?php
  $rows = $themedata['rows'];
?>

<table>
  <thead>
    <tr>
      <td></td>
      <td>Page Name</td>
      <td>Page URL</td>
      <td>Vars Sent for page</td>
    </tr>
  </thead>
  <tbody>
<?php
    for ( $i=0 ; $i < count($rows) ; $i++)  {
      print "<tr>";
      $link = l(t('edit'),
                "admin/settings/pdl2_ppc/edit/".$rows[$i]->ppc_id,
                array(
                  'attributes'=> array(
                    'class' => 'button_edit_tag',
                  )
                )
              );
      print "  <td>".$link."</td>";
      print "  <td>".$rows[$i]->page_name."</td>";
      print "  <td>".$rows[$i]->page_url."</td>";

      print "  <td>";
      $vars = unserialize($rows[$i]->serialized_vars);
      if (is_array($vars)) {
        foreach ($vars as $key => $value) {
          print "<span class='key'>".$key."</span>:<span class='value'>".$value."</span>, ";
        }
      }
      print "  </td>";

      print "</tr>";
    }
?>
  </tbody>
</table>

<?php
  $link = l(t('Add a new PPC tagging page'),
            "admin/settings/pdl2_ppc/edit/-1",
            array(
              'attributes'=> array(
                'class' => 'button_new_tag',
              )
            )
          );
  print $link;
?>
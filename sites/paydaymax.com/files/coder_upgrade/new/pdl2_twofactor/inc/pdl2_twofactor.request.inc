<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_twofactor_request_page() {

  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $pdl2 = pdl2_core_get_api();
  $verification_required = $pdl2->customerTwoStepVerificationIsVerificationRequired();
  if (!$verification_required) {
    drupal_goto('account');
  }

  // TODO Please change this theme call to use an associative array for the $variables parameter.
  $output = theme('twofactor/request');
  $output .= drupal_get_form('pdl2_twofactor_request_form');
  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_twofactor_request_form($form) {

  $pdl2 = pdl2_core_get_api();
  $cell_phone = $pdl2->getCellPhone()->phone;
  $home_phone = $pdl2->getHomePhone()->phone;
  $phones[''] = '--Select--';

  if ($cell_phone) {
    $phones[$cell_phone->number] = $cell_phone->number;
  }
  if ($home_phone) {
    $phones[$home_phone->number] = $home_phone->number;
  }

  if (count($phones) > 2) { // count phones + blank select
    drupal_add_js(drupal_get_path('module', 'pdl2_twofactor') . '/js/twofactor_request.js');

    $form['two_factor_phone_label'] = array(
      '#value' => '<p>' . t('Please select your preferred phone number:') . '</p>',
    );

    $form['two_factor_phone'] = array(
      '#type' => 'select',
      '#title' => t('Pick a Phone number'),
      '#options' => $phones,
      '#required' => true,
    );

  }
  else {

    $form['two_factor_phone_label'] = array(
      '#value' => '<p>' . t('You have given us the following phone number:') . '</p>',
    );

    $form['two_factor_phone_number'] = array(
      '#value' => '<p>' . $home_phone->number . '</p>',
    );

    $form['two_factor_phone'] = array(
      '#type' => 'hidden',
      '#value' => $home_phone->number,
    );

  }

  if ($cell_phone->can_get_sms || $home_phone->can_get_sms) {

    $form['sms_prompt_wrapper'] = array(
      '#type' => 'fieldset',
      '#attributes' => array('id' => 'two-factor-smsprompt-wrapper'),
    );

    $form['sms_prompt_wrapper']['two_factor_sms_prompt_label'] = array(
      '#value' => '<p>' . t('If you would prefer a text message instead of a call to your home phone, please select your mobile number above.') . '</p>',
    );

    $form['method_wrapper'] = array(
      '#type' => 'fieldset',
      '#attributes' => array('id' => 'two-factor-method-wrapper'),
    );

    $form['method_wrapper']['two_factor_method_label'] = array(
      '#value' => '<p>' . t('Please select the preferred delivery method for your temporary code via SMS text or voice call. Choosing SMS text delivery of this unique code will not enroll you in any additional text message communications, however, message and data rates may apply.') . '</p>',
    );

    $form['method_wrapper']['two_factor_method'] = array(
      '#type' => 'radios',
      '#title' => t('Method'),
      '#options' => array(
        'sms' => t('SMS'),
        'voice' => t('Phone call'),
      ),
    );

  }
  else {

    $form['two_factor_method'] = array(
      '#type' => 'hidden',
      '#value' => 'voice',
    );
  }

  $form['buttons']['next'] = array(
    '#type' => 'submit',
    '#value' => t('CONTINUE'),
    '#attributes' => array('class' => 'image continue rright'),
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_twofactor_request_form_validate($form, &$form_state) {

  $pdl2 = pdl2_core_get_api();
  $cell_phone = $pdl2->getCellPhone()->phone;
  $home_phone = $pdl2->getHomePhone()->phone;

  $medium_type = $form_state['values']['two_factor_method'];
  $phone_number = $form_state['values']['two_factor_phone'];

  if (!$medium_type) {
    if ($phone_number == $home_phone->number && $home_phone->can_get_sms) {
      form_set_error('', 'You must select a method.');
    }
    else if ($phone_number == $cell_phone->number && $cell_phone->can_get_sms) {
      form_set_error('', 'You must select a method.');
    }
    else {
      $form_state['values']['two_factor_method'] = 'voice';
    }
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_twofactor_request_form_submit($form, &$form_state) {

  $pdl2 = pdl2_core_get_api();

  $medium_type = $form_state['values']['two_factor_method'];
  $phone_number = $form_state['values']['two_factor_phone'];
  $strip_phone = str_replace('-', '', $phone_number);

  $result = $pdl2->customerTwoStepVerificationSendVerificationCode($strip_phone, $medium_type);

  if ($result) {
    $form_state['redirect'] = 'account/validate/verify';
  }
  else {
    form_set_error('', t('We were unable to validate your account. Please try again.'));
  }
}

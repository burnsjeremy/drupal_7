<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_twofactor_verification_page() {

  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }


  $pdl2 = pdl2_core_get_api();
  $verification_required = $pdl2->customerTwoStepVerificationIsVerificationRequired();
  if (!$verification_required) {
    drupal_goto('account');
  }

  $output = drupal_get_form('pdl2_twofactor_verification_form');
  $output .= '<div class="return-to-account">' . l("<< Return to Account", "account") . '</div>';
  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_twofactor_verification_form($form) {

  $form['two_factor_verify_intro'] = array(
    '#value' => '<p>' . t('Once you have received your verification code, please enter it here:.') . '</p>',
  );

  $form['two_factor_verification_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter code:'),
    '#required' => true,
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#title' => t('Verify'),
    '#attributes' => array('class' => 'image continue rright'),
  );

  $form['two_factor_new_code_link'] = array(
    '#value' => '<p>' . t('Didn\'t receive the text message or phone call? Click <a href="@validate-request">here</a> to resend.', array('@validate-request' => url('account/validate/request'))) . '</p>',
  );
  $form['two_factor_verify_outro'] = array(
    '#value' => '<p>' . t('Thank you for verifying your identity. To confirm your code and continue on to view your loan application status, please click "Continue" below.') . '</p>',
  );

  return $form;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_twofactor_verification_form_validate($form, &$form_state) {

}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_twofactor_verification_form_submit($form, &$form_state) {

  $pdl2 = pdl2_core_get_api();

  $verification_code = $form_state['values']['two_factor_verification_code'];

  try {
    $result = $pdl2->customerTwoStepVerificationVerifyCode($verification_code);
  }
  catch (Exception $e) {
    // 1207 too many attempts
    if ($e->getCode() == '1207') {
      $result = true; // set result true and let the handler below check status and redirect to denied page.
    }
    else {
      form_set_error('', $e->getMessage());
    }
  }

  if ($result) {

    $loanId = $pdl2->getOpenLoan();
    if (!$loanId) {
      $loanId = $pdl2->getLastDeniedLoan();
    }
    $loanType = $pdl2->getLoanType($loanId);
    $loanStatusObject = $pdl2->getLoanStatus($loanId, $loanType);
    $loanStatus = $loanStatusObject->loan_status;

    $slugs = array(
      'approved' => array(3, 4, 5),
      'denied' => array(9),
    );

    $slug = pdl2_application_multidimensional_search($loanStatus->code, $slugs);
    $slug = !$slug ? 'accepted' : $slug;
    $userType = ($pdl2->pdl2_account_isNewUser()) ? "new" : "previous";

    $form_state['redirect'] = 'confirm/' . $loanType . '/' . $slug . '/' . $userType;
  }
  else {
    drupal_set_message(t('We were unable to validate your account. Please try again.'), 'error');
  }
}

<?php
/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_twofactor_method_callback($phone_number) {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    echo '';
    exit;
  }
  else if (pdl2_account_not_opt_out()) {
    echo '';
    exit;
  }

  $pdl2 = pdl2_core_get_api();
  $home_phone = $pdl2->getHomePhone()->phone;
  $cell_phone = $pdl2->getCellPhone()->phone;

  switch ($phone_number) {

    case $home_phone->number:
      $array['selected'] = $home_phone;
      $array['selected']->type = 'home';
      $array['not_selected'] = $cell_phone;
      $array['not_selected']->type = 'cell';
      break;

    case $cell_phone->number:

      $array['selected'] = $cell_phone;
      $array['selected']->type = 'cell';
      $array['not_selected'] = $home_phone;
      $array['not_selected']->type = 'home';
      break;

  }

  $json = json_encode( $array );
  //this is an AJAX callback, print, don't return
  print $json;

}

$(document).ready(function ()
{
	var loading = '<span id="loading"><img src="' + Drupal.settings.basePath + Drupal.settings.pdl2_core.basepath + '/theme/ajax-loader.gif" /> Loading...</span>';
  $('#two-factor-method-wrapper').after(loading);
	$('#two-factor-method-wrapper').hide();
  $('#two-factor-smsprompt-wrapper').hide();
	$('#edit-two-factor-phone').val('');
  $('#loading').hide();
  
  function updateLoanInfo(phone_number){

		$('#two-factor-method-wrapper').hide();
    $('#two-factor-smsprompt-wrapper').hide();
    $('#loading').show();
		var ajax_obj = $.getJSON(Drupal.settings.basePath + "account/validate/request/callback/" + phone_number,function(data){
    	
      $('#loading').hide();
      
      if (data.selected.can_get_sms) {
        $('#two-factor-method-wrapper').show();
        $('#two-factor-smsprompt-wrapper').hide();
      }
      else {
        $('#two-factor-method-wrapper').hide();
        
        if (data.not_selected.can_get_sms) {
          $('#two-factor-smsprompt-wrapper').show();
        }
        else {
          $('#two-factor-smsprompt-wrapper').hide();
        }
      }
		
    });
	}

	$('#edit-two-factor-phone').change(function(e){
		updateLoanInfo( $(this).val() );
	});

});
<?php
class Pdl2_Validate_BankAccount extends Zend_Validate_Abstract {
  public function isValid($value) {
    $valChain = new Zend_Validate();
    $valDigits = new Zend_Validate_Digits();
    $valLength = new Zend_Validate_StringLength(array('min' => 4,
                                                      'max' => 14));
    
    $valChain->addValidator($valDigits)
             ->addValidator($valLength);
    
    if (!$valChain->isValid($value)) {
      $this->_messages = $valChain->getMessages();
      return false;
    }
    
    return true;
  }
}
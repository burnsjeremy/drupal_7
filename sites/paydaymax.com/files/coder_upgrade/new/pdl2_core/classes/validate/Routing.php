<?php
class Pdl2_Validate_Routing extends Zend_Validate_Abstract {
  public function isValid($value) {
    $valDigits = new Zend_Validate_Digits();
    $valLength = new Zend_Validate_StringLength(array('min' => 9,
                                                      'max' => 9));
    
    $valChain = new Zend_Validate();
    $valChain->addValidator($valDigits)
             ->addValidator($valLength);
    
    if (!$valChain->isValid($value)) {
      $this->_messages = $valChain->getMessages();
      return false;
    }
    return true;
  }
}
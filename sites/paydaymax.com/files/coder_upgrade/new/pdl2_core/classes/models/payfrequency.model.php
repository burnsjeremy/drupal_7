<?php

require_once('record.interface.php');

class PayFrequencyModel implements IRecord
{
	public $payFrequencyId;
	public $payDayTable;
	public $frequency;
	public $description;
	public $method;
	public $realMethod;

	public function __construct($payFrequencyId, $payDayTable, $frequency, $description, $method, $realMethod)
	{
		$this->payFrequencyId = $payFrequencyId;
		$this->payDayTable = $payDayTable;
		$this->frequency = $frequency;
		$this->description = $description;
		$this->method = $method;
		$this->realMethod = $realMethod;
	}
	
	public function getPrimaryKey()
	{
		return $this->payFrequencyId;
	}
}
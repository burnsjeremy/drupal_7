<?php

require_once('response.model.php');

/*
	Class that contains a list of ReferralModels for a given user.
*/

class UserReferralsListModel extends ResponseModel
{
	public $referrals;
	
	public function __construct($status, $referrals)
	{
		$this->status = $status;
		
		$this->referrals = $referrals;
	}
}
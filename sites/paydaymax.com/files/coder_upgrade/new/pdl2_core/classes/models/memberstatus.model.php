<?php

require_once('response.model.php');

class MemberStatusModel extends ResponseModel
{
	public $memberStatus;
	
	public function __construct(StatusModel $status, $memberStatus)
	{
		$this->status = $status;
		$this->memberStatus = $memberStatus;
	}
}
<?php

require_once('response.model.php');

/*
	Class that contains boolean values representing a user's
	privacy preferences.
*/

class PrivacyPreferencesModel extends ResponseModel
{
	public $birthdayOptOut;
	public $holidayOptOut;
	public $specialOptOut;
	public $affiliateOptOut;
	public $thirdPartyOptOut;
	public $smsOptOut;
	
	public function __construct($status, $birthdayOptOut, $holidayOptOut, $specialOptOut, $affiliateOptOut, $thirdPartyOptOut, $smsOptOut)
	{
		// Instance of StatusModel, can just be set to null if creating an instance of this model w/o a service response.
		$this->status = $status;
		
		$this->birthdayOptOut = $birthdayOptOut;
		$this->holidayOptOut = $holidayOptOut;
		$this->specialOptOut = $specialOptOut;
		$this->affiliateOptOut = $affiliateOptOut;
		$this->thirdPartyOptOut = $thirdPartyOptOut;
		$this->smsOptOut = $smsOptOut;
	}
	
}
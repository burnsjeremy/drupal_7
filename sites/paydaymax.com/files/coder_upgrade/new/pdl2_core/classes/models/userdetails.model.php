<?php

require_once('response.model.php');

class UserDetailsModel extends ResponseModel
{
	public $userId;
	public $incomeSource;
	public $firstName;
	public $lastName;
	public $birthDate;
	public $ssn;
	public $email;
	public $password;
	
	public $referralCode;
	
	public $driverLicenseNum;
	public $driverState;
	
	public $homeStreet1;
	public $homeStreet2;
	public $homeCity;
	public $homeState;
	public $homeZip;
	public $homePhone;
	public $homeFax;
	public $cellPhone;
	public $altPhone;
	
	
	public $employer;
	public $position;
	public $workStreet1;
	public $workStreet2;
	public $workCity;
	public $workState;
	public $workZip;
	public $workPhone;
	public $workVerifyPhone;
	public $startDate;
	
	public $rentOrOwn;
	
	// TODO: Find out what these 2 members are?
	public $refRelPhone;
	public $ref1Phone;
	
	public $monthlySalary;
	public $directDeposit;
	
	public $bankName;
	public $bankPhone;
	public $bankRouting;
	public $bankAccount;
	
	public $badStatus;
	public $loanCountSuccessful;
	
	// TODO: Another look up table property here, I've been told this represents one of the 4 sites.
	public $interestId;
	
	public $maxLoanAmount;
	
	public $payDayText;
}
<?php

require_once('record.interface.php');

class IncomeSourceModel implements IRecord
{
	public $incomeSourceId;
	public $name;
	public $display;
	public $sortOrder;

	public function __construct($incomeSourceId, $name, $display, $sortOrder)
	{
		$this->incomeSourceId = $incomeSourceId;
		$this->name = $name;
		$this->display = $display;
		$this->sortOrder = $sortOrder;
	}
	
	public function getPrimaryKey()
	{
		return $this->incomeSourceId;
	}
}
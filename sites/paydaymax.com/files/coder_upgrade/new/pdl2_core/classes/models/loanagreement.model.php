<?php

class LoanAgreementModel
{
	public $agreementId;
	public $agreementDate;
	
	public function __construct($agreementId, DateTime $agreementDate)
	{
		$this->agreementId = $agreementId;
		$this->agreementDate = $agreementDate;
	}
}
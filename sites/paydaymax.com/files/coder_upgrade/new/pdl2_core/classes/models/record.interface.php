<?php

interface IRecord
{
	public function getPrimaryKey();
}
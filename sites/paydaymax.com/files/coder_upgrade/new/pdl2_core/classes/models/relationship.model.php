<?php

require_once('record.interface.php');

class RelationshipModel implements IRecord
{
	public $relationshipId;
	public $relationship;

	public function __construct($relationshipId, $relationship)
	{
		$this->relationshipId = $relationshipId;
		$this->relationship = $relationship;
	}
	
	public function getPrimaryKey()
	{
		return $this->relationshipId;
	}
}
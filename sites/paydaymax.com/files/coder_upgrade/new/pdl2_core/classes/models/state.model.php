<?php

class StateModel
{
	public $initials;
	public $name;

	public function __construct($initials, $name)
	{
		$this->initials = $initials;
		$this->name = $name;
	}
}
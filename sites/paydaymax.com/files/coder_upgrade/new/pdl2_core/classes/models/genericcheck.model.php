<?php

require_once('response.model.php');

class GenericCheckModel extends ResponseModel
{
	public $checkSuccess;
	public $stop;
	public $denyReason;
	public $denyMessage;
	public $lastCheck;
	
	public function __construct(StatusModel $status, $checkSuccess, $stop, $denyReason, $denyMessage, $lastCheck)
	{
		// Instance of StatusModel, can just be set to null if creating an instance of this model w/o a service response.
		$this->status = $status;
		
		$this->checkSuccess = $checkSuccess;
		$this->stop = $stop;
		$this->denyReason = $denyReason;
		$this->denyMessage = $denyMessage;
		$this->lastCheck = $lastCheck;
	}
	
}
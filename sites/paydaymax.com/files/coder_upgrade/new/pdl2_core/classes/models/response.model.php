<?php

abstract class ResponseModel
{
	public $status;
	
	public function hasError()
	{
		if (!isset($this->status))
		{
			throw new Exception("Status has not been set on model.");
		}
		
		return false;
	}
}
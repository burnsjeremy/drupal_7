<?php

class UserModel extends ResponseModel
{
  public $userId;
  public $sessionId;
  public $token;
  public $optOut;

  public function __construct(StatusModel $status, $userId, $token, $sessionId, $optOut=false)
  {
    $this->status = $status;

    $this->userId = $userId;
    $this->token = $token;
    $this->sessionId = $sessionId;
    $this->optOut = $optOut;
  }
}
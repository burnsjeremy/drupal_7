<?php

/*
	Class that contains data for a user's single historical loan.
*/

class LoanHistoryModel
{
	public $id;
	public $loanStatus;
	
	public $createdDate;
	public $loanDueDate;
	public $entryDate;
	
	public $loanDays;
	public $loanDaysTotal;
	
	public $loanAmount;
	
	public $interestFees;
    public $amountDue;
	
	public $paymentMethod;
	
	public function __construct()
	{
		
	}
}
<?php

require_once('response.model.php');

class LoanAgreementDetailsModel extends ResponseModel
{
	// TODO: Find out what this property is.
	public $lafId;
	
	public $loanId;
	public $borrower;
	public $homeStreet1;
	public $homeStreet2;
	public $homeCity;
	public $homeState;
	public $homeZip;
	public $ssn;
	public $signature;
	
	public $actualApr;
	public $financeCharge;
	public $loanAmount;
	
	public $dueDate;
	public $createDate;
	public $returnFee;
	
	// TODO: This member is referring to a specific PHP template file for structuring the output of this data. Still a lot of questions about this.
	public $typeOfDisplay;
	
	public $otherText;
}
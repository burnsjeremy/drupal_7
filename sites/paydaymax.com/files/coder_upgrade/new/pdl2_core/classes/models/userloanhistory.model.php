<?php

require_once('response.model.php');

/*
	Class that contains a list of LoanHistoryModels for a given user.
*/

class UserLoanHistoryModel extends ResponseModel
{
	public $loans;
	
	public function __construct($status, $loans)
	{
		$this->status = $status;
		
		$this->loans = $loans;
	}
}
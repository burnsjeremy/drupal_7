<?php
class PDL2Controller
{
  const TOKEN_COOKIE_NAME = 'token';
  const USER_ID_COOKIE_NAME = 'userid';
  const SESSION_ID_COOKIE_NAME = 'sessionid';
  const USER_OPT_OUT = 'useroptout';
  const BROWSER_SESSION_COOKIE_NAME = "doublecheck";

  const NO_RESULTS_FOUND_CODE = 53;
  const NO_RESPONSE_BODY = 21;
  const LOAN_IN_COLLECTIONS = 1150;
  const NON_BUSINESS_STATE = 749;

  const GENERIC_ERROR = 'No response received from remote server';

  private $_apiConfig;
  private $_cookieHelper;
  private $_webServiceHelper;

  private $_identity = "self";

  public $currUser;

  public $debugMode = 0;

  public function __construct(APIConfigModel $apiConfig, CookieHelper $cookieHelper, WebServiceHelper $webServiceHelper) {
    $this->_apiConfig = $apiConfig;
    $this->_cookieHelper = $cookieHelper;
    $this->_webServiceHelper = $webServiceHelper;

    // Can we restore a user from cookies?
    if ($this->hasUserCookies()) {
      $this->currUser = $this->getUserFromCookies();
      $this->validateApiToken();
    }
  }

  public function isProfilingCalls()
  {
    return $this->_apiConfig->isProfilingCalls();
  }

  public function getSiteID() {
    return $this->_apiConfig->siteIdentifier;
  }

  public function isAsyncCalls()
  {
    return $this->_apiConfig->isAsyncCalls();
  }

  public function logger()
  {
    return $this->_webServiceHelper->loggingHelper;
  }

  public function log($type, $message, $variables = array(), $severity = WATCHDOG_NOTICE)
  {
    $this->_webServiceHelper->loggingHelper->log($type, $message, $variables, $severity);
  }

  public function hasSessionData() {
    // Kick out if user close/reopened browser
    if (!$this->_cookieHelper->hasCookie(PDL2Controller::BROWSER_SESSION_COOKIE_NAME))
      return false;
      
    return isset($this->currUser);
  }

  public function getSessionId() {
    $params = array("identity" => "self");

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::GET_SESSION_ID, $params,
      NULL, $this->debugMode);
    return $res;
  }

  public function getHash() {
    return $_SESSION['data']['hash'];
  }

  public function setHash($hash) {
    $_SESSION['data']['hash'] = $hash;
  }
  
  public function clearHash() {
    unset($_SESSION['data']['hash']);
  }

  public function associateAffiliates()
  {
//      watchdog("pdl2_application", "associateAffiliates", array(), WATCHDOG_NOTICE);
      if (isset($_COOKIE["CJ_AID"]) && isset($_COOKIE["CJ_PID"]))
      {
        $_SESSION['CJ_Action_id'] = $this->affiliatePartnersCommissionJunctionAssociateUserToken($_COOKIE["CJ_AID"], $_COOKIE["CJ_PID"], $_COOKIE["CJ_SID"]);
        watchdog("pdl2_aff_cj", "associate CJ AID:%AID PID:%PID SID:%SID Action:%Action", array("%AID"=>$_COOKIE["CJ_AID"], "%PID"=>$_COOKIE["CJ_PID"], "%SID"=>$_COOKIE["CJ_SID"], "%Action"=>$_SESSION['CJ_Action_id']), WATCHDOG_NOTICE);
      }
      else if (isset($_COOKIE["MP_VisitID"]))
        $this->affiliatePartnersMypointsAssociateUserToken($_COOKIE["MP_VisitID"]);
      else if (isset($_COOKIE['LS_SiteID']))
        $this->affiliatePartnersLinkshareAssociateUserToken($_COOKIE['LS_SiteID'], $_COOKIE['LS_TimeEntered']);
      else
        $this->affiliateTrackingSessionAssociateUserToken();
  }

  public function pdl2_account_isNewUser() {
  	// Is this a new or existing user?
  	$loans = $this->getCustomerLoanHistory();
    if ($loans){
      foreach ($loans->result[0] as $loan){
        if ($loan->loanHistory->loan->loan->status->loan_status->code == 10) { // 10 is Closed Successfull
          return false;
        }
      }
    }

    return true;
  }

  /*
   * Log the user in.
   * Also check to see if the user has selected OPT_OUT_ALL for communication prefs.
   */
  public function login($username, $password, $adapter = NULL) {
    $params = array("apiKey"=>$this->_apiConfig->applicationToken,
                    "siteName"=>$this->_apiConfig->siteIdentifier,
                    "userName"=>$username,
                    "password"=>$password,
                    "adapter" => $adapter);
    
    if ( ip2long($_SERVER['HTTP_ORIGINATINGIP']) !== false ){
      $params["ipAddress"] = $_SERVER['HTTP_ORIGINATINGIP'];
    }

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOGIN,
                                                 $params,
                                                 NULL,
                                                 $this->debugMode,
                                                 'CustomerLoginExecute');
    if (isset($res)) {

      $success = $res->status;
      $status = new StatusModel($success,
                                $res->message->code,
                                $res->message->text,
                                $res->message->detail);

      if ($success == 1) {
        $opt_out = $this->customerPrivacyGetOptOutAll($res->result->token, $res->result->userId);
        $user = new UserModel($status,
                              $res->result->userId,
                              $res->result->token,
                              $res->result->sessionId,
                              $opt_out);
  
        $this->currUser = $user;

        $this->establishUserCookies();
        $this->associateAffiliates();
      }
      else {
        $opt_out = false;
        $user = new UserModel($status,
                              $res->result->userId,
                              $res->result->token,
                              $res->result->sessionId,
                              $opt_out);
      }

      return $user;
    } else {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
  }
  
  public function customerPrivacyGetOptOutAll($token = '', $clientid=''){
    
    // passed from login or affiliateTrackingLeadpileAuthenticate
    $token = $token ? $token : $this->currUser->token;
    $params = array('clientId' => $clientid ? $clientid : $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::ACCOUNT_CHECK_OPT_OUT_ALL,
                                                 $params,
                                                 $token,
                                                 $this->debugMode,
                                                 'CustomerPrivacyGetOptOutAllExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }
    return $res->result;
    
  }

  public function customerGetTransactions($transactionType){
    $params = array('clientId' => $this->currUser->userId,
                    'transactionType' => $transactionType);
    
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_TRANSACTIONS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetTransactionsExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }

    // single result returns the object instead of an
    // array of size 1 containing the object.
    // Munge it into an array.
    if(isset($res->result->customerTransactions->result) && !is_array($res->result->customerTransactions->result)) {
      $store = $res->result->customerTransactions->result;
      $res->result->customerTransactions->result = array();
      $res->result->customerTransactions->result[0] = $store;
    }
    
    return $res->result;
  }




  public function logout() {
    $params = array("userToken" => $this->currUser->token);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOGOUT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerLogoutExecute');

    // Clear cookies
    $this->clearUserCookies();

    unset($this->currUser);

    return $res;
  }

  /*
   * Get the customers open loan.
   * returns a loanID
   */
  public function getOpenLoan() {
    $params = array('clientId' => $this->currUser->userId);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETOPENLOAN,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetOpenLoanExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }
    return $res->result;
  }


  /*
   * Get the customers loan type.
   * returns a loan type as a string 'payday' & I assume 'personal'
   */
  public function getLoanType($loanId) {
    $params = array('clientId' => $this->currUser->userId,
                    'loanId' => $loanId);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GET_TYPE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'LoanGetTypeExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }
    return $res->result;
  }


  public function getLoanStatus($loanId, $loan_type='payday') {
    $params = array("loanId"=>$loanId);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_GETSTATUS;
      $service_execute = 'LoanPersonalGetStatusExecute';
    }
    else {
      $service_url = PDL2Services::LOAN_GETSTATUS;
      $service_execute = 'LoanGetStatusExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                  $params,
                                                  $this->currUser->token,
                                                  $this->debugMode,
                                                  $service_execute);

    return $res->result;
  }

  public function getLoanStatusAsync($loanId) {
    $params = array('loanId' => $loanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETSTATUS,
                                                   $params,
                                                   $this->currUser->token,
                                                   $this->debugMode,
                                                   'LoanGetStatusExecute',
                                                   true);

    return $request;
  }

  public function getFromEmailHash($hash) {
    $params = array('emailHash' => $hash);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETFROMEMAILHASH,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetFromEmailHashExecute');
    return $res->result;
  }

  public function extensionCreate($loanId, $paymentAmount = 0) {
    $params = array('loanId' => $loanId);

    if (!empty($paymentAmount) && $paymentAmount > 0) {
      $params['paymentAmount'] = $paymentAmount;
    }

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_CREATE,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanExtensionCreateExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function extensionCreateAsync($loanId, $paymentAmount = 0) {
    $params = array('loanId' => $loanId);

    if (!empty($paymentAmount) && $paymentAmount > 0) {
      $params['paymentAmount'] = $paymentAmount;
    }

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_CREATE,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanExtensionCreateExecute',
                          true);

    return $request;
  }

  public function getLoanDueDate($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETDUEDATE, $params,
            $this->currUser->token, $this->debugMode, 'LoanGetDueDateExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }

    return $res->result;
  }

  public function getLoanDueDateAsync($loanId) {
    $params = array('loanId' => $loanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETDUEDATE,
                            $params,
                            $this->currUser->token,
                            $this->debugMode,
                            'LoanGetDueDateExecute',
                            true);

    return $request;
  }

  public function getLoanPrincipal($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETPRINCIPAL, $params,
            $this->currUser->token, $this->debugMode, 'LoanGetPrincipalExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }

    return $res->result;
  }

  public function getLoanPrincipalAsync($loanId) {
    $params = array('loanId' => $loanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETPRINCIPAL, $params,
            $this->currUser->token, $this->debugMode, 'LoanGetPrincipalExecute',true);

    return $request;
  }

  public function getLoanCollectionCompany($loanId, $loan_type='payday') {
    $params = array("loanId"=>$loanId);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_GETCOLLECTIONCOMPANY;
      $service_execute = 'LoanPersonalGetCollectionCompanyExecute';
    }
    else {
      $service_url = PDL2Services::LOAN_GETCOLLECTIONCOMPANY;
      $service_execute = 'LoanGetCollectionCompanyExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                  $params,
                                                  $this->currUser->token,
                                                  $this->debugMode,
                                                  $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }

    return $res->result;
  }

  public function getLoanBalance($loanId, $loan_type='payday') {
    $params = array("loanId"=>$loanId);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_GETBALANCE;
      $service_execute = 'LoanPersonalGetBalanceExecute';
    }
    else {
      $service_url = PDL2Services::LOAN_GETBALANCE;
      $service_execute = 'LoanGetBalanceExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                  $params,
                                                  $this->currUser->token,
                                                  $this->debugMode,
                                                  $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::LOAN_IN_COLLECTIONS) {  // Collections!
      return $status->detail;
    }

    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }

    return $res->result;
  }

  public function getLoanBalanceAsync($loanId) {
    $params = array('loanId' => $loanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETBALANCE,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanGetBalanceExecute',
                          true);

    return $request;
  }

  public function allowExtension() {
    $params = array('clientId' => $this->currUser->userId);


    $res = $this->_webServiceHelper->soapRequest(PDL2Services::EXTENSION_CHECK, $params,
            $this->currUser->token, $this->debugMode, 'CustomerAllowExtensionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0 || empty($res->result)) {
      return false;
    }

    return true;
  }

  public function allowExtensionAsync() {
    $params = array('clientId' => $this->currUser->userId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::EXTENSION_CHECK,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'CustomerAllowExtensionExecute',
                          true);

    return $request;
  }

  public function createCustomer($email) {
    $params = array('emailAddress' => $email);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_CREATE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerCreateExecute');

    return $res;
  }

  public function customerDraftSetName($draftId, $firstName, $middleName, $lastName) {
    $params = array('draftId' => $draftId,
                    'firstName' => $firstName,
                    'middleName' => $middleName,
                    'lastName' => $lastName);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETNAME, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetNameExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetHomeAddress($draftId, $zipCode, $city, $street, $streetSecondary) {
    $params = array('draftId' => $draftId,
                    'zipCode' => $zipCode,
                    'city' => $city,
                    'street' => $street,
                    'streetSecondary' => $streetSecondary,
                    );
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETHOMEADDRESS, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetHomeAddressExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }

  }

  public function customerDraftSetRelativeReference($draftId, $fullName, $phoneNumber, $relationship) {
    $params = array('draftId' => $draftId,
                    'fullName' => $fullName,
                    'phoneNumber' => $phoneNumber,
                    'relationship' => $relationship,
                    );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETRELATIVEREFERENCE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetRelativeReferenceExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }

  }

  public function customerDraftSetNonRelativeReference($draftId, $fullName, $phoneNumber, $relationship) {
    $params = array('draftId' => $draftId,
                    'fullName' => $fullName,
                    'phoneNumber' => $phoneNumber,
                    'relationship' => $relationship,
                    );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETNONRELATIVEREFERENCE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetNonRelativeReferenceExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }

  }

  public function customerDraftSetMailAddress($draftId, $zipCode, $city, $street) {
    $params = array('draftId' => $draftId,
                    'zipCode' => $zipCode,
                    'city' => $city,
                    'street' => $street,
                    );
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETMAILADDRESS, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetMailAddressExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }

  }

  public function customerDraftSetHomePhone($draftId, $phoneNumber, $isMobile) {
    $params = array('draftId' => $draftId,
                    'phoneNumber' => $phoneNumber,
                    'isMobile' => $isMobile,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETHOMEPHONE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetHomePhoneExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetCellPhone($draftId, $phoneNumber) {
    $params = array('draftId' => $draftId,
                    'phoneNumber' => $phoneNumber,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETCELLPHONE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetCellPhoneExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetDateOfBirth($draftId, $dateOfBirth) {
    $params = array('draftId' => $draftId,
                    'dateOfBirth' => $dateOfBirth,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETDATEOFBIRTH,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerDraftSetDateOfBirthExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetResidenceType($draftId, $residenceType) {
    $params = array('draftId' => $draftId,
                    'residenceType' => $residenceType,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETRESIDENCETYPE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetResidenceTypeExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftEmploymentSetEmployerName($draftId, $employerName) {
    $params = array('draftId' => $draftId,
                    'employerName' => $employerName);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_EMPLOYMENT_SETEMPLOYERNAME,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerDraftEmploymentSetEmployerNameExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    }
    else {
      return $res->result;
    }
  }

  public function customerDraftSetSocialSecurityNumber($draftId, $socialSecurityNumber) {
    $params = array('draftId' => $draftId,
                    'socialSecurityNumber' => $socialSecurityNumber);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETSOCIALSECURITYNUMBER,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerDraftSetSocialSecurityNumberExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    }
    else {
      return $res->result;
    }
  }

  public function customerDraftGetById($draftId) {
    $params = array('draftId' => $draftId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_GETBYID, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftGetByIdExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerSave($draftId) {
    $params = array('draftId' => $draftId
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SAVE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerSaveExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetPassword($draftId, $password) {
    $params = array('draftId' => $draftId,
                    'password' => $password
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETPASSWORD, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetPasswordExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetEmail($draftId, $email) {
    $params = array('draftId' => $draftId,
                    'emailAddress' => $email
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETEMAIL, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetEmailExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetSecurityAnswerOne($draftId, $securityAnswer) {
    $params = array('draftId' => $draftId,
                    'securityAnswer' => $securityAnswer
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETSECURITYANSWERONE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetSecurityAnswerOneExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetSecurityAnswerTwo($draftId, $securityAnswer) {
    $params = array('draftId' => $draftId,
                    'securityAnswer' => $securityAnswer
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETSECURITYANSWERTWO, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetSecurityAnswerTwoExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }


  public function customerDraftSetMilitaryStatus($draftId, $isInMilitary) {
    $params = array('draftId' => $draftId,
                    'isInMilitary' => $isInMilitary
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETMILITARYSTATUS, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetMilitaryStatusExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetBankAccount($draftId, $accountNumber, $routingNumber) {
    $params = array('draftId' => $draftId,
                    'accountNumber' => $accountNumber,
                    'routingNumber' => $routingNumber
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETBANKACCOUNT, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetBankAccountExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function referenceGetBank($routingNumber) {
    $params = array('routingNumber' => $routingNumber);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::REFERENCE_GETBANK,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'ReferenceGetBankExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftEmploymentSetDepartment($draftId, $department) {
    $params = array('draftId' => $draftId,
                    'department' => $department,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_EMPLOYMENT_SETDEPARTMENT, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftEmploymentSetDepartmentExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftEmploymentSetStartDate($draftId, $startDate) {
    $params = array('draftId' => $draftId,
                    'startDate' => $startDate);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_EMPLOYMENT_SETSTARTDATE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerDraftEmploymentSetStartDateExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    }
    else {
      return $res->result;
    }
  }

  public function customerDraftEmploymentSetAddress($draftId, $zipCode, $city, $street, $streetSecondary) {
    $params = array('draftId' => $draftId,
                    'zipCode' => $zipCode,
                    'city' => $city,
                    'street' => $street,
                    'streetSecondary' => $streetSecondary,
                    );
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_EMPLOYMENT_SETADDRESS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerDraftEmploymentSetAddressExecute');
    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    }
    return $res->result;
  }


  public function customerDraftEmploymentGetAddress($draftId) {
    $params = array('draftId' => $draftId);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_EMPLOYMENT_GETADDRESS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerDraftEmploymentGetAddressExecute');
    return $res;
  }


  public function customerDraftEmploymentSetPhones($draftId, $mainNumber, $verificationNumber) {
    $params = array('draftId' => $draftId,
                    'mainNumber' => $mainNumber,
                    'verificationNumber' => $verificationNumber,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_EMPLOYMENT_SETPHONES, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftEmploymentSetPhonesExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftEmploymentSetIncomeSourceType($draftId, $incomeSourceType) {
    $params = array('draftId' => $draftId,
                    'incomeSourceType' => $incomeSourceType,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_EMPLOYMENT_SETINCOMESOURCETYPE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftEmploymentSetIncomeSourceTypeExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftPaymentSetWhetherDirectDeposit($draftId, $isDirectDeposit) {
    $params = array('draftId' => $draftId,
                    'isDirectDeposit' => $isDirectDeposit);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_PAYMENT_SETWHETHERDIRECTDEPOSIT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerDraftPaymentSetWhetherDirectDepositExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftPaymentSetScheduleEveryOtherWeek($draftId, $paycheckAmount, $lastPayDate, $weekday) {
    $params = array('draftId' => $draftId,
                    'paycheckAmount' => $paycheckAmount,
                    'lastPayDate' => $lastPayDate,
                    'weekday' => $weekday
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEEVERYOTHERWEEK, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftPaymentSetScheduleEveryOtherWeekExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftPaymentSetScheduleMonthlySpecificDay($draftId, $paycheckAmount, $dayOfMonth) {
    $params = array('draftId' => $draftId,
                    'paycheckAmount' => $paycheckAmount,
                    'dayOfMonth' => $dayOfMonth,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEMONTHLYSPECIFICDAY, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftPaymentSetScheduleMonthlySpecificDayExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftPaymentSetScheduleMonthlyWeekdayAfterSpecificDay($draftId, $paycheckAmount, $dayOfMonth, $weekday) {
    $params = array('draftId' => $draftId,
                    'paycheckAmount' => $paycheckAmount,
                    'dayOfMonth' => $dayOfMonth,
                    'weekday' => $weekday
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEMONTHLYWEEKDAYAFTERSPECIFICDAY, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftPaymentSetScheduleMonthlyWeekdayAfterSpecificDayExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftPaymentSetScheduleMonthlyWeekdayInSpecificWeek($draftId, $paycheckAmount, $weekOfMonth, $weekday) {
    $params = array('draftId' => $draftId,
                    'paycheckAmount' => $paycheckAmount,
                    'weekOfMonth' => $weekOfMonth,
                    'weekday' => $weekday
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEMONTHLYWEEKDAYINSPECIFICWEEK, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftPaymentSetScheduleMonthlyWeekdayInSpecificWeekExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftPaymentSetScheduleTwiceAMonthSpecificDays($draftId, $paycheckAmount, $firstDay, $secondDay) {
    $params = array('draftId' => $draftId,
                    'paycheckAmount' => $paycheckAmount,
                    'firstDay' => $firstDay,
                    'secondDay' => $secondDay
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_PAYMENT_SETSCHEDULETWICEAMONTHSPECIFICDAYS, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftPaymentSetScheduleTwiceAMonthSpecificDaysExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftPaymentSetScheduleTwiceAMonthSpecificWeeks($draftId, $paycheckAmount, $weekday, $firstWeek, $secondWeek) {
    $params = array('draftId' => $draftId,
                    'paycheckAmount' => $paycheckAmount,
                    'weekday' => $weekday,
                    'firstWeek' => $firstWeek,
                    'secondWeek' => $secondWeek
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_PAYMENT_SETSCHEDULETWICEAMONTHSPECIFICWEEKS, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftPaymentSetScheduleTwiceAMonthSpecificWeeksExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftPaymentSetScheduleWeekly($draftId, $paycheckAmount, $weekday) {
    $params = array('draftId' => $draftId,
                    'paycheckAmount' => $paycheckAmount,
                    'weekday' => $weekday,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEWEEKLY, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftPaymentSetScheduleWeeklyExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  //Personal Loan Methods
  public function createPersonalLoanDraft() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_LOAN_PERSONAL_CREATE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerLoanPersonalCreateExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerLoanPersonalSave($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_LOAN_PERSONAL_SAVE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerLoanPersonalSaveExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerLoanPersonalGetDueDates() {
    $params = array('clientId' => $this->currUser->userId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_LOAN_PERSONAL_GETDUEDATES, $params,
            $this->currUser->token, $this->debugMode, 'CustomerLoanPersonalGetDueDatesExecute');
    
    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    // Make sure it always returns an array.
    if (is_array($res->result->result)) {
      return $res->result->result;
    }
    else {
      return array($res->result->result);
    }

  }

  public function customerLoanPersonalIsEligible() {
    $params = array('clientId' => $this->currUser->userId);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_LOAN_PERSONAL_ISELIGIBLE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerLoanPersonalIsElegibleExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    }
    else {
      return $res->result;
    }
  }

  public function customerLoanPersonalGetMaxOffer() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_LOAN_PERSONAL_GETMAXOFFER,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerLoanPersonalGetMaxOfferExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftAgreementGetClarity($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_AGREEMENT_GETCLARITY, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftAgreementGetClarityExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftAgreementSetClarity($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_AGREEMENT_SETCLARITY, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftAgreementSetClarityExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftAgreementGetEft($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_AGREEMENT_GETEFT, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftAgreementGetEftExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftAgreementSetEft($draftLoanId, $agrees) {
    $params = array('draftLoanId' => $draftLoanId,
                    'agrees' => $agrees
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_AGREEMENT_SETEFT, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftAgreementSetEftExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }


  public function loanPersonalDraftGetApr($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_GETAPR, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftGetAprExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftGetDueDate($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_GETDUEDATE, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftGetDueDateExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftGetLender($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_GETLENDER, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftGetLenderExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftGetPrincipal($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_GETPRINCIPAL, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftGetPrincipalExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftSetCoupon($draftLoanId, $code) {
    $params = array('draftLoanId' => $draftLoanId,
                    'code' => $code
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_SETCOUPON, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftSetCouponExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftSetDueDate($draftLoanId, $date) {
    $params = array('draftLoanId' => $draftLoanId,
                    'date' => $date
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_SETDUEDATE, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftSetDueDateExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftSetPaymentMethod($draftLoanId, $method) {
    $params = array('draftLoanId' => $draftLoanId,
                    'method' => $method
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_SETPAYMENTMETHOD, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftSetPaymentMethodExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftSetPrincipal($draftLoanId, $amount) {
    $params = array('draftLoanId' => $draftLoanId,
                    'amount' => $amount
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_SETPRINCIPAL, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftSetPrincipalExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftSetRequestedAmount($draftLoanId, $amount) {
    $params = array('draftLoanId' => $draftLoanId,
                    'amount' => $amount
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_SETREQUESTEDAMOUNT, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftSetRequestedAmountExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetApr($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETAPR, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetAprExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetBalance($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETBALANCE, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetBalanceExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetById($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETBYID, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetByIdExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetDueDate($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETDUEDATE, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetDueDateExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetFinanceCharges($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETFINANCECHARGES, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetFinanceChargesExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetLender($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETLENDER, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetLenderExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetMoneyGramReferenceNumber($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETMONEYGRAMREFERENCENUMBER, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetMoneyGramReferenceNumberExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetPrincipal($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETPRINCIPAL, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetPrincipalExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetStatus($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETSTATUS, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetStatusExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetAgreements($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETAGREEMENTS, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetAgreementsExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftGetSchedule($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_GETSCHEDULE, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftGetScheduleExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetHistory() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETHISTORY, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetHistoryExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetSchedule($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETSCHEDULE, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetScheduleExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetAgreement($loanId) {
    $params = array('loanId' => $loanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETAGREEMENT, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetAgreementExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalDraftGetAgreement($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_GETAGREEMENT, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalDraftGetAgreementExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalGetAgreementById($agreementId) {
    $params = array('agreementId' => $agreementId,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_GETAGREEMENTBYID, $params,
            $this->currUser->token, $this->debugMode, 'LoanPersonalGetAgreementByIdExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerGetPayFrequency() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_PAYMENT_GET_FREQUENCY,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerPaymentGetFrequencyExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    }
    else {
      return $res->result;
    }
  }

  public function customerIovationSetBlackboxValue($blackboxValue) {
    
    $ipAddress = variable_get('pdl2_iovation_enable_originating_ip', 0) ? $_SERVER['HTTP_ORIGINATINGIP'] : $_SERVER['REMOTE_ADDR'];

    if ( ip2long($ipAddress) == -1 || ip2long($ipAddress) === FALSE ){
      watchdog("pdl2_core", "customerIovationSetBlackboxValue: Can't convert IP %ip to usable format (HTTP_ORIGINATINGIP: %orig, REMOTE_ADDR: %remote)", array("%ip" => $ipAddress, "%orig"=>$_SERVER['HTTP_ORIGINATINGIP'], "%remote"=>$_SERVER['REMOTE_ADDR']), WATCHDOG_ERROR);
      return false;
    };
    
    $params = array('clientId'=>$this->currUser->userId,
                    'blackboxValue' => $blackboxValue,
                    'ipAddress' => $ipAddress);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_IOVATION_SETBLACKBOXVALUE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerIovationSetBlackboxValueExecute');

    if (!$res->status) {
      return false;
    }
    else {
      return $res->result;
    }
  }
  
  public function customerTwoStepVerificationVerifyCode($verificationCode) {
    
    $params = array('clientId'=>$this->currUser->userId,
                    'verificationCode' => $verificationCode);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_TWOSTEP_VERIFYCODE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerTwoStepVerificationVerifyCodeExecute');

    if ($res->message->code == '1207') {
      throw new Exception($res->message->detail, $res->message->code);
    }
    else {
      return $res->result;
    }
  }

  public function customerTwoStepVerificationIsVerificationRequired() {
    
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_TWOSTEP_ISVERIFICATIONREQUIRED,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerTwoStepVerificationIsVerificationRequiredExecute');

    if (!$res->status) {
      return false;
    }
    else {
      return $res->result;
    }
  }

  public function customerTwoStepVerificationSendVerificationCode($phoneNumber, $mediumType) {
    
    $params = array('clientId'=>$this->currUser->userId,
                    'phoneNumber' => $phoneNumber,
                    'mediumType' => $mediumType);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_TWOSTEP_SENDVERIFICATIONCODE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerTwoStepVerificationSendVerificationCodeExecute');

    if (!$res->status) {
      return false;
    }
    else {
      return $res->result;
    }
  }

  public function customerTwoStepVerificationHasActiveVerificationCode() {
    
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_TWOSTEP_HASACTIVEVERIFICATIONCODE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerTwoStepVerificationHasActiveVerificationCodeExecute');

    if (!$res->status) {
      return false;
    }
    else {
      return $res->result;
    }
  }

  public function customerPaymentSetScheduleEveryOtherWeek($clientId, $paycheckAmount, $lastPayDate, $weekday) {
    $params = array('clientId' => $clientId,
                    'paycheckAmount' => $paycheckAmount,
                    'lastPayDate' => $lastPayDate,
                    'weekday' => $weekday
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_PAYMENT_SETSCHEDULEEVERYOTHERWEEK, $params,
            $this->currUser->token, $this->debugMode, 'CustomerPaymentSetScheduleEveryOtherWeekExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerPaymentSetScheduleMonthlySpecificDay($clientId, $paycheckAmount, $dayOfMonth) {
    $params = array('clientId' => $clientId,
                    'paycheckAmount' => $paycheckAmount,
                    'dayOfMonth' => $dayOfMonth,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_PAYMENT_SETSCHEDULEMONTHLYSPECIFICDAY,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerPaymentSetScheduleMonthlySpecificDayExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerPaymentSetScheduleMonthlyWeekdayAfterSpecificDay($clientId, $paycheckAmount, $dayOfMonth, $weekday) {
    $params = array('clientId' => $clientId,
                    'paycheckAmount' => $paycheckAmount,
                    'dayOfMonth' => $dayOfMonth,
                    'weekday' => $weekday
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_PAYMENT_SETSCHEDULEMONTHLYWEEKDAYAFTERSPECIFICDAY, $params,
            $this->currUser->token, $this->debugMode, 'CustomerPaymentSetScheduleMonthlyWeekdayAfterSpecificDayExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerPaymentSetScheduleMonthlyWeekdayInSpecificWeek($clientId, $paycheckAmount, $weekOfMonth, $weekday) {
    $params = array('clientId' => $clientId,
                    'paycheckAmount' => $paycheckAmount,
                    'weekOfMonth' => $weekOfMonth,
                    'weekday' => $weekday
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_PAYMENT_SETSCHEDULEMONTHLYWEEKDAYINSPECIFICWEEK, $params,
            $this->currUser->token, $this->debugMode, 'CustomerPaymentSetScheduleMonthlyWeekdayInSpecificWeekExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerPaymentSetScheduleTwiceAMonthSpecificDays($clientId, $paycheckAmount, $firstDay, $secondDay) {
    $params = array('clientId' => $clientId,
                    'paycheckAmount' => $paycheckAmount,
                    'firstDay' => $firstDay,
                    'secondDay' => $secondDay
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_PAYMENT_SETSCHEDULETWICEAMONTHSPECIFICDAYS, $params,
            $this->currUser->token, $this->debugMode, 'CustomerPaymentSetScheduleTwiceAMonthSpecificDaysExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerPaymentSetScheduleTwiceAMonthSpecificWeeks($clientId, $paycheckAmount, $weekday, $firstWeek, $secondWeek) {
    $params = array('clientId' => $clientId,
                    'paycheckAmount' => $paycheckAmount,
                    'weekday' => $weekday,
                    'firstWeek' => $firstWeek,
                    'secondWeek' => $secondWeek
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_PAYMENT_SETSCHEDULETWICEAMONTHSPECIFICWEEKS, $params,
            $this->currUser->token, $this->debugMode, 'CustomerPaymentSetScheduleTwiceAMonthSpecificWeeksExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerPaymentSetScheduleWeekly($clientId, $paycheckAmount, $weekday) {
    $params = array('clientId' => $clientId,
                    'paycheckAmount' => $paycheckAmount,
                    'weekday' => $weekday,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_PAYMENT_SETSCHEDULEWEEKLY, $params,
            $this->currUser->token, $this->debugMode, 'CustomerPaymentSetScheduleWeeklyExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanDraftSetRequestedAmount($draftLoanId, $requestedAmount) {
    $params = array('draftLoanId' => $draftLoanId,
                    'amount' => $requestedAmount,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_SETREQUESTEDAMOUNT, $params,
            $this->currUser->token, $this->debugMode, 'LoanDraftSetRequestedAmountExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanDraftCancel($draftLoanId, $loan_type='payday') {
    $params = array('draftLoanId' => $draftLoanId);

    if ($loan_type == "payday") {
      $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_CANCEL, $params,
              $this->currUser->token, $this->debugMode, 'LoanDraftCancelExecute');
    } else {
      $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_DRAFT_CANCEL, $params,
              $this->currUser->token, $this->debugMode, 'LoanPersonalDraftCancelExecute');
    }

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalRefinanceIsLoanEligible($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_REFINANCE_ISLOANELIGIBLE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'LoanPersonalRefinanceIsLoanEligibleExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function loanPersonalRefinanceDraftCreateRefinanceDraft($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_PERSONAL_REFINANCE_DRAFT_CREATEREFINANCEDRAFT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'LoanPersonalRefinanceDraftCreateRefinanceDraftExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetDriversLicense($draftId, $licenseNumber, $state) {
    $params = array('draftId' => $draftId,
                    'licenseNumber' => $licenseNumber,
                    'state' => $state
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETDRIVERSLICENSE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetDriversLicenseExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function customerDraftSetReferral($draftId, $referralCode) {
    $params = array('draftId' => $draftId,
                    'referralCode' => $referralCode,
                   );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DRAFT_SETREFERRAL, $params,
            $this->currUser->token, $this->debugMode, 'CustomerDraftSetReferralExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function getStates() {
    $params = array();

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::REFERENCE_GETSTATESPROVINCES, $params,
            $this->currUser->token, $this->debugMode, 'ReferenceGetStatesProvincesExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function getState($abbreviation) {

    $states = $this->getStates();
    
    foreach ($states->result as $state){
      if($state->id == $abbreviation){
        return $state->name;
      }
    }
    
    return false;
    
  }

  public function getEveryOtherWeekPayDates() {
    $params = array();

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::REFERENCE_GETEVERYOTHERWEEKPAYDATES,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'ReferencePaymentGetEveryOtherWeekPayDatesExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function getRelationships() {
    $params = array();

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::GET_RELATIONSHIPS,
      $params, $this->currUser->token, $this->debugMode, 'ReferenceGetRelationshipsExecute');

    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function allowCCPayment() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_ALLOWCCPAYMENT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerPaymentCreditcardIsAllowedExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }
    return $res->result;
  }

  public function getCCPaymentOptions($loanId, $loan_type='payday') {
    $params = array("loanId"=>$loanId);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_GETCCPAYMENTOPTIONS;
      $service_execute = 'LoanPersonalPaymentCreditcardGetPaymentOptionsExecute';
    }
    else {
      $service_url = PDL2Services::LOAN_GETCCPAYMENTOPTIONS;
      $service_execute = 'LoanPaymentCreditcardGetPaymentOptionsExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                  $params,
                                                  $this->currUser->token,
                                                  $this->debugMode,
                                                  $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }
    return $res->result;
  }

  public function getCCSubmissoinValues($loanId, $paymentAmount, $loan_type='payday') {
    $params = array('loanId' => $loanId,
            'paymentAmount' => $paymentAmount,
            'ipAddress' => $_SERVER['REMOTE_ADDR']);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_GETCCSUBMISSIONVALUES;
      $service_execute = 'LoanPersonalPaymentCreditcardGetSubmissionValuesExecute';
    }
    else {
      $service_url = PDL2Services::LOAN_GETCCSUBMISSIONVALUES;
      $service_execute = 'LoanPaymentCreditcardGetSubmissionValuesExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                  $params,
                                                  $this->currUser->token,
                                                  $this->debugMode,
                                                  $service_execute);


    if (!$res->status) {
      throw new Exception($res->message->detail, $res->message->code);
    } else {
      return $res->result;
    }
  }

  public function processCCResponse($loanId, $params, $loan_type) {
    $params = array('loanId'          => $loanId,
                'creditCardTransactionHash'  => $params['creditCardTransactionHash'],
                'UMresponseHash'          => $params['UMresponseHash'],
                'UMresult'                 => $params['UMresult'],
                'UMrefNum'                 => $params['UMrefNum'],
                'UMstatus'                 => $params['UMstatus'],
                'UMavsResultCode'           => $params['UMavsResultCode'],
                'UMavsResult'             => $params['UMavsResult'],
                'UMcvv2ResultCode'           => $params['UMcvv2ResultCode'],
                'UMcvv2Result'             => $params['UMcvv2Result'],
                'UMerror'                 => $params['UMerror']);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_PROCESSCCRESPONSE;
      $service_execute = 'LoanPersonalPaymentCreditcardProcessResponseExecute';
    } else {
      $service_url = PDL2Services::LOAN_PROCESSCCRESPONSE;
      $service_execute = 'LoanPaymentCreditcardProcessResponseExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    if ($status->code == PDL2Controller::NO_RESULTS_FOUND_CODE) {
      return NULL;
    }
    return $res->result;
  }


  /*
   * Can this customer use Moneygram?
   * returns true/false
   */
  public function allowMoneyGram() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_ALLOWMONEYGRAM,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerAllowMoneyGramExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    if ($status->code == 0 || $res->result == 0) {
      return false;
    }
    return true;
  }

  public function allowMoneyGramAsync() {
    $params = array('clientId' => $this->currUser->userId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_ALLOWMONEYGRAM,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerAllowMoneyGramExecute',
                                                 true);

    return $request;
  }


  /*
   * Does htis usr have a moneygram ref #?
   */
  public function getMoneyGramReferenceNumber() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GET_MONEYGRAM_REF_NUMBER,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'LoanGetMoneyGramReferenceNumberExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }


  public function getMaxLoanOffer() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETMAXLOANOFFER, $params,
            $this->currUser->token, $this->debugMode, 'CustomerGetMaxLoanOfferExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getMaxLoanOfferAsync() {
    $params = array('clientId' => $this->currUser->userId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETMAXLOANOFFER,
                            $params,
                            $this->currUser->token,
                            $this->debugMode,
                            'CustomerGetMaxLoanOfferExecute',
                            true);

    return $request;
  }

  public function getResult($request,$full=false) {

    $this->logger()->startTask("CALL ASYNC for $request->_url");
  $res = $request->getResult();
    $this->logger()->endTask();

/*
  $status = PDL2Utils::GetStatusFromSoapResponse($res);

  if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
    return NULL;
  }
*/

  return $full ? $res : $res->result;

  }

  public function getLoanDraftAgreement($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETAGREEMENT,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanDraftGetAgreementExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getLoanDraftAgreementAsync($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETAGREEMENT,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanDraftGetAgreementExecute',
                          true);

    return $request;
  }

  public function getSsn() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETSSN, $params,
            $this->currUser->token, $this->debugMode, 'CustomerGetSocialSecurityNumberMaskedExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getSsnAsync() {
    $params = array('clientId' => $this->currUser->userId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETSSN,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'CustomerGetSocialSecurityNumberMaskedExecute',
                          true);

    return $request;
  }

  public function getLoanApr($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETAPR,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanDraftGetAprExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getLoanAprAsync($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETAPR,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanDraftGetAprExecute',
                          true);

    return $request;
  }



  public function getCompanyInfo() {
    $params = array('companyId' => variable_get('pdl2_core_site_interest_id',null));

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::GET_COMPANY_INFO, $params,
            $this->currUser->token, $this->debugMode, 'CompanyGetByIdExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getCompanyChatInfo($email) {
    $params = array('emailAddress' => $email);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::GET_COMPANY_CHAT_INFO, $params,
            $this->currUser->token, $this->debugMode, 'CompanyGetChatExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getCompanyInfoAsync() {
    $params = array('companyId' => variable_get('pdl2_core_site_interest_id',null));

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::GET_COMPANY_INFO,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'CompanyGetByIdExecute',
                          true);

    return $request;
  }

  public function saveLoan($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_SAVE, $params,
            $this->currUser->token, $this->debugMode, 'CustomerLoanSaveExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getFinanceCharge($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETFINANCECHARGE, $params,
            $this->currUser->token, $this->debugMode, 'LoanDraftGetFinanceChargeExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getFinanceChargeAsync($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETFINANCECHARGE,
                            $params,
                            $this->currUser->token,
                            $this->debugMode,
                            'LoanDraftGetFinanceChargeExecute',
                            true);

    return $request;
  }

  public function getLender($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETLENDER, $params,
            $this->currUser->token, $this->debugMode, 'LoanDraftGetLenderExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function setEft($draftLoanId,$agrees) {
    $params = array('draftLoanId' => $draftLoanId, 'agrees' => $agrees);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_AGREEMENT_SETEFT, $params,
            $this->currUser->token, $this->debugMode, 'LoanDraftAgreementSetEftExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function setClarity($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_AGREEMENT_SETCLARITY, $params,
            $this->currUser->token, $this->debugMode, 'LoanDraftAgreementSetClarityExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function getClarity($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_AGREEMENT_GETCLARITY, $params,
            $this->currUser->token, $this->debugMode, 'LoanDraftAgreementGetClarityExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function setCoupon($draftLoanId, $coupon, $loan_type='payday') {
    $params = array('draftLoanId' => $draftLoanId,
                    'code' => $coupon);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_DRAFT_SETCOUPON;
      $service_execute = 'LoanPersonalDraftSetCouponExecute';
    }
    else {
      $service_url = PDL2Services::LOAN_DRAFT_SETCOUPON;
      $service_execute = 'LoanDraftSetCouponExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                  $params,
                                                  $this->currUser->token,
                                                  $this->debugMode,
                                                  $service_execute);

    return $res;
  }

  public function setPrincipal($draftLoanId, $principal, $loan_type='payday') {
    $params = array('draftLoanId' => $draftLoanId,
                    'amount' => $principal);
    
    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_DRAFT_SETPRINCIPAL;
      $service_execute = 'LoanPersonalDraftSetPrincipalExecute';
    }
    else {
      $service_url = PDL2Services::LOAN_DRAFT_SETPRINCIPAL;
      $service_execute = 'LoanDraftSetPrincipalExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                  $params,
                                                  $this->currUser->token,
                                                  $this->debugMode,
                                                  $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function setPrincipalAsync($draftLoanId, $principal) {
    $params = array('draftLoanId' => $draftLoanId,
                    'amount' => $principal);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_SETPRINCIPAL,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanDraftSetPrincipalExecute',
                          true);

    return $request;
  }

  public function getPrincipal($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETPRINCIPAL, $params,
            $this->currUser->token, $this->debugMode, 'LoanDraftGetPrincipalExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function getPrincipalAsync($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);
    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETPRINCIPAL,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanDraftGetPrincipalExecute',
                          true);

    return $request;
  }

  public function setPayment($loanId, $principal) {
    $params = array('loanId' => $loanId,
                    'amount' => $principal);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_DRAFT_SETPAYMENT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'LoanExtensionDraftSetPaymentExecute');

    return $res;
  }

  public function setPaymentMethod($draftLoanId, $method) {
    $params = array('draftLoanId' => $draftLoanId,
                    'method' => $method);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_SETPAYMENTMETHOD, $params,
            $this->currUser->token, $this->debugMode, 'LoanDraftSetPaymentMethodExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function setPaymentMethodAsync($draftLoanId, $method) {
    $params = array('draftLoanId' => $draftLoanId,
                    'method' => $method);
    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_SETPAYMENTMETHOD,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanDraftSetPaymentMethodExecute',
                          true);

    return $request;
  }

  /*
   * What is the maxiumum amount of funds that moneygram can send this customer.
   */
  public function getMoneyGramMaximum() {
    $params = array('clientId' => $this->currUser->userId);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETMONEYGRAMMAX,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetMoneyGramMaximumExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function getMoneyGramMaximumAsync() {
    $params = array('clientId' => $this->currUser->userId);
    $request = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETMONEYGRAMMAX,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetMoneyGramMaximumExecute',
                                                 true);

    return $request;
  }

  public function setLoanDueDate($draftLoanId, $dueDate, $loan_type='payday') {
    $params = array('draftLoanId' => $draftLoanId,
                    'date' => $dueDate);
    
    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_DRAFT_SETDUEDATE;
      $service_execute = 'LoanPersonalDraftSetDueDateExecute';
    }
    else {
      $service_url = PDL2Services::LOAN_DRAFT_SETDUEDATE;
      $service_execute = 'LoanDraftSetDueDateExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                  $params,
                                                  $this->currUser->token,
                                                  $this->debugMode,
                                                  $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function setLoanDueDateAsync($draftLoanId, $dueDate) {
    $params = array('draftLoanId' => $draftLoanId,
                    'date' => $dueDate);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_SETDUEDATE,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanDraftSetDueDateExecute',
                          true);

    return $request;
  }

  public function getLoanDraftDueDate($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETDUEDATE, $params,
            $this->currUser->token, $this->debugMode, 'LoanDraftGetDueDateExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function getLoanDraftDueDateAsync($draftLoanId) {
    $params = array('draftLoanId' => $draftLoanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_DRAFT_GETDUEDATE,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanDraftGetDueDateExecute',
                          true);

    return $request;
  }


  /**
   * Get code this client users to give to hois referrals so he gets credit.
   * returns: referral code (eg 'ABCDEF')
   */
  public function getReferralCode() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETREFERRALCODE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetReferralCodeExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }


  public function getReferralCodeAsync() {
    $params = array('clientId' => $this->currUser->userId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETREFERRALCODE,
                                                     $params,
                                                     $this->currUser->token,
                                                     $this->debugMode,
                                                     'CustomerGetReferralCodeExecute',
                                                     true);
    return $request;
  }

  public function getCoupons() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETCOUPONS, $params,
            $this->currUser->token, $this->debugMode, 'CustomerGetCouponsExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function getCouponsAsync() {
    $params = array('clientId' => $this->currUser->userId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETCOUPONS,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'CustomerGetCouponsExecute',
                          true);

    return $request;
  }

  public function getDueDates() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETDUEDATES, $params,
            $this->currUser->token, $this->debugMode, 'CustomerLoanGetDueDatesExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }

  public function getDueDatesAsync() {
    $params = array('clientId' => $this->currUser->userId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETDUEDATES,
                            $params,
                            $this->currUser->token,
                            $this->debugMode,
                            'CustomerLoanGetDueDatesExecute',
                            true);

    return $request;
  }
  
  public function getLastDeniedLoan() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETLASTDENIED,
                            $params,
                            $this->currUser->token,
                            $this->debugMode,
                            'CustomerLoanGetLastDeniedExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    return $res->result;
  }
  
  public function customerLoanPaydayIsEligible() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_LOAN_PAYDAY_ISELIGIGLE,
                            $params,
                            $this->currUser->token,
                            $this->debugMode,
                            'CustomerLoanPaydayIsEligibleExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == 0) {
      return null;
    }

    if ($status->code == self::NON_BUSINESS_STATE) {
      return self::NON_BUSINESS_STATE;
    }

    return $res->result;
  }

  public function sendResetPassword($email) {
    $params = array('email' => $email);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SENDRESETPASSWORD,
                                                 $params,
                                                 NULL,
                                                 $this->debugMode,
                                                 'CustomerSendResetPasswordExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result;
  }

  public function singleclickGetDuration() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SINGLECLICK_GETDURATION,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSingleclickGetDurationExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    // shouldnt we be checking $status here ?
    return $res;
  }

  public function singleclickGetAmount() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SINGLECLICK_GETAMOUNT, $params,
            $this->currUser->token, $this->debugMode, 'CustomerSingleclickGetAmountExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    return $res;
  }

  /*
   *This doesn't exist anymore in PDL2.
   *maybe a problem ?
  public function doPrequalCheck() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_DOPREQUALIFICATIONCHECK,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerDoPrequalificationCheckExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }
*/
  public function getPrequalification() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETPREQUALIFICATION, $params,
      $this->currUser->token, $this->debugMode, 'CustomerGetPrequalificationExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function isAutoApproved() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_ISAUTOAPPROVED, $params,
      $this->currUser->token, $this->debugMode, 'CustomerIsAutoApprovedExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function isAutoApprovedAsync() {
    $params = array('clientId' => $this->currUser->userId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_ISAUTOAPPROVED,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'CustomerIsAutoApprovedExecute',
                          true);

    return $request;
  }

  public function createLoanDraft() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_CREATE, $params,
      $this->currUser->token, $this->debugMode, 'CustomerLoanCreateExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getExtensionFinanceCharge($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_DRAFT_GETFINANCECHARGE, $params,
            $this->currUser->token, $this->debugMode, 'LoanExtensionDraftGetFinanceChargeExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getExtensionFinanceChargeAsync($loanId) {
    $params = array('loanId' => $loanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_DRAFT_GETFINANCECHARGE,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanExtensionDraftGetFinanceChargeExecute',
                          true);

    return $request;
  }

  public function getFinanceCharges($loanId) {
    $params = array('loanId' => $loanId);


    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETFINANCECHARGES, $params,
            $this->currUser->token, $this->debugMode, 'LoanGetFinanceChargesExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getFinanceChargesAsync($loanId) {
    $params = array('loanId' => $loanId);


    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETFINANCECHARGES,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanGetFinanceChargesExecute',
                          true);

    return $request;
  }

  public function getExtensionConfirmation($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_GETCONFIRMATIONCODE, $params,
            $this->currUser->token, $this->debugMode, 'LoanExtensionGetConfirmationCodeExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getDraftExtensionAgreement($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_DRAFT_GETAGREEMENT, $params,
            $this->currUser->token, $this->debugMode, 'LoanExtensionDraftGetAgreementExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getExtensionAgreementAsync($loanId) {
    $params = array('loanId' => $loanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_DRAFT_GETAGREEMENT,
                            $params,
                            $this->currUser->token,
                            $this->debugMode,
                            'LoanExtensionDraftGetAgreementExecute',
                            true);

    return $request;
  }

  public function getExtensionApr($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_DRAFT_GETAPR, $params,
            $this->currUser->token, $this->debugMode, 'LoanExtensionDraftGetAprExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getExtensionAprAsync($loanId) {
    $params = array('loanId' => $loanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_DRAFT_GETAPR,
                          $params,
                          $this->currUser->token,
                          $this->debugMode,
                          'LoanExtensionDraftGetAprExecute',
                          true);

    return $request;
  }

  public function extensionSave($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_SAVE, $params,
            $this->currUser->token, $this->debugMode, 'LoanExtensionSaveExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getMinimumPrincipalDue($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETMINIMUMPRINCIPALDUE, $params,
            $this->currUser->token, $this->debugMode, 'LoanGetMinimumPrincipalDueExecute');


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getMinimumPrincipalDueAsync($loanId) {
    $params = array('loanId' => $loanId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_GETMINIMUMPRINCIPALDUE,
                            $params,
                            $this->currUser->token,
                            $this->debugMode,
                            'LoanGetMinimumPrincipalDueExecute',
                            true);

    return $request;
  }

  public function getCurrentPaymentTotal($loanId) {
    $params = array('loanId' => $loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::LOAN_EXTENSION_DRAFT_GETCURRENTPAYMENTTOTAL, $params,
            $this->currUser->token, $this->debugMode, 'LoanExtensionDraftGetCurrentPaymentTotalExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getUserInfo() {
    return $this->getUserInfoByClientId($this->currUser->userId);
  }

  public function getUserInfoByClientId() {
    $params = array('clientId' => $this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::GET_USER_BY_ID,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetByIdExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);


    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result;
  }

  public function getUserInfoAsync() {
    $params = array('clientId' => $this->currUser->userId);

    $request = $this->_webServiceHelper->soapRequest(PDL2Services::GET_USER_BY_ID,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetByIdExecute',
                                                 true);

    return $request;
  }


  public function getLoanById($loanId, $loan_type='payday') {
    $params = array("loanId"=>$loanId);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_GETBYID;
      $service_execute = 'LoanPersonalGetByIdExecute';
    }
    else {
      $service_url = PDL2Services::LOAN_GETBYID;
      $service_execute = 'LoanGetByIdExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                  $params,
                                                  $this->currUser->token,
                                                  $this->debugMode,
                                                  $service_execute);

    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    if ($res->status == 1 && !empty($res->result)) {
      return $res->result;
    }

    return null;
  }

  public function validateApiToken() {

    $params = array('userToken'  => $this->currUser->token);

    $response = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_ISTOKENVALID,
                                                      $params,
                                                      $this->currUser->token,
                                                      $this->debugMode,
                                                      'CustomerIsTokenValidExecute');

    $result = $response->status && $response->result;

    if ($result) {
      $this->establishUserCookies();
    }
    else {
      unset($this->currUser);
      $this->clearUserCookies();
    }

    return $result;
  }

  /*
   * Get the users (password recovery) hash
   */
  public function getChangePasswordHash() {
    $params = array("clientId"=>$this->currUser->userId);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_CHANGEPASSWORD_HASH ,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetChangePasswordHashFromIdExecute');
    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    return $res;
  }


  /*
   * Change the password
   */
  public function changePassword($newpassword, $draftLoanId) {

     return $this->changeUserPassword($this->currUser->userId,
                                      $newpassword,
                                      $draftLoanId->result);
  }


  public function changeUserPassword($clientId, $newpassword, $hash) {
    $params = array("clientId"=>$clientId,
                    "password"=>$newpassword,
                    "hash"=>$hash);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_CHANGEPASSWORD,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerChangePasswordExecute');

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res;
  }


  public function getLoanAgreement($loanId, $loan_type='payday') {
    $params = array("loanId"=>$loanId);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_GETAGREEMENT;
      $service_execute = 'LoanPersonalGetAgreementExecute';
    }
    else {
      $service_url = PDL2Services::ACCOUNT_GET_LOAN_AGREEMENT;
      $service_execute = 'LoanGetAgreementExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 $service_execute);


    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }


  public function getLoanHistoryById($loanId, $loan_type='payday') {
    $params = array("loanId"=>$loanId);

      if($loan_type == 'personal'){
          $service_url = PDL2Services::LOAN_PERSONAL_GETHISTORY;
          $service_execute = 'LoanPersonalGetHistoryExecute';
      } else {
          $service_url = PDL2Services::GET_LOAN_HISTORY_BY_ID;
          $service_execute = 'LoanGetHistoryExecute';
      }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    // more PDL2 stupidness.
    // yet again a single result returns the object instead of an
    // array of size 1 containing the object.
    // Munge it into an array.
    if(!is_array($res->result->loanHistory->statuses->result)) {
      $store = $res->result->loanHistory->statuses->result;
      $res->result->loanHistory->statuses->result = array();
      $res->result->loanHistory->statuses->result[0] = $store;
    }

    return $res->result;
  }


  public function getLoanTransactionsById($loanId, $loan_type='payday', $transactionType = '') {
    $params = array("loanId"=>$loanId, 'transactionType' => $transactionType);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_GETTRANSACTIONS;
      $service_execute = 'LoanPersonalGetTransactionsExecute';
    }
    else {
      $service_url = PDL2Services::GET_LOAN_TRANSACTIONS_BY_ID;
      $service_execute = 'LoanGetTransactionsExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }


  public function getLoanAgreementById($agreementid) {
    $params = array('agreementId'=>$agreementid);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::ACCOUNT_GET_LOAN_AGREEMENT_BY_ID,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'LoanGetAgreementByIdExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getLoanAgreements($loanId, $loan_type='payday') {
    $params = array("loanId"=>$loanId);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::LOAN_PERSONAL_GETAGREEMENTS;
      $service_execute = 'LoanPersonalGetAgreementsExecute';
    }
    else {
      $service_url = PDL2Services::ACCOUNT_GET_LOAN_AGREEMENTS;
      $service_execute = 'LoanGetAgreementsExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    // PDL2 is stupid. If there is a single agreement it returns an object,
    // if there is more than one it returns an array of those objects.
    // Make sure it always returns an array.
    if (is_array($res->result->result)) {
      return $res->result->result;
    }
    else {
      return array($res->result);
    }
  }


  /** OLD SERVICES **/
  public function saveNewClient($params) {
    $this->debugMode = true;
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::SAVE_CLIENT, $params, $this->currUser->token, $this->debugMode);

    die;
  }



  /*
   * Is the user allowed to edit his details.
   * Returns true/false
   */
  public function checkUserEditRights() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::ACCOUNT_ALLOW_EDIT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerAllowAccountEditExecute');
    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    // Note: data is a boolean value in this scenario.
    return $res->result;
  }


  /**
   * Get the users bank details
   */
  public function getBankAccount() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_BANK_ACCOUNT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetBankAccountExecute');
    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    // Note: data is a boolean value in this scenario.
    return $res->result;
  }


  /**
   * Set the users bank details
   */
  public function setBankAccount($routing_number, $account_number) {
    $params = array("clientId"=>$this->currUser->userId,
                    "accountNumber" => $account_number,
                    "routingNumber" => $routing_number);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SET_BANK_ACCOUNT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSetBankAccountExecute');

    if ($res->status != 1) {
      throw new Exception($res->message->detail, $res->message->code);
    }
    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    // Note: data is a boolean value in this scenario.
    return $res->result;
  }


  public function getPrivacyPreferences() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::ACCOUNT_GET_PRIVACY_PREFERENCES,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerPrivacyGetPrivacyPreferencesExecute');
    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    $data = $res->result;

    $birthdayOffers = $data->birthday_emails;
    $holidayOffers = $data->holiday_emails;
    $specialOffers = $data->special_emails;
    $affiliateOffers = $data->affiliates;
    $thirdPartyOffers = $data->opt_out_third_party;
    // Not implemented properly in JSON response, currently is just set to null.
    $smsOffers = $data->opt_out_sms;

    $privacyPrefs = new PrivacyPreferencesModel($status, $birthdayOffers, $holidayOffers, $specialOffers, $affiliateOffers, $thirdPartyOffers, $smsOffers);

    return $privacyPrefs;
  }


  public function setPrivacyPreferences(PrivacyPreferencesModel $privacyPrefs) {

    $params = array("clientId"=>$this->currUser->userId);

    if (isset($privacyPrefs->birthdayOptOut))
      $params["optOutBirthday"] = $privacyPrefs->birthdayOptOut;

    if (isset($privacyPrefs->holidayOptOut))
      $params["optOutHoliday"] = $privacyPrefs->holidayOptOut;

    if (isset($privacyPrefs->specialOptOut))
      $params["optOutSpecials"] = $privacyPrefs->specialOptOut;

    if (isset($privacyPrefs->affiliateOptOut))
      $params["optOutAffiliates"] = $privacyPrefs->affiliateOptOut;

    if (isset($privacyPrefs->thirdPartyOptOut))
      $params["optOutThirdParty"] = $privacyPrefs->thirdPartyOptOut;

    if (isset($privacyPrefs->smsOptOut))
      $params["optOutSms"] = $privacyPrefs->smsOptOut;
      
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::ACCOUNT_SET_PRIVACY_PREFERENCES,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerPrivacySetPrivacyPreferencesExecute');

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    return $res->status;
  }


  public function setOptOutSms($isTrue) {
    $params = array("clientId"=>$this->currUser->userId,
                    'isTrue' => $isTrue);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::ACCOUNT_SET_OPT_OUT_SMS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerPrivacySetOptOutSmsExecute');

    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    return $res->result;

  }


  public function customerPrivacySetOptOutThirdParty($isTrue) {
    $params = array("clientId"=>$this->currUser->userId,
                    'isTrue' => $isTrue);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::ACCOUNT_SET_OPT_OUT_THIRDPARTY,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerPrivacySetOptOutThirdPartyExecute');

    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    return $res->result;

  }


  public function customerPrivacyGetOptOutThirdParty() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::ACCOUNT_GET_OPT_OUT_THIRDPARTY,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerPrivacyGetOptOutThirdPartyExecute');

    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    return $res->result;

  }


  /*
   * Perm opt out of correspondance.
   * Can opt back in by sending 'false' as an argument.
   */
  public function privacyOptOutAll($optout = false) {
    $params = array("clientId"=>$this->currUser->userId,
                    "isTrue"=>$optout);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::ACCOUNT_OPT_OUT_ALL,
                                                $params,
                                                $this->currUser->token,
                                                $this->debugMode,
                                                'CustomerPrivacySetOptOutAllExecute');

    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    // set opted out in cookie.
    $this->currUser->optOut = $optout;
    $this->establishUserCookies();

    return $res->status;
  }


  /*
   * Get a users preferred status.
   */
  public function getPreferredMemberStatus() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GETPREFERREDMEMBERSTATUS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetPreferredMemberStatusExecute');

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $res->result;
  }


  public function doExtension($wireCompany) {
    $params = array("identity"=>$this->_identity,
                    "clientId"=>$this->currUser->userId,
                    "wireCompany"=>$wireCompany);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::DO_EXTENSION, $params, $this->currUser->token, $this->debugMode);

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res;
  }

  public function getCustomerLoanHistory($loan_type='payday') {
    $params = array("clientId"=>$this->currUser->userId);

    if($loan_type == 'personal'){
      $service_url = PDL2Services::CUSTOMER_LOAN_PERSONAL_GETHISTORY;
      $service_execute = 'CustomerLoanPersonalGetHistoryExecute';
    }
    else {
      $service_url = PDL2Services::ACCOUNT_GET_LOAN_HISTORY;
      $service_execute = 'CustomerLoanGetHistoryExecute';
    }

    $res = $this->_webServiceHelper->soapRequest($service_url,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 $service_execute);

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    if (is_array($res->result)) {
      return $res->result;
    }
    elseif ($res->result == null) {
      return $res->result;
    }
    else {
      $res->result = array($res->result->result);
      return $res;
    }
  }

  public function getReferralsList() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_REFERRALS_LIST,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetReferralsListExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    // more PDL2 stupidness.
    // yet again a single result returns the object instead of an
    // array of size 1 containing the object.
    // Munge it into an array.
    if(!is_array($res->result->result) && !empty($res->result)) {
      $store = $res->result->result;
      $res->result->result = array();
      $res->result->result[0] = $store;
    }
    
    return $res->result->result;
  }

  /**
   * Send a referral email to the recipient.
   */
  public function sendReferralEmail($emailAddress, $firstName, $lastName, $phone='') {
    $params = array("clientId"=>$this->currUser->userId,
                    "emailAddress"=>$emailAddress,
                    "firstName"=>$firstName,
                    "lastName"=>$lastName,
                    "phone"=>$phone);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SEND_REFERRAL_EMAIL,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSendReferralEmailExecute');

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res;
  }


  /*
   * How much has the user recieved in referrals.
   */
  public function getReferralAmountReceived() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_REFERRAL_AMMOUNT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetReferralAmountReceivedExecute');

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    return $res->result;
  }


  /*
   * How much has the user recieved in referrals.
   */
  public function getReferralBalance() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_REFERRAL_BALANCE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetReferralBalanceExecute');

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res->result;
  }


  /**
   * Apply referral to loan balance.
   */
  public function setReferralDepositToAccount() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SET_REFERRAL_DEPOSIT_TO_ACCOUNT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSetReferralDepositToAccountExecute');

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res->status;
  }

  /**
   * Deposit referral balance to account.
   */
  public function setReferralApplyToLoanBalance() {
    $params = array("clientId"=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SET_REFERRAL_APPLY_TO_LOAN_BALANCE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSetReferralApplyToLoanBalanceExecute');

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res->status;
  }


  public function checkForExistingSsn($interestId, $ssn) {
    $params = array("identity"=>$this->_identity,
                    "interestId"=>$interestId,
                    "ssn"=>$ssn);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CHECK_FOR_EXISTING_SSN,
      $params, $this->currUser->token, $this->debugMode);

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res;
  }

  public function checkForExistingEmail($interestId, $email) {
    $params = array("identity"=>$this->_identity,
                    "interestId"=>$interestId,
                    "email"=>$email);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CHECK_FOR_EXISTING_EMAIL,
      $params, $this->currUser->token, $this->debugMode);

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res;
  }

  public function getIncomeSources() {
    $params = array();

    $this->debugMode = true;
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::GET_INCOME_SOURCES, $params, $this->currUser->token, $this->debugMode);

    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    $incomeSources = array();

    foreach ($res->result as $incomeSource) {
      $source = new IncomeSourceModel($incomeSource->id, $incomeSource->name, $incomeSource->display, $incomeSource->sortOrder);
      array_push($incomeSources, $source);
    }
    return $incomeSources;
  }



  public function getPayFrequencyTypes() {
    $params = array();

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::GET_PAY_FREQUENCY_TYPES, $params, $this->currUser->token, $this->debugMode);

    if (!isset($res)) {
      throw new Exception(PDL2Controller::GENERIC_ERROR);
    }

    $payFrequencies = array();

    foreach ($res->result as $frequency) {
      $frequency = new PayFrequencyModel($frequency->id, $frequency->paydayTable, $frequency->frequency, $frequency->description, $frequency->method, $frequency->realMethod);
      array_push($payFrequencies, $frequency);
    }
    return $payFrequencies;
  }

  private function establishUserCookies() {
    $this->_cookieHelper->setBrowserSessionCookie(PDL2Controller::TOKEN_COOKIE_NAME, $this->currUser->token);
    $this->_cookieHelper->setBrowserSessionCookie(PDL2Controller::USER_ID_COOKIE_NAME, $this->currUser->userId);
    $this->_cookieHelper->setBrowserSessionCookie(PDL2Controller::SESSION_ID_COOKIE_NAME, $this->currUser->sessionId);
    $this->_cookieHelper->setBrowserSessionCookie(PDL2Controller::USER_OPT_OUT, $this->currUser->optOut);
    
    // Set temp cookie so we kick people out if they close/reopen their browser. Use hardcoded value just to confuse people. This number is unused :)
    $this->_cookieHelper->setBrowserSessionCookie(PDL2Controller::BROWSER_SESSION_COOKIE_NAME, "187548");
  }

  private function getUserFromCookies() {
    $token = $this->_cookieHelper->getCookie(PDL2Controller::TOKEN_COOKIE_NAME);
    $userId = intval($this->_cookieHelper->getCookie(PDL2Controller::USER_ID_COOKIE_NAME));
    $sessionId = $this->_cookieHelper->getCookie(PDL2Controller::SESSION_ID_COOKIE_NAME);
    $optOut = $this->_cookieHelper->getCookie(PDL2Controller::USER_OPT_OUT);

    // Create a "fake" status model since we are retrieving from a cookie.
    $status = StatusModel::Create();
    $user = new UserModel($status, $userId, $token, $sessionId, $optOut);
    return $user;
  }

  private function hasUserCookies() {
    $tokenCookie = $this->_cookieHelper->hasCookie(PDL2Controller::TOKEN_COOKIE_NAME);
    $userIdCookie = $this->_cookieHelper->hasCookie(PDL2Controller::USER_ID_COOKIE_NAME);
    $sessionIdCookie = $this->_cookieHelper->hasCookie(PDL2Controller::SESSION_ID_COOKIE_NAME);
    return ($tokenCookie && $userIdCookie && $sessionIdCookie);
  }

  private function clearUserCookies() {
    $this->_cookieHelper->clearCookie(PDL2Controller::TOKEN_COOKIE_NAME);
    $this->_cookieHelper->clearCookie(PDL2Controller::USER_ID_COOKIE_NAME);
    $this->_cookieHelper->clearCookie(PDL2Controller::SESSION_ID_COOKIE_NAME);
    $this->_cookieHelper->clearCookie(PDL2Controller::USER_OPT_OUT);
  }

  private function echoObject($obj) {
    $text = print_r($obj, true);
    echo "<pre>{$text}</pre>";
  }


/*
  setReftrack($refName)            Creates a ref_track record with a unique ID
  getReftrackId($refTrackCode)        Returns refname ID based on refname
  getReftrackName($refId)            Returns refname based on refname ID
  setReftrackKeyword($keyword)        Log entry to the site via a keyword
  logReftrackNewClientAction($refId)      Log create client via rtID & clientID
  logReftrackModifyClient($refId)        Log update client via rtID & clientID
  logReftrackNewLoanAction($refId, $loanId)  Log create loan via rtID & loanID
  logReftrackClickAction($refId)        Log a generic action via reftrack ID
*/
/*
  public function setReftrack($refName) {
    $params = array('refName'=>$refName);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_REFTRACK_CREATE,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingReftrackCreateExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result;
  }

  public function getReftrackId($refTrackCode) {
    $params = array('refTrackCode'=>$refTrackCode);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_REFTRACK_GETREFID,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingReftrackGetRefIdExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function getReftrackName($refId) {
    $params = array('refId'=>$refId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_REFTRACK_GETREFNAME,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingReftrackGetRefNameExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }
  

  

  public function logReftrackNewClientAction($refId) {
    $params = array('refId'=>$refId,
            'clientId'=>$this->currUser->userId,
            'ipAddress'=>$_SERVER['REMOTE_ADDR']);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_REFTRACK_RECORDNEWCLIENTACTION,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingReftrackRecordNewClientActionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function logReftrackModifyClient($refId) {
    $params = array('refId'=>$refId,
            'clientId'=>$this->currUser->userId,
            'ipAddress'=>$_SERVER['REMOTE_ADDR']);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_REFTRACK_RECORDMODIFYCLIENTACTION,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingReftrackRecordModifyClientActionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function logReftrackNewLoanAction($refId, $loanId) {
    $params = array('refId'=>$refId,
            'loanId'=>$loanId,
            'ipAddress'=>$_SERVER['REMOTE_ADDR']);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_REFTRACK_RECORDNEWLOANACTION,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingReftrackRecordNewLoanActionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function logReftrackClickAction($refId) {
    $params = array('refId'=>$refId,
            'ipAddress'=>$_SERVER['REMOTE_ADDR']);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_REFTRACK_RECORDCLICKACTION,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingReftrackRecordClickActionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }


  public function affiliatePartnersLinkshareGetSuperRefCode($affCode) {
    $params = array('affCode'=>$affCode);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_LS_GETSUPERREFCODE,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersLinkshareGetSuperRefCodeExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function affiliatePartnersLinkshareCreateTransaction($loanId, $siteId, $timeEntered) {
    $params = array('loanId'=>$loanId,
                    'siteId'=>$siteId,
                    'timeEntered'=>$timeEntered);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_LS_CREATE_TRANSACTION,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersLinkshareCreateTransactionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }


  public function affiliatePartnersCommissionjunctionCreateTransaction($loanId, $adId, $publisherId, $shopperId) {
    $params = array('loanId'=>$loanId,
                    'adId'=>$adId,
                    'publisherId'=>$publisherId,
                    'shopperId'=>$shopperId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_CJ_CREATE_TRANSACTION,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersCommissionjunctionCreateTransactionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function affiliatePartnersMypointsCreateTransaction($loanId, $visitId) {
    $params = array('loanId'=>$loanId,
                    'visitId'=>$visitId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_MP_CREATE_TRANSACTION,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersMypointsCreateTransactionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }
*/

  public function getReftrackChannelFromName($refName) {
    $params = array('name'=>$refName);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_TRACKING_REFTRACK_GETCHANNELNAME,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingReftrackGetChannelNameExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result;
  }


/*
  const AFFILIATE_TRACKING_SESSION_INITIALIZE = 'affiliate/tracking/session/initialize';
  const AFFILIATE_TRACKING_SESSION_ASSOCIATEUSERTOKEN = 'affiliate/tracking/session/associateUserToken';
  
  const AFFILIATE_PARTNERS_CJ_ASSOCIATEUSERTOKEN = 'affiliate/partners/commissionjunction/associateUserToken';
  const AFFILIATE_PARTNERS_LINKSHARE_ASSOCIATEUSERTOKEN = 'affiliate/partners/linkshare/associateUserToken';
  const AFFILIATE_PARTNERS_MYPOINTS_ASSOCIATEUSERTOKEN = 'affiliate/partners/mypoints/associateUserToken';
*/

  public function affiliateLeadsellGetStatus() {
    //return array('status' => 'rejected');
    $params = array('clientId'=>$this->currUser->userId);
    
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_LEADSELL_GETSTATUS,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateLeadsellGetStatusExecute');
    
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    
    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    
    return $res->result;
  }

  public function affiliateTrackingSessionInitialize($refName, $keyword="") {
    $params = array('referralCampaignShortName'=>$refName,
                    'ipAddress'=>$_SERVER['REMOTE_ADDR']);

    if ($keyword != "")
      $params["keyword"] = $keyword;

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_TRACKING_SESSION_INITIALIZE,
                        $params,
                        NULL,
                        $this->debugMode,
                        'AffiliateTrackingSessionInitializeExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    
    $this->_cookieHelper->setAffiliateCookie("refSessionId", $res->result);
    watchdog("pdl2_aff_rt", "Incoming ref:%name refsessionid:%refsessionid", array("%name"=>$refcode, "%refsessionid"=>$refSessionId), WATCHDOG_NOTICE);

    return $res->result;
  }

  public function affiliateTrackingSessionGetCampaign($referralSessionId) {
    $params = array('referralSessionId'=>$referralSessionId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_TRACKING_SESSION_GETCAMPAIGN,
                        $params,
                        NULL,
                        $this->debugMode,
                        'AffiliateTrackingSessionGetCampaignExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function affiliateTrackingPartnersSessionInitialize($affiliateCode, $keyword="") {
    $params = array('affiliateCode'=>$affiliateCode,
                    'ipAddress'=>$_SERVER['REMOTE_ADDR']);

    if ($keyword != "")
      $params["keyword"] = $keyword;

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_TRACKING_PARTNERS_SESSION_INITIALIZE,
                        $params,
                        NULL,
                        $this->debugMode,
                        'AffiliateTrackingPartnersSessionInitializeExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    
    $this->_cookieHelper->setAffiliateCookie("refSessionId", $res->result);
    watchdog("pdl2_aff_rt", "Incoming ref:%name refsessionid:%refsessionid", array("%name"=>$refcode, "%refsessionid"=>$refSessionId), WATCHDOG_NOTICE);

    return $res->result;
  }

  public function affiliateTrackingSessionAssociateUserToken() {
    watchdog("pdl2_aff_rt", "assoc user token refSessionId:%sessionid token:%token", array("%sessionid" => $this->_cookieHelper->getCookie("refSessionId"), "%token"=>$this->currUser->token), WATCHDOG_NOTICE);
    
    if (!$this->_cookieHelper->hasCookie("refSessionId"))
      return NULL;

    $params = array('referralSessionId'=>$this->_cookieHelper->getCookie("refSessionId"),
                    'userToken'=>$this->currUser->token);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_TRACKING_SESSION_ASSOCIATEUSERTOKEN,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingSessionAssociateUserTokenExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      watchdog("pdl2_aff_rt", "No response body", array(), WATCHDOG_NOTICE);
      return NULL;
    }
    
    watchdog("pdl2_aff_rt", "assoc user token response:%result", array("%result" => $res->result), WATCHDOG_NOTICE);

    return $res->result;
  }

  public function affiliatePartnersCommissionJunctionAssociateUserToken($aid, $pid, $sid) {

    $params = array('adId'=>$aid,
                    'publisherId'=>$pid,
                    'userToken'=>$this->currUser->token);

    if ($sid != "")
      $params["shopperId"] = $sid;
      
    if ($this->_cookieHelper->hasCookie("refSessionId"))
      $params['referralSessionId'] = $this->_cookieHelper->getCookie("refSessionId");
 

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_CJ_ASSOCIATEUSERTOKEN,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersCommissionjunctionAssociateUserTokenExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    watchdog("pdl2_aff_cj", "assoc user token response:%result", array("%result" => $res->result), WATCHDOG_NOTICE);

    return $res->result;
  }

  public function affiliatePartnersLinkshareAssociateUserToken($linkshareSiteId, $timeEntered) {
    
    // modify pdl1 timeEntered formats to pdl2 format
    $timeEntered = str_replace("/"," ",$timeEntered);
    
    $params = array('linkshareSiteId' => $linkshareSiteId,
                    'timeEntered' => $timeEntered,
                    'userToken'=>$this->currUser->token);

    if ($this->_cookieHelper->hasCookie("refSessionId"))
      $params['referralSessionId'] = $this->_cookieHelper->getCookie("refSessionId");

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_LINKSHARE_ASSOCIATEUSERTOKEN,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersLinkshareAssociateUserTokenExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function affiliatePartnersMypointsAssociateUserToken($visitid) {

    $params = array('visitId'=>$visitid,
                    'userToken'=>$this->currUser->token);

    if ($this->_cookieHelper->hasCookie("refSessionId"))
      $params['referralSessionId'] = $this->_cookieHelper->getCookie("refSessionId");

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_MYPOINTS_ASSOCIATEUSERTOKEN,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersMypointsAssociateUserTokenExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }






  public function affiliatePartnersEfrontierCreateClientTransaction($efId) {
    $params = array('efId'=>$efId,
                    'clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_EF_CREATE_CLIENT_TRANSACTION,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersEfrontierCreateClientTransactionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function affiliatePartnersEfrontierCreateLoanTransaction($efId, $loanId) {
    $params = array('efId'=>$efId,
                    'loanId'=>$loanId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_EF_CREATE_LOAN_TRANSACTION,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersEfrontierCreateLoanTransactionExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }


  public function getReftrackRedirectPage($redirectId) {
    $params = array('redirectId'=>$redirectId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_REFTRACK_GETREDIRECTPAGE,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliateTrackingReftrackGetRedirectPageExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  public function affiliatePartnersMediamindGetRefNameChannelAndType() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_MEDIAMIND_GETCHANNELANDTYPE,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'AffiliatePartnersMediamindGetRefNameChannelAndTypeExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }

  // clientId provided via GET. DO NOT USE $this->currUser->userId
  public function affiliateTrackingLeadpileAuthenticate($clientId, $leadPileToken) {
    $params = array('apiKey'=>$this->_apiConfig->applicationToken,
                    'siteName'=>$this->_apiConfig->siteIdentifier,
                    'clientId' => $clientId,
                    'leadPileToken' => $leadPileToken);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_LP_AUTHENTICATE,
                        $params,
                        NULL,
                        $this->debugMode,
                        'AffiliateTrackingLeadpileAuthenticateExecute');
    if (isset($res)) {

      $success = $res->status;
      $status = new StatusModel($success,
                                $res->message->code,
                                $res->message->text,
                                $res->message->detail);

      if ($success == 1) {
        // Has the user opted out of all contact ?
        $opt_out = $this->customerPrivacyGetOptOutAll($res->result->token, $res->result->userId);

        $user = new UserModel($status,
                              $res->result->userId,
                              $res->result->token,
                              $res->result->sessionId,
                              $opt_out);
  
        //dd($user);
        $this->currUser = $user;
  
        $this->establishUserCookies();
        
        $this->affiliateTrackingSessionAssociateUserToken();
  
        return $user;
      }
      else {
        $opt_out = false;
        return false;
      }

    }
    else {
      return false;
    }
  }

  public function customerPingdupeGetPingDupeCheck($key, $ssn, $bankAccount, $bankRouting, $address1, $city, $zip, $email, $birthday, $phone1) {
    $params = array('key' => $key,
                    'ssn' => $ssn,
                    'bankAccount' => $bankAccount,
                    'bankRouting' => $bankRouting,
                    'address1' => $address1,
                    'city' => $city,
                    'state' => 'NY',
                    'zip' => $zip,
                    'email' => $email,
                    'birthday' => $birthday,
                    'phone1' => $phone1,
                );

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::AFFILIATE_PARTNERS_PINGDUPE_DOCHECK,
                        $params,
                        $this->currUser->token,
                        $this->debugMode,
                        'CustomerPingdupeGetPingDupeCheckExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }


  public function getDateOfBirth() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_DOB,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetDateOfBirthExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result;
  }


  public function getDriversLicense() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_DRIVERS_LICENSE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetDriversLicenseExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result->drivers_license;
  }


  public function getHomePhone() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_HOME_PHONE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetHomePhoneExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result;
  }


  public function getCellPhone() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_CELL_PHONE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetCellPhoneExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result;
  }


  public function getHomeFax() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_HOME_FAX,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetHomeFaxExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result;
  }


  public function getResidencyType() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_RESIDENCE_TYPE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetResidenceTypeExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result;
  }


  public function getEmploymentAddress() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_GET_ADDRESS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentGetAddressExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }
    return $res->result->address;
  }


  public function getIncomeSourceType() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_GET_INCOME_SOURCE_TYPE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentGetIncomeSourceTypeExecute');
    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res->result;
  }

  public function getEmployerName() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_GET_EMPLOYER_NAME,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentGetEmployerNameExecute');
    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res->result;
  }

  public function getEmploymentDepartment() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_GET_DEPARTMENT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentGetDepartmentExecute');
    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res->result;
  }


  public function getEmploymentStartDate() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_GET_START_DATE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentGetStartDateExecute');
    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res->result->date;
  }


  public function getEmploymentPhones() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_GET_PHONES,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentGetPhonesExecute');
    if (!isset($res)) {
        throw new Exception(PDL2Controller::GENERIC_ERROR);
    }
    return $res->result;
  }


  public function setEmploymentAddress($zip, $city, $street1, $street2) {
    $params = array('clientId'=>$this->currUser->userId,
                    'zipCode'=>$zip,
                    'city'=>$city,
                    'street'=>$street1,
                    'streetSecondary'=>$street2);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_SET_ADDRESS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentSetAddressExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function getHomeAddress() {
    $params = array('clientId'=>$this->currUser->userId);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_GET_HOME_ADDRESS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerGetHomeAddressExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $res->result;
  }


  public function setHomeAddress($zip, $city, $street1, $street2) {
    $params = array('clientId'=>$this->currUser->userId,
                    'zipCode'=>$zip,
                    'city'=>$city,
                    'street'=>$street1,
                    'streetSecondary'=>$street2);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SET_HOME_ADDRESS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSetHomeAddressExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setMailAddress($zip, $city, $street) {
    $params = array('clientId'=>$this->currUser->userId,
                    'zipCode'=>$zip,
                    'city'=>$city,
                    'street'=>$street);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SET_MAIL_ADDRESS,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSetMailAddressExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setDriversLicense($licenseNumber, $state) {
    $params = array('clientId'=>$this->currUser->userId,
                    'licenseNumber'=>$licenseNumber,
                    'state' => $state);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SET_DRIVERS_LICENSE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSetDriversLicenseExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setHomePhone($number, $isMobile) {
    $params = array('clientId'=>$this->currUser->userId,
                    'phoneNumber' => $number,
                    'isMobile' => $isMobile);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SET_HOME_PHONE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSetHomePhoneExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setCellPhone($number) {
    $params = array('clientId'=>$this->currUser->userId,
                    'phoneNumber' => $number);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SET_CELL_PHONE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSetCellPhoneExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setEmploymentPhones($mainNumber, $verificationNumber) {
    $params = array('clientId'=>$this->currUser->userId,
                    'mainNumber' => $mainNumber,
                    'verificationNumber' => $verificationNumber);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_SET_PHONES,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentSetPhonesExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setResidencyType($type) {
    $params = array('clientId'=>$this->currUser->userId,
                    'residenceType' => $type);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_SET_RESIDENCE_TYPE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerSetResidenceTypeExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setIncomeSourceType($incomeType) {
    $params = array('clientId'=>$this->currUser->userId,
                    'incomeSourceType' => $incomeType);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_SET_INCOME_SOURCE_TYPE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentSetIncomeSourceTypeExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setEmployerName($name) {
    $params = array('clientId'=>$this->currUser->userId,
                    'employerName' => $name);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_SET_EMPLOYER_NAME,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentSetEmployerNameExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setEmploymentDepartment($dept) {
    $params = array('clientId'=>$this->currUser->userId,
                    'department' => $dept);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_SET_DEPARTMENT,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentSetDepartmentExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }


  public function setEmploymentStartDate($startDate) {
    $params = array('clientId'=>$this->currUser->userId,
                    'startDate' => $startDate);
    $res = $this->_webServiceHelper->soapRequest(PDL2Services::CUSTOMER_EMPLOYMENT_SET_START_DATE,
                                                 $params,
                                                 $this->currUser->token,
                                                 $this->debugMode,
                                                 'CustomerEmploymentSetStartDateExecute');
    $status = PDL2Utils::GetStatusFromSoapResponse($res);
    return $status;
  }
  
  public function saveTestimonial($text) {
    $params = array('clientId'=>$this->currUser->userId,
                    'text'=>$text);

    $res = $this->_webServiceHelper->soapRequest(PDL2Services::SAVE_TESTIMONIAL, $params,
            $this->currUser->token, $this->debugMode, 'CustomerSaveTestimonialExecute');

    $status = PDL2Utils::GetStatusFromSoapResponse($res);

    if ($status->code == PDL2Controller::NO_RESPONSE_BODY) {
      return NULL;
    }

    return $res->result;
  }


}

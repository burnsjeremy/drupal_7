<?php

class PDL2Bootstrap
{
	public static function load()
	{
        
        $modulePath = drupal_get_path('module', 'pdl2_core');
        require_once "$modulePath/Dklab_SoapClient/Dklab_SoapClient.php";
        require_once "$modulePath/Zend/Loader/Autoloader.php";
        Zend_Loader_Autoloader::getInstance();
        
		$path = dirname(__FILE__);
		$di = new RecursiveDirectoryIterator($path);
		$iter = new RecursiveIteratorIterator($di);
		
		foreach ($iter as $filename => $file)
		{
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$ext = strtolower($ext);
			
			if ($ext != "php")
			{
				continue;
			}
			require_once($filename);
		}     
    }
}
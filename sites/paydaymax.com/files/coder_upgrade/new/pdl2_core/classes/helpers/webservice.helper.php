<?php

class WebServiceHelper
{
	private $apiConfig;
	public $loggingHelper; /* This should be private and I should have accessor methods. In fact, this shouldn't even be here! */
	
	public function __construct($apiConfig, $loggingHelper)
	{
		$this->apiConfig = $apiConfig;
		$this->loggingHelper = $loggingHelper;
	}
	
	public function jsonRequest($service, $data, $token=NULL, $debug=false)	
	{
		$headers = array(
			'Method: POST',
			'Connection: Close',
			'User-Agent: drupal-pdl2'
		);
		
		$query = http_build_query($data, '', '&');
		$url = $this->apiConfig->getUrlForService($service);
		
		if (isset($token))
			$url = "{$url}?token={$token}";
		
		$ch = curl_init($url);
		
		curl_setopt_array($ch, array(
			CURLOPT_VERBOSE=>false,
			CURLOPT_RETURNTRANSFER=>true,
			CURLOPT_HEADER=>false,
			CURLOPT_HTTPHEADER=>$headers,
			CURLOPT_POST=>count($data),
			CURLOPT_POSTFIELDS=>$query
		));
		
		$response = curl_exec($ch);
		
		$decoded = $this->jsonDecode($response);
		
		if ($debug)
		{
			$rawDebugTxt = print_r($response, true);
			$decodedDebugTxt = print_r($decoded, true);
			echo "<p>Raw Json Response:</p>";
			echo "<pre>{$rawDebugTxt}</pre>";
			echo "<p>Decoded Json Response:</p>";
			echo "<pre>{$decodedDebugTxt}</pre>";
		}
		
		if (!isset($decoded))
		{
			$this->loggingHelper->log("pdl2_core", "Received improper response from server.<br/>
							 			 	   		Location: %location<br/>
							 			 	   		Params: %opts<br/>
							 			 	   		Response: %resp<br/>", 
													 	array("%location" => $service,
								   			   		 	"%opts" => $query,
								   			   		 	"%resp" => $response), WATCHDOG_ERROR);
		}
		
	    return $decoded;
	}
	
	public function soapRequest($service, $data, $token=NULL, $debug=false, $serviceName = 'execute', $async = false)
	{
		$httpHeaders = array(
			'http' => array(
				'header' => "Accept: application/vnd.com.terenine.pdl+soap\r\n" .
							"Connection: Close\r\n",
			)
		);
		$context = stream_context_create($httpHeaders);
		
		$url = $this->apiConfig->url;
        
        if ($url == 'mock.local') {
          $mockWebserviceHelper = new MockWebserviceHelper();
          $res = $mockWebserviceHelper->getData($service, $data);
        } else {
          $apiKey = $this->apiConfig->applicationToken;
          $password = $this->apiConfig->password;

          $serviceUrl = $this->apiConfig->getUrlForService($service);

          $location = $serviceUrl;

          $locationAddOns = array();
          
          if (!empty($locationAddOns)) {
              $location = "{$location}?" . http_build_query($locationAddOns);          
          }           

          $opts = array(
              'trace' => 1,
              'exceptions' => false,
              'use' => SOAP_ENCODED,
              'style' => 'http://schemas.xmlsoap.org/soap/encoding/',
              'stream_context' => $context,
              'location' => $location,
              'cache_wsdl' => WSDL_CACHE_BOTH,
              'features' => SOAP_SINGLE_ELEMENT_ARRAYS
          );


          $wsdl = "{$serviceUrl}?wsdl";

          /*print('WSDL: ' . $wsdl . '<br/>');
          print('opts:');
          print('<pre>');
          print_r($opts);
          print('</pre>');*/
       
          if($this->apiConfig->asyncCalls){
	          $this->loggingHelper->startTask("Dklab WSDL for $serviceUrl");
	          $client = new Dklab_SoapClient($wsdl, $opts);
	          $this->loggingHelper->endTask();   
          }else{
	          $this->loggingHelper->startTask("WSDL for $serviceUrl");
	          $client = new SoapClient($wsdl, $opts);
	          $this->loggingHelper->endTask(); 
          }
        
          
          $applicationArray = array('apiKey' => $apiKey,
                                    'password' => $password);

          if (!empty($token)) {
            $applicationArray['token'] = $token;
            $applicationArray['account'] = array('identity' => 'self');
          }

          $credentialParameters = array('application' => $applicationArray);
          $credentials = array('CredentialParameters' => $credentialParameters);

          $header = new SoapHeader($serviceUrl, 'Credential', $credentials);
          $client->__setSoapHeaders($header);
                    
          $parametersName = $serviceName . 'Parameters';
          $execute = array('$serviceName' => array($parametersName => $data));

          if($async){
	          $res = $client->async->__call($serviceName, $execute);
	      }
	      else{
	          $this->loggingHelper->startTask("CALL SYNC for $serviceUrl");

	          try {
  	          $res = $client->__soapCall($serviceName, $execute);  // Can't catch fatal. this doesn't work :(
  	        } catch (Exception $e) {
    	        watchdog("pdl2_core", "SOAP error:<br/>" . $client->__getLastRequest() . "<br/>" . $client->__getLastResponse() . "<br/>" . $res->getMessage());
  	        }
	          $this->loggingHelper->endTask();
	      }
          
          if (variable_get('pdl2_core_drupal_debug_service_calls', 0) && function_exists('dd'))
          {

								dd('');
								dd('');
								dd('');
								dd('');
								dd($client->__getLastRequestHeaders());
								dd("-------------------------------------------");
								dd($client->__getLastRequest());
								dd("-------------------------------------------");
								dd($client->__getLastResponseHeaders());
								dd("-------------------------------------------");
								dd($client->__getLastResponse());
								dd("-------------------------------------------");
								dd("RESULT");
								dd($res);
								dd("-------------------------------------------");
			
          }
   	      
   	      if (variable_get('pdl2_core_log_service_calls', 0))
   	      {
			$this->log_response_error($client, $data, $location, "DEBUG $service", WATCHDOG_DEBUG);
		  }
		 
        }
        
		if (is_soap_fault($res))
		{
			$this->log_response_error($client, $data, $location, 'SOAP fault', WATCHDOG_ERROR);
			drupal_goto("oops");
		}
		else
		{
					
			if($res == NULL || $res == "") {
				$this->log_response_error($client, $data, $location, 'Received NULL response from server.', WATCHDOG_ERROR);
				drupal_goto("oops");
         	}
			else if ( !is_object($res) ) {
				$this->log_response_error($client, $data, $location, 'Received non-object response from server.', WATCHDOG_ERROR);
				drupal_goto("oops");
			}
			else {
				return $res;
			}
			//return ($res == NULL || $res == "") ? new stdClass() : $res;
		}
	}
	
	private function log_response_error($client, $data, $location, $title = "Received improper response from server.", $severity = WATCHDOG_ERROR)
	{
		// Special case - usually means we got some unhandled error from the server
		// Package up the params
		$optsstr = "";
		foreach ($data as $key => $val)
		{
			$optsstr .= $key . " = " . $val ." || ";
		}
				
		// And the response
		$reqheader = $client->__getLastRequestHeaders();
		$req = $client->__getLastRequest();
		$respheader = $client->__getLastResponseHeaders();
		$resp = $client->__getLastResponse();
		
		$this->loggingHelper->log("pdl2_core", "$title:<br />
				 			 	   Location: %location<br />
				 			 	   Params: %opts<br /><br />
				 			 	   Request Headers: %reqheader<br /><br />
				 			 	   Request: %req<br /><br />
				 			 	   Response Headers: %respheader<br /><br />
				 			 	   Response: %resp<br /><br />",
				 		 	 	   array("%location" => $location,
					   			   		 "%opts" => $optsstr,
					   			   		 "%reqheader" => $reqheader,
					   			   		 "%req" => $req,
					   			   		 "%respheader" => $respheader,
					   			   		 "%resp" => $resp,
					   			   		 ),
					   			   $severity);
		
		
	}
	
	private function jsonDecode($str)
	{
		// Strip any noise from the beginning and end of the JSON string
		$openpos = strpos($str, '{');
		$closepos = strrpos($str, '}');
		
		$rawJson = substr($str, $openpos, $closepos-$openpos+1);
		
		$decoded = json_decode($rawJson);

		return $decoded;
	}
	

}
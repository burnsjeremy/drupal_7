<?php
class MockWebserviceHelper
{
  public function __construct() {
    
  }
  
  public function getData($service, $params) {
    $mockdata = new PDL2Mockdata();
    return $mockdata->getData($service, $params);
  }
}

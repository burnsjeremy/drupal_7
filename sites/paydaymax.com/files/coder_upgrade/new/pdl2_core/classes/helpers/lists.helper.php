<?php
class listsHelper {
	/**
	*
	* Takes a min and max value and builds a list based on the given increment.
	* 
	* @param integer $min 
	* @param integer $max
	* @param integer $interval
	* @param boolean $ascend 
	* 
	* @return array $incrementArray
	*/
	public static function incrementalList($min, $max, $increment, $ascend = true, $extra = null) {
		$incrementArray = array();
		
		if ($ascend) {
			for ($i = $min; $i < $max; $i = $i + $increment) {
				
				$incrementArray[$i] = $i;
				
				if($extra > $i && $extra < $i + $increment && $extra < $max) {
					$incrementArray[$extra] = $extra;	
				}
				
			}
			
			if ($i >= $max) {
				$incrementArray[$max] = $max;
			}
		} else {
			for ($i = $max; $i > $min; $i = $i - $increment) {

				$incrementArray[$i] = $i;
				
				if($extra < $i && $extra > $i + $increment && $extra < $max) {
					$incrementArray[$extra] = $extra;	
				}

			}
			if ($i <= $min) {
			
				$incrementArray[$min] = $min;
			}
		
		}
		
		return $incrementArray;
	}
	
	/**
	* Takes an array of stdClass Objects with id and name properties and makes them an array of id => name 
	*/
	public static function idNameList($idNameArray) {
		$idNameList = array();
		foreach ($idNameArray as $keyList => $valList) {
			$idNameList[$valList->id] = $valList->name;
		}
		
		return $idNameList;
	}
}

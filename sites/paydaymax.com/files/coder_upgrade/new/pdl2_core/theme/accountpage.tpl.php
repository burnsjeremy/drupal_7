<?php
  $loan = $themedata["loan"];
  $currentBalance = $themedata['currentBalance'];
  if ($currentBalance == '') {
    $currentBalance = 0.0;
  }

  
  $loanType = $themedata["loanType"];
  $userInfo = $themedata["userInfo"];
  $memberStatus = $themedata["memberStatus"]->membership->name;
  $memberDiscount = $themedata["memberStatus"]->membership->discount;
  $dueDate = $themedata['dueDate'];

  if (is_float($currentBalance)) {
    $currentBalance = '$' . number_format($currentBalance, 2);
  } else {
    $dueDate = "0";
  }

  if ($currentBalance == "$0.00") {
    // tests have shown a due date with a zero balance - this can't be right - zero out date in this case.
    $dueDate = 0;
  }
  $allowExtension = $themedata['allowExtension'];
  $allowCCPayment = $themedata['allowCCPayment'];

  //drupal_set_message("This is a status", "status");
  //drupal_set_message("This is a warning", "warning");
  //drupal_set_message("This is an error", "error");
?>

<div id="myaccount_container">

  <h1>My Account Summary</h1>

  <div class="logout">
    <?php print $userInfo->first_name . ' ' . $userInfo->last_name.' | ' ?>
    <?php print l('Logout', 'account/logout'); ?>
  </div>

  <div id="myaccount_mainoptions">
    <div id="account_welcome">
      <p>Welcome | <span class="clientname"><?= $userInfo->first_name . ' ' . $userInfo->last_name ?></span></p>
      <div id="mainoptions_links">
      
        <?php if ($loanType == 'payday') : ?>
          <div class="pdl_button"><?= l("<div id='pdl_status_image'></div>", "account/status/payday", array("html" => TRUE)); ?></div>
        <?php elseif ($loanType == 'personal') : ?>
          <?php if (variable_get('pdl2_application_enable_ple_online', 0) && !empty($themedata['personalLoanRefinanceElegible'])) : ?>
              <div class="pl_button">
                <div class="lleft small"><?php print theme('refinance_subtext'); ?></div>
                <div class='rright'><?= l("<div id='perl_refinance_image'></div>", "apply/refinance", array("html" => TRUE)); ?></div>
              </div>
          <?php else:  ?>
            <div class="pl_button"><?= l("<div id='perl_status_image'></div>", "account/status/personal", array("html" => TRUE)); ?></div>
          <?php endif; ?>
        <?php else:  ?>
          <div class="pdl_button"><?= l("<div id='pdl_apply_image'></div>", "apply/payday", array("html" => TRUE)); ?></div>
          <?php if ( ($themedata['personalLoanElegible'] != null) && ($themedata['personalLoanElegible'] != "") ) :  ?>
            <div class="pl_button"><?= l("<div id='perl_apply_image'></div>", "apply/personal", array("html" => TRUE)); ?></div>
          <?php endif; ?>
        <?php endif; ?>
        <div class="button"><?php// print l("<div id='referral_image'></div>", "account/claimreferrals", array("html" => TRUE)); ?></div>
        
      </div>
    </div>
  </div>

  <div id="myaccount_summary">
    <?php if ($loanType == 'payday') : ?>
      <div class="label">Payday Loan Balance:</div>
    <?php elseif ($loanType == 'personal') : ?>
      <div class="label">Personal Loan Balance:</div>
    <?php else : ?>
      <div class="label">Loan Balance:</div>
    <?php endif; ?>
    <div class="value"><?= $currentBalance?></div>

    <?php if ($loanType == 'payday') : ?>
    <div class="label">Date Due:</div>
    <?php elseif ($loanType == 'personal') : ?>
    <div class="label">Next Payment Due:</div>
    <?php else : ?>
    <div class="label">Date Due:</div>
    <?php endif; ?>

    <div class="value"><?= ($dueDate instanceof DateTime) ? date_format($dueDate, 'F jS, Y') : 'N/A'; ?></div>
    <div class="label">Preferred Member Status:  </div>
    <div class="value"><?= $memberStatus ?> - <?= (string)$memberDiscount; ?>% Off <?= l("[More info]", "account/preferred_level"); ?></div>
  </div>

  <div class="clear-both"></div>

  <div id="myaccount_myloans" class="groupedbox">
    <h3>My Payday Loan Info</h3>
    <ul>
      <li><?= (!$loan) ? l("Apply for a New Loan", "apply/payday") : "<span class='disabled'>Apply for a new loan</span>" ; ?></li>
      <li><?= ($loan && $loanType=='payday') ? l("Current Loan Status", "account/status/payday") : '<span class="disabled">Current Loan Status</span>'; ?></li>
      <li><?= ($loan && $loanType=='payday') ? l("View Loan Agreements", "account/agreements") : '<span class="disabled">View Loan Agreements</span>'; ?></li>
      <li><?= ($allowExtension && $loanType=='payday') ? l("Extend My Loan", "account/extend") : '<span class="disabled">Extend My Loan</span>'; ?></li>
      <li><?= l("Loan History", "account/history/payday"); ?></li>
      <li><?= ($allowCCPayment && $loanType=='payday') ? l("Make payment with Debit/Credit card", "account/card-payment") : '<span class="disabled">Make payment with Debit/Credit card</span>'; ?></li>
      <li><?= l("How it works", "account/how-it-works/payday"); ?></li>
    </ul>
  </div>

  <?php if ( ($themedata['personalLoanElegible']) || ( $themedata['personalHistory']) ): ?>
  <div id="myaccount_myaccount" class="groupedbox">
    <h3>My Personal Loan Info</h3>
    <ul>
      <li><?= (!empty($themedata['personalLoanElegible'])) ? l("Apply for a New Loan", "apply/personal") : '<span class="disabled">Apply for a New Loan</span>'; ?></li>
      <li>
      <?
        if (variable_get('pdl2_application_enable_ple_online', 0)) {
          if (!empty($themedata['personalLoanRefinanceElegible'])) {
            print l("Refinance My Loan", "apply/refinance");
          } else {
            print '<span class="disabled">Refinance My Loan</span>';
          }
        } else { // if PLE Online disabled, link to node
          print l("Refinance My Loan", "refinance");
        }
      ?>
      </li>
      <li><?= ($loan && $loanType=='personal') ? l("Current Loan Status", "account/status/personal") : '<span class="disabled">Current Loan Status</span>'; ?></li>
      <li><?= ($loan && $loanType=='personal') ? l("View Loan Agreements", "account/agreements") : '<span class="disabled">View Loan Agreements</span>'; ?></li>
      <li><?= $themedata['hasPersonalHistory'] ? l("Loan History", "account/history/personal") : '<span class="disabled">Loan History</span>'; ?></li>
      <li><?= ($allowCCPayment && $loanType=='personal') ? l("Make payment with Debit/Credit card", "account/card-payment") : '<span class="disabled">Make payment with Debit/Credit card</span>'; ?></li>
      <li><?= l("How it works", "account/how-it-works/personal"); ?></li>
    </ul>
  </div>
  <?php endif; ?>

  <div id="myaccount_getcashfast" class="groupedbox">
    <h3>General Info</h3>
    <ul>
      <li><?= l("Testimonial", "account/feedback"); ?></li>
      <li><?= l("Suggestion", "account/feedback"); ?></li>
      <li><?= l("Referral Program", "account/claimreferrals"); ?></li>
      <li><?= l("My Information", "account/mydetails"); ?></li>
      <li><?= l("Privacy Preferences", "account/privacy-preferences"); ?></li>
      <li><?= l("Change Password", "account/password"); ?></li>
      <?php if (module_exists('pdl2_twofactor')) : ?>
        <?= $themedata['verification_required'] ? '<li>' . l("Validate Account", "account/validate/verify") . '</li>' : ''; ?>
      <?php endif; ?>
      <li><?= l("Log out", "account/logout"); ?></li>
    </ul>
  </div>
</div>
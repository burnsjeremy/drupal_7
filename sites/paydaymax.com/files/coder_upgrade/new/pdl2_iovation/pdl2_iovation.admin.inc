<?php

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function pdl2_iovation_admin($form, &$form_state) {
  $form = array();

  $form["pdl2_iovation_js_library_url"] = array(
    '#type' => 'textfield',
    '#title' => t('JS Library URL'),
    '#default_value' => variable_get('pdl2_iovation_js_library_url', ""),
    '#size' => 50,
    '#required' => TRUE,
  );

  $form["pdl2_iovation_enable_originating_ip"] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Originating IP'),
    '#default_value' => variable_get('pdl2_iovation_enable_originating_ip', 0),
    '#description' => t('Use Originating IP as set within the F5.'),
  );

  return system_settings_form($form);
}

<?php

function pdl2_account_extension_page() {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  } else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');

  }
  // Make sure user can extend their loan.
  $pdl2 = pdl2_core_get_api();

  $loanId = $pdl2->getOpenLoan();
  $allowExtension = $pdl2->allowExtension();

  if (empty($loanId) || !$allowExtension) {
    drupal_set_message('There was an error processing your extension.', 'error');
    drupal_goto('account');
  }

  $created_extension = $pdl2->extensionCreate($loanId);
  $principal = $pdl2->getLoanPrincipal($loanId);
  $loanDueDate = $pdl2->getLoanDueDate($loanId);
  $financeCharge = $pdl2->getFinanceCharges($loanId);
  $minimumPrincipalDue = $pdl2->getMinimumPrincipalDue($loanId);
  $pay_freq = $pdl2->customerGetPayFrequency();

  $themedata = array(
    'minimumFees' => $financeCharge,
    'minimumPrincipalDue' => $minimumPrincipalDue,
    'currentFees' => $financeCharge,
    'currentPrincipal' => $principal,
    'currentTotal' => $financeCharge + $principal,
    'minimumTotal' => $financeCharge + $minimumPrincipalDue,
    'loanDueDate' => $loanDueDate,
    'payFreq' => $pay_freq->PayFrequency->payday_name
  );

  $output = theme('account/extension', $themedata);

  return $output;
}

function pdl2_account_extension_form($formArray, $themedata) {
  $form = array();

  $form['payment_type'] = array('#type' => 'item');

  $form['payment_type']['minimum'] = array('#type' => 'radio',
                        '#title' => t('I will make the minimum payment of '). '$'. number_format($themedata['minimumTotal'], 2),
                        '#default_value' => 1,
                        '#return_value' => 1,
                       '#parents' => array('payment_type'));

  $form['payment_type']['custom_container'] = array('#prefix' => '<div class="container-inline form-item">',
                            '#suffix' => '</div>');

  $form['payment_type']['custom_container']['custom'] = array('#type' => 'radio',
                                '#title' => t('I would like my total payment to be: '),
                                '#default_value' => 0,
                                '#return_value' => 2,
                                '#parents' => array('payment_type'));

  $form['payment_type']['custom_container']['custom_textfield'] = array('#type' => 'textfield',
                                      '#default_value' => '',
                                        '#size' => 8,
                                      '#attributes' => array('onClick' => '$("input[name=payment_type][value=2]").attr("checked", true);'));

  $form['submit'] = array('#type' => 'submit',
                  '#value' => t('I Want to Extend My Loan'));

  return $form;
}

function pdl2_account_extension_form_validate($form, &$form_state) {
  $values = $form_state["values"];

  $pdl2 = pdl2_core_get_api();
  $loanId = $pdl2->getOpenLoan();

  if (empty($loanId)) {
    drupal_goto('account');
  }

  $financeCharge = $pdl2->getFinanceCharges($loanId);
  $minimumPrincipalDue = $pdl2->getMinimumPrincipalDue($loanId);

  $paymentAmount = $minimumAmountDue = $financeCharge + $minimumPrincipalDue;

  if ($values['payment_type'] == 2) {

    if (!$values["custom_textfield"]) {
      form_set_error('custom_textfield', t('Please provide an amount.'));
      return;
    }

    if (!is_numeric($values["custom_textfield"])) {
      form_set_error('custom_textfield', t('Please provide a valid money amount.'));
      return;
    }

    $paymentAmount = $values['custom_textfield'];

    if ($paymentAmount <= $minimumAmountDue || $paymentAmount == '') {
      form_set_error('custom_textfield', t('You must enter a valid amount more than the minimum.'));
      return;
    }

  }

  $res = $pdl2->setPayment($loanId, $paymentAmount);
//  dd($loanId);
//  dd($paymentAmount);
//  dd($res);
  if ($res->status != 1 || $res->result != 1) {
//    dd("error here");
    form_set_error('custom_textfield', t($res->message->detail));
    return;
  }

}

function pdl2_account_extension_form_submit($form, &$form_state) {
//  dd("in submit");
  drupal_goto('account/extend/agreement');
}
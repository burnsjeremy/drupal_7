<?php

function pdl2_account_agreementdetails_page($loanId, $agreementId) {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  // Check we were given an agreement ID
  if (!isset($agreementId) || $agreementId == "") {
    drupal_set_message('You will need to select one of the following agreements to view.');
    drupal_goto('account/agreements');
  }

  // Let's get the agreement details
  $pdl2 = pdl2_core_get_api();
  $ssn = $pdl2->getSsn();
  $borrower = $pdl2->getUserInfo();

  $agreement = $pdl2->getLoanAgreementById($agreementId);
  if (!$agreement) {
    $agreement = $pdl2->loanPersonalGetAgreementById($agreementId);
    $schedule = $pdl2->loanPersonalGetSchedule($loanId);
  }

  if (!$agreement) {
    drupal_goto('account/agreements');
  }

	$themedata = array(
		'agreement' => $agreement,
		'date' => $agreement->loan_agreement->createDate->date,
		'borrower' => $borrower,
		'ssn' => $ssn,
		'schedule' => (isset($schedule) ? $schedule->result : null),
		'loan_type' => $loan_type
	);

  $output = theme('account/agreement', $themedata);

  return $output;
}

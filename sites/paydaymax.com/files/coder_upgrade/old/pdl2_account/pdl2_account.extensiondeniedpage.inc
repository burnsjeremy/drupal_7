<?php

function pdl2_account_extension_denied_page() {

	// First let's see if we have an existing session
	if (!pdl2_account_is_logged_in()) {
		drupal_goto("account/login");
	}
	else if (pdl2_account_not_opt_out()) {
		drupal_goto('account/optedout');
	}
	else {
		// Make sure user can not extend their loan.
		$pdl2 = pdl2_core_get_api();
		$extensionCheck = $pdl2->extensionCheck();
		
		if ($extensionCheck->checkSuccess) {
			drupal_goto("account/extension");
			return;
		}
		
		$themedata = array("extensionCheck" => $extensionCheck);
		$output = theme('account/extension/denied', $themedata);
		
		return $output;
	}
}
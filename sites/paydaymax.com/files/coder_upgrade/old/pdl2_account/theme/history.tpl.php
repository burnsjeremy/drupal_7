<?php
  $loanHistory = $themedata["loanHistory"];
  $loan_type = $themedata["loan_type"];
?>

<h1>Loan History</h1>
<table id="loan_history_t">
  <thead>
    <tr>
      <th>Loan ID</th>
      <th class="status">Status</th>
      <th>Due Date</th>
      <th>Days</th>
      <th>Amount</th>
      <th>Fees/Interest</th>
      <th>Amount Due</th>
    </tr>
  </thead>
  <tbody>
    <?php
      if ($loanHistory != null) {
        /******* MB: added ->result[0] for PDLNEW-2523. If this page breaks and you're reading this, then the service output is being inconsistent. */
        if (is_array($loanHistory->result[0]))
          $history = $loanHistory->result[0];
        else
          $history = $loanHistory->result;
        /**** */
        
        foreach ($history as $result) {
          $loan = $result->loanHistory->loan->loan;
          echo "<tr>";
          if ($loan->status->loan_status->customer_view_name == "Denied")  {
            continue;
          }
          else {
            echo "<td>" . l($loan->id, "account/historystatus/{$loan_type}/" . $loan->id) . "</td>";
          }
          echo "<td>{$loan->status->loan_status->customer_view_name}</td>";
          echo "<td>" . strtoupper(date('M d, Y', strtotime($loan->due_date->date))) . "</td>";
          echo "<td>" . (int)$loan->length->interval_days.  "</td>";
          echo "<td>$" . $loan->original_principal . "</td>";
          echo "<td>$" . number_format(floatval($result->loanHistory->loan->loan->finance_charges), 2).  "</td>";
          echo "<td>$" . number_format($result->loanHistory->total_currently_due, 2) . "</td>";
          echo "</tr>";
        }
      }
    ?>
  </tbody>
</table>
<div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>

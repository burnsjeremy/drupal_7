<?php
  $referralCode = $themedata['referralCode'];
  $referralsList = $themedata["referralsList"];
  $siteName = $themedata["siteName"];
  $amountReceived = $themedata["referralAmount"];
  $amountBalance = $themedata["referralBalance"];
  $potential = number_format($themedata["potential"], 2);
?>
<?php if (isset($themedata["referralTransactions"])) { ?>
<h3>Transaction Updates</h3>
<table id="referraltransactions">
<tr>
<th style="width:150px;">Type</th>
<th style="width:150px;">Original Amount</th>
<th style="width:150px;">Status</th>
<th style="width:150px;">Amt Applied</th>
<th style="width:150px;">Notes</th>
</tr>
<?php foreach ($themedata["referralTransactions"]->customerTransactions->result as $trans) { 
  if ($trans->transaction->payment->payment->type == "referral-credit" ||
      (($trans->transaction->status == "pending") && ($trans->transaction->type == "finance-charge"))) {
?>
  <tr>
    <td><?php echo pdl2_account_getTransactionActionText($trans->transaction->type); ?></td>
    <td style="text-align:right;">$<?php echo number_format($trans->transaction->payment->payment->amount, 2); ?></td>
    <td><?php echo pdl2_account_getTransactionStatusText($trans->transaction->status); ?></td>
    <td style="text-align:right;"><?php if ($trans->transaction->status == "completed") echo '$' . number_format($trans->transaction->payment->payment->amount, 2); else echo "$0"; ?></td>
    <td><?php if ($trans->transaction->payment->payment->type == "referral-credit") echo 'referral fee'; ?></td>
  </tr>
<?php } } ?>
</table>
<br/><br/>
<?php } ?>

<h1>Claim Referrals</h1>
<div id="claim_referrals_page">
<p>To refer your family and friends you may click on the link below to print referral coupons, or simply follow these steps:</p>

<ol>
  <li>Enter the <strong>Refer My Friends Now</strong> information below</li>
  <li>Click the <strong>Submit Referrals</strong> button</li>
  <li><?= $siteName ?>, will email your referral code</li>
  <li>Your friends and family will give your referral code when they apply for a loan</li>
  <li>If their loan is approved you will receive $<?php print $potential; ?> for the referral</li>
</ol>

<p><?= l("Terms and Conditions", "account/referralterms") ?></p>

<p><strong>Referral Code:</strong> <span class="ref_code"><?= $referralCode ?></span> <span id="referral-what">What's this?</span></p>

<div id="referral-code-info">
  <p>A referral code is a unique code assigned to every customer. The code should be given to family members and friends interested in applying for a loan with our company. For every family member or friend approved for a loan you will receive a $<?php print $potential; ?> bonus to be applied to a current loan or your checking account.</p>
</div>

<p>
  <strong>Amount Received To Date </strong> $<?= $amountReceived ?><br/>
  <strong>Referral Balance </strong> $<?= $amountBalance ?>
</p>
<?php if ($amountBalance > 0) { ?>
  <?php echo drupal_get_form("pdl2_account_deposit_referral_form"); ?><br/>
<?php } ?>
<p>Send your friends a referral email and you could receive free money! Just type your friend's name, email, and (optionally) home phone number and click the "Submit Referrals" button!</p>
<?php $text = "Click to print ".$siteName." Referral Cards!"; ?>
<p><?= l($text, "account/referralcards", array('html' => TRUE)) ?></p>
<h2>Refer My Friends Now <span>(Persons Outside Your Household)</span></h2>

<div class="padding">
  <?php echo drupal_get_form("pdl2_account_claimreferrals_form"); ?>

  <p><small>*To refer your friends, simply complete the information above and we will send them an email with your referral code.</small></p>
</div>

<?php if ($referralsList) {
  echo "<h2>Friends Who Have Responded To Referral</h2>";
  $count = 1;
  foreach ($referralsList as $item) {
    echo $count . "<br/>";
    echo $item->referral->referred_customer->email . "<br/>";
    echo "CODE<br/>";
    echo "<strong>$" . number_format($item->referral->referral_amount, 2) . "</strong><br/>";
    echo "<strong>" . ($item->referral->redemption_type == "unredeemed" ? "Not Yet Claimed" : "Claimed") . "</strong><br/><br/>";
    $count++;
  }
} ?>

<script type="text/javascript">
  $(document).ready(function() {
    $('#referral-code-info').hide();
    $('#referral-what').click(function(){
      $('#referral-code-info').toggle();
    });
  });
</script>
</div>


<div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>

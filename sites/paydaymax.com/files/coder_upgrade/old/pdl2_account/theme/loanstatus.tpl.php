<?php
// sort loan status messages by time.
function dateSortCmp($a, $b) {
  $atime = strtotime($a->loan_status->created->date);
  $btime = strtotime($b->loan_status->created->date);

  if ($atime == $btime) return 0;
  return ($atime < $btime) ? 1 : -1;
}
$denied = FALSE;
$loan_statuses = $themedata['loan_history']->statuses->result;
//dd($loan_statuses);
if ($loan_statuses->loan_status->name != "Denied") {
  usort($loan_statuses, "dateSortCmp");
}
else {
  $denied = TRUE;
}

$reftrans = $themedata['referralTransactions'];

?>
<h1 class="clean">Loan Status</h1>
<div id="viewaccount_container" class="Wrapper Rounded">

  <div id='header_info'>
    <div id='name'><?php print $themedata['customer']->first_name . " " . $themedata['customer']->last_name; ?></div>
    <div id='loan_id'><span class="label">Loan Id:</span><?php print $themedata['loan']->id; ?></div>
    <?php if ($denied != TRUE) : ?>
      <div id='amount'><span class="label">Amount:</span> $<?php print number_format($themedata['agreement']->amountFinanced, 2); ?></div>
    <?php endif; ?>
    <?php if ($denied != TRUE) : ?>
      <?php if ($loan_statuses[0]->loan_status->name != "Closed (Successful)") : ?>
        <div id='loan-agreement'><?php print l("View Loan Agreements", "account/agreements"); ?> </div>
      <?php endif; ?>
    <?php endif; ?>

    <div class="clear-both"></div>
    <div id='loan_type'><span class="label">Loan Type: </span><?php print ucfirst($themedata['loan_type']); ?></div>
    <div id='application_date'><span class="label">Application Date: </span><?php print date('M jS, Y', strtotime($themedata['agreement']->createDate->date)); ?></div>

  </div>

  <div class="clear-both"></div>

  <?php if ($denied == TRUE) : ?>
    <div id='loan_status_info'>
      <h2 class="clean">Loan was Denied</h2>
      No more information available.
    </div>
  <?php else : ?>
    <div id='loan_status_info'>
      <h2 class="clean">Loan Status Steps: <span class="smaller">(most recent at top)</span></h2>
      <table class="Report" id="viewaccount_loan_status" cellspacing="0">
        <?php
          $count = 0;
          foreach ($loan_statuses as $key=>$value) {
            print "<tr>";
            if ($count == 0 && $themedata['overrideMsg']) {
              print "<td align='left'>".$themedata['overrideMsg']."</td>";
            } else {
              print "<td align='left'>".$value->loan_status->name."</td>";
            }
            print "<td align='right'>".date('M jS, Y', strtotime($value->loan_status->created->date))."</td>";
            print "</tr>";
            $count++;
          }
        ?>
      </table>
    </div>

    <?php if ($themedata['deposits']) : ?>
    
    <div id='deposit_info'>
      <h2 class="clean">Deposits</h2>
      <table class="viewaccount_infotable" id="viewaccount_deposit_info">
        <thead>
          <tr>
            <th class="date">Date</th>
            <th class="amount">Amount</th>
            <th class="status">Status</th>
          </tr>
        </thead>
        <tbody>
          <?php
              foreach($themedata['deposits'] as $deposit) {
                print "<tr>";
                    print '<td class="date">'.date('M jS, Y', strtotime($deposit['date'])).'</td>';
                    print '<td class="amount">$'.number_format($deposit['amount'], 2 ).'</td>';
                    print '<td class="status">'.pdl2_account_getTransactionStatusText($deposit['status']).'</td>';
                print "</tr>";
              }
          ?>
        </tbody>
      </table>
    </div>
    <?php endif; ?>

    <?php if ($themedata['payments']) : ?>
    <div id='payments_info'>
      <h2 class="clean">Payments</h2>
      <table class="viewaccount_infotable" id="viewaccount_payments_info">
        <thead>
          <tr>
            <th class="action">Action</th>
            <th class="date">Date</th>
            <th class="amount">Amount</th>
            <th class="status">Status</th>
          </tr>
        </thead>
        <tbody>
          <?php
            if ($themedata['payments']) {
              foreach($themedata['payments'] as $payment) {
                print "<tr>";
                  print '<td class="text">'.pdl2_account_getTransactionActionText($payment['action']).'</td>';
                  print '<td class="text">'.date('M jS, Y', strtotime($payment['date'])).'</td>';
                  print '<td class="number">$'.number_format($payment['amount'], 2).'</td>';
                  print '<td class="text">'.pdl2_account_getTransactionStatusText($payment['status']).'</td>';
              print "</tr>";
              }
            }
          ?>
        </tbody>
      </table>
    </div>
    <?php endif; ?>

    <div id='loan_summary'>
      <h2 class="clean">Loan Summary</h2>

      <div id='loansummary'>
        <div>
          <span class="summary_label text">Original Loan Amount:</span>
          <span class="summary_value number">$<?php print number_format($themedata['loan']->original_principal, 2); ?></span>
        </div>
        <div>
          <span class="summary_label text">Deposited to Your Account:</span>
          <span class="summary_value number">$<?php print number_format($themedata['amt_deposited'], 2); ?></span>
        </div>
        <?php if ($themedata['fees_finance_chanrges'] > 0) { ?>
        <div>
          <span class="summary_label text">Fees and Finance Charges Paid:</span>
          <span class="summary_value number">$<?php print number_format($themedata['fees_finance_chanrges'], 2); ?></span>
        </div>
        <?php } ?>
        <div>
          <span class="summary_label text">Next Balance Due (Pending):</span>
          <span class="summary_value number"><?php if (is_numeric($themedata['loan_balance'])) { print '$' . number_format($themedata['loan_balance'], 2); } else { print $themedata['loan_balance']; } ?></php></span>
        </div>
      </div>
    </div>

    <div id='referral_info'>
      <h2 class="clean">Referral Fees to be Deposited to Your Account: <span>(not applied to specific loan)</span></h2>
    <?php if (isset($reftrans) && isset($reftrans->customerTransactions) && isset($reftrans->customerTransactions->result) && count($reftrans->customerTransactions->result) > 0) { ?>
      <table class="viewaccount_infotable" id="viewaccount_payments_info">
        <thead>
          <tr>
            <th class="date">Date Claimed</th>
            <th class="amount">Amount</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($reftrans->customerTransactions->result as $trans) {
            print "<tr>";
              print '<td class="text">'.date('M jS, Y', strtotime($trans->transaction->date->date)).'</td>';
              print '<td class="number">$'.number_format($trans->transaction->payment->payment->amount, 2).'</td>';
            print "</tr>";
          } ?>
        </tbody>
      </table>
    <?php } ?>
    </div>

  <?php endif; ?>

</div>
<div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>

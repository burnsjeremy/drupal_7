<h1>Extend Your Loan</h1>
<p>Your loan is due on <?= date('M jS, Y', strtotime($themedata['loanDueDate']->date)) ?>.</p>

<table id="extendTable">
  <thead>
    <tr>
      <th class="charge">Charge</th>
      <th class="amount">Amount</th>
      <th class="mindue">Minimum Due</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td>Principal</td>
        <td>$<?php echo number_format($themedata['currentPrincipal'], 2);?></td>
        <td>$<?php echo number_format($themedata['minimumPrincipalDue'], 2);?></td>
    </tr>
    <tr>
        <td>Fees</td>
        <td>$<?php echo number_format($themedata['currentFees'], 2);?></td>
        <td>$<?php echo number_format($themedata['minimumFees'], 2);?></td>
    </tr>
    <tr>
        <td>Total</td>
        <td>$<?php echo number_format($themedata['currentTotal'], 2);?></td>
        <td>$<?php echo number_format($themedata['minimumTotal'], 2);?></td>
    </tr>
  </tbody>
</table>

<?php echo drupal_get_form('pdl2_account_extension_form', $themedata); ?>

<h2>Do I have the option to extend my loan?</h2>

<?php if ($themedata['payFreq']== "Monthly") { ?>
<p>Yes! You may extend your loan until your next pay period for a small fee. After your first extension, you are required to make a payment toward the principal.</p>
<p><strong>REMINDER:</strong> After one (1) extension, you are required to pay a minimum of $100.00 for loans of less than $600.00 and $200.00 for loans of $600.00 or more.</p>
<?php } else { ?>
<p>Yes! You may extend your loan until your next pay period for a small fee. After your first three (3) extensions, you are required to make a payment toward the principal.</p>
<p><strong>REMINDER:</strong> After three (3) extensions, you are required to pay a minimum of $50.00 for loans of less than $600.00 and $100.00 for loans of $600.00 or more.</p>
<?php } ?>

<h2>Why is a minimum payment required?</h2>
<p>We require the minimum payment to encourage you to pay off your loan as quickly as possible. Short-term loans are designed to be a temporary solution for unexpected financial circumstances. Repetitive extensions can put you in financial distress. We are here to help you when you need a little cash before payday. Our services are not intended to be used as a long term financial solution.</p>
<?php
  $siteName = $themedata['siteName'];
  $phone = $themedata['phone'];
?>
<h2>Remember, you can't get a loan with us if you opt out.</h2>
<p>TERMS AND CONDITIONS</p>
<p>If <?= $siteName ?> declines this application for credit or is unable to process my application and/or registration for any reason, I authorize <?= $siteName ?> to forward my application and/or registration to another lender who reviews these applications. I understand that this is a service to me to be considered for alternative sources of financing. I further understand that the other lender may offer credit on different terms and conditions. I understand that I may or may not be offered credit from an alternate lender and that I should review the privacy policy of any lender prior to applying for credit. I agree that I have read and understand these Terms and Conditions.</p>
<p>CONSENT FOR ELECTRONIC DISCLOSURES</p>
<p>Please read this information carefully and print and retain this information electronically for future reference.</p>
<ul>
<li>Doing Business on the Internet. We can only give you the benefits of our service by conducting business electronically over the Internet. One of the first steps in doing business with you electronically over the Internet is to obtain from you your consent to our communicating with you electronically. This communication informs you of your rights when receiving communications from us electronically.</li>
<li>Consenting to Do Business Electronically. By giving us your consent, you agree that all communications from us relating to your use of this web site, now or in the future, may be provided or made available to you electronically by e-mail or at our website.</li>
<li>Scope of Consent. Your consent to receive communications and do business electronically, and our agreement to do so, applies to all of your transactions with us.</li>
<li>Withdrawing Consent. You may withdraw your consent to receive communications electronically by contacting us. If you withdraw your consent, from that time forward, you will not be able to apply for new loans or services. The withdrawal of your consent will not affect the legal validity and enforceability of any pending loans obtained through our web site, or any electronic communications provided or business transacted between us prior to the time you withdraw your consent.</li>
<li>Changes to Your Contact Information. Please keep us informed of any changes in your email or mailing address so that you continue to receive all communications without interruption.</li>
<li>How to Contact Us. You can withdraw your consent to electronic communications with us by calling a customer service agent toll free at <?php echo $phone; ?></li>
<li>By accepting this agreement, you acknowledge that you can access the electronic communications in the designated formats described above, and you consent to having all communications provided or made available to you in electronic form and to doing business with us electronically.</li>
</ul>
<p>By accepting this agreement, you acknowledge that you can access the electronic communications in the designated formats described above, and you consent to having all communications provided or made available to you in electronic form and to doing business with us electronically.</p>
<p>Copyright &copy; <?php echo date('Y'); ?> All rights reserved</p>

<?= drupal_get_form("pdl2_account_optoutall_form"); ?>


<div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>
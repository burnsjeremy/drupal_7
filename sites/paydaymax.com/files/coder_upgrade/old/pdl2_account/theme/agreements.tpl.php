<?php
  if ($themedata == null) {
    echo "<p>An error has occurred</p>";
    return;
  }
  $loanAgreements = $themedata['loanAgreements'];
?>

<h1 class="clean">View Agreements for Loan #<?= $themedata['loanId'] ?></h1>
<table id="loan-agreements-table">
  <tr><th class="header">Agreement Date</th><th class="header">Actions</th></tr>
  <?php
    foreach ($loanAgreements as $agreement) {
      if ($agreement->result)
      	$agreement = $agreement->result;
      	
      echo "<tr>";
      echo "<td>" . date('M jS, Y h:i:s', strtotime($agreement->agreementDate->date)) . "</td>";
      echo "<td>" . l(t("View agreement"), "account/agreement/" . $themedata["loanId"] . "/" . $agreement->agreementId) . "</td>";
      echo "</tr>";
    }
  ?>
</table>

<div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>

<?php
  $siteName = $themedata['siteName'];
?>
<h1>Privacy Preferences</h1>
<p>Please use the form below to manage subscriptions for our special offers and newsletters.</p>
<ul>
  <li>Check the boxes you would like to opt out of.</li>
  <li>Check only the items you do not want to subscribe to.</li>
  <li>Click on 'Save My Preferences' to update your info.</li>
</ul>
<p>Please note: <?= $siteName ?> may still contact you regarding your account status, regardless of the preferences set below.</p>

<?= drupal_get_form("pdl2_account_privacypref_form") ?>

<P></P>
<p>To opt out of all electronic communications <?= l("click here", "account/optoutall") ?>.</p>
<p>(Please note, we can not do any new business with you if we cannot contact you regarding your account)</p>
<div class="return-to-account"><?php print l("<< Return to Account", "account"); ?></div>

<?php

function pdl2_account_logout_page() {

	// First let's see if we have an existing session
	if (!pdl2_account_is_logged_in())
		drupal_goto("account/login");
	else
	{
	  // Clear quick start session vars
    unset($_SESSION['email']);
    unset($_SESSION['password']);

		$pdl2 = pdl2_core_get_api();
		$pdl2->logout();
		//site name added
		$siteName = pdl2_core_get_site_name();
		$themedata = array('siteName'=>$siteName);
		$output = theme('account/logout', $themedata);
		return $output;
	}
}
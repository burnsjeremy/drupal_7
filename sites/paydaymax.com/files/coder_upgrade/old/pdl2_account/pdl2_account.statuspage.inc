<?php

function pdl2_account_loan_status($loan_type='payday') {
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
 
  $pdl2 = pdl2_core_get_api();
  $loan_id = $pdl2->getOpenLoan();
  return pdl2_account_loan_status_page($loan_type, $loan_id);
}


function pdl2_account_loan_status_page($loan_type, $loan_id) {
  drupal_goto("account/historystatus/{$loan_type}/{$loan_id}");
}

<?php

function pdl2_account_cardpayment_page() {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $pdl2 = pdl2_core_get_api();
  $allowCCPayment = $pdl2->allowCCPayment();

  if(!$allowCCPayment){
    drupal_goto('account');
  }

  $loanId = $pdl2->getOpenLoan();
  $loan_type = $pdl2->getLoanType($loanId);
  $transacitons = $pdl2->getLoanTransactionsById($loanId, $loan_type);

	$paid_sum = 0;
	foreach($transacitons->loanTransactions->result as $transaction) {
		if($transaction->transaction->status == 'completed'){
      switch ($transaction->transaction->type){
        case 'payment':
        case 'finance-charge':
        case 'extension fee':
        case 'return check fee':
          $paid_sum += $transaction->transaction->payment->payment->amount;
        break;
      
        case 'deposit':
          //dd($transaction->transaction->type);
          $deposit = $transaction->transaction->payment->payment->amount;
        break;
      }
		}
	}
  
  $themedata['paid'] = $paid_sum;
  $themedata['deposit'] = $deposit;
	$themedata['collection_co'] = $pdl2->getLoanCollectionCompany($loanId, $loan_type);
  $themedata['due'] = $pdl2->getLoanHistoryById($loanId, $loan_type)->loanHistory->total_currently_due;
  $themedata['ccOptions'] = $pdl2->getCCPaymentOptions($loanId, $loan_type);
  $themedata['siteName'] = pdl2_core_get_site_name();

  $output = theme('account/ccpayment', $themedata) . drupal_get_form('pdl2_account_cardpayment_form');
  $output .= '<div class="return-to-account">' . l("<< Return to Account", "account") . '</div>';
  return $output;
}

function pdl2_account_cardpayment_form($formArray) {

  $form['amount'] = array('#type' => 'textfield',
                          '#title' => 'How much are you able to pay today?',
                          '#size' => 6);

  $form['submit'] = array('#type' => 'submit',
                          '#value' => t('Continue'));

  return $form;
}


function pdl2_account_cardpayment_form_submit($form, &$form_state) {
  $paymentAmount = $form_state['values']['amount'];
  drupal_goto('account/card-payment/process/'.$paymentAmount);
}


function pdl2_account_cardpayment_callback($paymentAmount) {
  global $base_url;

  $pdl2 = pdl2_core_get_api();
  $loanId = $pdl2->getOpenLoan();
  $loan_type = $pdl2->getLoanType($loanId);

  try{
  	$ccSubmissionValues = $pdl2->getCCSubmissoinValues($loanId, $paymentAmount, $loan_type);
  }
  catch(Exception $e){
	  drupal_set_message($e->getMessage(), 'error');
	  drupal_goto('account/card-payment');
  }

  $ccSubmissionValues->UMredirApproved = $base_url . '/account/card-payment/confirm/accepted';
  $ccSubmissionValues->UMredirDeclined = $base_url . '/account/card-payment/confirm/declined';

  drupal_add_js(drupal_get_path('module', 'pdl2_account') .'/cardpayment.js');
  return drupal_get_form('pdl2_account_cardpayment_callback_form', $ccSubmissionValues);
}


function pdl2_account_cardpayment_callback_form($form, $ccSubmissionValues) {

  foreach($ccSubmissionValues as $key => $val){
    $form[$key] = array('#type' => 'hidden',
                        '#value' => $val);
  }
  $_SESSION['UMauthAmount'] = $ccSubmissionValues->UMamount;

  $form['submit'] = array('#type' => 'submit',
                          '#value' => t('Continue'));

  $form['#action'] = 'https://sandbox.usaepay.com/interface/epayform/';

  return $form;
}


function pdl2_account_cardpayment_confirm_page($status) {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
		
		$pdl2 = pdl2_core_get_api();
		$loanId = $pdl2->getOpenLoan();
		$loan_type = $pdl2->getLoanType($loanId);
    $balance = $pdl2->getLoanHistoryById($loanId, $loan_type)->loanHistory->total_currently_due;
		$collection_co = $pdl2->getLoanCollectionCompany($loanId, $loan_type);
		

		  try{
		  	$ccSubmissionValues = $pdl2->getCCSubmissoinValues($loanId, $_SESSION['UMauthAmount'], $loan_type);
		  }
		  catch(Exception $e){
			  drupal_set_message($e->getMessage(), 'error');
			  //TO DO : handle errors at this point
			  drupal_goto('account/card-payment');
		  }
		
		$params = array('loanId'                       => $loanId,
		              'creditCardTransactionHash'    => $ccSubmissionValues->transactionId,
		              'UMresponseHash'               => $_GET['UMresponseHash'],
		              'UMresult'                     => $_GET['UMresult'],
		              'UMrefNum'                     => $_GET['UMrefNum'],
		              'UMstatus'                     => $_GET['UMstatus'],
		              'UMavsResultCode'              => $_GET['UMavsResultCode'],
		              'UMavsResult'                  => $_GET['UMavsResult'],
		              'UMcvv2ResultCode'             => $_GET['UMcvv2ResultCode'],
		              'UMcvv2Result'                 => $_GET['UMcvv2Result'],
		              'UMerror'                      => $_GET['UMerror']);
		
		$response = $pdl2->processCCResponse($loanId, $params, $loan_type);
		
    $themedata = array('amount' => $ccSubmissionValues->UMamount,
                       'balance' => $balance,
                       'loanId' => $loanId,
                       'collection_co' => $collection_co->collection_company);
    
    if($response) {
      $output = theme('account/ccpayment/thankyou', $themedata);
    }
    else {
      $output = theme('account/ccpayment/denied', $themedata);
    }
    
    $output .= '<div class="return-to-account">' . l("<< Return to Account", "account") . '</div>';
    return $output;
}


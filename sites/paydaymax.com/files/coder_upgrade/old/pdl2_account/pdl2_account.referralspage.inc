<?php

function pdl2_account_referrals_page($pg="", $action="") {
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
  $pdl2 = pdl2_core_get_api();

  $referralsList = $pdl2->getReferralsList();
  $referralAmount = $pdl2->getReferralAmountReceived();
  $referralBalance = $pdl2->getReferralBalance();

  $referralCode = $pdl2->getReferralCode();
  $siteName = pdl2_core_get_site_name();
  $company = $pdl2->getCompanyInfo();
  
  if ($action == "appliedtobalance") {
    $referralTransactions = $pdl2->customerGetTransactions('');
  }
  
  $themedata = array("referralsList" => $referralsList,
                     "referralCode" => $referralCode,
                     "referralAmount" => $referralAmount,
                     "referralBalance" => $referralBalance->balance,
                     "potential" => $referralBalance->potential_referral_amount,
                     "siteName" => $siteName,
                     "company" => $company,
                     "referralTransactions" => $referralTransactions);

  $output = theme('account/claimreferrals', $themedata);
  return $output;
}

function pdl2_account_claimreferrals_form() {
  $pdl2 = pdl2_core_get_api();
  $referralCode = $pdl2->getReferralCode();

  $form = array();

  $form['1'] = array("#value" => '<table id="referral_table"><thead><tr><th>First Name</th><th>Last Name</th><th>Email</th><th>Home Phone <span>(Optional)</span></th></tr></thead><tbody>');

  $totalRows = 3;

  for ($i = 1; $i < $totalRows+1; $i++) {
    $form["firstname{$i}"] = array(
      "#type" => "textfield",
      "#prefix" => '<tr><td>',
      "#suffix" => "</td>",
      "#maxlength" => 255,
      "#size" => 35,
      "#required" => FALSE,
    );
    $form["lastname{$i}"] = array(
      "#type" => "textfield",
      "#prefix" => "<td>",
      "#suffix" => "</td>",
      "#maxlength" => 255,
      "#size" => 35,
      "#required" => FALSE,
    );
    $form["email{$i}"] = array(
      "#type" => "textfield",
      "#prefix" => "<td>",
      "#suffix" => "</td>",
      "#maxlength" => 255,
      "#size" => 35,
      "#required" => FALSE,
    );
    $form["homephone{$i}"] = array(
      "#type" => "textfield",
      "#prefix" => "<td>",
      "#suffix" => "</td>",
      "#maxlength" => 255,
      "#size" => 12,
      "#required" => FALSE,
    );
  }

  $form['2'] = array("#value" => "</tbody></table>");

  $form['submit'] = array('#type' => 'submit',
                          '#value' => t('Submit Referrals'));

  return $form;
}

function pdl2_account_claimreferrals_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  $hasNonEmptyRow = false;
  $totalRows = 3;
  for ($i = 1; $i < $totalRows+1; $i++) {
    // Create field names we're interested in for current row.
    $firstName = "firstname{$i}";
    $lastName = "lastname{$i}";
    $email = "email{$i}";
    $homephone = "homephone{$i}";

    // If row has no data just continue.
    if ($values[$firstName] == '' && $values[$lastName] == '' && $values[$email] == '' && $values[$homephone] == '') {
      continue;
    }

    $hasNonEmptyRow = true;
    $rowHasError = false;

    if ($values[$firstName] == '') {
      form_set_error($firstName, t("Please provide a first name."));
      $rowHasError = true;
    }

    if ($values[$lastName] == '') {
      form_set_error($lastName, t("Please provide a last name."));
      $rowHasError = true;
    }

    if ($values[$email] == '' || !valid_email_address($values[$email])) {
      form_set_error($email, t("Please provide a valid email."));
      $rowHasError = true;
    }

    // Break out of the loop if we have an error on a row
    // to prevent potentially spamming the user w/ duplicate error msgs.
    if ($rowHasError) {
      break;
    }
  }

  if (!$hasNonEmptyRow) {
    form_set_error('', t("Please provide a referral."));
  }
}


/**
 * This currently ignores all information from the form, but the email address.
 * Note - the referral code doesn't seem to be here either.....
 */
function pdl2_account_claimreferrals_form_submit($form, &$form_state) {
  $pdl2 = pdl2_core_get_api();
  $values = $form_state['values'];
  $totalRows = 3;
  $successCount = 0;

  for ($i = 1; $i < $totalRows+1; $i++) {
    // Create field names we're interested in for current row.
    $emailField = "email{$i}";
    $emailValue = $values[$emailField];
    $firstnameField = "firstname{$i}";
    $firstnameValue = $values[$firstnameField];
    $lastnameField = "lastname{$i}";
    $lastnameValue = $values[$lastnameField];
    $homephoneField = "homephone{$i}";
    $homephoneValue = $values[$homephoneField];
    if ($emailValue == '') {
      continue;
    }

    try {
      $result = $pdl2->sendReferralEmail($emailValue, $firstnameValue, $lastnameValue, $homephoneValue);
    }
    catch (Exception $e) {
      $result = false;
    }

    if ($result) {
      $successCount++;
    }
  }

  if ($successCount == 1) {
    drupal_set_message(t("@count referral submitted successfully.", array('@count' => $successCount)));
  }
  else if ($successCount > 1) {
    drupal_set_message(t("@count referrals submitted successfully.", array('@count' => $successCount)));
  }
  else {
    drupal_set_message(t("An error occurred and we were unable to send your referrals."), 'error');
  }
}

function pdl2_account_deposit_referral_form() {
  $pdl2 = pdl2_core_get_api();
  $referralBalance = $pdl2->getReferralBalance();

  $form = array();
  $options = array();

  if ($referralBalance->can_deposit_to_bank_account == 1)
    $options["deposit_to_account"] = t('Deposit to My Account Now');

  if ($referralBalance->can_apply_to_loan == 1)
    $options["apply_to_balance"] = t('YES: Apply To My Loan Balance');
    
  if (count($options) != 0) {
    $form['deposit_referral'] = array(
      '#type' => 'radios',
      '#title' => t(''),
      '#options' => $options,
      '#default_value' => 'deposit_to_account',
    );
    
    $form['submit'] = array('#type' => 'submit',
                            '#value' => t('Apply Now'));
  }
  
  return $form;
}

function pdl2_account_deposit_referral_form_submit($form, &$form_state) {
  $pdl2 = pdl2_core_get_api();
  $action = $form_state["values"]["deposit_referral"];
  if ($action == "deposit_to_account")
  {
    $referralBalance = $pdl2->getReferralBalance();
    $status = $pdl2->setReferralDepositToAccount();
    if ($status)
    {
      drupal_set_message("The amount of $" . $referralBalance->balance . " has been deposited to your account.");
      drupal_goto("account/claimreferrals/senttoaccount"); // We need to reset some of the values that have been loaded earlier
    }
    else
      drupal_set_message("Could not deposit funds to your account at this time. Please try again later.", 'error');
  }
  else if ($action == "apply_to_balance")
  {
    $referralBalance = $pdl2->getReferralBalance();
    $status = $pdl2->setReferralApplyToLoanBalance();
    if ($status)
    {
      drupal_set_message("The amount of $" . $referralBalance->balance . " has been applied to your loan balance.");
      drupal_goto("account/claimreferrals/appliedtobalance"); // We need to reset some of the values that have been loaded earlier
    }
    else
      drupal_set_message("Could not apply funds to your loan balance at this time. Please try again later.", 'error');
  }
}  



function pdl2_account_referral_terms() {
  $pdl2 = pdl2_core_get_api();
  $company = $pdl2->getCompanyInfo();
  $siteName = pdl2_core_get_site_name();
  $themedata = array("company" => $company,
                     "siteName" => $siteName);
  $output = theme('account/referralterms', $themedata);
  return $output;
}


function pdl2_account_referral_cards() {
  drupal_add_css(drupal_get_path('module', 'pdl2_account').'/css/referralcards.css');
  $pdl2 = pdl2_core_get_api();
  $company = $pdl2->getCompanyInfo();
  $siteName = pdl2_core_get_site_name();
  $referralCode = $pdl2->getReferralCode();
  $themedata = array("company" => $company,
                     "referralCode" => $referralCode,
                     "siteName" => $siteName);
  $output = theme('account/referralcards', $themedata);
  return $output;
}
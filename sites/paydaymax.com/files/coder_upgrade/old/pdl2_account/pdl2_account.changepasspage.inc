<?php


function pdl2_account_changepass_page() {
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $output = drupal_get_form("pdl2_account_changepass_form");
  $output .= '<div class="return-to-account">' . l("<< Return to Account", "account") . '</div>';
  return $output;
}

function pdl2_account_changepass_form() {
  $form = array();
  $form["headermsg"] = array(
    "#value" => t("Please enter your new password (minimum of 6 characters)"));
  $form["password1"] = array("#type" => "password",
    "#title" => t("New password"),
    "#maxlength" => 255,
    "#required" => TRUE);
  $form["password2"] = array("#type" => "password",
    "#title" => t("New password (confirm)"),
    "#maxlength" => 255,
    "#required" => TRUE);

    $form['submit'] = array('#type' => 'submit',
      '#value' => t('Change Password'));

  return $form;
}

function pdl2_account_changepass_form_validate($form, &$form_state) {
  $regexp = preg_match('/^[A-Za-z0-9!@#$%^&*()]+$/', $form_state['values']['password1']);
  if($regexp != 1) {
    form_set_error('password1', t('New password must be at least 6 characters in length, a maximum of 32 and contain at least 1 digit'));
  }
  if ((strlen($form_state['values']['password1']) < 6) || (strlen($form_state['values']['password1']) > 32)) {
    form_set_error('password1', t('New password must be at least 6 characters in length, a maximum of 32 and contain at least 1 digit'));
  }
  if (strlen($form_state['values']['password1'] != $form_state['values']['password2'])) {
    form_set_error('password2', t('The two new passwords do not match'));
  }
}

function pdl2_account_changepass_form_submit($form, &$form_state) {

  $values = $form_state['values'];
  $newpass = $values["password1"];

  $pdl2 = pdl2_core_get_api();

  if ($_SESSION['chpass']) {
    // We are resetting a password though an email link
    $res = $pdl2->changeUserPassword($_SESSION['chpass']['client'],
                                     $newpass,
                                     $_SESSION['chpass']['hash']);
    if ($res->status == 1) {
      // would like to log the user in, BUT I cannot recover the users email
      // address without being logged into PDL2.... so just redirect to login.
      drupal_set_message(t("Please log in with the new password. You changed your password."));
      drupal_goto('account/login');
    }
    else {
      drupal_set_message(t('Error: Your password was NOT changed.'), 'error');
    }

    unset($_SESSION['chpass']);
    drupal_goto("account/login");
  }
  else {
    // Changing the passowrd while a user is logged in.
    // Get the users password hash
    $hash = $pdl2->getChangePasswordHash();
    // Change the password
    $changepass = $pdl2->changePassword($newpass, $hash);

    if (!$changepass->status) {
      drupal_set_message(t('Error: Your password was NOT changed.'), 'error');
      drupal_goto("account/password");
    }
    else {
      drupal_set_message(t("Your password has been changed"));
      drupal_goto("account");
    }
  }
}

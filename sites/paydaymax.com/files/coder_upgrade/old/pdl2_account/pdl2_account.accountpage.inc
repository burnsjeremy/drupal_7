<?php
function pdl2_account_account_page() {

//if login form from other pages are used
if ( isset($_SESSION['email']) && isset($_SESSION['password']) ){  
  //set from session
  $email = $_SESSION['email'];
  $password = $_SESSION['password'];

  // Let's try and login!!!
  $pdl2 = pdl2_core_get_api();
  $login = $pdl2->login($email, $password, 'client');

  if (empty($login)) {
    //unset login session if error
    unset($_SESSION['email']);
    unset($_SESSION['password']);
    drupal_set_message(t("An error occurred during login."), 'error');
    drupal_goto("account/login");
  }

  if ($login->userId > 0) {
    
    $isEligible = $pdl2->customerLoanPaydayIsEligible();

    if (!is_bool($isEligible) && $isEligible === PDL2Controller::NON_BUSINESS_STATE) {
      $company = $pdl2->getCompanyInfo();
      $address = $pdl2->getHomeAddress();
      $state = $pdl2->getState($address->address->state);
      $pdl2->logout();
      //unset login session if error
      unset($_SESSION['email']);
      unset($_SESSION['password']);
      drupal_set_message(t("At this time, %company is not offering loans in the state of %state", array('%company' => $company->company->name, '%state' => $state)), 'error');
      drupal_goto("account/login");
    }
    
  }
  else {
    if ($login->status->code == 901) {
      $chatinfo = $pdl2->getCompanyChatInfo($email);
      $livechatlink = $chatinfo->chat->url;
      //unset login session if error
      unset($_SESSION['email']);
      unset($_SESSION['password']);
      drupal_set_message(t($login->status->detail . "<br/><br/>If you would like to chat to one of our friendly account representatives, please click " . l("here", $livechatlink)), 'error');
      drupal_goto("account/login");
    }
    else {
      //unset login session
      unset($_SESSION['email']);
      unset($_SESSION['password']);
      drupal_set_message(t("Incorrect username/password, try again!"), 'error');
      drupal_goto("account/login");
    }
  }
  
  if (isset($_SESSION["jumpto"]))
  {
    $jumpurl = $_SESSION["jumpto"];
    unset($_SESSION["jumpto"]);
    drupal_goto($jumpurl);
  }
  
}else{
  //not logged in logic, go to login pages or opted out
  // First let's see if we have an existing session, if above login doesnt work
  if (!pdl2_account_is_logged_in()) {
      drupal_goto("account/login");
  }
  //TODO - may have to move this to above check
  elseif (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
  
}

//continue on to account page  
  $_SESSION["existinguser"] = 1;
  
  $pdl2 = pdl2_core_get_api();

  $loanId = $pdl2->getOpenLoan();
  if($loanId) {
    $loanType = $pdl2->getLoanType($loanId);
    if ($loanType == 'payday') {
				$loan = $pdl2->getLoanById($loanId);
		} else {
				$loan = $pdl2->loanPersonalGetById($loanId)->loan;
				try {
					$personalLoanRefinanceElegible = $pdl2->loanPersonalRefinanceIsLoanEligible($loanId);
				} catch (Exception $e) {
					$personalLoanElegible = false;
				}
		}

    // TODO: there is no logic in figuring out why we show this. Need to try and fix this.
    $history = $pdl2->getCustomerLoanHistory($loanType);
    $tmpmsg = $history->result[0]->loanHistory->loan->loan->messages->customer_loan_summary;
    if ($tmpmsg) {
      $currentBalance = $tmpmsg;
    }
    else {
      $currentBalance = $pdl2->getLoanBalance($loanId, $loanType);
    }
  }

  $userInfo = $pdl2->getUserInfo()->customer;
  try {
    $personalLoanElegible = $pdl2->customerLoanPersonalIsEligible();
  } catch (Exception $e) {
    $personalLoanElegible = false;
  }
  $personalHistory = $pdl2->getCustomerLoanHistory('personal');
  $memberStatus = $pdl2->getPreferredMemberStatus();

  $hasPersonalHistory = ($personalHistory->result != null);

  if ($loan && $loan->messages && $loan->messages->result) {
    drupal_set_message($loan->messages->result);
  } else if ($loan && $loan->loan && $loan->loan->messages && $loan->loan->messages->customer_loan_summary) {
    drupal_set_message($loan->loan->messages->customer_loan_summary);
    $showZeroBalance = true;
  }
  
  $allowExtension = $pdl2->allowExtension();
  $allowCCPayment = $pdl2->allowCCPayment();
  $dueDate = $loan->due_date->date;  
    
  if ($loanType == "personal")
  {
   $due_dates = $pdl2->customerLoanPersonalGetDueDates();
   foreach ($due_dates as $date)
   {
     if (date_create($date->date) > date_create())
       break;
   }
   if ($date != null)
     $dueDate = $date->date;
  }
  
	$verification_required = $pdl2->customerTwoStepVerificationIsVerificationRequired();

  $themedata = array('loan' => $loan,
                     'loanType' => $loanType,
                     'dueDate' => $showZeroBalance ? "N/A" : new DateTime($dueDate),
                     'userInfo' => $userInfo,
                     'personalLoanElegible' => $personalLoanElegible,
										 'personalLoanRefinanceElegible' => $personalLoanRefinanceElegible,
                     'personalHistory' => $personalHistory,
                     'memberStatus' => $memberStatus,
                     'currentBalance' => $showZeroBalance ? "$0.00" : $currentBalance,
                     'allowExtension' => $allowExtension,
                     'allowCCPayment' => $allowCCPayment,
                     'hasPersonalHistory' => $hasPersonalHistory,
                     'verification_required' => $verification_required
                );

  $output = theme('accountpage', $themedata);
  return $output;
}


function pdl2_account_getCurrentBalance($loan) {
  $currentBalance = 0;

  if (empty($loan)) {
    return $currentBalance;
  }

  foreach ($loan->loanTransactions as $keyTransaction => $valTransaction) {
    if ($valTransaction->status == 1 && $valTransaction->trType != 3) {
      if ($valTransaction->returnAmount > 0) {
        return t('Contact Credit Protection Depot at 1-866-645-1661 with any questions you might have on your balance');
      }
      $currentBalance += $valTransaction->origAmount;
    }
  }
  return (float) $currentBalance;
}

jq(document).ready(function ()
{
		
	//
	//Tiny jQuery plugin to check if element exist 
	//
	//
	//usage:    $('div.test').exists(function() {
	//				this.append('<p>I exist!</p>');
	//			});
	//
	//
	$.fn.exists = function (callback)
	{
		var args = [].slice.call(arguments, 1);
		if (this.length)
		{
			callback.call(this, args);
		}
		return this;
	};

	var p;
	//Next adds focus if box is full until last box


	function next(i)
	{
		return function ()
		{
			p[i].value = p[i].value.replace(/[^0-9]/g, "");
			if (p[i].value.length == p[i].size && i < p.length && (p[i].size < 4 || p[i].id == 'edit-workverify3' || p[i].id == 'edit-workphone3' ) )
			{
				p[i + 1].focus();
			}

		};
	}
	//Back adds focus when user deletes


	function back(i)
	{
		return function (e)
		{
			e=e || window.event;
			//var keyCode = (window.event) ? e.which : e.keyCode;
			
			if (e.keyCode == 8 && p[i].value.length == 0 && i > 0)
			{
				p[i - 1].focus();
				var val = p[i].value; //store the value of the element
				p[i].value = ''; //clear the value of the element
				p[i].value = val; //set that value back.
			}
		};
	}
	$('.autotabs').exists(function ()
	{
		//onload start looping through 3 inputs 
		window.onload = function ()
		{
			//what to loop through
			p = jq('.autotabs input[type=text]');
			//keep looping through user input, and run function depending on keypress
			for (var i = 0; i < p.length; i++)
			{
				p[i].onkeyup = next(i);
				p[i].onkeydown = back(i);
			}
		};
	});

});
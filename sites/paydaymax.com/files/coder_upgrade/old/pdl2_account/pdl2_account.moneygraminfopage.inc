<?php

function pdl2_account_moneygraminfo_page() {

    // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
      drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
  else {

    $themedata = array();

    $pdl2 = pdl2_core_get_api();
    $userInfo = $pdl2->getUserInfo()->customer;
    $mg_allowed = $pdl2->allowMoneyGram();
    $mg_max = $pdl2->getMoneyGramMaximum();
    $themedata = array('allowed' => $mg_allowed,
                       'userInfo' => $userInfo,
                       'max' => $mg_max);
    $output = theme('account/moneygram', $themedata);
    return $output;
  }
}
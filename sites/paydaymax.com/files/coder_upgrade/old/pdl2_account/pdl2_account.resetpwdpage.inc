<?php

// Send the email with the hash to allow the user to click a link to reset his password.
function pdl2_account_reset_password_page() {
  $output = drupal_get_form("pdl2_account_reset_password_form");
  return $output;
}

function pdl2_account_reset_password_form() {
  $form = array();
  $form["headermsg"] = array("#value" => t("Please enter your email address so we can send you a new password"));
  $form["entered_email"] = array("#type" => "textfield",
                                 "#title" => t("Email"),
                                 "#maxlength" => 255,
                                 "#required" => TRUE);

    $form['submit'] = array('#type' => 'submit',
                            '#value' => t('Reset Password'));
  return $form;
}

function pdl2_reset_pwd_form_validate($form, &$form_state) {
}

function pdl2_account_reset_password_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $email = $values["entered_email"];
  $pdl2 = pdl2_core_get_api();
  $res = $pdl2->sendResetPassword($email) ;

  if ($res == 1) {
    drupal_set_message(t("Details on how to reset your password have been sent to your email address."));
    drupal_goto('account');
  }
  else {
    drupal_set_message(t("Invalid email address - please try again."), 'error');
  }
}


// do a reset password - the hash is in the url
function pdl2_account_reset_password_with_hash_page() {
  $pdl2 = pdl2_core_get_api();
  //hash value is in the url clicked from the reset email.
  $hash = $_GET['ui'];
  $emailhash = $_GET["em"];
  $clientId = $pdl2->getFromEmailHash($emailhash);
  $_SESSION['chpass'] = array();
  $_SESSION['chpass']['hash'] = strtoupper($hash);
  $_SESSION['chpass']['client'] = $clientId;

  include('pdl2_account.changepasspage.inc');
  return drupal_get_form('pdl2_account_changepass_form');
}

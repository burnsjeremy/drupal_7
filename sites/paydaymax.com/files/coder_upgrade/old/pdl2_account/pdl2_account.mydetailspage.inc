<?php

function pdl2_account_mydetails_page() {

  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
    drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }

  $pdl2 = pdl2_core_get_api();
  $details = $pdl2->getUserInfo();
  $details->customer->ssn = $pdl2->getSsn();
  $hasEditRights = $pdl2->checkUserEditRights();
  $ba = $pdl2->getBankAccount();
  $dob =  $pdl2->getDateOfBirth();
  $dlicense =  $pdl2->getDriversLicense();
  $hphone = $pdl2->getHomePhone()->phone->number;
  $cphone = $pdl2->getCellPhone()->phone->number;
  $hfax = $pdl2->getHomeFax();
  $residency = $pdl2->getResidencyType();
  $emp_address = $pdl2->getEmploymentAddress();
  $income_source = $pdl2->getIncomeSourceType();
  $employer_name = $pdl2->getEmployerName();
  $employ_department = $pdl2->getEmploymentDepartment();
  $employ_startdate = $pdl2->getEmploymentStartDate();
  $employ_phones = $pdl2->getEmploymentPhones();
  $pay_freq = $pdl2->customerGetPayFrequency();
  
  $themedata = array("userDetails" => $details,
                     "hasEditRights" => $hasEditRights,
                     "bankDetails" =>$ba,
                     "dob" =>$dob,
                     "dlicense" =>$dlicense,
                     "hphone" =>$hphone,
                     "cphone" =>$cphone,
                     "hfax" =>$hfax,
                     "residency" =>$residency,
                     "emp_address" =>$emp_address,
                     "income_source" =>$income_source,
                     "employer_name" =>$employer_name,
                     "employment_department" =>$employ_department,
                     "employment_startdate" =>$employ_startdate,
                     "employment_phones" =>$employ_phones,
                     "pay_frequency" => $pay_freq,
                      );

  if ($pdl2->getOpenLoan() != null) {
    $output = theme('account/mydetails', $themedata);
    return $output;
  }
  else {
    drupal_add_js(drupal_get_path('module', 'pdl2_account') .'/js/account.js');
    $output = drupal_get_form('pdl2_account_my_details_form', $themedata);
     $output .= '<div class="clear"></div><div class="return-to-account">' . l("<< Return to Account", "account") . '</div>';
    return $output;
  }
}


function pdl2_account_my_details_form($form_state, $themedata) {
  $form = array();
  $form['style'] = array(
    '#prefix' => '<div id="ApplicationArea"><div class="my-details">',
    '#suffix' => '</div></div>'
  );
  $form['style']['title'] = array(
    '#type' => 'markup',
    '#value' => t('Personal Information'),
    '#prefix' => '<h1 class="clean">',
    '#suffix' => '</h1>',
  );
  $form['style']['general'] = array(
    '#type' => 'markup',
    '#value' => t('General'),
    '#prefix' => '<h2 class="clean">',
    '#suffix' => '</h2><div class="borders"></div>',
  );
  $form['style']['personinfofieldset']['namelabel'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Name:',
    '#suffix' => '</span>',
  );
  $form['style']['personinfofieldset']['name'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  $themedata['userDetails']->customer->first_name." ".$themedata['userDetails']->customer->last_name,
    '#suffix' => '</span></div><div class="borders"></div>',
  );
  $form['style']['personinfofieldset']['dobtitle'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Date Of Birth:',
    '#suffix' => '</span>',
  );
  $form['style']['personinfofieldset']['dob'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  $themedata['dob']->date,
    '#suffix' => '</span></div><div class="borders"></div>',
  );
  $form['style']['personinfofieldset']['homestreet1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address 1:'),
    //'#required' => true,
    '#default_value' => $themedata['userDetails']->customer->home_address->address->street_primary,
    //'#description' => '<div class="tooltip" title="'.t('Please tell us your current home address.').'"></div>',
    '#suffix' => '<div class="borders"></div>',
  );
  $form['style']['personinfofieldset']['homestreet2'] = array(
    '#type' => 'textfield',
    '#required' => false,
    '#title' => 'Address 2:',
    '#default_value' => $themedata['userDetails']->customer->home_address->address->street_secondary,
    '#suffix' => '<div class="borders"></div>',
  );
  $form['style']['personinfofieldset']['homecity'] = array(
    '#type' => 'textfield',
    '#title' => t('City:'),
    //'#required' => true,
    '#default_value' => $themedata['userDetails']->customer->home_address->address->city,
    //'#description' => '<div class="tooltip" title="'.t('Please tell us the city you currently reside in.').'"></div>',
    '#suffix' => '<div class="borders"></div>',
  );
  $pdl2 = pdl2_core_get_api();
  $states = listsHelper::idNameList($pdl2->getStates()->result);
  //array_unshift($states, array('' => 'Select a State'));
  $form['style']['personinfofieldset']['homestate'] = array(
    '#type' => 'select',
    '#title' => t("State:"),
    //'#required' => true,
    '#default_value' => $themedata['userDetails']->customer->home_address->address->state,
    '#options' => array('' => '-Select a State-') + $states,
    '#suffix' => '<div class="borders"></div>',
    //'#description' => '<div class="tooltip" title="'.t('Please tell us the state in which your license was issued.').'"></div>',
  );
  $form['style']['personinfofieldset']['homezip'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip Code:'),
    //'#required' => true,
    '#size' => 6,
    '#maxlength' => 5,
    '#default_value' => $themedata['userDetails']->customer->home_address->address->zip,
    //'#description' => '<div class="tooltip" title="'.t('Please tell us your local zip code.').'"></div>',
    '#suffix' => '<div class="borders"></div>',
  );

  //Commenting this out for now b/c it doesnt exsist on sustaining
  // not everyone has a mailing address
  /*if ($themedata['userDetails']->customer->mail_address) {
    $form['style']['personinfofieldset']['homemail'] = array(
      '#type' => 'fieldset',
      '#title' => t('Mailing Address'),
      '#attributes' => array('class' => 'mailing_fs'),
    );
    $form['style']['personinfofieldset']['homemail']['homemailaddr'] = array(
      '#type' => 'textfield',
      '#title' => t('Mailing Address'),
      '#default_value' => $themedata['userDetails']->customer->mail_address->address->street_primary,
    );
    $form['style']['personinfofieldset']['homemail']['homemailcity'] = array(
      '#type' => 'textfield',
      '#title' => t('City'),
      '#default_value' =>  $themedata['userDetails']->customer->mail_address->address->city,
    );
    $form['style']['personinfofieldset']['homemail']['homemailzip'] = array(
      '#type' => 'textfield',
      '#title' => t('Zip Code'),
      '#size' => 6,
      '#maxlength' => 5,
      '#default_value' => $themedata['userDetails']->customer->mail_address->address->zip,
      '#attributes' => array('class' => 'zip'),
    );
  }*/

  $form['style']['personinfofieldset']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-Mail:'),
    //'#required' => true,
    '#default_value' => $themedata['userDetails']->customer->email->emailaddress->result,
    '#suffix' => '<div class="borders"></div>',
  );
  $form['style']['personinfofieldset']['namelabel'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Name:',
    '#suffix' => '</span>',
  );
  $form['style']['personinfofieldset']['name'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  $themedata['userDetails']->customer->first_name." ".$themedata['userDetails']->customer->last_name,
    '#suffix' => '</span></div><div class="borders"></div>',
  );
  $form['style']['personinfofieldset']['ssn_label'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'SSN:',
    '#suffix' => '</span>',
  );
  $form['style']['personinfofieldset']['ssn'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#suffix' => '</span></div>',
    '#value' => $themedata['userDetails']->customer->ssn,
  );
  // Drivers License.
  $form['style']['driverslicense_title'] = array(
    '#type' => 'markup',
    '#value' => t('Drivers License'),
    '#prefix' => '<h2 class="clean">',
    '#suffix' => '</h2><div class="borders"></div>',
  );
  $form['style']['driverslicense']['drivers_license_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Number:'),
    //'#required' => true,
    '#default_value' => $themedata['dlicense']->number,
    '#suffix' => '<div class="borders"></div>',
  );
  $pdl2 = pdl2_core_get_api();
  $states = listsHelper::idNameList($pdl2->getStates()->result);
  //array_unshift($states, array('' => 'Select a State'));
  $form['style']['driverslicense']['driverstate'] = array(
    '#type' => 'select',
    '#title' => t("State Issued:"),
    //'#required' => true,
    '#default_value' => $themedata['dlicense']->state,
    '#options' => array('' => '-Select a State-') + $states,
    //'#description' => '<div class="tooltip" title="'.t('Please tell us the state in which your license was issued.').'"></div>',
  );
  // Personal Phones
  $form['style']['personalphones_title'] = array(
    '#type' => 'markup',
    '#value' => t('Personal Phones'),
    '#prefix' => '<h2 class="clean">',
    '#suffix' => '</h2><div class="borders"></div>',
  );
  $home_phone = $themedata['userDetails']->customer->phonenumbers->phone_numbers->homephone;
  $home_phone_digits = explode("-", $home_phone);
  $form['style']['personalphones1']['inline_auto'] = array(
      '#prefix' => '<div class="inline phone autotabs">',
      '#suffix' => '</div>'
  );
  $form['style']['personalphones1']['inline_auto']['home_phone_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Home:'),
    //'#required' => true,
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $home_phone_digits[0],
  );
  $form['style']['personalphones1']['inline_auto']['home_phone_2'] = array(
    '#type' => 'textfield',
    //'#required' => true,
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $home_phone_digits[1],
  );
  $form['style']['personalphones1']['inline_auto']['home_phone_3'] = array(
    '#type' => 'textfield',
    //'#required' => true,
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => $home_phone_digits[2],
  );
  $cell_phone = $themedata['userDetails']->customer->phonenumbers->phone_numbers->cellphone;
  $cell_phone_digits = explode("-", $cell_phone);
  $form['style']['personalphones2']['inline_auto'] = array(
      '#prefix' => '<div class="inline phone autotabs borders">',
      '#suffix' => '</div>'
  );
  $form['style']['personalphones2']['inline_auto']['cell_phone_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Cell:'),
    //'#required' => true,
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $cell_phone_digits[0],
  );
  $form['style']['personalphones2']['inline_auto']['cell_phone_2'] = array(
    '#type' => 'textfield',
    //'#required' => true,
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $cell_phone_digits[1],
  );
  $form['style']['personalphones2']['inline_auto']['cell_phone_3'] = array(
    '#type' => 'textfield',
    //'#required' => true,
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => $cell_phone_digits[2],
  );
  // residency
  $form['style']['residency_title'] = array(
    '#type' => 'markup',
    '#value' => t('Residency'),
    '#prefix' => '<h2 class="clean">',
    '#suffix' => '</h2><div class="borders"></div>',
  );
  $residency_array = array(
    'rent' => 'Rent',
    'own' => 'Own',
  );
  $form['style']['residency'] = array(
    '#type' => 'select',
    '#title' => t("Rent Or Own:"),
    //'#required' => true,
    '#default_value' => $themedata['residency'],
    '#options' => $residency_array,
  );
  // Income/Employment
  $form['style']['income_employment_title'] = array(
    '#type' => 'markup',
    '#value' => t('Income/Employment'),
    '#prefix' => '<h2 class="clean">',
    '#suffix' => '</h2><div class="borders"></div>',
  );
  $incomeSources = array(
    'employment' => 'Employment',
    'retirement' => 'Retirement',
    'disability' => 'Disability',
    'self-employment' => 'Self Employment',
    'active-military' => 'Active Military',
    'unemployed' => 'Unemployment',
    'other' => 'Other',
  );
  $form['style']['income_employment']['incomesource'] = array(
    '#type' => 'select',
    //'#required' => true,
    '#title' => t('Income Source:'),
    '#default_value' => $themedata['income_source'],
    '#options' => $incomeSources,
    //'#description' => '<div class="tooltip" title="'.t('Please tell us your main source of income.').'"></div>',
    '#suffix' => '<div class="borders"></div>',
  );
  $form['style']['income_employment']['employer'] = array(
    '#type' => 'textfield',
    '#title' => t('Employer:'),
    //'#required' => true,
    '#default_value' => $themedata['employer_name'],
    '#suffix' => '<div class="borders"></div>',
  );
  $form['style']['income_employment']['department'] = array(
    '#type' => 'textfield',
    '#title' => t('Department:'),
    //'#required' => true,
    '#default_value' => $themedata['employment_department'],
    '#suffix' => '<div class="borders"></div>',
  );
  $form['style']['income_employment']['workaddress1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address 1:'),
    //'#required' => true,
    '#default_value' => $themedata['emp_address']->street_primary,
    '#suffix' => '<div class="borders"></div>',
  );
  $form['style']['income_employment']['workaddress2'] = array(
    '#type' => 'textfield',
    '#required' => false,
    '#title' => t('Address 2:'),
    '#default_value' => $themedata['emp_address']->street_secondary,
    '#suffix' => '<div class="borders"></div>',
  );
  $form['style']['income_employment']['workcity'] = array(
    '#type' => 'textfield',
    '#title' => t('City:'),
    //'#required' => true,
    '#default_value' => $themedata['emp_address']->city,
    '#suffix' => '<div class="borders"></div>',
  );
  $pdl2 = pdl2_core_get_api();
  $states = listsHelper::idNameList($pdl2->getStates()->result);
  //array_unshift($states, array('' => 'Select a State'));
  $form['style']['income_employment']['workstate'] = array(
    '#type' => 'select',
    '#title' => t("State:"),
    //'#required' => true,
    '#default_value' => $themedata['emp_address']->state,
    '#options' => array('' => '-Select a State-') + $states,
    '#suffix' => '<div class="borders"></div>',
    //'#description' => '<div class="tooltip" title="'.t('Please tell us the state in which your license was issued.').'"></div>',
  );
  $form['style']['income_employment']['workzip'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip Code:'),
    //'#required' => true,
    '#size' => 6,
    '#maxlength' => 5,
    '#default_value' => $themedata['emp_address']->zip,
    '#suffix' => '<div class="borders"></div>',
  );
  $months = array(
    '01' => '01',
    '02' => '02',
    '03' => '03',
    '04' => '04',
    '05' => '05',
    '06' => '06',
    '07' => '07',
    '08' => '08',
    '09' => '09',
    '10' => '10',
    '11' => '11',
    '12' => '12',
  );
  $years = array();
  $y = date("Y");
  $start_date = $themedata['employment_startdate'];
  $start_dates = explode("-", $start_date);
  for ($i = 0 ; $i <= 20 ; $i++) {
    $years[$y-$i] = $y - $i;
  }
  
  $form['style']['income_employment']['inline'] = array(
    '#prefix' => '<div class="inline month-year-inline">',
    '#suffix' => '</div>',
  
  );
  $form['style']['income_employment']['inline']['since_month'] = array(
    '#type' => 'select',
    //'#required' => true,
    '#title' => ''.t('Started:').'<span class="work-month">Month:</span>',
    '#default_value' => $start_dates[1],
    '#options' => $months,
    //'#prefix' => '<div class="">Month</div>',
    //'#suffix' => '',
  );
  $form['style']['income_employment']['inline']['since_year'] = array(
    '#type' => 'select',
    //'#required' => true,
    '#title' => '<span class="work-year">'.t('/Year:').'</span>',
    '#default_value' => $start_dates[0],
    '#options' => $years,
    //'#prefix' => '<div class=""></div>',
  );
  // work phones
  $form['style']['income_employment_phones_title'] = array(
    '#type' => 'markup',
    '#value' => t('Work Phones'),
    '#prefix' => '<h2 class="clean">',
    '#suffix' => '</h2><div class="borders"></div>',
  );
  $p_work = $themedata['employment_phones']->phone_numbers->workphone;
  $p_work_ext = explode("x", $p_work);
  $p_work_digits = explode("-", $p_work_ext[0]);
  $form['style']['income_employment_phones1']['inline_auto'] = array(
      '#prefix' => '<div class="inline phone autotabs">',
      '#suffix' => '</div>'
  );
  $form['style']['income_employment_phones1']['inline_auto']['main_phone_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Main:'),
    //'#required' => true,
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $p_work_digits[0],
  );
  $form['style']['income_employment_phones1']['inline_auto']['main_phone_2'] = array(
    '#type' => 'textfield',
    //'#required' => true,
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $p_work_digits[1],
  );
  $form['style']['income_employment_phones1']['inline_auto']['main_phone_3'] = array(
    '#type' => 'textfield',
    //'#required' => true,
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => $p_work_digits[2],
  );
  $form['style']['income_employment_phones1']['inline_auto']['main_phone_ext'] = array(
    '#type' => 'textfield',
    '#title' => t('Ext'),
    '#required' => false,
    '#size' => 6,
    '#maxlength' => 6,
    '#default_value' => $p_work_ext[1],
    '#prefix' => '<div class="short-label">',
    '#suffix' => '</div>',
  );
  $p_verification = $themedata['employment_phones']->phone_numbers->workpager;
  $p_verification_ext = explode("x", $p_verification);
  $p_verification_digits = explode("-", $p_verification_ext[0]);
  $form['style']['income_employment_phones2']['inline_auto'] = array(
      '#prefix' => '<div class="inline phone autotabs borders">',
      '#suffix' => '</div>'
  );
  $form['style']['income_employment_phones2']['inline_auto']['ver_phone_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Verification:'),
    //'#required' => true,
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $p_verification_digits[0],
  );
  $form['style']['income_employment_phones2']['inline_auto']['ver_phone_2'] = array(
    '#type' => 'textfield',
    //'#required' => true,
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $p_verification_digits[1],
  );
  $form['style']['income_employment_phones2']['inline_auto']['ver_phone_3'] = array(
    '#type' => 'textfield',
    //'#required' => true,
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => $p_verification_digits[2],
  );
  $form['style']['income_employment_phones2']['inline_auto']['ver_phone_ext'] = array(
    '#type' => 'textfield',
    '#title' => t('Ext'),
    '#required' => false,
    '#size' => 6,
    '#maxlength' => 6,
    '#default_value' => $p_verification_ext[1],
    '#prefix' => '<div class="short-label">',
    '#suffix' => '</div>',
  );
  // Pay Info
  $form['style']['payinfo_title'] = array(
    '#type' => 'markup',
    '#value' => t('Pay Info'),
    '#prefix' => '<h2 class="clean">',
    '#suffix' => '</h2><div class="borders"></div>',
  );
  $form['style']['payinfo']['monthlypay_label'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Monthly Pay:',
    '#suffix' => '</span>',
  );
  $form['style']['payinfo']['monthlypay_value'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  "$". $themedata['pay_frequency']->PayFrequency->montly_pay_amount,
    '#suffix' => '</span></div><div class="borders"></div>',
  );
  $form['style']['payinfo']['ddeposit_label'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Direct Deposit:',
    '#suffix' => '</span>',
  );
  $dd = ($themedata['pay_frequency']->PayFrequency->is_direct_deposit == 1) ? "Yes" : "No";
  $form['style']['payinfo']['ddeposit_value'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  $dd,
    '#suffix' => '</span></div><div class="borders"></div>',
  );
  $form['style']['payinfo']['payday_label'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Pay Day:',
    '#suffix' => '</span>',
  );
  $form['style']['payinfo']['payday'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  $themedata['pay_frequency']->PayFrequency->payday_name,
    '#suffix' => '</span></div>',
  );
  // Bank Info
  $form['style']['bankinfo_title'] = array(
    '#type' => 'markup',
    '#value' => t('Bank Info'),
    '#prefix' => '<h2 class="clean">',
    '#suffix' => '</h2><div class="borders"></div>',
  );
  $form['style']['bankinfo']['bankname_label'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Name:',
    '#suffix' => '</span>',
  );
  $form['style']['bankinfo']['bankname'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  $themedata['bankDetails']->bank_account->bank_name,
    '#suffix' => '</span></div><div class="borders"></div>',
  );
  $form['style']['bankinfo']['bankphone_label'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Phone:',
    '#suffix' => '</span>',
  );
  $form['style']['bankinfo']['bankphone'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  $themedata['bankDetails']->bank_account->bank_phone,
    '#suffix' => '</span></div><div class="borders"></div>'
  );
  $form['style']['bankinfo']['bankrouting_label'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Routing #:',
    '#suffix' => '</span>',
  );
  $form['style']['bankinfo']['bankrouting'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  $themedata['bankDetails']->bank_account->routing_number,
    '#suffix' => '</span></div><div class="borders"></div>',
  );
  $form['style']['bankinfo']['bankaccount_label'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="no-fields"><span class="label">',
    '#value' =>  'Account #:',
    '#suffix' => '</span>',
  );
  $form['style']['bankinfo']['bankaccount'] = array(
    '#type' => 'markup',
    '#prefix' => '<span class="data">',
    '#value' =>  $themedata['bankDetails']->bank_account->account_number,
    '#suffix' => '</span></div>',
  );
  
  
  $form['style']['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#prefix' => '<div class="clear"></div>',
  );

  return $form;
}


function pdl2_account_my_details_form_validate($form, &$form_state) {
  $validateAlphaNum = new Zend_Validate_Alnum(true);
  $validatePhone     = new Pdl2_Validate_PhoneNumber();
  $validateInt      = new Zend_Validate_Int();
  $validateEmail    = new Pdl2_Validate_EmailAddress();

  $homeStreet1 = $form_state['values']['homestreet1'];
  if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $homeStreet1)) {
    form_set_error('homestreet1', "Address : Please enter a valid address (letters, numbers, spaces, dots, dashes and apostrophes only)");
  }

  $homeStreet2 = $form_state['values']['homestreet2'];
  if ($homeStreet2) {
    if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $homeStreet2)) {
      form_set_error('homestreet2', "Address line 2 : Please enter a valid address (letters, numbers, spaces, dots, dashes  and apostrophes only)");
    }
  }

  $homeCity = $form_state['values']['homecity'];
  if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $homeCity)) {
    form_set_error('homecity', "City : Please enter a valid city (letters, numbers, spaces, dots, dashes and apostrophes only)");
  }

  $homeZip = $form_state['values']['homezip'];
  if (!preg_match('/[0-9]{5}/', $homeZip)) {
    form_set_error('homezip', "ZIP : Your zip code is invalid. Please check it carefully.");
  }
  
  // If seperate mailing address has values - validate.
  if (!empty($form_state['values']['homemailaddr']) ||
      !empty($form_state['values']['homemailcity']) ||
      !empty($form_state['values']['homemailzip'])) {

    $homeMailAddress = $form_state['values']['homemailaddr'];
    if (empty($homeMailAddress)) {
      form_set_error('homemailaddr', "Mailing Address is required if you use a separate mailing address.");
    } else if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $homeMailAddress)) {
      form_set_error('homemailaddr', "Mailing Address : Please enter a valid address (letters, numbers, spaces, dots, dashes and apostrophes only)");
    }

    $homeMailCity = $form_state['values']['homemailcity'];
    if (empty($homeMailCity)) {
      form_set_error('homemailcity', "Mailing City is required if you use a separate mailing address.");
    } else if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $homeMailCity)) {
      form_set_error('homemailcity', "Mailing City : Please enter a valid city (letters, numbers, spaces, dots, dashes and apostrophes only)");
    }
    
    $homeMailZip = $form_state['values']['homemailzip'];
    if (empty($homeMailZip)) {
      $message = 'Mailing ZIP is required if you use a separate mailing address.';
    } else if (!preg_match('/[0-9]{5}/', $homeMailZip)) {
      form_set_error('homemailzip', "Mailing ZIP : Your zip code is invalid. Please check it carefully.");
    }
  }

  // email
  $email = $form_state['values']['email'];
  // PHP 5.3.5 (for MAMP) has a bug that causes the zend isValid() to time out
  // as a temporary solution - don't validate emails (for test environments) that have
  // PHP 5.3.5 installed.
  // This has zero effect on production.
  // Lee Walker  1/18/13
  global $base_url;
  if ((phpversion() != "5.3.5") && ($base_url == "http://mcn.localhost")) {
    if (!$validateEmail->isValid($email)) {
      form_set_error('email', "Email : " . implode(',', $valdateEmail->isValid()));
    }
  }

  $driverLicenseNum = $form_state['values']['drivers_license_number'];
  if (!preg_match('/^[0-9A-Za-z-]+$/', $driverLicenseNum)) {
    form_set_error('drivers_license_number', "Drivers License : Please enter a valid driver license number (letters, numbers and dashes only)");
  }


  // home phone
  $homePhone = $form_state['values']['home_phone_1'] . $form_state['values']['home_phone_2'] . $form_state['values']['home_phone_3'];
  if (!$validatePhone->isValid($homePhone)) {
    $message = implode(',', $validatePhone->getMessages());
    form_set_error('home_phone_1', "Home Phone : " . $message);
  }
  $p1 = $form_state['values']['home_phone_1'];
  $p2 = $form_state['values']['home_phone_2'];
  $p3 = $form_state['values']['home_phone_3'];
  if (strlen($p1) != 3) {
    form_set_error('home_phone_1', "Home Phone1 : Must be 3 digits");
  }
  if (strlen($p2) != 3) {
    form_set_error('home_phone_2', "Home Phone2 : Must be 3 digits");
  }
  if (strlen($p3) != 4) {
    form_set_error('home_phone_3', "Home Phone3 : Must be 4 digits");
  }


  // Cell Phone
  $cellPhone = $form_state['values']['cell_phone_1'] . $form_state['values']['cell_phone_2'] . $form_state['values']['cell_phone_3'];
  if ($cellPhone != "")
  {
    if (!$validatePhone->isValid($cellPhone)) {
      $message = implode(',', $validatePhone->getMessages());
      form_set_error('cell_phone_1', "Cell Phone : " . $message);
    }
    $p1 = $form_state['values']['cell_phone_1'];
    $p2 = $form_state['values']['cell_phone_2'];
    $p3 = $form_state['values']['cell_phone_3'];
    if (strlen($p1) != 3) {
      form_set_error('cell_phone_1', "Cell Phone1 : Must be 3 digits");
    }
    if (strlen($p2) != 3) {
      form_set_error('cell_phone_2', "Cell Phone2 : Must be 3 digits");
    }
    if (strlen($p3) != 4) {
      form_set_error('cell_phone_3', "Cell Phone3 : Must be 4 digits");
    }
  }
  
  if ($form_state['values']['incomesource'] == "employment")
  {
    $employer = $form_state['values']['employer'];
    if (!preg_match('/^[0-9A-Za-z- .]+$/', $employer)) {
      form_set_error('employer', "Place of employment : Please enter a valid employer (letters, numbers, spaces, dots, and dashes only)");
    }

    $position = $form_state['values']['department'];
    if (!preg_match('/^[0-9A-Za-z- .]+$/', $position)) {
      form_set_error('department', "Department : Please enter a valid department (letters, spaces, dots, and dashes only)");
    }

    $workstreet1 = $form_state['values']['workaddress1'];
    if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $workstreet1)) {
      form_set_error('workaddress1', "Address : Please enter a valid address (letters, numbers, spaces, dots, dashes and apostrophes only)");
    }

    $workstreet2 = $form_state['values']['workaddress2'];
    if ($workstreet2 != "") {
      if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $workstreet2)) {
        form_set_error('workaddress2', "Address line 2 : Please enter a valid address (letters, numbers, spaces, dots, dashes and apostrophes only)");
      }
    }

    $city = $form_state['values']['workcity'];
    if (!preg_match('/^[A-Za-z .]+$/', $city)) {
      form_set_error('workcity', "City : Please enter a valid city (letters, spaces, and dots only)");
    }

    $zip = $form_state['values']['workzip'];
    if (!preg_match('/[0-9]{5}/', $zip)) {
      form_set_error('workzip', "Work ZIP : Please enter a valid 5 digit ZIP code");
    }
  
  
    // work phone
    $main_phone = $form_state['values']['main_phone_1'] . $form_state['values']['main_phone_2'] . $form_state['values']['main_phone_3'];
    if (!$validatePhone->isValid($main_phone)) {
      $message = implode(',', $validatePhone->getMessages());
      form_set_error('main_phone_1', "Work Phone : " . $message);
    }
    $p1 = $form_state['values']['main_phone_1'];
    $p2 = $form_state['values']['main_phone_2'];
    $p3 = $form_state['values']['main_phone_3'];
    if (strlen($p1) != 3) {
      form_set_error('main_phone_1', "Work Phone1 : Must be 3 digits");
    }
    if (strlen($p2) != 3) {
      form_set_error('main_phone_2', "Work Phone2 : Must be 3 digits");
    }
    if (strlen($p3) != 4) {
      form_set_error('main_phone_3', "Work Phone3 : Must be 4 digits");
    }
    if ($form_state['values']['main_phone_ext']) {
      if (!$validateInt->isValid($form_state['values']['main_phone_ext'])) {
        $message = implode(',', $validateInt->getMessages());
        form_set_error('main_phone_ext', "Work Phone Ext : " . $message);
      }
    }
  
  
    // verification phone
    $ver_phone = $form_state['values']['ver_phone_1'] . $form_state['values']['ver_phone_2'] . $form_state['values']['ver_phone_3'];
    if (!$validatePhone->isValid($ver_phone)) {
      $message = implode(',', $validatePhone->getMessages());
      form_set_error('ver_phone_1', "Work Phone : " . $message);
    }
    $p1 = $form_state['values']['ver_phone_1'];
    $p2 = $form_state['values']['ver_phone_2'];
    $p3 = $form_state['values']['ver_phone_3'];
    if (strlen($p1) != 3) {
      form_set_error('ver_phone_1', "Work Verification Phone1 : Must be 3 digits");
    }
    if (strlen($p2) != 3) {
      form_set_error('ver_phone_2', "Work Verification Phone2 : Must be 3 digits");
    }
    if (strlen($p3) != 4) {
      form_set_error('ver_phone_3', "Work Verification Phone3 : Must be 4 digits");
    }
    if ($form_state['values']['ver_phone_ext']) {
      if (!$validateInt->isValid($form_state['values']['ver_phone_ext'])) {
        $message = implode(',', $validateInt->getMessages());
        form_set_error('ver_phone_ext', "Work Verification Phone Ext : " . $message);
      }
    }
  }
}


function pdl2_account_my_details_form_submit($form, &$form_state) {
  $pdl2 = pdl2_core_get_api();

  // employment address
  $res = $pdl2->setEmploymentAddress($form_state['values']['workzip'],
                                     $form_state['values']['workcity'],
                                     $form_state['values']['workaddress1'],
                                     $form_state['values']['workaddress2']);

  // home address
  $res = $pdl2->setHomeAddress($form_state['values']['homezip'],
                               $form_state['values']['homecity'],
                               $form_state['values']['homestreet1'],
                               $form_state['values']['homestreet2']);

  $res = $pdl2->setMailAddress($form_state['values']['homemailzip'],
                               $form_state['values']['homemailcity'],
                               $form_state['values']['homemailaddr']);

  $res = $pdl2->setDriversLicense($form_state['values']['drivers_license_number'],
                                  $form_state['values']['driverstate']);


  // what about setting home phone as a mobile?
  $res = $pdl2->setHomePhone($form_state['values']['home_phone_1'] . $form_state['values']['home_phone_2'] . $form_state['values']['home_phone_3'],
                             false);


  $res = $pdl2->setCellPhone($form_state['values']['cell_phone_1'] . $form_state['values']['cell_phone_2'] . $form_state['values']['cell_phone_3']);


  $mp = $form_state['values']['main_phone_1'] . $form_state['values']['main_phone_2'] . $form_state['values']['main_phone_3'];
  if ($form_state['values']['main_phone_ext']) {
    $mp .= "x" . $form_state['values']['main_phone_ext'];
  }
  $vp = $form_state['values']['ver_phone_1'] . $form_state['values']['ver_phone_2'] . $form_state['values']['ver_phone_3'];
  if ($form_state['values']['ver_phone_ext']) {
    $vp .= "x" . $form_state['values']['ver_phone_ext'];
  }
  $res = $pdl2->setEmploymentPhones($mp, $vp);


  $res = $pdl2->setResidencyType($form_state['values']['residency']) ;


  $res = $pdl2->setIncomeSourceType($form_state['values']['incomesource']);


  $res = $pdl2->setEmployerName($form_state['values']['employer']);


  $res = $pdl2->setEmploymentAddress($form_state['values']['workzip'],
                                     $form_state['values']['workcity'],
                                     $form_state['values']['workaddress1'],
                                     $form_state['values']['workaddress2']);


  $res = $pdl2->setEmploymentDepartment($form_state['values']['department']);

  $startDate = $form_state['values']['since_year'] . '-' . $form_state['values']['since_month'] . '-1';
  $startDate = date('Y-m-d', strtotime($startDate));
  $res = $pdl2->setEmploymentStartDate($startDate);
}

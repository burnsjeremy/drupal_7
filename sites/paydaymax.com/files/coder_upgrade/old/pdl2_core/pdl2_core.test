<?php

class PDL2CoreFunctionalTest extends DrupalWebTestCase 
{
	public static function getInfo() 
	{
    	return array(
      		'name' => 'Core Tests',
      		'description' => 'Various tests on pdl2_core module.',
      		'group' => 'CPS PDL2',
    	);
  	}
  	
  	function setUp() 
  	{
    	parent::setUp('pdl2_core');
  	}
  	
  	function testAPISettingsFormForbidden() 
  	{
  		// No user has logged in, this form should return 403 Forbidden.
  		$this->drupalGet('admin/settings/pdl2_core');
		$this->assertResponse(403);
  	}
  	
  	function testAPISettingsFormPermissions() 
  	{
  		$user = $this->createPDLAdministrator();
  		
  		$this->drupalLogin($user);
  		
  		$this->drupalGet('admin/settings/pdl2_core');
		$this->assertResponse(200);
  	}
  	
  	function testAPISettingsFormSubmission() 
  	{
  		$user = $this->createPDLAdministrator();
  		
  		$this->drupalLogin($user);
  		
  		$submitTitle = t('Save configuration');
  		
  		// We aren't posting any data here, let's test field validation.
  		$this->drupalPost('admin/settings/pdl2_core', array(), $submitTitle);
  		$this->assertText(t('API server field is required'));
  		$this->assertText(t('Application Token field is required'));
  		$this->assertText(t('Site Identifier field is required'));
  		$this->assertText(t('Site Name field is required'));
  		
  		// Properly filled out post data.
  		$postArr = array('pdl2_core_api_server' => 'api.static.dev.pdl',
  						 'pdl2_core_app_token' => 'NzAzNzU4NzkzOTQ3MmIzNjYx',
  						 'pdl2_core_app_password' => 'w41tre55',
  						 'pdl2_core_site_id' => 'mycashnow.com',
  						 'pdl2_core_site_name' => 'MyCashNow');
  		
  		$this->drupalPost('admin/settings/pdl2_core', $postArr, $submitTitle);
  		$this->assertText(t('The configuration options have been saved.'));
  	}
  	
  	// Create a user w/ appropriate permissions and ensure they can access
  	// the API settings form.
  	function createPDLAdministrator()
  	{
  		$perms = array(PDL2_PERM_ADMINISTER);
  		$user = $this->drupalCreateUser($perms);
  		
  		return $user;
  	}
}
<?php 
$borrower = $themedata['borrower']->customer;
$address = $borrower->home_address->address; 
$agreement = $themedata['agreement']->loan_agreement;
$date = $themedata['date'];
$ssn = $themedata['ssn'];
?>
<div class="dashboard">
	<table id="agreementtable" width="100%">
		<tr class="small">
			<td class="center" colspan="2">
				<p>Lender: <?=$agreement->lender?></p>
				<p>Address: <?=$agreement->lenderAddressPrimary?></p>
				<p><?=$agreement->lenderAddressSecondary?></p>
				<p>Date: <?=date('F jS, Y', strtotime($date))?></p>
				<p>Due: <?=date('F jS, Y', strtotime($agreement->dueDate->date))?></p>
			</td>
			<td class="center" colspan="2">
				<p>Borrower: <?=$borrower->first_name?> <?=$borrower->middle_name?> <?=$borrower->last_name?></p>
				<p><?=$address->street_primary?></p>
				<?php if($address->street_secondary){ echo '<p>'.$address->street_secondary.'</p>'; } ?>
				<p><?=$address->city?>, <?=$address->state?> <?=$address->zip?></p>
				<p>SSN: <?=$ssn?></p>
			</td>
		</tr>
		<tr class="small">
			<td class="center" width="100%" colspan="4">
			 <p><strong>FEDERAL TRUTH-IN-LENDING ACT DISCLOSURES</strong></p>
			</td>
		</tr>
		<tr class="small">
			<td class="center" width="25%">
			  <p><strong>ANNUAL PERCENTAGE RATE</strong></p>
				<p><em>Cost of your loan as a yearly rate</em></p>
				<h4><?=$agreement->apr?>%</h4>
			</td>
			<td class="center" width="25%">
				<p><strong>FINANCE CHARGE</strong></p>
				<p><em>Cost of Credit</em></p>
				<h4>$<?=number_format($agreement->financeCharge, 2)?></h4>
			</td>
			<td class="center" width="25%">
				<p><strong>AMOUNT FINANCED</strong></p>
				<p><em>The amount of credit provided to you or on your behalf</em></p>
				<h4>$<?=number_format($agreement->amountFinanced, 2)?></h4>
			</td>
			<td class="center" width="25%">
				<p><strong>TOTAL PAYMENTS</strong></p>
				<p><em>Amount you will have paid after making all payments as scheduled</em></p>
				<h4>$<?=number_format($agreement->totalPayments, 2)?></h4>
			</td>
		</tr>
		<tr class="small">
		  <td class="center" colspan="4">
				<p><strong>Itemization of the Amount Financed</strong></p>
				<table id="itemization">
				  <tbody>
				    <tr>
				      <td class='itemizationprefix'>1. Amount Paid Directly to the Borrower:</td>
				      <td>$<?=number_format($agreement->amountPaidBorrower, 2)?></td>
				    </tr>
				    <tr>
				      <td class='itemizationprefix'>2. Amount Paid to Creditor:</td>
				      <td>$<?=number_format($agreement->amountPaidCreditor, 2)?></td>
				    </tr>
				    <tr>
				      <td class='itemizationprefix'>3. Total Amount to pay Lender (Payback):</td>
				      <td>$<?=number_format($agreement->totalPayments, 2)?></td>
				    </tr>
				  </tbody>
				</table>
		  </td>
		</tr>
		<?php if (isset($themedata["schedule"])) { ?>
		  <tr class="small">
		  <td class="center" colspan="4">
		    <?php print theme('personal_payment_schedule', $themedata); ?>
		  </td></tr>
		<?php } ?>
	</table>
</div>
<?php

function pdl2_core_oops_page(){
	
	$pdl2 = pdl2_core_get_api();
	$companyinfo = $pdl2->getCompanyInfo();
	$company_number = $companyinfo->company->customer_service_number->phone->number;
	
	return "<div class='dashboard'>
	<h1>Error</h1>
	<p>There has been a technical error and your request cannot be completed. Please try your request again later, or call a customer service representative at $company_number.
</p>
	</div>

<form action='' method='post' id='pdl2-mobile-loanconfirm-status-form'>
	<input type='submit' name='op' id='edit-submit' value='RETURN HOME' class='form-submit btn lrg' onclick='window.location.href=Drupal.settings.basePath; return false;' />
</form>";
}
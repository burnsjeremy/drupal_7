<?php

//require_once('../enums/severity.enum.php');

class LoggingHelper
{
	private $processType;
	private $processMessage;
	private $processStart;

	private $task;
	private $tasks;
	
	public function startProcess($type, $message)
	{
		$this->processType = $type;
		$this->processMessage = $message;
		$this->processStart = pdl2_core_millisecs();
		$this->tasks = array();
	}
	
	public function startTask($message)
	{
		$this->task = array("message"=>$message, "start" => pdl2_core_millisecs());
	}
	
	public function endTask()
	{
		$this->task["duration"] = pdl2_core_millisecs() - $this->task["start"];
		$this->tasks[] = $this->task;
	}

	public function endProcess()
	{
		$msg = "";
		$taskdur = 0;
		$totaldur = pdl2_core_millisecs() - $this->processStart;

		foreach ($this->tasks as $task)
		{
			$msg .= "&nbsp;&nbsp; TASK " . $task["message"] . " duration " . $task["duration"] . " ms<br/>";
			$taskdur += $task["duration"];
		}

		$deltadur = $totaldur - $taskdur;
		$msg = $this->processMessage . " total: $totaldur ms, tasks: $taskdur ms, delta: $deltadur<br/>" . $msg;
		$this->log($this->processType, $msg, array(), 'debug');
	}	
	
	public function log($type, $message, $variables = array(), $severity = WATCHDOG_ERROR)
	{
		watchdog($type, $message, $variables, $severity);
	}
}
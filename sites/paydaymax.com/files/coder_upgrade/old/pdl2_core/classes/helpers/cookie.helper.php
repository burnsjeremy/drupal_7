<?php

// TODO: Determine proper cookie behavior.
class CookieHelper
{
	public function __construct()
	{
	
	}

	public function setCookie($name, $value)
	{
		setcookie($name, $value, time() + 3600, '/', variable_get('pdl2_core_cookie_domain', ""));
		$_COOKIE[$name] = $value;
	}
	
	public function setAffiliateCookie($name, $value)
	{
    $time = time() + (60*60*24*30); //30 days
		setcookie($name, $value, $time, '/', variable_get('pdl2_core_cookie_domain', ""));
		$_COOKIE[$name] = $value;
	}

	public function setBrowserSessionCookie($name, $value)
	{
		setcookie($name, $value, 0, '/', variable_get('pdl2_core_cookie_domain', ""));
		$_COOKIE[$name] = $value;
	}
	
	public function hasCookie($name)
	{
		return isset($_COOKIE[$name]) && $_COOKIE[$name] != "";
	}
	
	public function getCookie($name)
	{
		return $_COOKIE[$name];
	}
	
	public function clearCookie($name)
	{
		setcookie($name, '', time() - 3600, '/');
		unset($_COOKIE[$name]);
	}
}
<?php

require_once('response.model.php');

class UserLoanAgreementsModel extends ResponseModel
{
	public $loanId;
	public $loanAgreements;
	
	public function __construct(StatusModel $status, $loanId, $agreements)
	{
		$this->status = $status;
		$this->loanId = $loanId;
		$this->loanAgreements = $agreements;
	}
}
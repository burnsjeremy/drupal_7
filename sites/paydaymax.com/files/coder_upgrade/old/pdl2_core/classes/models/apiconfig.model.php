<?php

class APIConfigModel
{
	public $httpProtocol;
	public $url;
	public $applicationToken;
	public $password;
	public $siteIdentifier;
	public $siteName;
	public $profileCalls;
	public $asyncCalls;
	
	public function __construct($siteIdentifier, $siteName, $httpProtocol, $url, $applicationToken, $password, $profileCalls, $asyncCalls)
	{
		$this->siteIdentifier = $siteIdentifier;
		$this->siteName = $siteName;
		$this->httpProtocol = $httpProtocol;
		$this->url = $url;
		$this->applicationToken = $applicationToken;
		$this->password = $password;
		$this->profileCalls = $profileCalls;
		$this->asyncCalls = $asyncCalls;
	}
	
	public function getUrlForService($service)
	{
		return "{$this->httpProtocol}{$this->url}/{$service}";
	}
	
	public function isProfilingCalls()
	{
		return $this->profileCalls;
	}
	
	public function isAsyncCalls()
	{
		return $this->asyncCalls;
	}
}
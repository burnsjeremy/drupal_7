<?php

class StatusModel
{
	public $success;
	public $code;
	public $text;
	public $detail;
	
	static function Create()
	{
		return new StatusModel(true, 1, '', '');
	}

	function __construct($success, $code, $text, $detail)
	{
		$this->success = $success;
		$this->code = $code;
		$this->text = $text;
		$this->detail = $detail;
	}
}
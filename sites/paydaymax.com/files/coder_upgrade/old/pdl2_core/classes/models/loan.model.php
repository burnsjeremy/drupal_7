<?php

require_once('response.model.php');

class LoanModel extends ResponseModel
{
	public $loanId;
	public $clientId;
	
	public $loanStatuses;
	public $loanTransactions;
	
	public $loanAmount;
	public $loanDays;
	public $loanDaysTotal;
	public $overrideOk;
	public $signature;
	public $entryDate;
	public $interestRate;
	public $baseInterestRate;
	public $returnFee;
	
	public $loanDueDate;
	public $loanStartDate;
	
	public $bankStatement;
	public $bankStatementPostDate;
	public $bankStatementCpsId;
	
	public $random;
	public $risk;
	public $riskOverrideUserId;
	
	public $previousCustomer;
	
	public $phoneInApp;
	public $autoAppGroupId;
	public $autoAppGroupMatched;
	
	public $promoId;
	public $promoDiscount;
	public $promoAmount;
	public $promoExpired;
	public $promoTyped;
	public $fourDayWait;
	public $paymentMethod;
}
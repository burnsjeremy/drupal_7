<?php

class LoanTransactionModel
{
	public $loanTransactionId;
	public $userId;
	
	public $trType;
	public $origAmount;
	public $returnAmount;
	public $trEntryDate;
	public $startPostDate;
	
	// TODO: Find out what "ee" stands for.
	public $eeTransactionId;
	public $eeDateSent;
	public $eeReplyDate;
	public $eeResponse;
	
	public $notes;
	public $days;
	public $adjustment;
	public $noSend;
	public $status;
	
	// TODO: Find out how to retrieve the enumeration/look up table for this property.
	public $payArrangeId;
	
	public $wire;
}
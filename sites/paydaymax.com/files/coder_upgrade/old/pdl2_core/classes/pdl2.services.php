<?php

class PDL2Services
{
  // Account Services - Modified for PDL2 Mobile
  const LOGIN = 'customer/login';
  const LOGOUT = 'customer/logout';
  const GET_USER_BY_ID = 'customer/getById';
  const ACCOUNT_GET_LOAN_HISTORY = 'customer/loan/getHistory';
  const ACCOUNT_GET_PRIVACY_PREFERENCES = 'customer/privacy/getPrivacyPreferences';
  const ACCOUNT_SET_PRIVACY_PREFERENCES = 'customer/privacy/setPrivacyPreferences';
  const ACCOUNT_OPT_OUT_ALL = 'customer/privacy/setOptOutAll';
  const ACCOUNT_CHECK_OPT_OUT_ALL = 'customer/privacy/getOptOutAll';
  const ACCOUNT_SET_OPT_OUT_SMS = 'customer/privacy/setOptOutSms';
  const ACCOUNT_SET_OPT_OUT_THIRDPARTY = 'customer/privacy/setOptOutThirdParty';
  const ACCOUNT_GET_OPT_OUT_THIRDPARTY = 'customer/privacy/getOptOutThirdParty';
  const CUSTOMER_CREATE = 'customer/create';

  const LOAN_GETBYID = 'loan/getById';
  const LOAN_GET_TYPE = 'loan/getType';
  const LOAN_GETBALANCE = 'loan/getBalance';
  const LOAN_GETDUEDATE = 'loan/getDueDate';
  const LOAN_GETSTATUS = 'loan/getStatus';
  const LOAN_GETPRINCIPAL = 'loan/getPrincipal';
  const LOAN_GETCOLLECTIONCOMPANY = 'loan/getCollectionCompany';
  const LOAN_DRAFT_AGREEMENT_GETCLARITY = 'loan/draft/agreement/getClarity';
  const LOAN_DRAFT_AGREEMENT_SETCLARITY = 'loan/draft/agreement/setClarity';
  const LOAN_DRAFT_GETAGREEMENT = 'loan/draft/getAgreement';
  const LOAN_DRAFT_AGREEMENT_SETEFT = 'loan/draft/agreement/setEft';
  const LOAN_DRAFT_GETAPR = 'loan/draft/getApr';
  const LOAN_DRAFT_SETDUEDATE = 'loan/draft/setDueDate';
  const LOAN_DRAFT_GETDUEDATE = 'loan/draft/getDueDate';
  const LOAN_DRAFT_SETCOUPON = 'loan/draft/setCoupon';
  const LOAN_DRAFT_SETPRINCIPAL = 'loan/draft/setPrincipal';
  const LOAN_DRAFT_GETPRINCIPAL = 'loan/draft/getPrincipal';
  const LOAN_DRAFT_SETPAYMENTMETHOD = 'loan/draft/setPaymentMethod';
  const LOAN_DRAFT_GETFINANCECHARGE = 'loan/draft/getFinanceCharge';
  const LOAN_DRAFT_GETLENDER = 'loan/draft/getLender';
  const LOAN_DRAFT_SETREQUESTEDAMOUNT = 'loan/draft/setRequestedAmount';
  const LOAN_DRAFT_CANCEL = 'loan/draft/cancel';
  const GET_LOAN_HISTORY_BY_ID = 'loan/getHistory';
  const GET_LOAN_TRANSACTIONS_BY_ID = 'loan/getTransactions';
  const ACCOUNT_GET_LOAN_AGREEMENT = 'loan/getAgreement';
  const ACCOUNT_GET_LOAN_AGREEMENTS = 'loan/getAgreements';
  const ACCOUNT_GET_LOAN_AGREEMENT_BY_ID = 'loan/getAgreementById';

  const LOAN_CREATE = 'customer/loan/create';
  const LOAN_SAVE = 'customer/loan/save';
  const LOAN_GETDUEDATES = 'customer/loan/getDueDates';
  const LOAN_GETLASTDENIED = 'customer/loan/getLastDenied';
  const CUSTOMER_LOAN_PAYDAY_ISELIGIGLE = 'customer/loan/payday/isEligible';
  const CUSTOMER_GETOPENLOAN = 'customer/getOpenLoan';
  const CUSTOMER_GETMAXLOANOFFER = 'customer/getMaxLoanOffer';
  const CUSTOMER_GETCOUPONS = 'customer/getCoupons';
  const CUSTOMER_ALLOWMONEYGRAM = 'customer/allowMoneyGram';
  const CUSTOMER_GETREFERRALCODE = 'customer/getReferralCode';
  const CUSTOMER_GETPREFERREDMEMBERSTATUS = 'customer/getPreferredMemberStatus';

  const CUSTOMER_ALLOWCCPAYMENT = 'customer/payment/creditcard/isAllowed';
  const LOAN_GETCCPAYMENTOPTIONS = 'loan/payment/creditcard/getPaymentOptions';
  const LOAN_GETCCSUBMISSIONVALUES = 'loan/payment/creditcard/getSubmissionValues';
  const LOAN_PROCESSCCRESPONSE = 'loan/payment/creditcard/processResponse';

  const CUSTOMER_DRAFT_SETNAME = 'customer/draft/setName';
  const CUSTOMER_DRAFT_SETHOMEADDRESS = 'customer/draft/setHomeAddress';
  const CUSTOMER_DRAFT_SETMAILADDRESS = 'customer/draft/setMailAddress';
  const CUSTOMER_DRAFT_SETHOMEPHONE = 'customer/draft/setHomePhone';
  const CUSTOMER_DRAFT_SETCELLPHONE = 'customer/draft/setCellPhone';
  const CUSTOMER_DRAFT_SETDATEOFBIRTH = 'customer/draft/setDateOfBirth';
  const CUSTOMER_DRAFT_SETDRIVERSLICENSE = 'customer/draft/setDriversLicense';
  const CUSTOMER_DRAFT_PAYMENT_SETWHETHERDIRECTDEPOSIT = 'customer/draft/payment/setWhetherDirectDeposit';
  const CUSTOMER_DRAFT_EMPLOYMENT_SETINCOMESOURCETYPE = 'customer/draft/employment/setIncomeSourceType';
  const CUSTOMER_DRAFT_SETREFERRAL = 'customer/draft/setReferral';
  const CUSTOMER_DRAFT_SETBANKACCOUNT = 'customer/draft/setBankAccount';
  const CUSTOMER_DRAFT_SETSOCIALSECURITYNUMBER = 'customer/draft/setSocialSecurityNumber';
  const CUSTOMER_DRAFT_SETPASSWORD = 'customer/draft/setPassword';
  const CUSTOMER_DRAFT_SETEMAIL = 'customer/draft/setEmail';
  const CUSTOMER_DRAFT_GETBYID = 'customer/draft/getById';
  const CUSTOMER_DRAFT_SAVE = 'customer/save';
  const CUSTOMER_GET_TRANSACTIONS = 'customer/getTransactions';

  const CUSTOMER_GETMONEYGRAMMAX = 'customer/getMoneyGramMaximum';
  const CUSTOMER_DRAFT_SETSECURITYANSWERONE = 'customer/draft/setSecurityAnswerOne';
  const CUSTOMER_DRAFT_SETSECURITYANSWERTWO = 'customer/draft/setSecurityAnswerTwo';
  const CUSTOMER_DRAFT_SETMILITARYSTATUS = 'customer/draft/setMilitaryStatus';
  const CUSTOMER_DRAFT_SETRELATIVEREFERENCE = 'customer/draft/setRelativeReference';
  const CUSTOMER_DRAFT_SETNONRELATIVEREFERENCE = 'customer/draft/setNonRelativeReference';
  const CUSTOMER_DRAFT_EMPLOYMENT_SETEMPLOYERNAME = 'customer/draft/employment/setEmployerName';
  const CUSTOMER_DRAFT_EMPLOYMENT_SETDEPARTMENT = 'customer/draft/employment/setDepartment';
  const CUSTOMER_DRAFT_EMPLOYMENT_SETSTARTDATE = 'customer/draft/employment/setStartDate';
  const CUSTOMER_DRAFT_EMPLOYMENT_SETADDRESS = 'customer/draft/employment/setAddress';
  const CUSTOMER_DRAFT_EMPLOYMENT_GETADDRESS = 'customer/draft/employment/getAddress';
  const CUSTOMER_DRAFT_EMPLOYMENT_SETPHONES = 'customer/draft/employment/setPhones';
  const CUSTOMER_DRAFT_SETRESIDENCETYPE = 'customer/draft/setResidenceType';
  const CUSTOMER_GETSSN = 'customer/getSocialSecurityNumberMasked';
  const CUSTOMER_SENDRESETPASSWORD = 'customer/sendResetPassword';
  const CUSTOMER_GETFROMEMAILHASH = 'customer/getFromEmailHash';
  const CUSTOMER_CHANGEPASSWORD = 'customer/changePassword';
  const CUSTOMER_GET_CHANGEPASSWORD_HASH = 'customer/getChangePasswordHashFromId';
  const CUSTOMER_ISAUTOAPPROVED = 'customer/isAutoApproved';

  const CUSTOMER_EMPLOYMENT_GET_ADDRESS = 'customer/employment/getAddress';
  const CUSTOMER_EMPLOYMENT_SET_ADDRESS = 'customer/employment/setAddress';
  const CUSTOMER_EMPLOYMENT_GET_DEPARTMENT = 'customer/employment/getDepartment';
  const CUSTOMER_EMPLOYMENT_SET_DEPARTMENT = 'customer/employment/setDepartment';
  const CUSTOMER_EMPLOYMENT_GET_EMPLOYER_NAME = 'customer/employment/getEmployerName';
  const CUSTOMER_EMPLOYMENT_SET_EMPLOYER_NAME = 'customer/employment/setEmployerName';
  const CUSTOMER_EMPLOYMENT_GET_INCOME_SOURCE_TYPE = 'customer/employment/getIncomeSourceType';
  const CUSTOMER_EMPLOYMENT_SET_INCOME_SOURCE_TYPE = 'customer/employment/setIncomeSourceType';
  const CUSTOMER_EMPLOYMENT_GET_PHONES = 'customer/employment/getPhones';
  const CUSTOMER_EMPLOYMENT_SET_PHONES = 'customer/employment/setPhones';
  const CUSTOMER_EMPLOYMENT_GET_START_DATE = 'customer/employment/getStartDate';
  const CUSTOMER_EMPLOYMENT_SET_START_DATE = 'customer/employment/setStartDate';
  const CUSTOMER_GET_CELL_PHONE = 'customer/getCellPhone';
  const CUSTOMER_SET_CELL_PHONE = 'customer/setCellPhone';
  const CUSTOMER_GET_DOB = 'customer/getDateOfBirth';
  const CUSTOMER_GET_DRIVERS_LICENSE = 'customer/getDriversLicense';
  const CUSTOMER_SET_DRIVERS_LICENSE = 'customer/setDriversLicense';
  const CUSTOMER_GET_EMAIL = 'customer/getEmail';
  const CUSTOMER_SET_EMAIL = 'customer/setEmail';
  const CUSTOMER_GET_HOME_ADDRESS = 'customer/getHomeAddress';
  const CUSTOMER_SET_HOME_ADDRESS = 'customer/setHomeAddress';
  const CUSTOMER_SET_MAIL_ADDRESS = 'customer/setMailAddress';
  const CUSTOMER_GET_HOME_FAX = 'customer/getHomeFax';
  const CUSTOMER_SET_HOME_FAX = 'customer/setHomeFax';
  const CUSTOMER_GET_HOME_PHONE = 'customer/getHomePhone';
  const CUSTOMER_SET_HOME_PHONE = 'customer/setHomePhone';
  const CUSTOMER_GET_NAME = 'customer/getName';
  const CUSTOMER_GET_RESIDENCE_TYPE = 'customer/getResidenceType';
  const CUSTOMER_SET_RESIDENCE_TYPE = 'customer/setResidenceType';
  const CUSTOMER_PAYMENT_GET_FREQUENCY = 'customer/payment/getFrequency';
  const CUSTOMER_IOVATION_SETBLACKBOXVALUE = 'customer/iovation/setBlackboxValue';
  
  const CUSTOMER_TWOSTEP_VERIFYCODE = 'customer/twoStepVerification/verifyCode';
  const CUSTOMER_TWOSTEP_ISVERIFICATIONREQUIRED = 'customer/twoStepVerification/isVerificationRequired';
  const CUSTOMER_TWOSTEP_SENDVERIFICATIONCODE = 'customer/twoStepVerification/sendVerificationCode';
  const CUSTOMER_TWOSTEP_HASACTIVEVERIFICATIONCODE = 'customer/twoStepVerification/hasActiveVerificationCode';
  
  const EXTENSION_CHECK = 'customer/allowExtension';
  const LOAN_EXTENSION_CREATE = 'loan/extension/create';
  const LOAN_EXTENSION_DRAFT_GETFINANCECHARGE = 'loan/extension/draft/getFinanceCharge';
  const LOAN_GETMINIMUMPRINCIPALDUE = 'loan/getMinimumPrincipalDue';
  const LOAN_GETFINANCECHARGES = 'loan/getFinanceCharges';
  const LOAN_EXTENSION_DRAFT_GETAGREEMENT = 'loan/extension/draft/getAgreement';
  const LOAN_EXTENSION_DRAFT_GETAPR = 'loan/extension/draft/getApr';
  const LOAN_EXTENSION_DRAFT_GETCURRENTPAYMENTTOTAL = 'loan/extension/draft/getCurrentPaymentTotal';
  const LOAN_EXTENSION_GETCONFIRMATIONCODE = 'loan/extension/getConfirmationCode';
  const LOAN_EXTENSION_DRAFT_SETPAYMENT = 'loan/extension/draft/setPayment';
  const LOAN_EXTENSION_SAVE = 'loan/extension/save';

  const CUSTOMER_SINGLECLICK_CREATELOAN = 'customer/singleclick/createLoan';
  const CUSTOMER_SINGLECLICK_GETDURATION = 'customer/singleclick/getDuration';
  const CUSTOMER_SINGLECLICK_GETAMOUNT = 'customer/singleclick/getAmount';
  const CUSTOMER_ISTOKENVALID = 'customer/isTokenValid';
  const CUSTOMER_GETPREQUALIFICATION = 'customer/getPrequalification';

  // This does not exists in PDL2 - maybe it was removed?
  const CUSTOMER_DOPREQUALIFICATIONCHECK = 'customer/doPrequalificationCheck';

  const LOAN_GET_MONEYGRAM_REF_NUMBER = 'loan/getMoneyGramReferenceNumber';
  const GET_COMPANY_INFO = 'company/getById';
  const GET_COMPANY_CHAT_INFO = 'company/getChat';
  const REFERENCE_GETSTATESPROVINCES = 'reference/getStatesProvinces';
  const REFERENCE_GETEVERYOTHERWEEKPAYDATES = 'reference/payment/getEveryOtherWeekPayDates';
  const REFERENCE_GETBANK = 'reference/getBank';
  const GET_RELATIONSHIPS = 'reference/getRelationships';
  const ACCOUNT_ALLOW_EDIT = 'customer/allowAccountEdit';
  const CUSTOMER_GET_BANK_ACCOUNT = 'customer/getBankAccount';
  const CUSTOMER_SET_BANK_ACCOUNT = 'customer/setBankAccount';

  //Restored these because I don't think these were supposed to be commented out.
  const CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEEVERYOTHERWEEK = 'customer/draft/payment/setScheduleEveryOtherWeek';
  const CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEMONTHLYSPECIFICDAY = 'customer/draft/payment/setScheduleMonthlySpecificDay';
  const CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEMONTHLYWEEKDAYAFTERSPECIFICDAY = 'customer/draft/payment/setScheduleMonthlyWeekdayAfterSpecificDay';
  const CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEMONTHLYWEEKDAYINSPECIFICWEEK = 'customer/draft/payment/setScheduleMonthlyWeekdayInSpecificWeek';
  const CUSTOMER_DRAFT_PAYMENT_SETSCHEDULETWICEAMONTHSPECIFICDAYS = 'customer/draft/payment/setScheduleTwiceAMonthSpecificDays';
  const CUSTOMER_DRAFT_PAYMENT_SETSCHEDULETWICEAMONTHSPECIFICWEEKS = 'customer/draft/payment/setScheduleTwiceAMonthSpecificWeeks';
  const CUSTOMER_DRAFT_PAYMENT_SETSCHEDULEWEEKLY = 'customer/draft/payment/setScheduleWeekly';

  //These are very similar to those above, but they are for use with a customer that has been created
  const CUSTOMER_PAYMENT_SETSCHEDULEEVERYOTHERWEEK = 'customer/payment/setScheduleEveryOtherWeek';
  const CUSTOMER_PAYMENT_SETSCHEDULEMONTHLYSPECIFICDAY = 'customer/payment/setScheduleMonthlySpecificDay';
  const CUSTOMER_PAYMENT_SETSCHEDULEMONTHLYWEEKDAYAFTERSPECIFICDAY = 'customer/payment/setScheduleMonthlyWeekdayAfterSpecificDay';
  const CUSTOMER_PAYMENT_SETSCHEDULEMONTHLYWEEKDAYINSPECIFICWEEK = 'customer/payment/setScheduleMonthlyWeekdayInSpecificWeek';
  const CUSTOMER_PAYMENT_SETSCHEDULETWICEAMONTHSPECIFICDAYS = 'customer/payment/setScheduleTwiceAMonthSpecificDays';
  const CUSTOMER_PAYMENT_SETSCHEDULETWICEAMONTHSPECIFICWEEKS = 'customer/payment/setScheduleTwiceAMonthSpecificWeeks';
  const CUSTOMER_PAYMENT_SETSCHEDULEWEEKLY = 'customer/payment/setScheduleWeekly';
  const SAVE_TESTIMONIAL = 'customer/saveTestimonial';
  const CUSTOMER_GET_REFERRALS_LIST = 'customer/getReferralsList';
  const CUSTOMER_SEND_REFERRAL_EMAIL = 'customer/sendReferralEmail';
  const CUSTOMER_GET_REFERRAL_AMMOUNT = 'customer/getReferralAmountReceived';
  const CUSTOMER_GET_REFERRAL_BALANCE = 'customer/getReferralBalance';
  const CUSTOMER_SET_REFERRAL_DEPOSIT_TO_ACCOUNT = 'customer/setReferralDepositToAccount';
  const CUSTOMER_SET_REFERRAL_APPLY_TO_LOAN_BALANCE = 'customer/setReferralApplyToLoanBalance';
/*
  const AFFILIATE_PARTNERS_LS_GETSUPERREFCODE = 'affiliate/partners/linkshare/getSuperRefCode'; //affCode
  const AFFILIATE_REFTRACK_CREATE = 'affiliate/tracking/reftrack/create'; //refName - Creates a ref_track record with a unique ID and returns the unique ID
  const AFFILIATE_REFTRACK_RECORDNEWCLIENTACTION = 'affiliate/tracking/reftrack/recordNewClientAction'; //refId, clientId, ipAddress - Log create client via rtID & clientID
  const AFFILIATE_REFTRACK_RECORDMODIFYCLIENTACTION = 'affiliate/tracking/reftrack/recordModifyClientAction'; //refId, clientId, ipAddress - Log update client via rtID & clientID
  const AFFILIATE_REFTRACK_RECORDCLICKACTION = 'affiliate/tracking/reftrack/recordClickAction'; //refId, ipAddress - Log a generic action via reftrack ID
  const AFFILIATE_REFTRACK_RECORDNEWLOANACTION = 'affiliate/tracking/reftrack/recordNewLoanAction'; //refId, loanId, ipAddress - Log create loan via rtID & loanID

  const AFFILIATE_REFTRACK_GETREFID = 'affiliate/tracking/reftrack/getRefId'; //refTrackCode - Returns refname ID based on refname (refname = refcode)
  const AFFILIATE_REFTRACK_GETREFNAME = 'affiliate/tracking/reftrack/getRefName'; //refId - Returns refname based on refname ID

  const AFFILIATE_PARTNERS_LS_CREATE_TRANSACTION = 'affiliate/partners/linkshare/createTransaction'; //loanId, siteId, timeEntered
  const AFFILIATE_PARTNERS_CJ_CREATE_TRANSACTION = 'affiliate/partners/commissionjunction/createTransaction'; //loanId, adId, publisherId, shopperId
  const AFFILIATE_PARTNERS_MP_CREATE_TRANSACTION = 'affiliate/partners/mypoints/createTransaction'; //loanId, visitId
  const AFFILIATE_PARTNERS_MEDIAMIND_GETCHANNELANDTYPE = 'affiliate/partners/mediamind/getRefNameChannelAndType'; //clientId
*/  
  const AFFILIATE_PARTNERS_LP_AUTHENTICATE = 'affiliate/tracking/leadpile/authenticate'; //apiKey, siteName, clientId, leadPileToken

  const AFFILIATE_REFTRACK_GETREDIRECTPAGE = 'affiliate/tracking/reftrack/getRedirectPage'; //redirectId - Returns redirect url by redirect id
  const AFFILIATE_PARTNERS_EF_CREATE_CLIENT_TRANSACTION = 'affiliate/partners/efrontier/createClientTransaction'; //efId, clientId
  const AFFILIATE_PARTNERS_EF_CREATE_LOAN_TRANSACTION = 'affiliate/partners/efrontier/createLoanTransaction'; //efId, loanId
  const AFFILIATE_TRACKING_REFTRACK_GETCHANNELNAME = 'affiliate/tracking/reftrack/getChannelName';

  // Affiliate/reftrack/lead stuff
  const AFFILIATE_LEADSELL_GETSTATUS = 'affiliate/leadsell/getStatus';
  
  const AFFILIATE_TRACKING_SESSION_INITIALIZE = 'affiliate/tracking/session/initialize';
  const AFFILIATE_TRACKING_SESSION_GETCAMPAIGN = 'affiliate/tracking/session/getCampaign';
  const AFFILIATE_TRACKING_PARTNERS_SESSION_INITIALIZE = 'affiliate/tracking/partners/session/initialize';
  const AFFILIATE_TRACKING_SESSION_ASSOCIATEUSERTOKEN = 'affiliate/tracking/session/associateUserToken';
  const AFFILIATE_PARTNERS_CJ_ASSOCIATEUSERTOKEN = 'affiliate/partners/commissionjunction/associateUserToken';
  const AFFILIATE_PARTNERS_LINKSHARE_ASSOCIATEUSERTOKEN = 'affiliate/partners/linkshare/associateUserToken';
  const AFFILIATE_PARTNERS_MYPOINTS_ASSOCIATEUSERTOKEN = 'affiliate/partners/mypoints/associateUserToken';
  const AFFILIATE_PARTNERS_PINGDUPE_DOCHECK = 'customer/pingdupe/getPingDupeCheck';
  
  //Personal Loan services
  const CUSTOMER_LOAN_PERSONAL_CREATE = 'customer/loan/personal/create';
  const CUSTOMER_LOAN_PERSONAL_SAVE = 'customer/loan/personal/save';
  const CUSTOMER_LOAN_PERSONAL_GETDUEDATES = 'customer/loan/personal/getDueDates';
  const CUSTOMER_LOAN_PERSONAL_ISELIGIBLE = 'customer/loan/personal/isElegible';
  const CUSTOMER_LOAN_PERSONAL_GETMAXOFFER = 'customer/loan/personal/getMaxOffer';
  const CUSTOMER_LOAN_PERSONAL_GETHISTORY = 'customer/loan/personal/getHistory';

  const LOAN_PERSONAL_DRAFT_AGREEMENT_GETCLARITY = 'loan/personal/draft/agreement/getClarity';
  const LOAN_PERSONAL_DRAFT_AGREEMENT_GETEFT = 'loan/personal/draft/agreement/getEft';
  const LOAN_PERSONAL_DRAFT_AGREEMENT_SETCLARITY = 'loan/personal/draft/agreement/setClarity';
  const LOAN_PERSONAL_DRAFT_AGREEMENT_SETEFT = 'loan/personal/draft/agreement/setEft';
  const LOAN_PERSONAL_DRAFT_GETAPR = 'loan/personal/draft/getApr';
  const LOAN_PERSONAL_DRAFT_GETDUEDATE = 'loan/personal/draft/getDueDate';
  const LOAN_PERSONAL_DRAFT_GETLENDER = 'loan/personal/draft/getLender';
  const LOAN_PERSONAL_DRAFT_GETPRINCIPAL = 'loan/personal/draft/getPrincipal';
  const LOAN_PERSONAL_DRAFT_SETCOUPON = 'loan/personal/draft/setCoupon';
  const LOAN_PERSONAL_DRAFT_SETDUEDATE = 'loan/personal/draft/setDueDate';
  const LOAN_PERSONAL_DRAFT_SETPAYMENTMETHOD = 'loan/personal/draft/setPaymentMethod';
  const LOAN_PERSONAL_DRAFT_SETPRINCIPAL = 'loan/personal/draft/setPrincipal';
  const LOAN_PERSONAL_DRAFT_SETREQUESTEDAMOUNT = 'loan/personal/draft/setRequestedAmount';
  const LOAN_PERSONAL_DRAFT_CANCEL = 'loan/personal/draft/cancel';
  const LOAN_PERSONAL_REFINANCE_ISLOANELIGIBLE = 'loan/personal/refinance/isLoanEligible';
  const LOAN_PERSONAL_REFINANCE_DRAFT_CREATEREFINANCEDRAFT = 'loan/personal/refinance/draft/createRefinanceDraft';

  const LOAN_PERSONAL_GETAPR = 'loan/personal/getApr';
  const LOAN_PERSONAL_GETBALANCE = 'loan/personal/getBalance';
  const LOAN_PERSONAL_GETBYID = 'loan/personal/getById';
  const LOAN_PERSONAL_GETCOLLECTIONCOMPANY = 'loan/personal/getCollectionCompany';
  const LOAN_PERSONAL_GETDUEDATE = 'loan/personal/getDueDate';
  const LOAN_PERSONAL_GETFINANCECHARGES = 'loan/personal/getFinanceCharges';
  const LOAN_PERSONAL_GETLENDER = 'loan/personal/getLender';
  const LOAN_PERSONAL_GETMONEYGRAMREFERENCENUMBER = 'loan/personal/getMoneyGramReferenceNumber';
  const LOAN_PERSONAL_GETPRINCIPAL = 'loan/personal/getPrincipal';
  const LOAN_PERSONAL_GETSTATUS = 'loan/personal/getStatus';
  const LOAN_PERSONAL_GETAGREEMENTS = 'loan/personal/getAgreements';
  const LOAN_PERSONAL_DRAFT_GETSCHEDULE = 'loan/personal/draft/getSchedule';
  const LOAN_PERSONAL_GETHISTORY = 'loan/personal/getHistory';
  const LOAN_PERSONAL_GETTRANSACTIONS = 'loan/personal/getTransactions';
  const LOAN_PERSONAL_GETSCHEDULE = 'loan/personal/getSchedule';
  const LOAN_PERSONAL_GETAGREEMENT = 'loan/personal/getAgreement';
  const LOAN_PERSONAL_DRAFT_GETAGREEMENT = 'loan/personal/draft/getAgreement';
  const LOAN_PERSONAL_GETAGREEMENTBYID = 'loan/personal/getAgreementById';
  const LOAN_PERSONAL_GETCCPAYMENTOPTIONS = 'loan/personal/payment/creditcard/getPaymentOptions';
  const LOAN_PERSONAL_GETCCSUBMISSIONVALUES = 'loan/personal/payment/creditcard/getSubmissionValues';
  const LOAN_PERSONAL_PROCESSCCRESPONSE = 'loan/personal/payment/creditcard/processResponse';

  public function get_class_constants() {
    $reflect = new ReflectionClass(get_class($this));
    return $reflect->getConstants();
  }
}

<?php

class PDL2Utils
{
	public static function GetRecordByPrimaryKey($primaryKey, $records)
	{
		foreach($records as $r)
		{
			if (!$r instanceof IRecord)
			{
				throw new Exception("Object must implement IRecord interface.");
			}
	
			if ($r->getPrimaryKey() == $primaryKey)
			{
				return $r;
			}
		}
	
		return NULL;
	}
	
	public static function GetStatusFromJsonResponse(stdClass $jsonObj)
	{
		return new StatusModel($jsonObj->success, $jsonObj->msg->code, $jsonObj->msg->text, $jsonObj->msg->detail);
	}

    
	public static function GetStatusFromSoapResponse(stdClass $soapObj)
	{
		return new StatusModel($soapObj->status, $soapObj->message->code, $soapObj->message->text, $soapObj->message->detail);
	}    
}
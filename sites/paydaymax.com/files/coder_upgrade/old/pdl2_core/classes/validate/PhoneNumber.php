<?php
class Pdl2_Validate_PhoneNumber extends Zend_Validate_Regex
{
  const INVALID   = 'regexInvalid';
  const NOT_MATCH = 'regexNotMatch';
  const ERROROUS  = 'regexErrorous';
  
  /**
    * @var array
    */
  protected $_messageTemplates = array(
      self::INVALID   => "Invalid type given. String, integer or float expected",
      self::NOT_MATCH => "'%value%' is an invalid phone number.",
      self::ERROROUS  => "'%pattern%' is an invalid pattern.",
  );
  
  public function __construct()
  {
    $pattern = '/^\(?([2-9][0-8][0-9])\)?[-. ]?([2-9][0-9]{2})[-. ]?([0-9]{4})$/';
  
    $this->setPattern($pattern);
  }
}
<?php

define ( 'PDL2_EFFICIENT_FRONTIER_ADMIN', 'Administer efficient_frontier Settings' );


/**
 * Implementation of hook_perm().
 */
function pdl2_efficient_frontier_perm() {
  return array (
    PDL2_EFFICIENT_FRONTIER_ADMIN,
  );
}


/**
 * Implementation of hook_menu().
 */
function pdl2_efficient_frontier_menu() {

  // Admin pages.
  $items['admin/settings/pdl2_efficient_frontier'] = array(
    'title' => "CPS PDL2 Efficient Frontier Management",
    'description' => "Efficient Frontier variable matrix table",
    'page callback' => 'pdl2_efficient_frontier_list_tags',
    'access callback' => 'user_access', //default
    'access arguments' => array(PDL2_EFFICIENT_FRONTIER_ADMIN),
    'type' => 'MENU_CALLBACK', //default
  );
  $items['admin/settings/pdl2_efficient_frontier/sitewidesetup'] = array(
    'title' => "Sitewide Setup",
    'description' => "Site wide config",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pdl2_efficient_frontier_sitewide_setup_form'),
    'access callback' => 'user_access', //default
    'access arguments' => array(PDL2_EFFICIENT_FRONTIER_ADMIN),
    'type' => 'MENU_CALLBACK', //default
  );
  $items['admin/settings/pdl2_efficient_frontier/edit/%'] = array(
    'title' => "CPS PDL2 Efficient Frontier Management Edit",
    'description' => "Efficient Frontier variable matrix table edit",
    'page callback' => 'pdl2_efficient_frontier_edit_tags',
    'page arguments' => array(4),
    'access callback' => 'user_access', //default
    'access arguments' => array(PDL2_EFFICIENT_FRONTIER_ADMIN),
    'type' => 'MENU_CALLBACK', //default
  );

  return $items;
}


function pdl2_efficient_frontier_list_variables() {
  return array("ev_new_amount",
               "ev_new_application",
               "ev_transid",
               "ev_previous_amount",
               "ev_previous_application",
               "ev_contact_us",
               );
}


function pdl2_efficient_frontier_list_tags() {
/*
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
      drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
*/
  $rows = pdl2_efficient_frontier_list_all_page_tags();
  $themedata = array('rows' => $rows);
  $output = theme('pdl2_efficient_frontier_var_list', $themedata);
  return $output;
}


function pdl2_efficient_frontier_edit_tags($ef_id) {
/*
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
      drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
*/
  $output = drupal_get_form('pdl2_efficient_frontier_form', $ef_id);
  return $output;
}


function pdl2_efficient_frontier_form(&$form_state, $ef_id) {

  $ef_obj = pdl2_efficient_frontier_load_byid($ef_id);
  $set_ebVars = pdl2_efficient_frontier_get_page_vars($ef_obj);

  $form['ef_id'] = array(
    '#type' => 'hidden',
    '#value' => $ef_obj->ef_id,
    );

  $form['page_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Name'),
    '#size' => 30,
    '#maxlength' => 60,
    '#required' => TRUE,
    '#description' => t('Please enter a name for the page that gets the tagging.'),
    '#default_value' => $ef_obj->page_name,
    );

  $form['page_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Page URL'),
    '#size' => 30,
    '#maxlength' => 300,
    '#required' => TRUE,
    '#description' => t('Please enter the URL for the page that gets the tagging.'),
    '#default_value' => $ef_obj->page_url,
    );

  $form['vars_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Variables passed for this tagged page'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $ebVars = pdl2_efficient_frontier_list_variables();
  foreach ($ebVars as $key => $value) {
    if (in_array($value, $set_ebVars)) {
      $form['vars_fieldset'][$value] = array(
        '#type' => 'checkbox',
        '#title' => t($value),
        '#default_value' => 1,
      );
    }
    else {
      $form['vars_fieldset'][$value] = array(
        '#type' => 'checkbox',
        '#title' => t($value),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Changes',
  );

  return $form;
}


function pdl2_efficient_frontier_form_submit($form, &$form_state) {

  $ef_id = $form_state['values']['ef_id'];
  $page_name = $form_state['values']['page_name'];
  $page_url = $form_state['values']['page_url'];
//  $activity_id = $form_state['values']['activity_id'];

  $ebVars = pdl2_efficient_frontier_list_variables();
  $vars = array();
  foreach ($ebVars as $key => $value) {
    if ($form_state['values'][$value] == 1) {
      $vars[$value] = $form_state['values'][$value];
    }
  }
  $serialized_vars = serialize($vars);

  $form_state['redirect'] = 'admin/settings/pdl2_efficient_frontier';

  // if a ef_id was given, it was an edit else it is a new insert.
  if (!$ef_id) {
    $query = "insert into {pdl2_efficient_frontier} (page_name, page_url, serialized_vars) ".
             "values ('%s', '%s', '%s')";
    db_query($query, $page_name, $page_url, $serialized_vars);
    drupal_set_message("Tagging page values inserted for page [".$page_name."]");
  }
  else {
    $query = "update {pdl2_efficient_frontier} set page_name='%s', page_url='%s', "
            ." serialized_vars='%s' "
            ."where ef_id = %d";
    db_query($query, $page_name, $page_url, $serialized_vars, $ef_id);
    drupal_set_message("Tagging page values updated for page [".$page_name."]");
  }

}


function pdl2_efficient_frontier_list_all_page_tags() {
  // Load all saved DB entries.
  $sql_query = "select ef_id, page_name, page_url, serialized_vars "
              ."from {pdl2_efficient_frontier} order by page_name";
  $results = db_query($sql_query);
  $results_array = array();
  while ($row = db_fetch_object($results)) {
    $results_array[] = $row;
  }
  return $results_array;
}


/**
 * Implemenation of hook_theme().
 */
function pdl2_efficient_frontier_theme() {
  $path = drupal_get_path('module', 'pdl2_efficient_frontier');
  $base = array(
    'file' => 'theme.inc',
    'path' => "$path/theme",
  );

  return array(
    'pdl2_efficient_frontier_var_list' => $base + array(
       'template' => 'pdl2_efficient_frontier_var_list',
       'arguments' => array('themedata' => NULL)
    ),
  );

}


function pdl2_efficient_frontier_load_byid($ef_id) {
  $query = "select * from "
          ."{pdl2_efficient_frontier} where ef_id = %d";
  $res = db_query($query, $ef_id);
  while($data = db_fetch_object($res)) {
    break;
  }

  return $data;
}


/**
 * Get an array of serialized efficient_frontier vars for this object.
 */
function pdl2_efficient_frontier_get_page_vars($ef_obj) {
  $vars = unserialize($ef_obj->serialized_vars);
  if (is_array($vars)) {
    $set_ebVars = array_keys($vars);
  }
  else {
    $set_ebVars = array();
  }
  return $set_ebVars;
}


/**
 * Implementaion of hook_preprocess_HOOK().
 * for each page url that is in the efficient_frontier - add the efficient_frontier javascript tagging
 */
function pdl2_efficient_frontier_preprocess_page(&$variables) {
/*
    // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
      // not logged in
      return;
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
*/
  $page_url = drupal_get_path_alias($_GET['q']);
  $ex_path = explode("/", $page_url);

  if ($ex_path[0] != 'admin') {

    $wanted_vars = pdl2_efficient_frontier_get_vars_for_page($page_url);
    if ($wanted_vars != null) {

      //dd("preprocess page hook efficient_frontier");

      $userid = variable_get('pdl2_efficient_frontier_userid' , 'default');
      $seg = variable_get('pdl2_efficient_frontier_segment' , 'default');

      $tagging = '<!-- EF Tagging --><script language="javascript" src="http://www.everestjs.net/static/st.v2.js"></script> '.
                 '<script language="javascript"> '.
                 'var ef_event_type="transaction"; '.
                 'var ef_transaction_properties = "ev_new_amount=&ev_new_application=1&ev_transid="; '.
                 '/* '.
                 ' * Do not modify below this line '.
                 ' */ '.
                 'var ef_segment = "'.$seg.'"; '.
                 'var ef_search_segment = ""; '.
                 'var ef_userid="'.$userid.'"; '.
                 'var ef_pixel_host="pixel.everesttech.net"; '.
                 'var ef_fb_is_app = 0; '.
                 'effp(); '.
                 '</script> '.
                 '<noscript><img src="http://pixel.everesttech.net/'.$userid.'/t?ev_new_amount=&ev_new_application=1&ev_transid=" width="1" height="1"/></noscript> ';
      //dd("efficient_frontier Tagging - PAGE OUTOUT");
      //dd($tagging);
      $variables['analytics'] .= $tagging;
    }
  }
}


/**
 * Whats the tagging values to go on page $page_url.
 *
 * @return null if no tagging for this page, else the string of tagged name/value pairs.
 */
function pdl2_efficient_frontier_get_vars_for_page($page_url) {
  // get the var list for the url/page
  $ef_obj = pdl2_efficient_frontier_get_efficient_frontier_obj_from_url($page_url);
  if ($ef_obj == null) {
    return null;
  }
  $selected_vars = pdl2_efficient_frontier_get_page_vars($ef_obj);

  $pdl2 = pdl2_core_get_api();
  $userInfo = $pdl2->getUserInfo()->customer;

  $output = "";
  foreach ($selected_vars as $key => $value) {
    switch ($value) {

      case "ev_new_amount":
        /*
         * ev_new_amount =This is the amount of the application. This value
         * is be should be the actual value of the application for
         * the transaction
         */
        // Is there a loan application currently running ? Use that amount.
        $output .= "ev_new_ammount=,";
        break;
      case "ev_new_application":
        /*
         * ev_new_application =This is the count of new applications.
         * This value is 1 ( so it is hardcoded) unless it is possible
         * to have more than 1application per transaction.
         */
        $output .= "ev_new_application=1,";
        break;
      case "ev_transid":
        /*
         * ev_transid = This is a unique identifier that helps
         * eliminate duplicates based on your business logic and also
         * serves to audit data when needed. You are currently pass a
         * loan application id ( e.g  1437460187)
         */
        // use id for loan application, else last loan they have.
        $output .= "ev_transid=,";
        break;
      case "ev_previous_amount":
        /*
         * ev_previous_amount =This is the amount of the application.
         * This value is be should be the actual value of the
         * application for the transaction
         */
        // if they have a previous loan use that ammount.
        $output .= "ev_previous_amount=,";
        break;
      case "ev_previous_application":
        /*
         * ev_previous_application =This is the count of previous
         * applications. This value is 1 ( so it is hardcoded) unless
         * it is possible to have more than 1application per transaction.
         */
        $output .= "ev_previous_application=,";
        break;
      case "ev_contact_us":
        /*
         * ev_contact_us = This value is 1 ( so it is hardcoded) unless
         * it is possible to submit more than 1 contact us per transaction.
         * */
        $output .= "ev_contact_us=1,";
        break;
    }
  }
  $output = substr($output, 0, strlen($output)-1);
  return $output;
}


/**
 * Load the efficient_frontier object from the database for the given URL.
 * URL will be unique in Database.
 */
function pdl2_efficient_frontier_get_efficient_frontier_obj_from_url($page_url) {
  $query = "select * from {pdl2_efficient_frontier} where page_url = '%s'";
  $data = db_fetch_object(db_query($query, $page_url));
  return $data;
}


function pdl2_efficient_frontier_sitewide_setup_form(&$form_state) {

  $form['id_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t("Id's to tag this site for analytics."),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    );

  $form['id_fieldset']['userid'] = array(
    '#type' => 'textfield',
    '#title' => t('Tagging User ID'),
    '#size' => 20,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#description' => t('Please enter the unique User ID for tagging.'),
    '#default_value' => variable_get('pdl2_efficient_frontier_userid' ,""),
    );

  $form['id_fieldset']['segment'] = array(
    '#type' => 'textfield',
    '#title' => t('Tagging Segment #'),
    '#size' => 20,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#description' => t('Please enter the Segment # for tagging.'),
    '#default_value' => variable_get('pdl2_efficient_frontier_segment' ,""),
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Changes',
  );

  return $form;

}


function pdl2_efficient_frontier_sitewide_setup_form_submit ($form, &$form_state) {
  variable_set('pdl2_efficient_frontier_userid' , $form['id_fieldset']['userid']['#value']);
  variable_set('pdl2_efficient_frontier_segment' , $form['id_fieldset']['segment']['#value']);
  drupal_set_message("SiteWide values saved.");
}

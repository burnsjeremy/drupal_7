<?php

define ( 'PDL2_PPC_ADMIN', 'Administer PPC Settings' );


/**
 * Implementation of hook_perm().
 */
function pdl2_ppc_perm() {
  return array (
    PDL2_PPC_ADMIN,
  );
}


/**
 * Implementation of hook_menu().
 */
function pdl2_ppc_menu() {

  // Admin pages.
  $items['admin/settings/pdl2_ppc'] = array(
    'title' => "CPS PDL2 PPC Management",
    'description' => "PPC variable matrix table",
    'page callback' => 'pdl2_ppc_list_tags',
    'access callback' => 'user_access', //default
    'access arguments' => array(PDL2_PPC_ADMIN),
    'type' => 'MENU_CALLBACK', //default
  );
  $items['admin/settings/pdl2_ppc/sitewidesetup'] = array(
    'title' => "Sitewide PPC Setup",
    'description' => "Site wide PPC config",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pdl2_ppc_sitewide_setup_form'),
    'access callback' => 'user_access', //default
    'access arguments' => array(PDL2_PPC_ADMIN),
    'type' => 'MENU_CALLBACK', //default
  );
  $items['admin/settings/pdl2_ppc/edit/%'] = array(
    'title' => "CPS PDL2 PPC Management Edit",
    'description' => "PPC variable matrix table edit",
    'page callback' => 'pdl2_ppc_edit_tags',
    'page arguments' => array(4),
    'access callback' => 'user_access', //default
    'access arguments' => array(PDL2_PPC_ADMIN),
    'type' => 'MENU_CALLBACK', //default
  );

  return $items;
}


function pdl2_ppc_list_variables() {
  return array(//"google_conversion_id",
               //"google_conversion_language",
               //"google_conversion_format",
               "google_conversion_color"=>"",
               "google_conversion_label"=>"",
               "google_conversion_value"=>"",
               );
}


function pdl2_ppc_list_tags() {
/*
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
      drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
*/
  $rows = pdl2_ppc_list_all_page_tags();
  $themedata = array('rows' => $rows);
  $output = theme('pdl2_ppc_var_list', $themedata);
  return $output;
}


function pdl2_ppc_edit_tags($ppc_id) {
/*
  // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
      drupal_goto("account/login");
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
*/
  $output = drupal_get_form('pdl2_ppc_form', $ppc_id);
  return $output;
}


function pdl2_ppc_form(&$form_state, $ppc_id) {

  $ppc_obj = pdl2_ppc_load_byid($ppc_id);
  $set_ebVars = pdl2_ppc_get_page_vars($ppc_obj);

  $form['ppc_id'] = array(
    '#type' => 'hidden',
    '#value' => $ppc_obj->ppc_id,
    );

  $form['page_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Name'),
    '#size' => 30,
    '#maxlength' => 60,
    '#required' => TRUE,
    '#description' => t('Please enter a name for the page that gets the tagging.'),
    '#default_value' => $ppc_obj->page_name,
    );

  $form['page_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Page URL'),
    '#size' => 30,
    '#maxlength' => 300,
    '#required' => TRUE,
    '#description' => t('Please enter the URL for the page that gets the PPC tagging.'),
    '#default_value' => $ppc_obj->page_url,
    );

  $form['vars_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Variables passed for this tagged page'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );

  $ebVars = pdl2_ppc_list_variables();
  foreach ($ebVars as $key => $value) {
    if (array_key_exists($key, $set_ebVars)) {
      $form['vars_fieldset'][$key] = array(
        '#type' => 'textfield',
        '#title' => t($key),
        '#size' => 20,
        '#required' => TRUE,
        '#default_value' => $set_ebVars[$key],
      );
    }
    else {
      $form['vars_fieldset'][$key] = array(
        '#type' => 'textfield',
        '#title' => t($key),
        '#size' => 20,
        '#required' => TRUE,
        '#default_value' => $value,
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Changes',
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => 'Delete this entry',
  );

  return $form;
}


function pdl2_ppc_form_submit($form, &$form_state) {

  $ppc_id = $form_state['values']['ppc_id'];
  $page_name = $form_state['values']['page_name'];
  $page_url = $form_state['values']['page_url'];
  $form_state['redirect'] = 'admin/settings/pdl2_ppc';

  if ($form_state['clicked_button']['#value'] == 'Delete this entry') {
    $query = "delete from {pdl2_ppc} where ppc_id = %d";
    db_query($query, $ppc_id);
    drupal_set_message("Tagging page values deleted for page [".$page_name."/".$page_url."]");
  }
  else {

    $ebVars = pdl2_ppc_list_variables();
    $vars = array();
    foreach ($ebVars as $key => $value) {
      $vars[$key] = $form_state['values'][$key];
    }
    $serialized_vars = serialize($vars);

    $form_state['redirect'] = 'admin/settings/pdl2_ppc';

    // if a ppc_id was given, it was an edit else it is a new insert.
    if (!$ppc_id) {
      $query = "insert into {pdl2_ppc} (page_name, page_url, serialized_vars) ".
               "values ('%s', '%s', '%s')";
      db_query($query, $page_name, $page_url, $serialized_vars);
      drupal_set_message("Tagging page values inserted for page [".$page_name."/".$page_url."]");
    }
    else {
      $query = "update {pdl2_ppc} set page_name='%s', page_url='%s', "
              ." serialized_vars='%s' "
              ."where ppc_id = %d";
      db_query($query, $page_name, $page_url, $serialized_vars, $ppc_id);
      drupal_set_message("Tagging page values updated for page [".$page_name."/".$page_url."]");
    }
  }
}


function pdl2_ppc_list_all_page_tags() {
  // Load all saved DB entries.
  $sql_query = "select ppc_id, page_name, page_url, serialized_vars "
              ."from {pdl2_ppc} order by page_name";
  $results = db_query($sql_query);
  $results_array = array();
  while ($row = db_fetch_object($results)) {
    $results_array[] = $row;
  }
  return $results_array;
}


/**
 * Implemenation of hook_theme().
 */
function pdl2_ppc_theme() {
  $path = drupal_get_path('module', 'pdl2_ppc');
  $base = array(
    'file' => 'theme.inc',
    'path' => "$path/theme",
  );

  return array(
    'pdl2_ppc_var_list' => $base + array(
       'template' => 'pdl2_ppc_var_list',
       'arguments' => array('themedata' => NULL)
    ),
  );

}


function pdl2_ppc_load_byid($ppc_id) {
  $query = "select * from "
          ."{pdl2_ppc} where ppc_id = %d";
  $res = db_query($query, $ppc_id);
  while($data = db_fetch_object($res)) {
    break;
  }

  return $data;
}


/**
 * Get an array of serialized PPC vars for this object.
 */
function pdl2_ppc_get_page_vars($ppc_obj) {
  $vars = unserialize($ppc_obj->serialized_vars);
  if (is_array($vars)) {
    $set_ebVars = $vars;
  }
  else {
    $set_ebVars = array();
  }
  return $set_ebVars;
}


/**
 * Implementaion of hook_preprocess_HOOK().
 * for each page url that is in the PPC - add the PPC javascript tagging
 */
function pdl2_ppc_preprocess_page(&$variables) {
/*
    // First let's see if we have an existing session
  if (!pdl2_account_is_logged_in()) {
      // not logged in
      return;
  }
  else if (pdl2_account_not_opt_out()) {
    drupal_goto('account/optedout');
  }
*/
  $page_url = drupal_get_path_alias($_GET['q']);
  $ex_path = explode("/", $page_url);

  if ($ex_path[0] != 'admin') {

    $wanted_vars = pdl2_ppc_get_vars_for_page($page_url);
    if ($wanted_vars != null) {
      //dd("preprocess page hook PPC");

      $conversion_id = variable_get('pdl2_ppc_conversionid' , 'default');
      $language = variable_get('pdl2_ppc_language' ,"en_US");
      $format = variable_get('pdl2_ppc_format' ,"1");

      // value and conversion id are number everything else is a string.
      $tagging = '<!-- PPC Tagging --> '.
                  '<script type="text/javascript">'.
                  '/* <![CDATA[ */'.
                  'var google_conversion_id = '.$conversion_id.';'.
                  'var google_conversion_language = "'.$language.'";'.
                  'var google_conversion_format = "'.$format.'";'.
                  'var google_conversion_color = "'.$wanted_vars['google_conversion_color'].'";'.
                  'var google_conversion_label = "'.$wanted_vars['google_conversion_label'].'";'.
                  'var google_conversion_value = '.$wanted_vars['google_conversion_value'].';'.
                  '/* ]]> */'.
                  '</script>'.
                  '<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">'.
                  '</script>'.
                  '<noscript>'.
                  '<div style="display:inline;">'.
                  '<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/'
                  .$conversion_id.'/?value='.$wanted_vars['google_conversion_value']
                  .'&amp;label='.$wanted_vars['google_conversion_label'].'&amp;guid=ON&amp;script=0"/>'.
                  '</div>'.
                  '</noscript>';

      //dd("PPC Tagging - PAGE OUTOUT");
      //dd($tagging);
      $variables['analytics'] .= $tagging;
    }
  }
}


/**
 * Whats the tagging values to go on page $page_url.
 *
 * @return null if no tagging for this page, else the string of tagged name/value pairs.
 */
function pdl2_ppc_get_vars_for_page($page_url) {
  // get the var list for the url/page
  $ppc_obj = pdl2_ppc_get_ppc_obj_from_url($page_url);
  if ($ppc_obj == null) {
    return null;
  }
  $selected_vars = pdl2_ppc_get_page_vars($ppc_obj);
  return $selected_vars;
}


/**
 * Load the ppc object from the database for the given URL.
 * URL will be unique in Database.
 */
function pdl2_ppc_get_ppc_obj_from_url($page_url) {
  $query = "select * from {pdl2_ppc} where page_url = '%s'";
  $data = db_fetch_object(db_query($query, $page_url));
  //dd("loading ppc_obj for '". $page_url."'");
  //dd($data);
  return $data;
}


function pdl2_ppc_sitewide_setup_form(&$form_state) {

  $form['id_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t("Id's to tag this site for analytics."),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['id_fieldset']['conversion_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Conversion ID'),
    '#size' => 20,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#description' => t('Please enter the unique site conversion ID for tagging.'),
    '#default_value' => variable_get('pdl2_ppc_conversionid' ,""),
  );

  $form['id_fieldset']['language'] = array(
    '#type' => 'textfield',
    '#title' => t('Language'),
    '#size' => 20,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#description' => t('Please enter the Language code for tagging. (default is en_US)'),
    '#default_value' => variable_get('pdl2_ppc_language' ,"en_US"),
  );

  $form['id_fieldset']['format'] = array(
    '#type' => 'textfield',
    '#title' => t('Format'),
    '#size' => 20,
    '#maxlength' => 40,
    '#required' => TRUE,
    '#description' => t('Please enter the foramt for tagging. (default is 1)'),
    '#default_value' => variable_get('pdl2_ppc_format' ,"1"),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );

  return $form;

}


function pdl2_ppc_sitewide_setup_form_submit ($form, &$form_state) {
  variable_set('pdl2_ppc_conversionid' , $form['id_fieldset']['conversion_id']['#value']);
  variable_set('pdl2_ppc_language' , $form['id_fieldset']['language']['#value']);
  variable_set('pdl2_ppc_format' , $form['id_fieldset']['format']['#value']);
  drupal_set_message("SiteWide values saved.");
}

<div class="dashboard">
	<p><?php echo $themedata['company_name']; ?> would like to thank you for your patience in our application process. Based on the information you provided, we were unable to approve you for a loan.</p>
  <p>Please try reapplying with us at a later date.</p>
</div>
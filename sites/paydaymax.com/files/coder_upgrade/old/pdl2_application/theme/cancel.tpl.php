<div class="cancel-page">
	<h1>You've chosen to cancel your loan application.</h1>
	<h2>We're sorry that we were unable to help you at this time.</h2>
	<p>Login to your account now and check out the services that we provide to all of our customers. If you have any questions, please call 1-866-427-7963 and one of our customer service representatives will be happy to assist you.</p>
</div> 
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<table>
  <tbody>
    <tr>
      <th>Payment Number</th>
      <th>Payment Amount</th>
      <th>Principal Portion</th>
      <th>Interest Portion</th>
      <th>Payment Due Date</th>
      <th>Remaining Balance</th>
    </tr>
    <?php foreach ($themedata['schedule']->result as $keySchedule => $valSchedule):?>
      <tr>
        <td><?php echo $keySchedule + 1;?></td>
        <td>$<?php echo number_format($valSchedule->totalPayment, 2);?></td>
        <td>$<?php echo number_format($valSchedule->principalPayment, 2);?></td>
        <td>$<?php echo number_format($valSchedule->financeCharge, 2);?></td>
        <td><?php echo $valSchedule->date->date;?></td>
        <?php $remainingBalance = $valSchedule->priorPrincipal - $valSchedule->principalPayment;?>
        <td>$<?php echo number_format($remainingBalance, 2);?></td>
      </tr>
    <?php endforeach;?>
  </tbody>
</table>
<?php if ($themedata['real_amount']) { ?>
  <div id="to-be-deposited" class="padding10-nobottom loan-change font12 l-info lleft">
    <strong>The amount that to be deposited to your account will be: $<?= $themedata["real_amount"] ?></strong>
  </div>
<?php } ?>
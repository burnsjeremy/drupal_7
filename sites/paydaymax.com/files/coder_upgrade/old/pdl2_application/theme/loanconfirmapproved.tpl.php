<div>
  
  <p>Your loan has been APPROVED, pending verification of your information.</p>
  <form action="" method="post"><div><input type="button" value="Print this page" onclick="window.print();"></div></form>
  
  
  <h1 class="clean">LOAN SUMMARY</h1>
  
  <p>Due date:       <?php echo date('D M j, Y', strtotime($themedata['agreement']->loan_agreement->dueDate->date)); ?></p>
  <p>Loan Amount:    <?php echo number_format($themedata['agreement']->loan_agreement->amountFinanced, 2); ?></p>
  <p>Finance Charge: <?php echo number_format($themedata['agreement']->loan_agreement->financeCharge, 2); ?></p>
  <p>Total Due:      <?php echo number_format($themedata['agreement']->loan_agreement->totalPayments, 2); ?></p>
  
  <p>
    Loan approval and funds disbursement are based on our ability to verify information provided in your application.
    We reserve the right to request additional information and/or cancel the transaction if we are unable to verify your information.
  </p>
  
  <h2 class="clean">Money Delivery Information</h2>
  <p>Funds Sent Via: Bank Account</p>
  <p>
    Pending verification, the funds will be released to your bank on <?php echo date('D M j, Y', strtotime($themedata['funds_release_date'])); ?> at 4:30 pm Pacific Time.
    The funds should be available on <?php echo date('D M j, Y', strtotime($themedata['funds_expected_completion_time'])); ?>.
    Some banks do not immediately post electronic deposits, so call your bank's ACH department if you have any questions.
  </p>
    
  <?php if ($themedata['loanType'] == 'payday') : ?>
    <h2 class="clean">Other Information</h2>
    <p>Please Note: Payday loans are meant for short-term money needs, however, we allow you to extend your loan if necessary.</p>
    <p>
      If you decide to extend your loan, you must let us know before <?php echo date('D M j, Y', strtotime($themedata['agreement']->loan_agreement->dueDate->date)); ?>.  
      That is 7:00 pm Eastern Time on the day before your scheduled due date.
    </p>
    <?php if ($themedata['payFreq']== "Monthly") { ?>
    <p>
      After the first (1) extension, you are required to pay on your principal amount (details are in
      the frequently asked questions section of the web site).
    </p>
    <?php } else { ?>
    <p>
      After the first (3) extensions, you are required to pay on your principal amount (details are in
      the frequently asked questions section of the web site).
    </p>
    <?php } ?>
  <?php endif; //end if loantype == payday ?>
  <div class="center">
    <p>
      We have a referral program set up for our customers in which you can earn $100.00 for each successful referral!
      Your referral code is <?php echo $themedata['referral_code']; ?>.
      Please use this code to refer your friends and family.
    </p>
  </div>

</div>
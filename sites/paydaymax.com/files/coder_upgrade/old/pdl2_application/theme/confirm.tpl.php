<div class="confirm_page">
<h1 class="step fourof5">Review</h1>
	<?php foreach ($themedata['fieldArray'] as $keyHeader => $valHeader) :?>
		<div class="info_container padding10">
		<?php if($keyHeader == "Personal Information") { ?>
			<div class="review_text">Please review the following information to confirm it is all correct</div>
			<div class="page_head uppercase">Almost done, let's review</div>
		<?php } ?>
	    	<div class="ic_head uppercase"><?php echo $keyHeader;?></div>
	    	
	    	
	    	
	    	<?php foreach ($valHeader as $keyFields => $valFields):?>
	        	<div class="ic_contain">
		        	<div class="info_3 ic_right"><div class="ic_right">[<?php echo l('edit', $valFields['url']);?>]</div></div>
		        	<div class="info_2 ic_right"><?php echo $valFields['value'];?></div>
		        	<div class="info_1"><div class="ic_right"> <?php echo t($valFields['title']);?></div></div>
		        	<div class="clear"></div>
		        </div>
		    <?php endforeach;?>
		    
		    
		</div>
	<?php endforeach;?>
</div>
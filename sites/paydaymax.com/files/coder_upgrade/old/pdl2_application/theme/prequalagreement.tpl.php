<?php
  $siteName = $themedata['siteName'];
?>
<div><p>By Clicking Next, you Agree to the Prequalification Agreement</p></div>
<div class="dashboard">
	<p>I understand that <?= $siteName ?> will not perform credit checks with traditional credit bureaus, but I agree to allow verification of the information given in my application with national consumer reporting agencies  which provide access to non-traditional consumer credit data, including, but not limited to, Teletrack and Clarity</p>
</div>

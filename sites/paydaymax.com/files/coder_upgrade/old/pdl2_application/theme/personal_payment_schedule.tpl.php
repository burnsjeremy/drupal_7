<p><strong>Your Payment Schedule</strong></p>
<table id="itemization" class="small" width="100%">
  <tbody>
    <tr>
      <td width="16%">Payment Number</td>
      <td width="16%">Payment Amount</td>
      <td width="16%">Principal Portion</td>
      <td width="16%">Interest Portion</td>
      <td width="16%">Payment Due Date</td>
      <td width="16%">Remaining Balance</td>
    </tr>
    
    <?php 
    $count = 1;
    foreach ($themedata["schedule"] as $item) { ?>
    
    <tr>
      <td><?php print $count; ?></td>
      <td>$<?php print number_format($item->totalPayment, 2); ?></td>
      <td>$<?php print number_format($item->principalPayment, 2); ?></td>
      <td>$<?php print number_format($item->financeCharge, 2); ?></td>
      <td><?php print $item->date->date; ?></td>
      <td>$<?php print number_format($item->principal, 2); ?></td>
    </tr>
    
    <?php $count++;
    }?>
    
  </tbody>
</table>
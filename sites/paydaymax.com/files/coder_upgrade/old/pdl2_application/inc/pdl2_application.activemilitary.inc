<?php
function pdl2_application_activemilitary_page() {
  
  //Get core API
  $pdl2 = pdl2_core_get_api();
  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  //is optOut out?
  else if ($pdl2->currUser->optOut) {
    drupal_goto('account/logout');
  }
  
  $themedata = array();
  $imagePath = base_path() . drupal_get_path("module", "pdl2_application") . "/images/";
  $themedata['imagePath'] = $imagePath;
  $output = theme('activemilitary', $themedata);

  return $output;
}
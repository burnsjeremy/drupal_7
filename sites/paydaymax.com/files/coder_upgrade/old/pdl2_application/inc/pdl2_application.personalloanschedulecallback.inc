<?php
function pdl2_application_loanschedulecallback_function($loan_type, $key, $val) {

	// First let's see if we have an existing session
	if (!pdl2_account_is_logged_in()) {
		echo '';
		exit;
	}
	else if (pdl2_account_not_opt_out()) {
		echo '';
		exit;
	}
	
	$pdl2 = pdl2_core_get_api();
	
	$draftLoanId = $_SESSION['personalLoanDraftHash'];
	switch($key) {
		case 'coupon':
			$_SESSION['couponCode'] = $val;
			$pdl2->loanPersonalDraftsetCoupon($draftLoanId, $val);
			break;
		case 'principal':
      $pdl2->loanPersonalDraftSetPrincipal($draftLoanId, $val);
			break;
		case 'loanduedate':
		  $pdl2->loanPersonalDraftSetDueDate($draftLoanId, $val);
		  break;
	}
	
    $personalSchedule = $pdl2->loanPersonalDraftGetSchedule($draftLoanId);
	  $real_amount = $pdl2->loanPersonalDraftGetAgreement($draftLoanId)->loan_agreement->realAmount;
		
    $themedata['schedule'] = $personalSchedule;
		$themedata['real_amount'] = $real_amount;
    
	$res = array('schedule'=> theme('personalschedule', $themedata));
	$json = json_encode($res);
	echo $json;
}

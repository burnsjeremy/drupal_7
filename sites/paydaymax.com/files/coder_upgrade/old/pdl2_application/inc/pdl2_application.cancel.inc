<?php
function pdl2_application_cancel_page() {

  // Let's allow people to hit this at any point. They don't need to be logged in.
  $pdl2 = pdl2_core_get_api();
  //is logged in?
  if (!$pdl2->hasSessionData()) {
    drupal_goto("account/login");
  }
  //is opt out?
  else if ($pdl2->currUser->optOut) {
    drupal_goto('account/logout');
  }

  // Cancel the loan (required for tracking reasons)
  try {
    if (isset($_SESSION["loanDraftHash"]))
      $pdl2->loanDraftCancel($_SESSION["loanDraftHash"], "payday");
    else if (isset($_SESSION["personalLoanDraftHash"]))
      $pdl2->loanDraftCancel($_SESSION["personalLoanDraftHash"], "personal");
  } catch (Exception $e) {
    watchdog("pdl2_application", "Failed to cancel loan: %msg", array("%msg"=>$e->getMessage()), WATCHDOG_ERROR);
  }
  
  // Clear it all
  unset($_SESSION['data']);
  unset($_SESSION["personalLoanDraftHash"]);
  unset($_SESSION["loanDraftHash"]);
  unset($_SESSION['loanid']);
  unset($_SESSION['loanType']);

  $themedata = array();
  //$imagePath = base_path() . drupal_get_path("module", "pdl2_application") . "/images/";
  //$themedata['imagePath'] = $imagePath;
  $output = theme('cancel', $themedata);

  return $output;
}
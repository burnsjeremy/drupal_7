<?php

function pdl2_application_loanagreement_page($loan_type) {

  drupal_add_css(drupal_get_path('module', 'pdl2_application') .'/theme/pdl2_application.css');

	$pagename = 'apply-agreement';
	$catid = '102APPL';

	$isPersonal = ($loan_type == "personal");

	$pdl2 = pdl2_core_get_api();

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_application", "pdl2_application_loanagreement_page");

	//is logged in?
	if (!$pdl2->hasSessionData()) {
		drupal_goto("account/login");
	}
	//is opt out?
	else if ($pdl2->currUser->optOut) {
		drupal_goto('account/logout');
	}
	else if (!$isPersonal && !isset($_SESSION['loanDraftHash']) && !$_SESSION['loanDraftHash']){
    drupal_goto('account');
	}
	else if ($isPersonal && !isset($_SESSION['personalLoanDraftHash']) && !$_SESSION['personalLoanDraftHash']){
    drupal_goto('account');
	}
	
	   
	if ($isPersonal) {
      $draftLoanId = $_SESSION['personalLoanDraftHash'];
      $agreement = $pdl2->loanPersonalDraftGetAgreement($draftLoanId);
      if (!$agreement) {
        drupal_goto('account');
      }
      
      $borrower = $pdl2->getUserInfo();
      $ssn = $pdl2->getSsn();
      $schedule= $pdl2->loanPersonalDraftGetSchedule($draftLoanId);
      
	} else {
      $draftLoanId = $_SESSION['loanDraftHash'];
      $agreement = $pdl2->getLoanDraftAgreement($draftLoanId);
      if (!$agreement) {
        drupal_goto('account');
      }

      $borrower = $pdl2->getUserInfo();
      $ssn = $pdl2->getSsn();
     
	}

	$themedata = array(
		'agreement' => $agreement,
		'date' => $agreement->loan_agreement->createDate->date,
		'borrower' => $borrower,
		'ssn' => $ssn,
		'schedule' => (isset($schedule) ? $schedule->result : null),
		'loan_type' => $loan_type
	);
	
	$output = drupal_get_form('pdl2_application_loanagreement_page_form', $themedata) . pdl2_application_write_pageview($pagename, $catid);

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

	return $output;
}

function pdl2_application_loanagreement_page_form($formArray, $themedata) {

  $form["disablesubmit"] = array("#value" => '<script type="text/javascript">$("#pdl2-application-loanagreement-page-form").submit(function(){$("#edit-submit", this).attr("disabled", "disabled");});</script>');

	$form['agreementIntro'] = array(
		'#value' => '<p class="agreementintro"><strong>Before a loan can be completed, you must check "I Agree" at the bottom of this agreement.</strong></p>'
	);
	
	$form['openDiv'] = array('#value' => '<div id="fullAgreement">');

   if ($themedata['agreement']->loan_agreement->parentLoanId){
      $form['lenderborrower'] = array('#value' => theme('personal_lenderborrower', $themedata));
   } else {
      $form['lenderborrower'] = array('#value' => theme('lenderborrower', $themedata));
   }


	$form['dashboard'] = array(
		'#prefix' => '<div class="agreementBottom">',
		'#suffix' => '</div>',
	);

	$form['dashboard']['agreementHeader'] = array('#value' => '<h3>Loan Agreement</h3>');

	$form['dashboard']['agreementText'] = array('#value' => $themedata['agreement']->loan_agreement->agreementText);

	$form['dashboard']['eftTextQuestion'] = array('#value' => $themedata['agreement']->loan_agreement->eftTextQuestion);

	$form['dashboard']['eftOptions'] = array(
		'#type' => 'radios',
		'#required' => true,
		'#options' => array($themedata['agreement']->loan_agreement->eftTextYes, $themedata['agreement']->loan_agreement->eftTextNo),
        '#default_value' => '0',
	);

	$form['dashboard']['eftConsentText'] = array('#value' => $themedata['agreement']->loan_agreement->consentText);

	$form['loan_agree'] = array(
	  '#prefix' => '<div class="agreementAgree">',
	  '#suffix' => '</div>',
		'#type' => 'checkbox',
		'#title' => 'I agree <span class="form-required">*</span>',
	);

	$form['buttons'] = array(
		'#type' => 'fieldset',
		'#attributes' => array('class' => 'btns')
	);

	$form['buttons']['next'] = array(
		'#type' => 'submit',
		'#value' => t('NEXT'),
		'#attributes' => array('class' => 'image continue rright')
	);

	$form['buttons']['back'] = array(
		'#type' => 'submit',
		'#value' => t('BACK'),
		'#validate' => array('pdl2_application_loanagreement_back_validate'),
		'#submit' => array('pdl2_application_loanagreement_back_submit'),
		'#attributes' => array('class' => 'image back lleft')
	);

	$form['buttons']['cancel'] = array(
		'#type' => 'submit',
		'#value' => t('CANCEL'),
		'#validate' => array('pdl2_application_loanagreement_cancel_validate'),
		'#submit' => array('pdl2_application_loanagreement_cancel_submit'),
		'#attributes' => array('class' => 'image cancel lleft')
	);


	$form['required_key'] = array(
		'#value' => '<p class="center"><span class="form-required">*</span> Indicates a required field.</p>'
	);

	$form['closeDiv'] = array('#value' => '</div>');

	return $form;

}

function pdl2_application_loanagreement_page_form_validate($form, &$form_state) {

	$loan_agree = $form_state['values']['loan_agree'];

	if(!$loan_agree){
		form_set_error('loan_agree', t('You must agree to the terms.'));
	}

}

function pdl2_application_loanagreement_page_form_submit($form, &$form_state) {

	$pdl2 = pdl2_core_get_api();

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_application", "pdl2_application_loanagreement_page_form_submit");

	// options array 0 and 1. yes = 0. reverse bool
	$eftOption = $form_state['values']['eftOptions'];
	$agrees = $eftOption == 1 ? 0 : 1;

	if (isset($_SESSION['loanDraftHash'])) {
  	$draftLoanId = $_SESSION['loanDraftHash'];
  	$setEft = $pdl2->setEft($draftLoanId,$agrees);
  	$loanId = $pdl2->saveLoan($draftLoanId);
	} else {
  	$draftLoanId = $_SESSION['personalLoanDraftHash'];
  	$setEft = $pdl2->loanPersonalDraftAgreementSetEft($draftLoanId,$agrees);
  	$loanId = $pdl2->customerLoanPersonalSave($draftLoanId);
	}

	$_SESSION['loanId'] = $loanId;
	
	$loanType = $pdl2->getLoanType($loanId);
	$loanStatusObject = $pdl2->getLoanStatus($loanId, $loanType);
	$loanStatus = $loanStatusObject->loan_status;
    
	$slugs = array(
		'approved' => array(3,4,5),
		'denied' => array(9)
	);
	    
	$slug = pdl2_application_multidimensional_search($loanStatus->code,$slugs);
	$slug = !$slug ? 'accepted' : $slug;
  $userType = ($pdl2->pdl2_account_isNewUser()) ? "new" : "previous";
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();
	
  $form_state['redirect'] = 'confirm/' . $loanType . '/' . $slug . '/' . $userType;

}

function pdl2_application_loanagreement_back_validate($form, &$form_state) {

}

function pdl2_application_loanagreement_cancel_validate($form, &$form_state) {

}

function pdl2_application_loanagreement_back_submit($form, &$form_state) {
	$pdl2 = pdl2_core_get_api();
	$loanType = $_SESSION["loanType"];
	drupal_goto('apply/' . $loanType . '/loanrequest');
}

function pdl2_application_loanagreement_cancel_submit($form, &$form_state) {
	drupal_goto('cancel');
}

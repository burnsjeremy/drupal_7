$(document).ready(function ()
{

    //hide fields to show them
    function hideAllFields()
    {
        hideBiWeeklyDateWrappers();
        hideDayWrappers();
        hideMonthlyWrappers();
        hideTwiceMonthlyWrappers();
        $('#edit-paycheckAmount-wrapper').hide();
    }


  //parents filter to reset form when needed
  jQuery.expr[':'].parents = function (a, i, m)
  {
      return jQuery(a).parents(m[3]).length < 1;
  };
  //IE7 hack //can be used throughout this now //TODO - remove when better solution is found
  var changeHeight = false;
  if (navigator.appVersion.indexOf("MSIE 7.") != -1){
    changeHeight = true;
  }

////////////////Pay Frequency/////////////////////

    //on pay freq change, reset all but pay frequency
    $('#edit-pay-frequency').change(function (e)
    {
        $(this).parents("#pay_day_wizard").find('input:text, input:password, input:file', 'textarea').val('');
        $(this).parents("#pay_day_wizard").find('input:radio').removeAttr('checked').removeAttr('selected').removeAttr('disabled');
        $(this).parents("#pay_day_wizard").find('option').filter(':parents(#edit-pay-frequency)').removeAttr('selected');

        showHideFrequencyFields($(this).val());
    });
    
    //Pay freq show/hide proper fields
    function showHideFrequencyFields(payFrequency)
    {
        //hide your kids
        hideAllFields();
        switch (payFrequency)
        {
        case 'paid_once_a_week':
            //IE7 hack //TODO - remove when better solution is found
            if (changeHeight){
              $('#pay_day_wizard').height(120);
            }
            //Show Select
            $('#edit-weekly-day-wrapper').show();
            //Append paycheck input, then show
            $('#edit-paycheckAmount-wrapper').appendTo("#weekly-1-date-options-2");
            $('#weekly-1-date-options-2').show();
            $('#edit-paycheckAmount-wrapper').show();
            break;
        case 'paid_every_other_week':
            //IE7 hack //TODO - remove when better solution is found
            if (changeHeight){
              $('#pay_day_wizard').height(120);
            }
            //append select, then show
            $('#edit-bi-weekly-day-wrapper').appendTo('#bi-weekly-1-date-options-2');
            $('#bi-weekly-1-date-options-2').show();
            $('#edit-bi-weekly-day-wrapper').show();
             //Append paycheck input, then show
            $('#edit-paycheckAmount-wrapper').appendTo("#bi-weekly-pay-amount-input");
            $('#bi-weekly-pay-amount-input').show();
            $('#edit-paycheckAmount-wrapper').show();
            break;
        case 'paid_twice_a_month':
            //IE7 hack //TODO - remove when better solution is found
            if (changeHeight){
              $('#pay_day_wizard').height(350);
            }
            //show radio wrappers
            showTwiceMonthlyWrappers();
            //hide paycheck input until needed b/c it could be shown in another choice
            $('#edit-paycheckAmount-wrapper').hide();
            break;
        case 'paid_once_a_month':
            //IE7 hack //TODO - remove when better solution is found
            if (changeHeight){
              $('#pay_day_wizard').height(350);
            }
            //show radio wrappers
            showHideMonthlyWrappers();
            //hide paycheck input until needed b/c it could be shown in another choice
            $('#edit-paycheckAmount-wrapper').hide();
            break;
        }
    }

    //get pay freq select val
    function getPayFrequency()
    {
        return $('#edit-pay-frequency').val();
    }

    

/////////////BI-weekly///////////////////

    function hideDayWrappers()
    {
        $('#edit-bi-weekly-day-wrapper').hide();
        $('#edit-weekly-day-wrapper').hide();
    }

    function showHideDateFields(term, dayOfWeek)
    {
        hideBiWeeklyDateWrappers();
        var wrapperName = '#' + term + '-date-' + dayOfWeek + '-wrapper';

        if ($('#edit-pay-frequency').val() == 'paid_every_other_week')
        {
            $(wrapperName).appendTo('#bi-weekly-1-date-options-2');
        }
        else if ($('#edit-pay-frequency').val() == 'paid_twice_a_month')
        {
            $(wrapperName).appendTo('#bi-weekly-2-date-options-2');
        }
        $(wrapperName).show();
    }
    
    function hideBiWeeklyDateWrappers()
    {
        $('#bi-weekly-date-monday-wrapper').hide();
        $('#bi-weekly-date-tuesday-wrapper').hide();
        $('#bi-weekly-date-wednesday-wrapper').hide();
        $('#bi-weekly-date-thursday-wrapper').hide();
        $('#bi-weekly-date-friday-wrapper').hide();
        $('#bi-weekly-date-saturday-wrapper').hide();
        $('#bi-weekly-date-sunday-wrapper').hide();
    }    

    $('#edit-bi-weekly-day').change(function (e)
    {
        showHideDateFields('bi-weekly', $(this).val());
    });

    
//////////Twice monthly Radios///////////
    
    //IE .change() requires blur
    $('input[name=twice_monthly_radios]').click(function (e)
    {
        this.blur();  
        //this.focus();
    });
    
    //Twice monthly radio change, make radio false, then show other fields
    $('input[name=twice_monthly_radios]').change(function (e)
    {
        
        $(this).parents("#pay_day_wizard").find('input:text, input:password, input:file', 'textarea').val('');
        $(this).parents("#pay_day_wizard").find('input:radio').filter(':parents(#twice-monthly-radios-wrapper)').removeAttr('checked').removeAttr('selected');
        $(this).parents("#pay_day_wizard").find('option').filter(':parents(#edit-pay-frequency)').removeAttr('selected');
        //had to put this for the select box problems with appending to a label
        if ($('input:radio[name=twice_monthly_radios]:checked').val() == $(this).val() && $(this).val != '')
        {
            $('input:radio[name=twice_monthly_radios]').attr("disabled", false);
            $(this).attr("disabled", true);
        }

        showHideFrequencyFields($(this).val());
    });
    
    function showTwiceMonthlyWrappers()
    {
        //reset, then re-show
        resetTwiceMonthlyWrappers();
        var twiceMonthlyRadio = '';
        if ($('#edit-twice-monthly-radios-bi-weekly').attr('checked'))
        {
            twiceMonthlyRadio = 'bi-weekly';
            if (changeHeight){
              $('#pay_day_wizard').height(350);
            }
        }
        else if ($('#edit-twice-monthly-radios-date').attr('checked'))
        {
            twiceMonthlyRadio = 'date';
            if (changeHeight){
              $('#pay_day_wizard').height(365);
            }
        }
        else if ($('#edit-twice-monthly-radios-week').attr('checked'))
        {
            twiceMonthlyRadio = 'week';
            if (changeHeight){
              $('#pay_day_wizard').height(370);
            }
        }
        //get checked above and pass through
        showHideTwiceMonthlyFields(twiceMonthlyRadio);
    }
    
    //Twice monthly Radios show/hide proper fields
    function showHideTwiceMonthlyFields(twiceMonthlyRadio)
    {
        switch (twiceMonthlyRadio)
        {
        case 'bi-weekly':
            $('#edit-bi-weekly-day-wrapper').appendTo('#bi-weekly-date-options-1');
            $('#bi-weekly-date-options-1').show();
            $('#edit-bi-weekly-day-wrapper').show();
            $('#edit-paycheckAmount-wrapper').hide();
            $('#edit-paycheckAmount-wrapper').appendTo("#edit-pay-bi-weekly");
            $('#edit-pay-bi-weekly').show();
            $('#edit-paycheckAmount-wrapper').show();
            break;
        case 'date':
            $('#edit-twice-monthly-date-1-wrapper').appendTo('#date-date-options-1');
            $('#edit-twice-monthly-date-2-wrapper').appendTo('#date-date-options-2');
            $('#date-date-options-1').show();
            $('#date-date-options-2').show();
            $('#edit-twice-monthly-date-1-wrapper').show();
            $('#edit-twice-monthly-date-2-wrapper').show();
            $('#edit-paycheckAmount-wrapper').hide();
            $('#edit-paycheckAmount-wrapper').appendTo("#edit-pay-date-weekly");
            $('#edit-pay-date-weekly').show();
            $('#edit-paycheckAmount-wrapper').show();
            break;
        case 'week':
            $('#edit-twice-monthly-day-select-1-wrapper').appendTo('#week-week-options-1');
            $('#edit-twice-monthly-day-select-2-wrapper').appendTo('#week-week-options-2');
            $('#week-week-options-1').show();
            $('#week-week-options-2').show();
            $('#edit-twice-monthly-day-select-1-wrapper').show();
            $('#edit-twice-monthly-day-select-2-wrapper').show();
            $('#edit-paycheckAmount-wrapper').hide();
            $('#edit-paycheckAmount-wrapper').appendTo("#edit-pay-week-weekly");
            $('#edit-pay-week-weekly').show();
            $('#edit-paycheckAmount-wrapper').show();
            break;
        }
    }
    
    //reset twice monthly radios
    function resetTwiceMonthlyWrappers()
    {
        hideTwiceMonthlyWrappers();
        $('#twice-monthly-radios-wrapper').show();
    }
    
    //hide wrappers
    function hideTwiceMonthlyWrappers()
    {
        $('#twice-monthly-radios-wrapper').hide();
        $('#edit-bi-weekly-day-wrapper').hide();
        hideBiWeeklyDateWrappers();
        hideTwiceMonthlyDateWrappers();
        hideTwiceMonthlyDayWrappers();
    }

    //hide date wrappers
    function hideTwiceMonthlyDateWrappers()
    {
        $('#edit-twice-monthly-date-1-wrapper').hide();
        $('#edit-twice-monthly-date-2-wrapper').hide();
    }

    //hide day wrappers
    function hideTwiceMonthlyDayWrappers()
    {
        $('#edit-twice-monthly-day-select-1-wrapper').hide();
        $('#edit-twice-monthly-day-select-2-wrapper').hide();
    }



//////////Monthly Radios///////////

    //IE .change() requires blur
    $('input[name=monthly_radios]').click(function (e)
    {
        this.blur();  
    });
    
    //Monthly radio change, make radio false, then show other fields
    $('input[name=monthly_radios]').change(function (e)
    {
        $(this).parents("#pay_day_wizard").find('input:text, input:password, input:file', 'textarea').val('');
        $(this).parents("#pay_day_wizard").find('input:radio').filter(':parents(#monthly-radios-wrapper)').removeAttr('checked').removeAttr('selected');
        $(this).parents("#pay_day_wizard").find('option').filter(':parents(#edit-pay-frequency)').removeAttr('selected');
        //had to put this for the select box problems with appending to a label
        if ($('input:radio[name=monthly_radios]:checked').val() == $(this).val() && $(this).val != '')
        {
            $('input:radio[name=monthly_radios]').attr("disabled", false);
            $(this).attr("disabled", true);
        }
        showHideFrequencyFields($(this).val());
    });
    
    //show/hide monthly wrappers
    function showHideMonthlyWrappers()
    {
        resetMonthlyWrappers();
        var monthlyRadio = '';
        if ($('#edit-monthly-radios-day').attr('checked'))
        {
            monthlyRadio = 'day';
            if (changeHeight){
              $('#pay_day_wizard').height(350);
            }
        }
        else if ($('#edit-monthly-radios-date').attr('checked'))
        {
            monthlyRadio = 'date';
            if (changeHeight){
              $('#pay_day_wizard').height(360);
            }
        }
        else if ($('#edit-monthly-radios-after').attr('checked'))
        {
            monthlyRadio = 'after';
            if (changeHeight){
              $('#pay_day_wizard').height(370);
            }
        }
        //get checked above and pass through
        showHideMonthlyFields(monthlyRadio);
    }
    
    //Monthly Radios show/hide proper fields
    function showHideMonthlyFields(monthlyRadio)
    {
        switch (monthlyRadio)
        {
        case 'day':
            $('#edit-monthly-week-wrapper').appendTo('#day-day-options-1');
            $('#edit-monthly-day-wrapper').appendTo('#day-day-options-2');
            $('#day-day-options-1').show();
            $('#day-day-options-2').show();
            $('#edit-monthly-week-wrapper').show();
            $('#edit-monthly-day-wrapper').show();
            $('#edit-paycheckAmount-wrapper').hide();
            $('#edit-paycheckAmount-wrapper').appendTo("#edit-pay-day-monthly");
            $('#edit-pay-day-monthly').show();
            $('#edit-paycheckAmount-wrapper').show();
            break;
        case 'date':
            $('#edit-monthly-date-wrapper').appendTo('#monthly-date-options-1');
            $('#monthly-date-options-1').show();
            $('#edit-monthly-date-wrapper').show();
            $('#edit-paycheckAmount-wrapper').hide();
            $('#edit-paycheckAmount-wrapper').appendTo("#edit-pay-date-monthly");
            $('#edit-pay-date-monthly').show();
            $('#edit-paycheckAmount-wrapper').show();
            break;
        case 'after':
            $('#edit-monthly-after-day-wrapper').appendTo('#after-week-options-1');
            $('#edit-monthly-after-date-wrapper').appendTo('#after-week-options-2');
            $('#after-week-options-1').show();
            $('#after-week-options-2').show();
            $('#edit-monthly-after-day-wrapper').show();
            $('#edit-monthly-after-date-wrapper').show();
            $('#edit-paycheckAmount-wrapper').hide();
            $('#edit-paycheckAmount-wrapper').appendTo("#edit-pay-after-monthly");
            $('#edit-pay-after-monthly').show();
            $('#edit-paycheckAmount-wrapper').show();
            break;
        }
    }
    
    //reset monthly radios
    function resetMonthlyWrappers()
    {
        hideMonthlyWrappers();
        $('#monthly-radios-wrapper').show();
    }
    
    //hide all monthly wrappers
    function hideMonthlyWrappers()
    {
        $('#edit-monthly-week-wrapper').hide();
        $('#edit-monthly-day-wrapper').hide();
        $('#edit-monthly-date-wrapper').hide();
        $('#edit-monthly-after-date-wrapper').hide();
        $('#edit-monthly-after-day-wrapper').hide();
        $('#monthly-radios-wrapper').hide();
    }

    $('#edit-monthly-radios-day').change(function (e)
    {
        resetMonthlyWrappers();
        showHideMonthlyFields($(this).val());
    });

    $('#edit-monthly-radios-date').change(function (e)
    {
        resetMonthlyWrappers();
        showHideMonthlyFields($(this).val());
    });

    $('#edit-monthly-radios-after').change(function (e)
    {
        resetMonthlyWrappers();
        showHideMonthlyFields($(this).val());
    });

    $('#edit-twice-monthly-radios-bi-weekly').change(function (e)
    {
        resetTwiceMonthlyWrappers();
        showHideTwiceMonthlyFields($(this).val());
    });

    $('#edit-twice-monthly-radios-date').change(function (e)
    {
        resetTwiceMonthlyWrappers();
        showHideTwiceMonthlyFields($(this).val());
    });

    $('#edit-twice-monthly-radios-week').change(function (e)
    {
        resetTwiceMonthlyWrappers();
        showHideTwiceMonthlyFields($(this).val());
    });


///////////////////Do When Page Loads///////////////////////

    //onpage load, if error, reset all but pay frequency
    //TODO - check to make sure this isnt the part that is causing problems with users being drilled back down to where they were when they submitted
    if ($('#edit-pay-frequency').attr('value') !== '')
    {
        //added these b/c of the bug that is on sustaining that doesnt keep radio buttons state //TODO - FIX THIS WHEN APPROVED
        $('#edit-pay-frequency').parents("#pay_day_wizard").find('input:radio').filter(':parents(#twice-monthly-radios-wrapper)').removeAttr('checked').removeAttr('selected');
        $('#edit-pay-frequency').parents("#pay_day_wizard").find('input:radio').filter(':parents(#monthly-radios-wrapper)').removeAttr('checked').removeAttr('selected');
        
    }
    //END TODO ////////

    //on page load show correct fields
    showHideFrequencyFields(getPayFrequency());
        

    //APPLY PAGE - Show today's Date
    var today = new Date();
    var day = today.getDay();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    //Days and Months array
    var m = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var d = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    //Choose days and months to display
    day = d[today.getDay()];
    mm = m[today.getMonth()];

    //add zeros to appr fields
    if (dd < 10)
    {
        dd = '0' + dd;
    }
    if (mm < 10)
    {
        mm = '0' + mm;
    }

    //format today string	
    today_format = day + ' ' + mm + ' ' + dd + ', ' + yyyy;

    //print date
    $("#print_today").text('Today is: ' + today_format);

    var needToConfirm = true;
    var cancelOut = false;
    //APPLY PAGE - Window popup and calculates monthly income  
    $(function ()
    {
        $('#pdl2-application-payday-page-form').submit(function (e)
        {
            $("#edit-submit").attr("disabled", "disabled");
            //this is set b/c of the confirm box hack, it only runs once says the powers that be
            needToConfirm = false;
            //alert("Code is being run");
            var pay_freq = $("select#edit-pay-frequency").val();
            var pay_amount = $("select#edit-requested-amount").val();
            //console.log(pay_freq);
            var multiplier;
            var pc_amount = 0;

            if (pay_freq == 'paid_once_a_week')
            {

                multiplier = 4.3333;
                pc_amount = $("input#edit-paycheckAmount").val();

            }
            else if (pay_freq == 'paid_every_other_week')
            {

                multiplier = 2.1666666667;
                pc_amount = $("input#edit-paycheckAmount").val();

            }
            else if (pay_freq == 'paid_twice_a_month')
            {

                multiplier = 2.0000;


                if ($('input:radio[name=twice_monthly_radios]:checked').val() == "bi-weekly")
                {

                    pc_amount = $("input#edit-paycheckAmount").val();

                }
                else if ($('input:radio[name=twice_monthly_radios]:checked').val() == "date")
                {

                    pc_amount = $("input#edit-paycheckAmount").val();

                }
                else if ($('input:radio[name=twice_monthly_radios]:checked').val() == "week")
                {

                    pc_amount = $("input#edit-paycheckAmount").val();

                }

            }
            else if (pay_freq == 'paid_once_a_month')
            {

                multiplier = 1.0000;

                if ($('input:radio[name=monthly_radios]:checked').val() == "date")
                {

                    pc_amount = $("input#edit-paycheckAmount").val();

                }
                else if ($('input:radio[name=monthly_radios]:checked').val() == "day")
                {

                    pc_amount = $("input#edit-paycheckAmount").val();

                }
                else if ($('input:radio[name=monthly_radios]:checked').val() == "after")
                {


                    pc_amount = $("input#edit-paycheckAmount").val();
                }

            }
            
          
            else
            {
                $("#edit-submit").removeAttr('disabled');
                return false;

            }

            if (pc_amount == 0 || pay_amount < 100)
            {
                //This is to remove the disabled hack placed in code above for handling paydaywizard
                $("input:radio[name=monthly_radios]").removeAttr('disabled');
                $("input:radio[name=twice_monthly_radios]").removeAttr('disabled');
                return true;
            }
            else
            {
                var monthly_income = multiplier * pc_amount;
                monthly_income = Math.round(monthly_income * Math.pow(10, 2)) / Math.pow(10, 2);
                if (cancelOut === true)
                {
                    //This is to remove the disabled hack placed in code above for handling paydaywizard
                    $("input:radio[name=monthly_radios]").removeAttr('disabled');
                    $("input:radio[name=twice_monthly_radios]").removeAttr('disabled');
                    return true;
                }
                else
                {
                    //console.log(monthly_income);
                    if (confirm("Please confirm your monthly income of $" + monthly_income))
                    {
                        //This is to remove the disabled hack placed in code above for handling paydaywizard
                        $("input:radio[name=monthly_radios]").removeAttr('disabled');
                        $("input:radio[name=twice_monthly_radios]").removeAttr('disabled');
                        return true;

                    }
                    else
                    {
                        $("#edit-submit").removeAttr('disabled');
                        //there be dragons in these waters
                        cancelOut = true;
                        return false;

                    }
                }
            }


        }); //end submit function
        
        $('#pdl2-application-personal-page-form').submit(function (e)
        {
            $("#edit-submit").attr("disabled", "disabled");

            needToConfirm = false;
            $("#edit-submit").removeAttr('disabled');
            return true;

        }); //end submit function
        
        
    }); //end function

    //do monthly income popup and leave page
    $(window).bind('beforeunload', function ()
    {
    
        if (needToConfirm)
        {
            return "Are you sure you want to leave this page?";
        }

    });
    var today2 = new Date();
    var day2 = today2.getDay();
    var dd2 = today2.getDate();
    var mm2 = today2.getMonth(); //January is 0!
    var yyyy2 = today2.getFullYear();

    ng.ready(function ()
    {
        var my_cal = new ng.Calendar(
        {
            object: 'calendar',
            days_text: 'short',
            visible: true,
            close_on_select: false,
            num_months: 4,
            num_col: 4,
            allow_selection: false,
            weekend: [0, 6],
            start_date: new Date(yyyy2, mm2, dd2).from_string('today'),
            display_date: new Date(yyyy2, mm2, dd2).from_string('today'),
            //range_off: [
            //	['Sep 5th, 2012', 'Sep 10th, 2012'],
            //	['Jan 1st, 2013', 'Jan 12th, 2013']
            //],
            buttons_color: 'none'
        });
    });
});

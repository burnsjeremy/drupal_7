<?php

function pdl2_application_admin() {
	$form = array();
	
	$form["pdl2_application_electronic_disclosure_agreement_link"] = array(
		'#type' => 'textfield',
		'#title' => t('Electronic Disclosure page'),
		'#description' => t('pop-up page url.'),
		'#default_value' => variable_get('pdl2_application_electronic_disclosure_agreement_link', ""),
		'#size' => 50,
		'#required' => TRUE
	);
	
	$form["pdl2_application_privacy_policy_link"] = array(
		'#type' => 'textfield',
		'#title' => t('Privacy Policy page'),
		'#description' => t('pop-up page url.'),
		'#default_value' => variable_get('pdl2_application_privacy_policy_link', ""),
		'#size' => 50,
		'#required' => TRUE
	);
	
	$form["pdl2_application_third_party_sharing_link"] = array(
		'#type' => 'textfield',
		'#title' => t('Third Party Sharing T&C page'),
		'#description' => t('pop-up page url.'),
		'#default_value' => variable_get('pdl2_application_third_party_sharing_link', ""),
		'#size' => 50,
		'#required' => TRUE
	);
	
	$form["pdl2_application_military_link"] = array(
		'#type' => 'textfield',
		'#title' => t('Active Military Personnel & Dependents page'),
		'#description' => t('page url.'),
		'#default_value' => variable_get('pdl2_application_military_link', ""),
		'#size' => 50,
		'#required' => TRUE
	);
	$form["pdl2_application_cancel_link"] = array(
		'#type' => 'textfield',
		'#title' => t('Cancelled Application'),
		'#description' => t('page url.'),
		'#default_value' => variable_get('pdl2_application_cancel_link', ""),
		'#size' => 50,
		'#required' => TRUE
	);

	$form["pdl2_application_enable_ple_online"] = array(
		'#type' => 'checkbox',
		'#title' => t('Enable PLE Online'),
		'#default_value' => variable_get('pdl2_application_enable_ple_online', 0),
		'#description' => t('Enable the use of Personal Loan Extensions online. Otherwise, forward user to PLE phone number.'),
	);

	$form["pdl2_application_enable_real_time_linkshare"] = array(
		'#type' => 'checkbox',
		'#title' => t('Enable Linkshare Real-time'),
		'#default_value' => variable_get("pdl2_application_enable_real_time_linkshare", 0),
		'#description' => t('Enable real time tracking for Linkshare'),
	);
	
	return system_settings_form($form);
}

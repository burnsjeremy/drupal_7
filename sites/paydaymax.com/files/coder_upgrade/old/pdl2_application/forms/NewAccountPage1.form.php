<?php
class NewAccountPage1 extends NewAccountPageAbstract
{
  protected $_confirmFields = array(
      'Personal Information' => array('name' => 'Name',
                                      'address' => 'Address',
                                      'mailaddress' => 'Mailing Address',
                                      'rentorown' => 'Rent or Own',
                                      'incomesource' => 'Income Source',
                                      'homephone' => 'Home Phone',
                                      'cellphone' => 'Cell Phone',
                                      'dob' => 'Date of Birth',
                                      'driverlicensenum' => 'Driver\'s License',
                                      'driverstate' => 'State Issued',
                                      'email' => 'Email',
                                      'ref_code' => 'Referral Code'),
                                    );


  public function getForm($formData) {
  //set quickstart items
  if (isset($_SESSION['qs_amount'])) { 
    $qs_firstname = $_SESSION['qs_firstname'];
    $qs_lastname = $_SESSION['qs_lastname'];
    $qs_email = $_SESSION['qs_email'];
    $qs_zip = $_SESSION['qs_zip'];
  }
  //call api
  $pdl2 = pdl2_core_get_api();
  $siteName = pdl2_core_get_site_name();
  $form = array();
  $form["disablesubmit"] = array("#value" => '<script type="text/javascript">$("#pdl2-application-newaccount-form").submit(function(){$("#edit-submit", this).attr("disabled", "disabled");});</script>');

  $form["toptitle"] = array(
    "#value" => '<h1 class="step oneof5">'.t('Personal Information').'</h1>'
  );

  $form['personinfofieldset'] = array( 
    '#prefix' => '<div class="fieldset">',
    '#suffix' => '</div>', 
  );
  
  $form['personinfofieldset']["isrequired"] = array(
    "#value" => '<p><span class="form-required" title="This field is required.">*</span> '.t('Required Field.').'</p>'
  );
  
  $form['personinfofieldset']["topheader"] = array(
    "#value" => '<h3>'.t('Tell us about yourself').'</h3>'
  );
  $form['personinfofieldset']["headermsg"] = array(
    "#value" => '<div class="padding10 font12">'.t('Please enter your legal name as seen on your social security card.').'</div>'
  );

  $form['personinfofieldset']['name'] = array(
    '#prefix' => '<div class="inline">',
    '#suffix' => '</div>',
  );
                                              
  $form['personinfofieldset']['name']['honorific'] = array(
    '#type' => 'select',
    '#required' => true,
    '#title' => t('First Name'),
    '#default_value' => ($formData['honorific']) ? $formData['honorific'] : '',
    '#options' => array(
      '' => 'Select One',
      3 => 'Mrs.',
      2 => 'Ms.',
      1 => 'Mr.'
    )
  );
  
  $form['personinfofieldset']['name']['firstname'] = array(
    '#type' => 'textfield',
    '#required' => true,
    '#maxlength' => 30,
    '#default_value' => ($formData['firstname']) ? $formData['firstname']: ''.$qs_firstname.''
  );
  
  $form['personinfofieldset']['name']['tooltip'] = array(
    '#value' => '<div class="tooltip" title="'.t('Please tell us your first name.').'"></div>'
  );

  $form['personinfofieldset']['middlename'] = array(
    '#type' => 'textfield',
    '#title' => t('Middle Name'),
    '#maxlength' => 20,
    '#default_value' => ($formData['middlename']) ? $formData['middlename'] : '',
    '#description' => '<div class="tooltip" title="'.t('Please tell us your middle name.').'"></div>'
  );
  
  $form['personinfofieldset']['lastname'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#required' => true,
    '#default_value' => ($formData['lastname']) ? $formData['lastname'] : ''.$qs_lastname.'',
    '#maxlength' => 30,
    '#description' => '<div class="tooltip" title="'.t('Please tell us your last name.').'"></div>'
  );
  
  $form['personinfofieldset']['homestreet1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'). '<br />('.t('Do not enter a PO Box').')',
    '#required' => true,
    '#maxlength' => 30,
    '#default_value' => ($formData['homestreet1']) ? $formData['homestreet1'] : '',
    '#description' => '<div class="tooltip" title="'.t('Please tell us your current home address.').'"></div>'
  );
  
  $form['personinfofieldset']['homestreet2'] = array(
    '#type' => 'textfield',
    '#title' => '&nbsp;',
    '#maxlength' => 30,
    '#default_value' => ($formData['homestreet2']) ? $formData['homestreet2'] : ''
  );
  
  $form['personinfofieldset']['homecity'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#maxlength' => 35,
    '#required' => true,
    '#default_value' => ($formData['homecity']) ? $formData['homecity'] : '',
    '#description' => '<div class="tooltip" title="'.t('Please tell us the city you currently reside in.').'"></div>'
  );
  
  $form['personinfofieldset']['homezip'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip Code'),
    '#required' => true,
    '#size' => 6,
    '#maxlength' => 5,
    '#default_value' => ($formData['homezip']) ? $formData['homezip'] : ''.$qs_zip.'',
    '#description' => '<div class="tooltip" title="'.t('Please tell us your local zip code.').'"></div>',
    '#attributes' => array('class' => 'zip')
  );

  $form['personinfofieldset']['homemail'] = array(
    '#type' => 'fieldset',
    '#required' => true,
    '#title' => t('Click here if mailing address is different than above.'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#tree' => false
  );
  
  $form['personinfofieldset']['homemail']['homemailaddr'] = array(
    '#type' => 'textfield',
    '#title' => t('Mailing Address'),
    '#maxlength' => 30,
    '#default_value' => ($formData['homemailaddr']) ? $formData['homemailaddr'] : ''
  );
  
  $form['personinfofieldset']['homemail']['homemailcity'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#maxlength' => 35,
    '#default_value' => ($formData['homemailcity']) ? $formData['homemailcity'] : ''
  );
  
  $form['personinfofieldset']['homemail']['homemailzip'] = array(
    '#type' => 'textfield',
    '#title' => t('Zip Code'),
    '#size' => 6,
    '#maxlength' => 5,
    '#default_value' => ($formData['homemailzip']) ? $formData['homemailzip'] : '',
    '#attributes' => array('class' => 'zip')
  );

  $form['personinfofieldset']['rentorown'] = array(
    '#type' => 'select',
    '#required' => true,
    '#title' => t('Do you rent or own?'),
    '#default_value' => ($formData['rentorown']) ? $formData['rentorown'] : '',
    '#options' => array('' => 'Select', 'Rent' => 'Rent', 'Own' => 'Own'),
    '#description' => '<div class="tooltip" title="'.t('Please tell us your rent or own your home.').'"></div>',
  );

  $incomeSources = array(
    '' => 'Select a Source',
    'employment' => 'Employment',
    'retirement' => 'Retirement',
    'disability' => 'Disability',
    'self-employment' => 'Self Employment',
    'active-military' => 'Active Military',
    'unemployed' => 'Unemployment',
    'other' => 'Other'
  );

  $form['personinfofieldset']['incomesource'] = array(
    '#type' => 'select',
    '#required' => true,
    '#title' => t('Income Source'),
    '#default_value' => ($formData['incomesource'] >= 0) ? $formData['incomesource'] : '',
    '#options' => $incomeSources,
    '#description' => '<div class="tooltip" title="'.t('Please tell us your main source of income.').'"></div>',
  );

  $form['personinfofieldset']['homephone'] = array(
    '#required' => true,
    '#tree' => false,
    '#prefix' => '<div class="inline phone autotabs">',
    '#suffix' => '</div>'
  );

  $form['personinfofieldset']['homephone']['homephone1'] = array(
    '#title' => t('Home Phone'),
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => ($formData['homephone1']) ? $formData['homephone1'] : '',
    '#size' => 3,
    '#maxlength' => 3,
    '#parent' => 'homephone'
  );
  
  $form['personinfofieldset']['homephone']['homephone2'] = array(
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => ($formData['homephone2']) ? $formData['homephone2'] : '',
    '#size' => 3,
    '#maxlength' => 3,
  );
  
  $form['personinfofieldset']['homephone']['homephone3'] = array(
    '#type' => 'textfield',
    '#required' => true,
    '#default_value' => ($formData['homephone3']) ? $formData['homephone3'] : '',
    '#size' => 4,
    '#maxlength' => 4,
    '#attributes' => array('class' => 'zip'),
  );
  $form['personinfofieldset']['homephone']['tooltip'] = array(
        '#value' => '<div class="tooltip" title="'.t('Please tell us your home phone number where we can reach you.').'"></div>',
  );
  
  $form['personinfofieldset']['homephone']['homephone_mobile'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is this a cellphone?'),
    '#default_value' => ($formData['homephone_mobile']) ? $formData['homephone_mobile'] : '',
    '#prefix' => '<div class="checkbox sub">',
    '#suffix' => '</div><div id="hide-opt-out-primary1" class="phone_info hidden">'.t('By providing your wireless telephone number, you are giving your express consent required under The Telephone Consumer Protection Act (TCPA) for the specific purpose of receiving autodialed calls or prerecorded message calls via your wireless telephone.').'</div>'
  );
  
  $form['personinfofieldset']['cellphone'] = array(
    '#tree' => false,
    '#prefix' => '<div class="inline phone autotabs">',
    '#suffix' => '</div>'
  );

  $form['personinfofieldset']['cellphone']['cellphone1'] = array(
    '#type' => 'textfield',
    '#title' => t('Cell Phone'),
    '#default_value' => ($formData['cellphone1']) ? $formData['cellphone1'] : '',
    '#size' => 3,
    '#maxlength' => 3,
  );
  
  $form['personinfofieldset']['cellphone']['cellphone2'] = array(
    '#type' => 'textfield',
    '#default_value' => ($formData['cellphone2']) ? $formData['cellphone2'] : '',
    '#size' => 3,
    '#maxlength' => 3,
  );
  
  $form['personinfofieldset']['cellphone']['cellphone3'] = array(
    '#type' => 'textfield',
    '#default_value' => ($formData['cellphone3']) ? $formData['cellphone3'] : '',
    '#size' => 4,
    '#maxlength' => 4,
    '#attributes' => array('class' => 'zip'),
  );
  
  $form['personinfofieldset']['cellphone']['tooltip'] = array(
    '#value' => '<div class="tooltip" title="'.t('Please tell us your cell phone number where we can reach you if you are unavailable at your primary numbe.').'"></div>',
  );
  
  $form['personinfofieldset']['cellphone']['cellphone_mobile'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is this a cellphone?'),
    '#default_value' => ($formData['cellphone_mobile']) ? $formData['cellphone_mobile'] : '',
    '#prefix' => '<div class="checkbox sub">',
    '#suffix' => '</div>'
  );
  
  $form['personinfofieldset']['sms'] = array(
    '#type' => 'checkbox',
    '#title' => t('Would you like to sign up for real time text alerts on your account?'),
    '#default_value' => ($formData['cellphone_mobile']) ? $formData['cellphone_mobile'] : '',
    '#prefix' => '<div id="hide-opt-out-primary2" class="phone_info hidden">'.t('By providing your wireless telephone number, you are giving your express consent required under The Telephone Consumer Protection Act (TCPA) for the specific purpose of receiving autodialed calls or prerecorded message calls via your wireless telephone.').'<div id="show-sms" class="checkbox sub padding10">',
    '#suffix' => '</div></div>',
  );
  
  $form['personinfofieldset']['sms_msg'] = array(
    '#value' => '<div id="hide-opt-out-primary3" class="font12 padding10-notop italic lleft hidden">'.t('There is no extra subscription cost to receive these alerts, but standard text messaging rates and data charges from your carrier apply to sending and/or receiving text messages. You may opt out at any time by replying with the word "STOP" to any text message you receive from us.').'</div>',
  );
  
  $form['personinfofieldset']['dob'] = array(
    '#prefix' => '<div class="inline">',
    '#suffix' => '</div>'
  );
  
  $months = DateHelper::$months;
  $form['personinfofieldset']['dob']['dob_month'] = array(
    '#type' => 'select',
    '#title' => t('Date of Birth'),
    '#required' => false,
    '#default_value' => ($formData['dob_month']) ? $formData['dob_month'] : '',
    '#options' => $months,
  );
  
  $days = DateHelper::$days;
  $form['personinfofieldset']['dob']['dob_day'] = array(
    '#type' => 'select',
    '#required' => false,
    '#default_value' => ($formData['dob_day']) ? $formData['dob_day'] : '',
    '#options' => $days,
  );
  
  $yearsArray = array('' => Year);
  $years = DateHelper::getYearArray(1994, 99);
  $yearsArray += $years;
  $form['personinfofieldset']['dob']['dob_year'] = array(
    '#type' => 'select',
    '#required' => false,
    '#default_value' => ($formData['dob_year']) ? $formData['dob_year'] : '',
    '#options' => $yearsArray,
  );
  
  $form['personinfofieldset']['dob']['tooltip'] = array(
    '#value' => '<div class="tooltip" title="'.t('Please tell us the month, day, and year of your birth.').'"></div>',
  );

  $form['personinfofieldset']['driverlicensenum'] = array(
    '#type' => 'textfield',
    '#title' => t("Driver's License or State ID Number"),
    '#required' => true,
    '#maxlength' => 30,
    '#default_value' => ($formData['driverlicensenum']) ? $formData['driverlicensenum'] : '',
    '#description' => '<div class="tooltip" title="'.t("Please tell us your driver's license number or state ID number as this will expedite your loan application.").'"></div>',
  );

  $states = listsHelper::idNameList($pdl2->getStates()->result);
  $form['personinfofieldset']['driverstate'] = array(
    '#type' => 'select',
    '#title' => t("State Issued"),
    '#required' => true,
    '#default_value' => ($formData['driverstate']) ? $formData['driverstate'] : '',
    '#options' => array('' => 'Select a State') + $states,
    '#description' => '<div class="tooltip" title="'.t('Please tell us the state in which your license was issued.').'"></div>',
  );

  $form['logincredentialsfieldset'] = array( 
    '#prefix' => '<div class="fieldset">',
    '#suffix' => '</div>', 
  );

  $form['logincredentialsfieldset']["logincredentials"] = array(
    "#value" => t('<h3>Create your login credentials</h3>'));

  $form['logincredentialsfieldset']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email Address'),
    '#required' => true,
    '#default_value' => ($formData['email']) ? $formData['email'] : ''.$qs_email.'',
    '#description' => '<div class="tooltip" title="'.t('Please tell us the email address you use most frequently.').'"></div>',
  );

  $form['logincredentialsfieldset']['emailconf'] = array(
    '#type' => 'textfield',
    '#title' => t('Confirm Email'),
    '#default_value' => ($formData['emailconf']) ? $formData['emailconf'] : '',
    '#required' => true,
    '#description' => '<div class="tooltip" title="'.t('Please confirm that this is the email address you use most frequently.').'"></div>',
  );

  $form['logincredentialsfieldset']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#required' => true,
    '#maxlength' => 32,
    '#description' => '<div class="tooltip" title="'.t('Please create an account password that contains a minimum of six characters and at least one numeral.').'"></div>',
  );

  $form['logincredentialsfieldset']['passwordconf'] = array(
    '#type' => 'password',
    '#title' => t('Password Confirm'),
    '#required' => true,
    '#maxlength' => 32,
    '#default_value' => ($formData['passwordconf']) ? $formData['passwordconf'] : '',
    '#description' => '<div class="tooltip" title="'.t('Please confirm your password.').'"></div>',
  );

  $form['referralfieldset'] = array(
    '#prefix' => '<div class="fieldset">',
    '#suffix' => '</div>',
  );
  $form['referralfieldset']['has_referral_code_label'] = array(
    '#value' => '<div class="padding10 font12">'.t($siteName .' has one of the industry leading referral programs in place.').'<br />'.t('
Were you referred by a friend or family member?').'</div>'
  );

  $form['referralfieldset']['has_referral_code'] = array(
    '#type' => 'select',
    '#required' => true,
    '#title' => '&nbsp;',
    '#default_value' => ($formData['has_referral_code']) ? $formData['has_referral_code'] : '',
    '#options' => array('' => 'Select', 1 => 'Yes', 2 => 'No'),
    '#description' => '<div class="tooltip" title="'.t('Please tell us if you were referred by a friend or family member.').'"></div>',
  );

  $form['referralfieldset']['ref_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Referral Code'),
    '#default_value' => ($formData['ref_code']) ? $formData['ref_code'] : '',
    '#size' => 7,
    '#maxlength' => 6,
  );

  $form['agreements'] = array(
    '#prefix' => '<div class="fieldset">',
    '#suffix' => '</div>', 
  );

  $form['agreements']['padding'] = array(
    '#prefix' => '<div class="padding10">',
    '#suffix' => '</div>',
  );

  $form['agreements']['padding']['ED_titletitle'] = array(
    '#value' => '<h2 class="clean">'.t('Electronic Disclosures').'</h2>',
  );

  $link = l('here', '', array('fragment' => ' ', 'external' => TRUE, "attributes" => array("onclick" => 'openPopWindow("/'.variable_get('pdl2_application_electronic_disclosure_agreement_link', "").'","Electronic Disclosures");')));
  
  $form['agreements']['padding']['ED'] = array(
    '#type' => 'checkbox',
    '#required' => true,
    '#title' => t('By checking this box you consent to Electronic Delivery as detailed ' . $link . '.'),
    '#default_value' => ($formData['ED']) ? $formData['ED'] : '',
    '#prefix' => '<div class="wide">',
    '#suffix' => '</div>',
  );

  $form['agreements']['padding']['agreetoterms_title'] = array(
    '#value' => '<h2 class="clean">'.t('Information Accuracy and Privacy Policy').'</h2>',
  );

  $link = l('Privacy Policy', '', array('fragment' => ' ', 'external' => TRUE, "attributes" => array("onclick" => 'openPopWindow("/'.variable_get('pdl2_application_privacy_policy_link', "").'","Privacy Policy");')));
  
  $form['agreements']['padding']['agreetoterms'] = array(
    '#type' => 'checkbox',
    '#required' => true,
    '#title' => t('By checking this box you verify that the information supplied on this loan application is accurate and true to the best of your knowledge, and agree that you are aware that it is a federal crime to make false statements on a loan or credit application or to misrepresent your Social Security number. You also acknowledge that you have read and agree to our ' . $link . '.'),
    '#default_value' => ($formData['agreetoterms']) ? $formData['agreetoterms'] : '',
    '#prefix' => '<div class="wide ia-pp-div">',
    '#suffix' => '</div>',
  );

  $form['agreements']['padding']['otherlenders_title'] = array(
    '#value' => '<h2 class="clean">'.t('Other Lenders').'</h2>',
  );

  $form['agreements']['padding']["info"] = array(
    "#value" => t('<div class="other-info">Should we attempt to find you a lender if ' . $siteName . ' is unable to approve your loan?</div><div class="clear"></div>'));

  $form['agreements']['padding']['otherlenders'] = array(
    '#type' => 'radios',
    '#required' => true,
    //'#title' => t('Should we attempt to find you a lender if My Cash Now is unable to approve my loan?'),
    '#default_value' => ($formData['otherlenders'] >= 0) ? $formData['otherlenders'] : '',
    '#options' => array(1 => 'Yes', 0 => 'No'),
    '#prefix' => '<div class="wide">',
    '#suffix' => '</div>',
  );

  $link = l('here', '', array('fragment' => ' ', 'external' => TRUE, "attributes" => array("onclick" => 'openPopWindow("/'.variable_get('pdl2_application_third_party_sharing_link', "").'","ThirdPartySharing");'))
  );
  
  $form['agreements']['padding']['thirdpartysharing'] = array(
    "#value" => '<p>' . t('Click ' . $link . ' to read the Terms and Conditions for this service.') . '</p>'
  );

  $form['buttons'] = array(
    '#prefix' => '<div class="buttons">',
    '#suffix' => '</div>'
  );
  
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
    '#attributes' => array('class' => 'image continue rright'),
  );
  
  $_SESSION["existinguser"] = 0;

    return $form;
  }


  public function validateForm($form, &$form_state) {
    //unset quickstart form values b/c they are being set...shouldnt need them again
    unset($_SESSION['qs_firstname']);
    unset($_SESSION['qs_lastname']);
    unset($_SESSION['qs_email']);
    unset($_SESSION['qs_zip']);
    //call api
    $pdl2 = pdl2_core_get_api();
    $draftId = $pdl2->getHash();
    $siteName = pdl2_core_get_site_name();
    $valPhone = new Pdl2_Validate_PhoneNumber();
    $valPassword = new Pdl2_Validate_Password();

    $firstName = $form_state['values']['firstname'];
    if (!preg_match('/^[A-Za-z-\']+$/', $firstName)) {
      form_set_error('firstname', "First Name : Please enter a valid name (letters, apostrophes and dashes only)");
    }

    $middleName = $form_state['values']['middlename'];
    if (!empty($middleName) && !preg_match('/^[A-Za-z-\']+$/', $middleName)) {
      form_set_error('middlename', "Middle Name : Please enter a valid name (letters, apostrophes and dashes only)");
    }

    $lastName = $form_state['values']['lastname'];
    if (!preg_match('/^[A-Za-z-\']+$/', $lastName)) {
      form_set_error('lastname', "Last Name : Please enter a valid name (letters, apostrophes and dashes only)");
    }

    $homeStreet1 = $form_state['values']['homestreet1'];
    if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $homeStreet1)) {
      form_set_error('homestreet1', "Address : Please enter a valid address (letters, numbers, spaces, dots, dashes and apostrophes only)");
    }

    $homeStreet2 = $form_state['values']['homestreet2'];
    if ($homeStreet2) {
      if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $homeStreet2)) {
        form_set_error('homestreet2', "Address line 2 : Please enter a valid address (letters, numbers, spaces, dots, dashes  and apostrophes only)");
      }
    }

    $homeCity = $form_state['values']['homecity'];
    if (!preg_match('/^[A-Za-z .]+$/', $homeCity)) {
      form_set_error('homecity', "City : Please enter a valid city (letters, spaces and dots only)");
    }

    $homeZip = $form_state['values']['homezip'];
    if (!preg_match('/[0-9]{5}/', $homeZip)) {
      form_set_error('homezip', "ZIP : Your zip code is invalid. Please check it carefully.");
    }

    // If seperate mailing address has values - validate.
    if (!empty($form_state['values']['homemailaddr']) ||
        !empty($form_state['values']['homemailcity']) ||
        !empty($form_state['values']['homemailzip'])) {

      $homeMailAddress = $form_state['values']['homemailaddr'];
      if (empty($homeMailAddress)) {
        form_set_error('homemailaddr', "Mailing Address is required if you use a separate mailing address.");
      } else if (!preg_match('/^[0-9A-Za-z-\' .]+$/', $homeMailAddress)) {
        form_set_error('homemailaddr', "Mailing Address : Please enter a valid address (letters, numbers, spaces, dots, dashes and apostrophes only)");
      }

      $homeMailCity = $form_state['values']['homemailcity'];
      if (empty($homeMailCity)) {
        form_set_error('homemailcity', "Mailing City is required if you use a separate mailing address.");
      } else if (!preg_match('/^[A-Za-z .]+$/', $homeMailCity)) {
        form_set_error('homemailcity', "Mailing City : Please enter a valid city (letters, spaces and dots only)");
      }
      
      $homeMailZip = $form_state['values']['homemailzip'];
      if (empty($homeMailZip)) {
        $message = 'Mailing ZIP is required if you use a separate mailing address.';
      } else if (!preg_match('/[0-9]{5}/', $homeMailZip)) {
        form_set_error('homemailzip', "Mailing ZIP : Your zip code is invalid. Please check it carefully.");
      }
    }

    $incomesource = $form_state['values']['incomesource'];
    if ( $incomesource == 'active-military' ) {
      $_SESSION['data'] = array();
      drupal_get_messages('error');
      drupal_goto(variable_get('pdl2_application_military_link', ""));
    }

    $homePhone = $form_state['values']['homephone1'] . $form_state['values']['homephone2'] . $form_state['values']['homephone3'];
    if (!$valPhone->isValid($homePhone)) {
      $message = implode(',', $valPhone->getMessages());
      form_set_error('homephone1', "Home Phone : " . $message);
    }

    $cellPhone = $form_state['values']['cellphone1'] . $form_state['values']['cellphone2'] . $form_state['values']['cellphone3'];
    if (!empty($cellPhone)) {
      if (!$valPhone->isValid($cellPhone)) {
        $message = implode(',', $valPhone->getMessages());
        form_set_error('cellphone1', "Cell Phone : " . $message);
      }
    }

    $driverLicenseNum = $form_state['values']['driverlicensenum'];
    if (!preg_match('/^[0-9A-Za-z-]+$/', $driverLicenseNum)) {
      form_set_error('driverlicensenum', "Drivers License : Please enter a valid driver license number (letters, numbers and dashes only)");
    }

    if ($form_state['values']['has_referral_code'] == 1) {
      if (!preg_match('/[A-Za-z]{6}/', $form_state['values']['ref_code'])) {
        form_set_error('ref_code', 'The referral code must consist of exactly 6 alphabetic characters.');
      }
      else {
        $haveRefCode = true;
      }
    }


    $validMonth = $form_state['values']['dob_month'];
    $validDay = $form_state['values']['dob_day'];
    $validYear = $form_state['values']['dob_year'];
    if (!$validMonth) {
      form_set_error('dob_month', 'Please select a valid Date of Birth');
    }
    elseif (!$validDay) {
      form_set_error('dob_day', 'Please select a valid Date of Birth');
    }
    elseif (!$validYear) {
      form_set_error('dob_year', 'Please select a valid Date of Birth');
    }
    else {
      $validDOB = checkdate($form_state['values']['dob_month'], $form_state['values']['dob_day'], $form_state['values']['dob_year']);
      if (!$validDOB) {
        form_set_error('dob_month', 'Please select a valid Date of Birth');
      }
    }

    $email = $form_state['values']['email'];
    $emailConfirm = $form_state['values']['emailconf'];
    if (!empty($email)) {
      if(!preg_match(PDL2_EMAIL_REGEX, $email)) {
        form_set_error('email', "Email Address : Please enter a valid email address.");
      }
      elseif ($email !== $emailConfirm) {
        form_set_error('email', "Email Address : Email and email confirmation must match each other.");
      }

      //I know, I know, submit in validation. However, as this is effectively validation AND the first step of submission, I'll allow it
      
      // If we don't have a hash, create a new customer
      $draftId = $pdl2->getHash();
      if (!isset($draftId)) {
        $customerDraft = $pdl2->createCustomer($email);

        if (!$customerDraft->status) {
          form_set_error('email', "Email Address : " . $customerDraft->message->detail);
        } else {
          $draftId = $customerDraft->result;
          $pdl2->setHash($draftId);
        }
      }

      if (isset($draftId)) {

        if ($email != $_SESSION['data']['email'])
        {
          try {
            $res = $pdl2->customerDraftSetEmail($draftId, $email);
          } catch (Exception $e) {
            form_set_error("error", $e->getMessage());
          }
        }

        if ($haveRefCode) {
          try {
            $ref_status = $pdl2->customerDraftSetReferral($draftId, $form_state['values']['ref_code']);
          }
          catch (Exception $e) {
            form_set_error('ref_code', 'Unknown referral code, please check and try again');
          }
        }

        try {
          $pdl2->customerDraftSetHomeAddress($draftId,
                                             $form_state['values']['homezip'],
                                             $form_state['values']['homecity'],
                                             $form_state['values']['homestreet1'],
                                             $form_state['values']['homestreet2']);
        }
        catch (Exception $e) {
          if ($e->getCode() == 749) {
            form_set_error('homezip', $siteName.' does not currently offer loans in your area.');
          }
          form_set_error('homezip', 'Zip Code does not validate to a valid location');
        }

        if (trim($form_state['values']['homemailaddr']) != '') {
          try {
            $pdl2->customerDraftSetMailAddress($draftId,
                                               $form_state['values']['homemailzip'],
                                               $form_state['values']['homemailcity'],
                                               $form_state['values']['homemailaddr']
                                               );
          }
          catch (Exception $e) {
            form_set_error('homezip', 'Mailing Address Zip Code does not validate to a valid location');
          }
        }
        
        $password = $form_state['values']['password'];
        $passwordConfirm = $form_state['values']['passwordconf'];
        try {
            $pdl2->customerDraftSetPassword($draftId,
                                            $form_state['values']['password']);
        }
        catch (Exception $e) {
          form_set_error('password', 'Password: Passwords must contain at least one digit and be between 6 and 32 characters long.');
        }
      }
    }


    if (!empty($password)) {
      if (!$valPassword->isValid($password)) {
        form_set_error('password', "Password : " . implode(',', $valPassword->isValid()));
      }
      elseif ($password !== $passwordConfirm) {
        form_set_error('password', 'Password and Password Confirmation must match each other.');
      }
    }

    // If we get ANY validation errors, the password fields are empty for security reasons.
    // Make sure the users know this, and display a message in the form.
    $ers = form_get_errors();
    if ($ers) {
      form_set_error('password', "Password : For your security, the password must be re-entered");
      form_set_error('passwordconf', "Password Confirm : For your security, the password confirmation must be re-entered");
    }
  }


  public function submitForm($form, &$form_state, $page) {
    $pdl2 = pdl2_core_get_api();
    $draftId = $pdl2->getHash();

    $setName = $pdl2->customerDraftSetName($draftId,
                                           $form_state['values']['firstname'],
                                           $form_state['values']['middlename'],
                                            $form_state['values']['lastname']);

    $customer_info = $pdl2->customerDraftGetById($draftId);
    $homeState = $customer_info->customer->home_address->address->state;
    $mailState = $customer_info->customer->mail_address->address->state;
    $form_state['values']['homestate'] = $homeState;
    $form_state['values']['homemailstate'] = $mailState;

    $home_phone = $this->_combineNumbers($form_state['values']['homephone1'],
                                         $form_state['values']['homephone2'],
                                         $form_state['values']['homephone3']);
    $pdl2->customerDraftSetHomePhone($draftId,
                                     $home_phone,
                                     $form_state['values']['homephone_mobile']);
    $_SESSION['apply_homephone'] = $home_phone;

    $cellPhone = $this->_combineNumbers($form_state['values']['cellphone1'],
                                        $form_state['values']['cellphone2'],
                                        $form_state['values']['cellphone3']
                                       );
    if ($cellPhone) {
      $pdl2->customerDraftSetCellPhone($draftId,
                                       $cellPhone,
                                       $form_state['values']['cellphone_mobile']);
      $_SESSION['apply_cellphone'] = $cellPhone;
    }

    if ($form_state['values']['sms']){
      $_SESSION['sms_opt_in'] = true;
    }
    else {
      $_SESSION['sms_opt_in'] = false;
    }

    $pdl2->customerDraftSetDateOfBirth($draftId,
                                       $this->_combineDates($form_state['values']['dob_year'],
                                                            $form_state['values']['dob_month'],
                                                            $form_state['values']['dob_day'])
                                      );

    $pdl2->customerDraftSetResidenceType($draftId, $form_state['values']['rentorown']);

    $pdl2->customerDraftEmploymentSetIncomeSourceType($draftId,
                                                      $form_state['values']['incomesource']
    );

    $pdl2->customerDraftSetDriversLicense($draftId,
                                          $form_state['values']['driverlicensenum'],
                                          $form_state['values']['driverstate']);

    //$form_state['values']['password'] = md5($form_state['values']['password']);
    $pdl2->customerDraftSetPassword($draftId,
                                    $form_state['values']['password']);

    if ($form_state['values']['has_referral_code'] == 1) {
      $pdl2->customerDraftSetReferral($draftId,
                                      $form_state['values']['ref_code']);
    }

    $_SESSION['data']['newaccount'][$page] = $form_state['values'];
    $_SESSION['data']['email'] = $form_state['values']['email'];
    $_SESSION['data']['password'] = $form_state['values']['password'];
    $_SESSION['data']['otherlenders'] = $form_state['values']['otherlenders'];
  }

}
<?php
class NewAccountPage3 extends NewAccountPageAbstract
{
  protected $_isValid = true;

  protected $_confirmFields = array(
      'Is this your bank?' => array('bankaddress' => 'Bank',
                                    'bankaccountnum' => 'Bank Account Number'),
      'Security Information' => array('personalanswer2' => 'Mother\'s Maiden Name',
                                      'socsec' => 'Social Security Number',
                                      'personalanswer1' => 'City of Birth')
    );

  public function getForm($formData) {
    $pdl2 = pdl2_core_get_api();
    $form = array();

    $form["disablesubmit"] = array("#value" => '<script type="text/javascript">$("#pdl2-application-newaccount-form").submit(function(){$("#edit-submit", this).attr("disabled", "disabled");});</script>');

    $form["toptitle"] = array( "#value" => '<h1 class="step threeof5">'.t('Banking, Security & Military').'</h1>' );

    $form['bankfieldset'] = array( 
      '#prefix' => '<div class="fieldset">',
      '#suffix' => '</div>',
    );
    
    $form['bankfieldset']["isrequired"] = array("#value" => '<p><span class="form-required" title="This field is required.">*</span> '.t('Required Field.').'</p>');
    $form['bankfieldset']["headermsg"] = array( "#value" => '<h3>'.t('Let\'s verify your bank').'</h3>' );

    $form['bankfieldset']["bankmsg1"] = array("#value" => '<div class="padding10-notop italic font12">'.t('This section must contain traditional checking account information only. No savings account, debit card, credit cards or payroll cards will be accepted.'). ' <strong>' . t('Do not use a debit card number.'). '</strong>'.'</div>');
    
    $form['bankfieldset']['checkimg'] = array('#value' => '<div class="padding10-notop"><img src="/sites/all/modules/pdl2-drupal-modules/pdl2_application/images/check.gif" /></div>', );
    
    $form['bankfieldset']['bankmsg2'] = array('#value' => '<div class="padding10 width50 font12">'.t('The name on the loan application must match the name on the provided bank account (By providing and submitting the bank account information above, you certify that you are the Primary or Secondary Account Holder and have the authority to authorize transactions on this account.').'</div>');
    $form['bankfieldset']['bankroutingnum'] = array('#type' => 'textfield',
          '#required' => true,
          '#title' => 'Routing Number',
          '#maxlength' => 9,
          '#size' => 12,
          '#default_value' => ($formData['bankroutingnum']) ? $formData['bankroutingnum']: '',
          '#description' => '<div class="tooltip" title="'.t('Please tell us the routing number of your bank. We use this to locate your bank.').'"></div>'
        );

    $form['bankfieldset']['bankaccountnum'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#title' => 'Bank Account Number',
          '#maxlength' => 14,
          '#size' => 24,
          '#default_value' => ($formData['bankaccountnum']) ? $formData['bankaccountnum']: '',
          '#description' => '<div class="tooltip" title="'.t('Please tell us your bank account number. We use this to deposit your loan.').'"></div>'
        );

    $form['bankfieldset']['bankaccountnum_confirm'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#title' => 'Confirm Bank Account Number',
          '#maxlength' => 14,
          '#size' => 24,
          '#default_value' => ($formData['bankaccountnum_confirm']) ? $formData['bankaccountnum_confirm']: '',
          '#description' => '<div class="tooltip" title="'.t('Please confirm that this is your bank account number.').'"></div>'
        );

    $form['socsecfieldset'] = array(
      '#prefix' => '<div class="fieldset">',
      '#suffix' => '</div>',
    );

    $form['socsecfieldset']["securitymsg"] = array('#prefix' => '<div class="inline phone">',
                                                   '#suffix' => '</div>',
                                                   '#value' => '<h3>'.t('For your security').'</h3>');

    $form['socsecfieldset']['socsec'] = array('#prefix' => '<div class="inline phone autotabs">',
                                              '#suffix' => '</div>');

    $form['socsecfieldset']['socsec']['socsec1'] = array(
          '#type' => 'textfield',
          '#title' => t('Your Social Security Number'),
          '#required' => true,
          '#default_value' => ($formData['socsec1']) ? $formData['socsec1'] : '',
          '#size' => 3,
          '#maxlength' => 3,
        );

    $form['socsecfieldset']['socsec']['socsec2'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#default_value' => ($formData['socsec2']) ? $formData['socsec2'] : '',
          '#size' => 2,
          '#maxlength' => 2,
        );

    $form['socsecfieldset']['socsec']['socsec3'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#default_value' => ($formData['socsec3']) ? $formData['socsec3'] : '',
          '#size' => 4,
          '#maxlength' => 4,
        );
    $form['socsecfieldset']['socsec']['tooltip'] = array('#value' => '<div class="tooltip" title="'.t("Please tell us your social security number.").'"></div>',);

    $form['socsecfieldset']['socsec_confirm'] = array('#prefix' => '<div class="inline phone autotabs">',
                                                      '#suffix' => '</div>',
                                                      );

    $form['socsecfieldset']['socsec_confirm']['socsec_confirm1'] = array(
          '#type' => 'textfield',
          '#title' => t('Confirm Your Social Security Number'),
          '#required' => true,
          '#default_value' => ($formData['socsec_confirm1']) ? $formData['socsec_confirm1'] : '',
          '#size' => 3,
          '#maxlength' => 3,
        );
    $form['socsecfieldset']['socsec_confirm']['socsec_confirm2'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#default_value' => ($formData['socsec_confirm2']) ? $formData['socsec_confirm2'] : '',
          '#size' => 2,
          '#maxlength' => 2,
        );

    $form['socsecfieldset']['socsec_confirm']['socsec_confirm3'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#default_value' => ($formData['socsec_confirm3']) ? $formData['socsec_confirm3'] : '',
          '#size' => 4,
          '#maxlength' => 4,
        );
    $form['socsecfieldset']['socsec_confirm']['tooltip'] = array('#value' => '<div class="tooltip" title="'.t("Please confirm that this is your social security number.").'"></div>',);

    $form['socsecfieldset']['personalanswer2'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#title' => 'Your Mother\'s Maiden Name',
          '#maxlength' => 30,
          '#size' => 25,
          '#default_value' => ($formData['personalanswer2']) ? $formData['personalanswer2']: '',
          '#description' => '<div class="tooltip" title="'.t('Please tell us your mother\'s maiden name.').'"></div>'
        );

    $form['socsecfieldset']['personalanswer1'] = array(
          '#type' => 'textfield',
          '#required' => true,
          '#title' => 'Your City of Birth',
          '#maxlength' => 30,
          '#size' => 25,
          '#default_value' => ($formData['personalanswer1']) ? $formData['personalanswer1']: '',
          '#description' => '<div class="tooltip" title="'.t('Please tell us the city you were born in.').'"></div>'
        );

    $form['militaryfieldset'] = array(
      '#prefix' => '<div class="fieldset">',
      '#suffix' => '</div>',
    );

    $form['militaryfieldset']["militarymsg"] = array(
          "#value" => t('<h3>Military Questionnaire</h3>'));

    $form['militaryfieldset']["militarymsg2"] = array(
          "#value" => t('<div class="padding10-notop italic font12" style="text-align:center;">Please answer the following questions to see if you qualify for a payday loan. In accordance with the law, we cannot make any loans to any member or dependants of the U.S. Military. We apologize if this may inconvenience you, and we urge you to find financial assistance with one of the military financial aid institutions.</div>'));


    $form['militaryfieldset']["box"] = array(
          "#value" => t('<div class="m-align-box">'));

    $form['militaryfieldset']['notarmedservicesmem'] = array(
          '#type' => 'radios',
          '#required' => true,
          //'#title' => t('Are you a member or reserve member of any branch of the armed forces?'),
          '#default_value' => ($formData['notarmedservicesmem'] >= 0) ? $formData['notarmedservicesmem'] : '',
          '#options' => array(0 => 'Yes', 1 => 'No'),
          '#prefix' => '<br/><div class="m-align">',
          '#suffix' => '</div>',
    );

    $form['militaryfieldset']["info"] = array(
          "#value" => t('<div class="m-info">Are you a member or reserve member of any branch of the armed forces?</div><div class="clear"></div>'));

    $form['militaryfieldset']['notarmedservicesdep'] = array(
          '#type' => 'radios',
          '#required' => true,
          //'#title' => t('Are you a spouse or dependent of a member or reserve member of any branch of the Armed Forces?'),
          '#default_value' => ($formData['notarmedservicesdep'] >= 0) ? $formData['notarmedservicesdep'] : '',
          '#options' => array(0 => 'Yes', 1 => 'No'),
          '#prefix' => '<br/><div class="m-align">',
          '#suffix' => '</div>',
    );
    $form['militaryfieldset']["info2"] = array(
          "#value" => t('<div class="m-info">Are you a spouse or dependent of a member or reserve member of any branch of the Armed Forces?</div><div class="clear"></div>'));
    $form['militaryfieldset']["box2"] = array(
          "#value" => t('</div>'));


    $form['buttons'] = array('#prefix' => '<div class="buttons">',
                             '#suffix' => '</div>' );
    $form['buttons']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Next'),
          '#attributes' => array('class' => 'image continue rright'),
    );
    $form['buttons']['back'] = array(
          '#type' => 'submit',
          '#value' => t('Back'),
          '#attributes' => array('class' => 'image back lleft'),
          '#validate' => array('pdl2_application_newaccount_back_validate'),
          '#submit' => array('pdl2_application_newaccount_back_submit'),
    );
    $form['buttons']['notice'] = array(
          '#value' => '<div class="infoText">'.t('Note: Hitting the back button will erase all the fields on the current page.').'</div>',
    );


    $form['#after_build'][] = 'pdl2_application_newaccount_form_after_build';

    $_SESSION["existinguser"] = 0;

    return $form;
  }


  public function validateForm($form, &$form_state) {

    $valAlnumWithSpaces = new Zend_Validate_Alnum(true);
    $valInt = new Zend_Validate_Int();
    $valRouting = new Pdl2_Validate_Routing();
    $valBankAccount = new Pdl2_Validate_BankAccount();

    $bankRoutingNum = $form_state['values']['bankroutingnum'];
    if (strlen($bankRoutingNum) != 9) {
      form_set_error('bankroutingnum', "Bank routing numbers are 9 digits");
    }
    // PHP 5.3.5 (for MAMP) has a bug that causes the zend isValid() to time out
    // as a temporary solution - don't validate emails (for test environments) that have
    // PHP 5.3.5 installed.
    // Lee Walker  1/10/13
    global $base_url;
    if ((phpversion() != "5.3.5") && ($base_url == "http://mcn.localhost")) {
      if (!$valRouting->isValid($bankRoutingNum)) {
        form_set_error('bankroutingnum', implode(',', $valRouting->getMessages()));
      }
    }

    $bankAccountNum = $form_state['values']['bankaccountnum'];
    $bankAccountConfirm = $form_state['values']['bankaccountnum_confirm'];

    if ($bankAccountNum != $bankAccountConfirm) {
      form_set_error("bankaccountnum", t("Bank account numbers do not match."));
    }
    if (!preg_match('/^[0-9]+$/', $bankAccountNum)) {
      form_set_error("bankaccountnum", t("Bank account numbers is bad - digits only."));
    }


    // PHP 5.3.5 (for MAMP) has a bug that causes the zend isValid() to time out
    // as a temporary solution - don't validate emails (for test environments) that have
    // PHP 5.3.5 installed.
    // Lee Walker  1/10/13
    global $base_url;
    if ((phpversion() != "5.3.5") && ($base_url == "http://mcn.localhost")) {
      if (!$valBankAccount->isValid($bankAccountNum)) {
        form_set_error('bankaccountnum', implode(',', $valBankAccount->getMessages()));
      }
      elseif ($bankAccountConfirm !== $bankAccountNum) {
        form_set_error('bankaccountnum', t('Account Number does not match confirmation'));
      }
    }

    $socsec = $form_state['values']['socsec1'] . $form_state['values']['socsec2'] . $form_state['values']['socsec3'];
    $socsecConfirm = $form_state['values']['socsec_confirm1'] . $form_state['values']['socsec_confirm2'] . $form_state['values']['socsec_confirm3'];

    if (strlen($socsec) != 9) {
      form_set_error('socsec', t('Your SSN must be nine digits.'));
    }
    elseif (!$valInt->isValid($socsec)) {
      form_set_error('socsec', implode(',', $valInt->getMessages()));
    }
    elseif ($socsecConfirm !== $socsec) {
      form_set_error('socsec', t('Your SSN does not match the confirmation.'));
    }

    $personalanswer2 = $form_state['values']['personalanswer2'];
    if (!$valAlnumWithSpaces->isValid($personalanswer2)) {
      form_set_error('personalanswer2', implode(',', $valAlnumWithSpaces->getMessages()));
    }

    $personalanswer1 = $form_state['values']['personalanswer1'];
    if (!$valAlnumWithSpaces->isValid($personalanswer1)) {
      form_set_error('personalanswer1', implode(',', $valAlnumWithSpaces->getMessages()));
    }

    // Run some PDL2 calls that may return a validation error
    $pdl2 = pdl2_core_get_api();
    $draftId = $pdl2->getHash();
    $ssn = $this->_combineNumbers($form_state['values']['socsec1'],
                                  $form_state['values']['socsec2'],
                                  $form_state['values']['socsec3']);

    try {
      $pdl2->customerDraftSetSocialSecurityNumber($draftId, $ssn);
    }
    catch (Exception $e) {
      // Note that the error code from PDL2 is 749 for all "customer domain" errors - be it dupe SSN or messed up customer draft hashes. Only way of differentiating is to do string comparisons.
      if (strpos($e->getMessage(), "The draft customer account matching") !== false ||
          strpos($e->getMessage(), "Draft customer Id must be a string of numbers and letters") !== false) {
        drupal_set_message("Your session has expired. For security reasons, please re-enter your information", "error");
        $_SESSION['data']['page'] = 1;
        $pdl2->clearHash();
        drupal_goto("/newaccount");
      } else {
        form_set_error('socsec1', 'That Social Security Number is already in use. If you already have an account with us, please ' . l('login', 'account') . ' with your existing details.');
      }
    }


    $notArmedServicesMem = $form_state['values']['notarmedservicesmem'];
    $notArmedServicesDep = $form_state['values']['notarmedservicesdep'];


    //if there are any "field is required" messages - its because of the silly OO style forms.
    // we need to replace those errors with better ones.
    // remove all "field is required" errors.
    $ers = form_get_errors();
    if ($ers) {
      if (array_key_exists('notarmedservicesmem', $ers)) {
        form_set_error('notarmedservicesmem',
                       'You must answer the question "Are you a member or reserve member of any branch of the armed forces?"'
                       ,TRUE);
      }
      if (array_key_exists('notarmedservicesdep', $ers)) {
        form_set_error('notarmedservicesdep',
                       'You must answer the question "Are you a spouse or dependent of a member or reserve member of any branch of the Armed Forces?"'
                       ,TRUE);
      }
    }
    // clean up any bad error messages
    $this->pdl2_cleanup_form_error_messages();

    // Only do the Military checking after everything else has validated
    // because it does a drupal_goto() and will break the form.
    if (!$ers) {
      if ($notArmedServicesMem == 0 || $notArmedServicesDep == 0) {
        $_SESSION['data'] = array();
        drupal_goto(variable_get('pdl2_application_military_link', ""));
      }
    }
  }



  public function submitForm($form, &$form_state, $page)
  {
    $pdl2 = pdl2_core_get_api();
    $draftId = $pdl2->getHash();
    try {
      $pdl2->customerDraftSetBankAccount($draftId,
                                         $form_state['values']['bankaccountnum'],
                                         $form_state['values']['bankroutingnum']);
    }
    catch (Exception $e) {
      form_set_error('bankaccountnum', "Bad Banking Routing number");
      drupal_goto('newaccount/3');
    }
    $ssn = $this->_combineNumbers($form_state['values']['socsec1'],
                                  $form_state['values']['socsec2'],
                                  $form_state['values']['socsec3']);

    $bank_info = $pdl2->referenceGetBank($form_state['values']['bankroutingnum']);
    $bank_account = $bank_info->bank_account;
    $form_state['values']['bankaddress'] = $bank_account;

    $pdl2->customerDraftSetSocialSecurityNumber($draftId, $ssn);
    $pdl2->customerDraftSetSecurityAnswerOne($draftId, $form_state['values']['personalanswer1']);
    $pdl2->customerDraftSetSecurityAnswerTwo($draftId, $form_state['values']['personalanswer2']);
    $pdl2->customerDraftSetMilitaryStatus($draftId, 0);

    $_SESSION['data']['newaccount'][$page] = $form_state['values'];
  }

  public function isValid()
  {
    return $this->_isValid;
  }
}
<?php
class NewAccountPageAbstract {
  protected $_confirmFields = array();

  public function getConfirmFields() {
    return $this->_confirmFields;
  }

  function __construct(){
    drupal_add_js(drupal_get_path('module', 'pdl2_application') .'/js/newaccount.js',  'module', 'footer');
  }

  protected function _combineNumbers($part1, $part2, $part3) {
    $phoneNumber = $part1 . $part2 . $part3;

    return $phoneNumber;
  }

  protected function _combineDates($year, $month, $day) {
    $dateOfBirth = $year . '-' . $month . '-' . $day;
    $dateOfBirth = date('Y-m-d', strtotime($dateOfBirth));

    if ($dateOfBirth == '--') {
      return '';
    }
    else {
      return $dateOfBirth;
    }
  }

  /*
   * This silly OO form creation amkes some of the auto validation messages
   * just say ' field is required.' withtou saying what field.
   * If you catch the validation in your form validate function and add a better
   * error message, you need to call this function to clean up the old error mesages.
   * Messy but necessary due to non-standard retared OO forms that someone built. Sigh.
   */
  function pdl2_cleanup_form_error_messages() {
    $er_mes = $_SESSION['messages']['error'];
    for ($i = 0 ; $i< sizeof($er_mes) ; $i++) {
      // some control chat in front of error message - not a space. Clip it.
      $tt = substr($er_mes[$i], 1, strlen($er_mes[$i])-1);
      if (strcmp($tt, 'field is required.') == 0) {
        unset($_SESSION['messages']['error'][$i]);
      }
    }
  }

}

<div>
  <h2>Thank you for your application!</h2>
  <p>To complete the process and receive your funds, an additinoal security step is required to prevent unauthorized access to your account and financial information.</p>
  <p>We confirm your identity by sending a unique, temporary code to your phone that will expire in 60 minutes. You will be required to enter the code on the next page.</p>
</div>
<?php

function pdl2_mobile_loanfeestotalcallback_function($key,$val) {
	
	$pdl2 = pdl2_core_get_api();

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_loanfeestotalcallback_function($key,$val)");
	
	//is logged in?
	if (!$pdl2->hasSessionData()) {
		echo '';
		exit;
	}
	//is opt out?
	else if ($pdl2->currUser->optOut) {
		echo '';
		exit;
	}
	$draftLoanId = $_SESSION['loanDraftHash'];
	
	switch($key) {
		case 'coupon';
			$_SESSION['couponCode'] = $val;
			$loan = $pdl2->setCoupon($draftLoanId, $val);
			break;
		case 'loanduedate';
			$loan = $pdl2->setLoanDueDate($draftLoanId, $val);
			break;
		case 'principal';
			$loan = $pdl2->setPrincipal($draftLoanId, $val);
			break;
		case 'paymentmethod';
			$loan = $pdl2->setPaymentMethod($draftLoanId, $val);
			break;
	}
	
	$loanAmount = $loan->loan->principal;
	$financeCharge = $loan->loan->finance_charges;
	$total = $loanAmount + $financeCharge;
	
	$res = array('loan'=>$loanAmount, 'fees'=>$financeCharge, 'total'=>$total);
	$json = json_encode($res);
	echo $json;

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();
}

<?php

function pdl2_mobile_loanagreement_page() {

	$pagename = 'apply-agreement';
	$catid = '102APPL';
	
	$pdl2 = pdl2_core_get_api();

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_loanagreement_page");
	
	//is logged in?
	if (!$pdl2->hasSessionData()) {
		drupal_goto("account/login");
	}
	//is opt out?
	else if ($pdl2->currUser->optOut) {
		drupal_goto('account/logout');
	}
	else if (!isset($_SESSION['loanDraftHash']) && !$_SESSION['loanDraftHash']){
		drupal_goto('account');
	}
	
	$draftLoanId = $_SESSION['loanDraftHash'];

	if (!$draftLoanId) {
		drupal_goto('account');
	}
	
	if ($pdl2->isAsyncCalls()) {
		$async_requests['agreement'] = $pdl2->getLoanDraftAgreementAsync($draftLoanId);
		$async_requests['borrower'] = $pdl2->getUserInfoAsync();
		$async_requests['ssn'] = $pdl2->getSsnAsync();
		$async_requests['apr'] = $pdl2->getLoanAprAsync($draftLoanId);
		
		foreach($async_requests as $key => $request){
			$$key = $pdl2->getResult($request);
			unset($async_requests[$key]);
		}
	}else{ //if not async
		$agreement = $pdl2->getLoanDraftAgreement($draftLoanId);
		$borrower = $pdl2->getUserInfo();
		$ssn = $pdl2->getSsn();
		$apr = $pdl2->getLoanApr($draftLoanId);
	}
	
	$themedata = array(
		'agreement' => $agreement,
		'date' => $agreement->loan_agreement->createDate->date,
		'borrower' => $borrower,
		'apr' => $apr,
		'ssn' => $ssn
	);
	
	$output = drupal_get_form('pdl2_mobile_loanagreement_page_form', $themedata) . pdl2_mobile_write_pageview($pagename, $catid);

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

	return $output;

}

function pdl2_mobile_loanagreement_page_form($formArray, $themedata) {

	$form['lenderborrower'] = array('#value' => theme('lenderborrower', $themedata));
	
	$form['agreementIntro'] = array(
		'#value' => '<p><strong>Before a loan can be completed, you must check "I Agree" at the bottom of this agreement.</strong></p>'
	);
	
	$form['dashboard'] = array(
		'#prefix' => '<div class="dashboard">',
		'#suffix' => '</div>',
	);
	
	$form['dashboard']['agreementText'] = array('#value' => $themedata['agreement']->loan_agreement->agreementText);
	
	$form['dashboard']['eftTextQuestion'] = array('#value' => $themedata['agreement']->loan_agreement->eftTextQuestion);
	
	$form['dashboard']['eftOptions'] = array(
		'#type' => 'radios',
		'#required' => true,
		'#options' => array($themedata['agreement']->loan_agreement->eftTextYes, $themedata['agreement']->loan_agreement->eftTextNo),
        '#default_value' => '0',
	);
	
	$form['dashboard']['eftConsentText'] = array('#value' => $themedata['agreement']->loan_agreement->consentText);
	
	$form['loan_agree'] = array(
		'#type' => 'checkbox',
		'#title' => 'I agree <span class="form-required">*</span>',
	);
	
	$form['buttons'] = array(
		'#type' => 'fieldset',
		'#attributes' => array('class' => 'btns') 
	);
	
	$form['buttons']['next'] = array(
		'#type' => 'submit',
		'#value' => t('NEXT'),
		'#attributes' => array('class' => 'btn')   
	);
	
	$form['buttons']['back'] = array(
		'#type' => 'submit',
		'#value' => t('BACK'),
		'#validate' => array('pdl2_mobile_loanagreement_back_validate'),
		'#submit' => array('pdl2_mobile_loanagreement_back_submit'),
		'#attributes' => array('class' => 'btn back')   
	); 
	
	$form['required_key'] = array(
		'#value' => '<p class="center"><span class="form-required">*</span> Indicates a required field.</p>'
	);
	
	return $form;
	
}

function pdl2_mobile_loanagreement_page_form_validate($form, &$form_state) {
	
	$loan_agree = $form_state['values']['loan_agree'];
	
	if(!$loan_agree){
		form_set_error('loan_agree', t('You must agree to the terms.'));
	}
	
}

function pdl2_mobile_loanagreement_page_form_submit($form, &$form_state) {
	
	$pdl2 = pdl2_core_get_api();
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_loanagreement_page_form_submit");

	$draftLoanId = $_SESSION['loanDraftHash'];
	
	// options array 0 and 1. yes = 0. reverse bool
	$eftOption = $form_state['values']['eftOptions'];
	$agrees = $eftOption == 1 ? 0 : 1;
	
	$setEft = $pdl2->setEft($draftLoanId,$agrees);
	
	$loanSave = $pdl2->saveLoan($draftLoanId);
	$_SESSION['loanId'] = $loanSave;
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

	drupal_goto('confirm/apply');
	
}

function pdl2_mobile_loanagreement_back_validate($form, &$form_state) {
  
}

function pdl2_mobile_loanagreement_back_submit($form, &$form_state) {
	drupal_goto('apply/loanrequest');
}

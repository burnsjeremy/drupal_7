<?php

function pdl2_mobile_login_page() {
	$pagename = 'login';
	$catid = '101ACCT';
	return drupal_get_form('pdl2_mobile_login_form') . pdl2_mobile_write_pageview($pagename, $catid);
}

function pdl2_mobile_login_form() {
		
	$form['entered_email'] = array(
		'#type' => 'textfield',
		'#attributes' => array('placeholder' => 'Email'),
	);
	
	$form['entered_password'] = array(
		'#type' => 'password',
		'#attributes' => array('placeholder' => 'Password'),
	);
	
	$form['submit'] = array(
		'#type' => 'submit', 
		'#value' => 'LOG IN',
		'#attributes' => array('class' => 'btn')
	);

	$form["forgotpwd"] = array(
		"#value" => "<p><a href='forgot-password' class='center'>Forgot Password</a></p>"
	);
	
	return $form;
	
}

function pdl2_mobile_login_form_submit($form, &$form_state) {

  $values = $form_state['values'];
  $email = $values["entered_email"];
  $password = $values["entered_password"];

  // Let's try and login!!!
  $pdl2 = pdl2_core_get_api();
  
  //Begin Profiling call //call was in old mobile login code so this was placed back here 2013-06-11 JB
  if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_login_form_submit");
  
  $login = $pdl2->login($email, $password, 'client');

  if (empty($login)) {
    drupal_set_message(t("An error occurred during login."), 'error');
    return;
  }

  if ($login->userId > 0) {
    
    $isEligible = $pdl2->customerLoanPaydayIsEligible();

    if (!is_bool($isEligible) && $isEligible === PDL2Controller::NON_BUSINESS_STATE) {
      $company = $pdl2->getCompanyInfo();
      $address = $pdl2->getHomeAddress();
      $state = $pdl2->getState($address->address->state);
      $pdl2->logout();
      drupal_set_message(t("At this time, %company is not offering loans in the state of %state", array('%company' => $company->company->name, '%state' => $state)), 'error');
    }
    //redirect to account now that we are past state check
    $form_state["redirect"] = "account";
  }
  else {
    if ($login->status->code == 901) {
      $chatinfo = $pdl2->getCompanyChatInfo($email);
      $livechatlink = $chatinfo->chat->url;
      drupal_set_message(t($login->status->detail . "<br/><br/>If you would like to chat to one of our friendly account representatives, please click " . l("here", $livechatlink)), 'error');
    }
    else {
      drupal_set_message(t("Incorrect username/password, try again!"), 'error');
    }
  }
  //end profiling call //call was in old mobile login code so placed back 2013-06-11 JB
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

}

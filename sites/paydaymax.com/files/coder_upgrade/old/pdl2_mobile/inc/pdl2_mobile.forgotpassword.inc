<?php 

function pdl2_mobile_forgot_password_page() {
	$pagename = 'forgot-password';
	$catid = '101ACCT';
	return drupal_get_form('pdl2_mobile_forgot_password_form') . pdl2_mobile_write_pageview($pagename, $catid);
}

function pdl2_mobile_forgot_password_form() {
	
	$form["headermsg"] = array(
		"#value" => t("Enter your email address and we will send you an email to reset your password."));
		
	$form['email'] = array(
		'#type' => 'textfield',
		'#attributes' => array('placeholder' => 'Email'),
	);
	
	$form['submit'] = array(
		'#type' => 'submit', 
		'#value' => 'Request Password',
		'#attributes' => array('class' => 'btn lrg alt'),
	);

	$form["forgotpwd"] = array(
		"#value" => "<p><a href='account' class='center'>Cancel</a></p>"
	);
	
	return $form;
	
}

function pdl2_mobile_forgot_password_form_submit($form, &$form_state) {

	$email = $form_state['values']['email'];
	
	if ($email == '' || !valid_email_address($email)) {
		form_set_error('paymentAmount', t('You must enter a valid email.'));
		$form_state['redirect'] = url('account/forgot-password', array( 'query' => 'error=invalid', 'absolute' => true ));
		return;
	}
		
	$pdl2 = pdl2_core_get_api();

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_forgot_password_form_submit");
	
	$res = $pdl2->sendResetPassword($email);
	
	if (!$res) {
		form_set_error('paymentAmount', t('The email you entered is unknown.'));
		$form_state['redirect'] = url('account/forgot-password', array( 'query' => 'error=unknown', 'absolute' => true ));
		return;
	}
	
	$_SESSION['forgotemail'] = $email;
	$form_state['redirect'] = 'account/password-sent';

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

}
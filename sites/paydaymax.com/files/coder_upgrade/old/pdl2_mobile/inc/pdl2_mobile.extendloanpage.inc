<?php

function pdl2_mobile_extendloan_page() {

	$pagename = 'extend';
	$catid = '103EXT';
	
	$pdl2 = pdl2_core_get_api();
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_extendloan_page");
	
	//is logged in?
	if (!$pdl2->hasSessionData()) {
		drupal_goto("account/login");
	}
	//is opt out?
	else if ($pdl2->currUser->optOut) {
		drupal_goto('account/logout');
	}
	
	$loanId = $pdl2->getOpenLoan();
	$allowExtension = $pdl2->allowExtension();
	
	if (empty($loanId) || !$allowExtension) {
		drupal_goto("account");
	}
	
	$created_extension = $pdl2->extensionCreate($loanId);
	
	if ($pdl2->isAsyncCalls()) {
		$async_requests['principal'] = $pdl2->getLoanPrincipalAsync($loanId);
		$async_requests['loanDueDate'] = $pdl2->getLoanDueDateAsync($loanId);
		$async_requests['financeCharge'] = $pdl2->getFinanceChargesAsync($loanId);
		$async_requests['minimumPrincipalDue'] = $pdl2->getMinimumPrincipalDueAsync($loanId);
		
		foreach($async_requests as $key => $request){
			$$key = $pdl2->getResult($request);
			unset($async_requests[$key]);
		}
	}else{ //if not async
		$principal = $pdl2->getLoanPrincipal($loanId);
		$loanDueDate = $pdl2->getLoanDueDate($loanId);
		$financeCharge = $pdl2->getFinanceCharges($loanId);
		$minimumPrincipalDue = $pdl2->getMinimumPrincipalDue($loanId);
	}
	
	$themedata = array(
		'minimumFees' => $financeCharge,
		'minimumPrincipalDue' => $minimumPrincipalDue,
		'currentFees' => $financeCharge,
		'currentPrincipal' => $principal,
		'currentTotal' => $financeCharge + $principal,
		'minimumTotal' => $financeCharge + $minimumPrincipalDue,
		'loanDueDate' => $loanDueDate,
	);
		
	drupal_add_js(drupal_get_path('module', 'pdl2_mobile') .'/disable-option-text-input.js');

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

	return drupal_get_form('pdl2_mobile_extendloan_form', $themedata) . pdl2_mobile_write_pageview($pagename, $catid);
}

function pdl2_mobile_extendloan_form($formArray, $themedata) {
		
	$form['due'] = array(
		'#value' => '<p>Loan Due Date: ' . date('F jS, Y', strtotime($themedata['loanDueDate']->date)) . '</p>',
	);
	
	$form['dashboard'] = array(
		'#prefix' => '<div class="dashboard">',
		'#suffix' => '</div>',
	);
	
	$form['dashboard']['extendTable'] = array(
		'#value' => theme('extendtable', $themedata),
	);
	
	$form['dashboard']['paymentType'] = array(
		'#type' => 'radios',
		'#options' => array(
			0 => 'I will make the minimum payment of $' . number_format($themedata['minimumTotal'], 2), 
			1 => 'I would like my total payment to be:'
		),
		'#default_value' => 0,
	);
	
	$form['dashboard']['paymentAmount'] = array(
		'#type' => 'textfield',
		'#size' => 9,
		'#maxlength' => 7,
	);
	
	$form['dashboard']['description'] = array(
		'#value' => '<p><strong>REMINDER:</strong> After three(3) extensions, you are required to pay a minimum of $50.00 for loans of less than $600.00 and $100.00 for loans of $600.00 or more.</p>
<h3>Why is a minimum payment required?</h3>
<p>We require the minimum payment to encourage you to pay off your loan as quickly as possible. Short-term loans are designed to be a temporary solution for unexpected financial circumstances. Repetitive extensions can put you in financial distress. We are here to help you when you need a little cash before payday. Our services are not intended to be used as a long term financial solution.</p>',
	);
	
	$form['buttons'] = array(
		'#type' => 'fieldset',
		'#attributes' => array('class' => 'btns') 
	);
	
	$form['buttons']['next'] = array(
		'#type' => 'submit',
		'#value' => t('NEXT'),
		'#attributes' => array('class' => 'btn')            
	);
	
	$form['buttons']['back'] = array(
		'#type' => 'submit',
		'#value' => t('CANCEL'),
		'#validate' => array('pdl2_mobile_extendloan_back_validate'),
		'#submit' => array('pdl2_mobile_extendloan_back_submit'),
		'#attributes' => array('class' => 'btn back') 
	);      
	
	return $form;

}

function pdl2_mobile_extendloan_form_validate($form, &$form_state) {
	
	if ($form_state['values']['paymentType'] == '') {
		form_set_error('paymentAmount', t('You must choose a payment amount below'));
		return;
	}
	
	$pdl2 = pdl2_core_get_api();
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_extendloan_form_validate");

	$loanId = $pdl2->getOpenLoan();
	
	if (empty($loanId)) {
		drupal_goto('account');
	}
	
	$financeCharge = $pdl2->getFinanceCharges($loanId);
	$minimumPrincipalDue = $pdl2->getMinimumPrincipalDue($loanId);
	
	$paymentAmount = $minimumAmountDue = $financeCharge + $minimumPrincipalDue;
	
	if ($form_state['values']['paymentType'] == 1) {
		
		$paymentAmount = $form_state['values']['paymentAmount'];
		
		if ($paymentAmount <= $minimumAmountDue || $paymentAmount == '') {
			form_set_error('paymentAmount', t('You must enter a valid amount more than the minimum.'));
			return;
		}
			
	}
		
	$res = $pdl2->setPayment($loanId, $paymentAmount);
	if ($res->status != 1 || $res->result != 1) {
		form_set_error('paymentAmount', t($res->message->detail));
		return;
	}

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();
}

function pdl2_mobile_extendloan_form_submit($form, &$form_state) {
	drupal_goto('extend/agreement');
}

function pdl2_mobile_extendloan_back_validate($form, &$form_state) {
	
}

function pdl2_mobile_extendloan_back_submit($form, &$form_state) {
	drupal_goto('account');
}
<?php
function pdl2_mobile_loanconfirm_page($loanType) {
	
	$pdl2 = pdl2_core_get_api();
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_loanconfirm_page");

	//is logged in?
	if (!$pdl2->hasSessionData()) {
		drupal_goto("account/login");
	}
	//is opt out?
	else if ($pdl2->currUser->optOut) {
		drupal_goto('account/logout');
	}
	
	$loanId = $_SESSION['loanId'];
    
	if (empty($loanId)) {
		drupal_goto('account');
	}
	
	$loanStatusObject = $pdl2->getLoanStatus($loanId);
	$loanStatus = $loanStatusObject->loan_status;
	
	$slugs = array(
		'approved' => array(3,4,5),
		'denied' => array(9)
	);
	    
	$slug = multidimensional_search($loanStatus->code,$slugs);
	$slug = !$slug ? 'accepted' : $slug;
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

	drupal_goto( 'confirm/' . $loanType . '/' . $slug );
	
}

function multidimensional_search($needle,$haystack) {

	if (empty($haystack) || !isset($needle)) {
		return false;
	}
	
	foreach ($haystack as $slug => $values) {
		if(array_search($needle,$values) === false){ continue; } else { return $slug; }
	}
	
	return;
	
} 

function pdl2_mobile_loanconfirm_status($loanType,$loanStatus) {

	$pagename = 'confirm-'.$loanType.'-'.$loanStatus;
	$catid = '104CONF';
	
	$pdl2 = pdl2_core_get_api();
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_loanconfirm_status");

	//is logged in?
	if (!$pdl2->hasSessionData()) {
		drupal_goto("account/login");
	}
	//is opt out?
	else if ($pdl2->currUser->optOut) {
		drupal_goto('account/logout');
	}
	
	setcookie ("paymentmethod", "", time() - 3600);
	unset($_SESSION['loanId']);
	
	$themedata['loanType'] = $loanType;
	$themedata['loanStatus'] = $loanStatus;
	
	$referralCode = $pdl2->getReferralCode();
	$themedata['referralCode'] = $referralCode;
	
	$companyinfo = $pdl2->getCompanyInfo();
	$company_number = $companyinfo->company->customer_service_number->phone->number;
	$company_name = $companyinfo->company->name;
	$themedata['number'] = $company_number;
	$themedata['company_name'] = $company_name;

	if($loanType == 'extend' && $loanStatus == 'approved'){
		$output = theme('extendconfirmapproved', $themedata);
	}else{
	
		switch($loanStatus) {
			case 'denied':
				$output = theme('loanconfirmdenied', $themedata);
				break;
			case 'approved':
				$output = theme('loanconfirmapproved', $themedata);
				break;
			default:
				$output = theme('loanconfirmaccepted', $themedata);
				break;
		}
	}
	
	$output .= theme('return_home') . pdl2_mobile_write_pageview($pagename, $catid);
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

	return $output;
	
}
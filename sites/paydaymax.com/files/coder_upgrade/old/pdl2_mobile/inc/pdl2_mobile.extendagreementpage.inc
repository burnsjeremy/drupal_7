<?php

function pdl2_mobile_extendagreement_page() {

	$pagename = 'extend-agreement';
	$catid = '103EXT';
	
	$pdl2 = pdl2_core_get_api();

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_extendagreement_page");
	
	//is logged in?
	if (!$pdl2->hasSessionData()) {
		drupal_goto("account/login");
	}
	//is opt out?
	else if ($pdl2->currUser->optOut) {
		drupal_goto('account/logout');
	}
	
	$loanId = $pdl2->getOpenLoan();
	
	if (empty($loanId)) {
		drupal_goto('account');
	} else {
	
		if ($pdl2->isAsyncCalls()) {
			$async_requests['financeCharge'] = $pdl2->getExtensionFinanceChargeAsync($loanId);
			$async_requests['agreement'] = $pdl2->getExtensionAgreementAsync($loanId);
			$async_requests['borrower'] = $pdl2->getUserInfoAsync();
			$async_requests['ssn'] = $pdl2->getSsnAsync();
			$async_requests['apr'] = $pdl2->getExtensionAprAsync($loanId);
			
			foreach($async_requests as $key => $request){
				$$key = $pdl2->getResult($request);
				unset($async_requests[$key]);
			}
		}else{ //if not async
			$financeCharge = $pdl2->getExtensionFinanceCharge($loanId);
			$agreement = $pdl2->getDraftExtensionAgreement($loanId);
			$borrower = $pdl2->getUserInfo();
			$ssn = $pdl2->getSsn();
			$apr = $pdl2->getExtensionApr($loanId);
		}
		
		$themedata = array(
			'agreement' => $agreement,
			'date' => $agreement->loan_agreement->dueDatePrevious->date,
			'borrower' => $borrower,
			'apr' => $apr,
			'ssn' => $ssn
		);
		
		$output = drupal_get_form('pdl2_mobile_extendagreement_form', $themedata) . pdl2_mobile_write_pageview($pagename, $catid);
		
		if ($pdl2->isProfilingCalls())
			$pdl2->logger()->endProcess();

		return $output;
	}
}

function pdl2_mobile_extendagreement_form($form = array(), $themedata = array()) {
	$form = array();

	$form['lenderborrower'] = array('#value' => theme('lenderborrower', $themedata));
	
	$form['agreementIntro'] = array(
		'#value' => '<p><strong>Before an extension can be completed, you must check "I Agree" at the bottom of this agreement.</strong></p>'
	);
	
	$form['dashboard'] = array(
		'#prefix' => '<div class="dashboard">',
		'#suffix' => '</div>',
	);
	
	$form['dashboard']['agreementText'] = array('#value' => $themedata['agreement']->loan_agreement->agreementText);
	
	$form['extend_agree'] = array(
		'#type' => 'checkbox',
		'#title' => 'I agree <span class="form-required">*</span>',
	);
	
	$form['buttons'] = array(
		'#type' => 'fieldset',
		'#attributes' => array('class' => 'btns') 
	);
	
	$form['buttons']['next'] = array(
		'#type' => 'submit',
		'#value' => t('NEXT'),
		'#attributes' => array('class' => 'btn')   
	);
	
	$form['buttons']['back'] = array(
		'#type' => 'submit',
		'#value' => t('BACK'),
		'#validate' => array('pdl2_mobile_extendagreement_back_validate'),
		'#submit' => array('pdl2_mobile_extendagreement_back_submit'),
		'#attributes' => array('class' => 'btn back')   
	); 
	
	$form['required_key'] = array(
		'#value' => '<p class="center"><span class="form-required">*</span> Indicates a required field.</p>'
	);
	
	return $form;
}

function pdl2_mobile_extendagreement_form_validate($form, &$form_state) {
	
	$extend_agree = $form_state['values']['extend_agree'];
	
	if(!$extend_agree){
		form_set_error('loan_agree', t('You must agree to the terms.'));
	}
	
}

function pdl2_mobile_extendagreement_form_submit($form, &$form_state) {

	$pdl2 = pdl2_core_get_api();
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_extendagreement_form_submit");

	$loanId = $pdl2->getOpenLoan();
	
	if (empty($loanId)) {
		drupal_goto('account');
	} else {
		$result = $pdl2->extensionSave($loanId);
		$_SESSION['loanId'] = $loanId;
		
		if ($pdl2->isProfilingCalls())
			$pdl2->logger()->endProcess();
		
		drupal_goto('confirm/extend');
	}
}

function pdl2_mobile_extendagreement_back_validate($form, &$form_state) {
	
}

function pdl2_mobile_extendagreement_back_submit($form, &$form_state) {
	drupal_goto('extend');
}

<?php

function pdl2_mobile_loanrequest_page() {

	$pagename = 'apply-loanrequest';
	$catid = '102APPL';
	
	$pdl2 = pdl2_core_get_api();
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_loanrequest_page");
	
	//is logged in?
	if (!$pdl2->hasSessionData()) {
		drupal_goto("account/login");
	}
	//is opt out?
	else if ($pdl2->currUser->optOut) {
		drupal_goto('account/logout');
	}
	else if (!isset($_SESSION['loanDraftHash']) && !$_SESSION['loanDraftHash']){
		drupal_goto('account');
	}
	
	$draftLoanId = $_SESSION['loanDraftHash'];
	
	$clarity = $pdl2->getClarity($draftLoanId);
	
	if (empty($clarity)) {
		drupal_goto('apply');
	}
	
	if ($pdl2->isAsyncCalls()) {
		$async_requests['loanDueDates'] = $pdl2->getDueDatesAsync();
		$async_requests['maxLoanObject'] = $pdl2->getMaxLoanOfferAsync('maxLoanObject');
		$async_requests['dueDate'] = $pdl2->getLoanDraftDueDateAsync($draftLoanId);
		$async_requests['loanAmount'] = $pdl2->getPrincipalAsync($draftLoanId);
		$async_requests['hasCoupons'] = $pdl2->getCouponsAsync();
		$async_requests['financeCharge'] = $pdl2->getFinanceChargeAsync($draftLoanId);
		
		foreach($async_requests as $key => $request){
			$$key = $pdl2->getResult($request);
			unset($async_requests[$key]);
		}
	}else{ //if not async
		$loanDueDates = $pdl2->getDueDates();
		$maxLoanObject = $pdl2->getMaxLoanOffer('maxLoanObject');
		$dueDate = $pdl2->getLoanDraftDueDate($draftLoanId);
		$loanAmount = $pdl2->getPrincipal($draftLoanId);
		$hasCoupons = $pdl2->getCoupons();
		$financeCharge = $pdl2->getFinanceCharge($draftLoanId);
	}
	
	drupal_add_js(drupal_get_path('module', 'pdl2_mobile') .'/loanfeestotal.js');
	
	$loanIncrements = listsHelper::incrementalList(
		$maxLoanObject->minLoan, 
		$maxLoanObject->maxLoan, 
		$maxLoanObject->increment,
		true
	);
	
	if(is_array($loanDueDates->result)){
		foreach ($loanDueDates->result as $keyDue => $valDue) {
			$dueDates[$valDue->date] = $valDue->date;
		}
	} else {
		$dueDates[$loanDueDates->result->date] = $loanDueDates->result->date;
	}
	
	$themedata['loan'] = $loanAmount;
	$themedata['dueDate'] = $dueDate->date;
	$themedata['fees'] = $financeCharge;
	$themedata['total'] = $loanAmount + $financeCharge;
	
	$themedata['amounts'] = $loanIncrements;
	$themedata['dueDates'] = $dueDates;
	$themedata['hasCoupons'] = $hasCoupons;
	
	$output = drupal_get_form('pdl2_mobile_loanrequest_page_form', $themedata) . pdl2_mobile_write_pageview($pagename, $catid);
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

	return $output;
}

function pdl2_mobile_loanrequest_page_form($formArray, $themedata) {

	$form['loanfeestotal'] = array('#value' => theme('loanfeestotal', $themedata));
	
	$form['principal'] = array(
		'#type' => 'select',
		'#required' => true,
		'#title' => t('Select your desired loan amount'),
		'#options' => $themedata['amounts'],
        '#default_value' => $themedata['loan'],
	);
	
	if ($themedata['hasCoupons']) {
	
		$form['coupon'] = array(
			'#type' => 'textfield',
			'#title' => t('Enter a coupon code if you have one.'),
		);
	
	}
	
	$form['loanduedate'] = array(
		'#type' => 'select',
		'#required' => true,
		'#title' => t('Choose a due date for your loan.'),
		'#options' => $themedata['dueDates'],
        '#default_value' => $themedata['dueDate'],
	);
	
	$form['bank'] = array(
		'#value' => theme('bank')
	);
	
	$form['buttons'] = array(
		'#type' => 'fieldset',
		'#attributes' => array('class' => 'btns') 
	);
	
	$form['buttons']['next'] = array(
		'#type' => 'submit',
		'#value' => t('NEXT'),
		'#attributes' => array('class' => 'btn')   
	);
	
	$form['buttons']['back'] = array(
		'#type' => 'submit',
		'#value' => t('BACK'),
	    '#validate' => array('pdl2_mobile_loanrequest_back_validate'),
	    '#submit' => array('pdl2_mobile_loanrequest_back_submit'),
		'#attributes' => array('class' => 'btn back')   
	);
	
	$form['required_key'] = array(
		'#value' => '<p class="center"><span class="form-required">*</span> Indicates a required field.</p>'
	);
	
	return $form;

}

function pdl2_mobile_loanrequest_page_form_validate($form, &$form_state) {

	$pdl2 = pdl2_core_get_api();
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_loanrequest_page_form_validate");

	$principal = $form_state['values']['principal'];
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();
	
}

function pdl2_mobile_loanrequest_page_form_submit($form, &$form_state) {
	
	$pdl2 = pdl2_core_get_api();

	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_loanrequest_page_form_submit");

	$draftLoanId = $_SESSION['loanDraftHash'];
	
	if ($form_state['values']['coupon_code']) {
		$pdl2->setCoupon($draftLoanId, $form_state['values']['coupon_code']);
	}
		
	$pdl2->setLoanDueDate($draftLoanId, $form_state['values']['loanduedate']);
	$pdl2->setPrincipal($draftLoanId, $form_state['values']['principal']);
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->endProcess();

	drupal_goto('apply/agreement');
}

function pdl2_mobile_loanrequest_back_validate($form, &$form_state) {
	
}

function pdl2_mobile_loanrequest_back_submit($form, &$form_state) {
	drupal_goto('apply');
}

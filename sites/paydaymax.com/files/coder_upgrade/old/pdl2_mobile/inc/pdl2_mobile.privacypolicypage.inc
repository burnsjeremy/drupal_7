<?php

function pdl2_mobile_privacypolicy_page() {
	
	$pagename = 'privacy-policy';
	$catid = '101ACCT';
	
	$output = theme('privacypolicy') . drupal_get_form('pdl2_mobile_privacypolicy_form') . pdl2_mobile_write_pageview($pagename, $catid);
		
	return $output; 
	   
}

function pdl2_mobile_privacypolicy_form(){
	
	$form['buttons'] = array(
		'#type' => 'fieldset',
		'#attributes' => array('class' => 'btns') 
	);
	
	$form['buttons']['back'] = array(
		'#type' => 'submit',
		'#value' => t('BACK'),
		'#attributes' => array('class' => 'btn back')   
	);  
	
	return $form;
}

function pdl2_mobile_privacypolicy_form_submit($form, &$form_state) {
	drupal_goto('apply');
}
<?php

function pdl2_mobile_apply_page() {

	$pagename = 'apply';
	$catid = '102APPL';
	
	$pdl2 = pdl2_core_get_api();
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_apply_page");
	
	//is logged in?
	if (!$pdl2->hasSessionData()) {
		drupal_goto("account/login");
	}
	//is opt out?
	else if ($pdl2->currUser->optOut) {
		drupal_goto('account/logout');
	}
	
	$openLoan = $pdl2->getOpenLoan();
	
	if (!empty($openLoan)) {
		drupal_set_message(t('You have an open loan, and cannot apply for another at this time.'));
		drupal_goto('account');
	} else {
		$output = drupal_get_form('pdl2_mobile_apply_page_form') . pdl2_mobile_write_pageview($pagename, $catid);
		
		if ($pdl2->isProfilingCalls())
			$pdl2->logger()->endProcess();

		return $output;    
	}  
}

function pdl2_mobile_apply_page_form() {
	$form = array();
	
	$form['applyHeader'] = array(
		'#value' => '<p>By Clicking Next, you Agree to the Prequalification Agreement and the <a href="privacy-policy">Privacy Policy</a></p>'
	);
	
	$form['prequalAgreement'] = array(
		'#value' => theme('prequalagreement')
	);
	
	$form['buttons'] = array(
		'#type' => 'fieldset',
		'#attributes' => array('class' => 'btns') 
	);
	
	$form['buttons']['next'] = array(
		'#type' => 'submit',
		'#value' => t('NEXT'),
		'#attributes' => array('class' => 'btn')   
	);
	
	$form['buttons']['back'] = array(
		'#type' => 'submit',
		'#value' => t('CANCEL'),
		'#validate' => array('pdl2_mobile_apply_back_validate'),
		'#submit' => array('pdl2_mobile_apply_back_submit'),
		'#attributes' => array('class' => 'btn back')   
	);  
	
	return $form;
}

function pdl2_mobile_apply_page_form_validate($form, &$form_state) {
  
} 

function pdl2_mobile_apply_page_form_submit($form, &$form_state) {

	$pdl2 = pdl2_core_get_api();
	
	if ($pdl2->isProfilingCalls())
		$pdl2->logger()->startProcess("pdl2_mobile", "pdl2_mobile_apply_page_form_submit");

	$res = $pdl2->createLoanDraft();
	
	if (!empty($res)) {
		setcookie ("paymentmethod", "", time() - 3600);
		unset($_COOKIE['paymentmethod']);
		$_SESSION['loanDraftHash'] = $res;
		$pdl2->setClarity($_SESSION['loanDraftHash']);
		
		if ($pdl2->isProfilingCalls())
			$pdl2->logger()->endProcess();
		
		drupal_goto('apply/loanrequest'); 
	}  
  
}

function pdl2_mobile_apply_back_validate($form, &$form_state) {
  
}

function pdl2_mobile_apply_back_submit($form, &$form_state) {
	drupal_goto('account');
}

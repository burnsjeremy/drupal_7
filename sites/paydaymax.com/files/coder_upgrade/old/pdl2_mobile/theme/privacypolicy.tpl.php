<div class="dashboard">
	<h1>MyCashNow&trade; Privacy Policy</h1>
	<p>This online Privacy Policy applies to the MyCashNow website. This Privacy Policy explains how MyCashNow collects, uses and shares information from or about you when you visit our site as well as provides information on how we advertise, both on our site and other websites, and how we collect information based on your interactions with our site and our online ads. </p>
	
	<p>MyCashNow is committed to protecting the privacy of your personal information. Because of this, we want you to know that we view your security as a major concern, and that it is important to us. MyCashNow has dedicated a large portion of electronic, physical, and procedural safeguards to keep our clients' personal information safe. The security and confidence of our clients is an ongoing focus of our company.</p>
	<ol class="pp-order-list">

		<!-- 1 -->
		<li><strong>Protecting the Privacy of Customer Information</strong>
		<br />
			We are committed to protecting your privacy and providing a secure online experience. We restrict access to your personal information to those who need to know that information to provide products or services to you that were requested. We maintain physical, electronic, and procedural safeguards that comply with federal regulations to transmit and store information about you.  If you become an inactive customer, we will continue to adhere to the privacy policies and practices described in this notice.</li>
		
		<!-- 2 -->
		<li><strong>Collecting Information:</strong>
		<br />
			Types of information collected:
			<br />
			<br />

			We may collect different types of personal and other information based on your use of our services and our business relationship with you. Some examples include:
			<br />
			<br />
			
			<ul class="padding-bottom" style="list-style:lower-alpha;">
			  <!--a-->
				<li>Personal information that you provide via forms, surveys, applications, or using other online fields, or other forms of communication including telephone or email. Personal information obtained is used to verify representations made by you such as identity, employment history or income; collected information includes:
					<ul class="no-padding" style="list-style:lower-roman;">
						<li>Your name,</li>
						<li>Your address,</li>
						<li>Your telephone number,</li>
						<li>Social Security number,</li>
						<li>Your driver license number,</li>
						<li>Your email address,</li>
						<li>Your mobile location,</li>
						<li>Your bank account information,</li>
						<li>Your employment information,</li>
						<li>Your income.</li>
					</ul>
				</li>
				<!--b-->
				<li>Financial information related to your business relationship with us or others regarding your credit worthiness and credit history; such as:
					<ul class="no-padding" style="list-style:lower-roman;">
						<li>Your account history,</li>
						<li>Your transaction balances,</li>
						<li>Your payment and overdraft histories,</li>
						<li>Additional bank accounts owned by you.</li>

					</ul>
				</li>
				<!--c-->
				<li>Website usage and browsing information including your preferences, pages visited, and technical information regarding your computer and operating systems such as:
					<ul class="no-padding" style="list-style:lower-roman;">
						<li>Your Internet Protocol (IP) address,</li>
						<li>Domain name,</li>
						<li>System configuration and settings.</li>
					</ul>
				</li>
			</ul>
			
			Sources of collected information:
			<br />
			<br />
			
			We collect information about you from several sources:
			<br />
			<br />
			
			<ul class="no-padding" style="list-style:lower-alpha;">
				<li>Information you provide online,</li>
				<li>Information you provide on applications and forms,</li>
				<li>Other information you may provide when communicating with us via telephone or email,</li>
				<li>Information from third parties such as consumer reporting agencies and other lenders,</li>
				<li>Automatically when you visit our website or use our services.</li>
			</ul>
		</li>
		
		<!-- 3 -->
		<li><strong>Cookies and Other Technologies</strong>
		<br />
		<br />
		
		Cookies are pieces of information stored directly on the computer you are using. MyCashNow, or our third-party service providers (including advertising service providers) may place cookies or Flash objects on the computer you are using so that we can continually improve the design, content, and functionality of our site to better serve you. 
		<br />
		<br />
		
		Cookies allow us to:<br>
		<br />
			
			<ul class="no-padding" style="list-style:lower-alpha;">
				<li>Offer more relevant content and/or services</li>
				<li>Collect technical and navigational information, such as browser type, time spent on site and pages visited</li>
				<li>Monitor viewing and responses to MyCashNow advertisements</li>
			</ul>
			
			<br />
			
			When you visit our site, MyCashNow utilizes cookies to monitor your navigation of our site and provide you with information on products and services that best fit your needs. Cookies we use do not contain or capture personally identifiable information. If you choose not to accept cookies from MyCashNow you may not have access to use all or part of our site or you may not benefit fully from the information and services offered.
			
			<br />
			<br />

			MyCashNow&rsquo;s advertising service providers may serve advertising through the use of web beacons. A web beacon is placed on a web page and, in addition to setting HTML cookies, can capture information such as IP address, time, web browser, time zone and domain. Other technologies we use may capture customer specific data to help us understand how customers use our site.
		</li>
		
		<!-- 4 -->
		<li><strong>Advertising</strong>
		<br />
		
			MyCashNow&rsquo;s advertising service providers advertise our services on websites not affiliated with us. MyCashNow uses cookies on our website and the sites on which we advertise to track advertising performance and to collect aggregate data on webpage viewing. Cookies are not used to collect or disseminate any personal information.
			<br />
			<br />
			
			We and our non-MyCashNow advertising service providers use this information, as well as other information they have or we may have, to help make decisions about ads you see on other sites. 
			<br />
			<br />
			
			Some ads that you receive are customized based on your visits over time and across different Web sites. This type of customization - sometime referred to as behavioral or interest-based advertising - is enabled through your computer browser and browser cookies. Information collected by these cookies is not personally identifiable. This type of advertising not only helps you to receive more customized advertising offers and promotions but also helps support the free content, products, and services you get online.
			<br />
			<br />
			
			If you prefer not to have non-personal information used in this way, you may opt out of online behavioral advertising by visiting this link <a href="http://www.aboutads.info/">http://www.aboutads.info/</a>.
			<br />
			<br />
			
			Please note that if you opt out, you may still receive online advertising from MyCashNow. Opting out from a specific advertising provider means that the ads you do receive will not be based on your preferences or behavior. In order for behavioral advertising opt-outs to work on the computer you are using, your browser must be set to accept cookies. <strong>If you delete cookies, buy a new computer, access the site from a different computer, login under a different screen name, or change web browsers you will need to opt out again.</strong> If the browser you are using has scripting disabled, you do not need to opt out, as online behavioral advertising technology does not work when scripting is disabled. Please check the browser's security settings to validate whether scripting is active or disabled.
			<br />
			<br />
			
			MyCashNow is not affiliated with <a href="http://www.aboutads.info/">www.aboutads.info/</a> and cannot warrant the effectiveness of the opt out procedures.
		</li>
		
		<!-- 5 -->
		<li><strong>Using and Sharing Information</strong>
		  <br />
			<br />
			
			<em><strong>How We Use Information We Collect</strong></em>
			<br />

			<ul class="no-padding" style="list-style:lower-alpha;">
				<li>To provide you with the best customer service and experience possible</li>
				<li>To provide products and services you request or may be of interest to you and to respond to your questions</li>
				<li>To communicate with you regarding service updates, offers, and promotions</li>
				<li>To deliver customized content and advertising that may be of interest to you</li>
				<li>To process payments and update account records</li>
				<li>To analyze customer demographics, payment histories, and preferences</li>
				<li>To comply with reporting and other legal requirements</li>
			</ul>
			<br />
			
			<em><strong>Parties to Whom We Disclose</strong></em>
			<br />
			<br />
			
			<strong>Information we share with Third Parties:</strong>
			<br />

			We share the nonpublic personal information we collect from you with consumer reporting and related agencies and third-party service providers. If you submit your information to us or apply for our services, you may not limit this sharing. Finally, we may share the nonpublic personal information we collect from you with companies or organizations with whom we have a marketing or other agency relationship, generally to offer you goods or services in which we believe you may have an interest.  You may opt-out of this sharing by selecting the &ldquo;No&rdquo; option on the Other Lenders disclosure at the time of your application, or at any time by logging into your MyCashNow account and changing your sharing preferences.
			<br />
			<br />
			
			<em><strong>Furnishing Negative Information Notice</strong></em>
			<br />
			<br />
			
			We may report information about your account to consumer reporting agencies. Late payments, missed payments or other defaults on your account may be reflected in your credit report.
		</li>
		
		<!-- 6 -->
		<li><strong>Information or Email from Us</strong>
		  <br />
			<br />
			
			Our email marketing is permission based. If you receive a mailing from us our records indicate that you have 1) shared this information for the purpose of receiving information in the future; or 2) have entered into a financial agreement with MyCashNow. <br>
			<br />
			
			We may send you email regarding our services from time to time, as well as updates regarding your current transaction. We may also send out periodic emails informing you of technical service issues related to a service you requested.  If you do not want to continue to receive opted in promotional messaging, please opt out by logging into your MyCashNow account and changing your email preference settings. Please note you will continue to receive emails in regard to your current transaction or business relationship with MyCashNow.
		</li>
		
		<!-- 7 -->
		<li><strong>Opt Out Rights</strong>
		  <br />
			<br />
			
			You may opt out in the following ways:
			<br />
			<br />
			
			<ul class="no-padding" style="list-style:lower-alpha;">
				<li>By choosing the &ldquo;No&rdquo; option on the Other Lenders disclosure during your application or at any time by changing your MyCashNow account preferences</li>

				<li>By opting out of behavioral targeting at <a href="http://www.aboutads.info/">http://www.aboutads.info/</a>.</li>
				<li>By adjusting your email settings and preferences to reflect your decision to opt out of marketing emails.</li>
			</ul>
		</li>
		
		<!-- 8 -->
		<li><strong>Accuracy of Your Account Information</strong>
		<br />
		<br />
		
		We have established procedures to ensure that your financial information is accurate, current and complete, in keeping with reasonable industry standards. We continually strive to maintain complete and accurate information about you and your accounts. Should you ever believe that our records contain inaccurate or incomplete information about you, please notify us.
    </li>
		
		<!-- 9 -->
		<li><strong>Changes to this Privacy Policy</strong>
		<br />
		<br />
		
		We reserve the right to change or remove portions of our Privacy Policy at any time without prior notification. Please check the website frequently to apprise yourself of any changes that may affect you. The posting of any changes will be the only notice that you will receive regarding any such amendments or changes in policy.
		</li>
		
		<!-- 10 -->
		<li><strong>Additional Security Precautions</strong>
		<br />
		<br />
		
		You also have a significant role in protecting your information. Do not share your login information with others. If you use this site, you are responsible for maintaining the confidentiality of your account, password, and restricting access to your computer. You agree to accept responsibility for all activities that occur under your account and password.
		</li>
		
		<!-- 11 -->
		<li><strong>Children and the Internet </strong>
		<br />
		<br />
		
		We do not knowingly collect or retain personally identifiable information from consumers under the age of eighteen (18). You must be at least eighteen (18) years of age to obtain services from us. By completing an application, you are representing to us that you are 18 years of age or older.
		</li>

		<!-- 12 -->
		<li><strong>What If I Disagree with the Terms of Your Privacy Policy?</strong>
		<br />
		<br />
		If you disagree with the terms of our privacy policy, please do not use our website or provide us with any of your personal information.
		</li>
	</ol>
</div>
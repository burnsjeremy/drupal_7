<div class="dashboard">
	<h3>Application Accepted.</h3>
	<p class="center">A decision on your loan is still pending. We will notify you via email when we reach a decision, but you can contact customer service at <?php echo $themedata['number']; ?> if you have questions.</p>
	<p class="center">You may view the loan agreement by logging in to your <?php echo $themedata['company_name']; ?> account from a desktop computer.</p>
</div>
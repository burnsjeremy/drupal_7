<?php if ( $themedata['openLoan'] && $themedata['loantype'] === 'payday' ) :?>
	<div class="dashboard">
		<?php
		if($themedata['loanStatus']->name == 'Pending') :
			print '<p class="center">A decision on your loan is still pending. We will notify you via email when we reach a decision, but you can contact customer service at ' . $themedata['companyNumber'] . ' if you have questions.</p>';
		else:
		?>
			<table>
				<tr class="center">
					<th rowspan="2" style="text-transform: uppercase;">ACTIVE</th>
					<td><?php echo ($themedata['loanStatus']->code == 0) ? 'Deposit' : 'Balance';?></td>
					<th rowspan="2"></th>
					<td>Due Date</td>
				</tr>
				<tr class="center">
					<th>$<?php echo number_format($themedata['loanBalance'], 2);?></th>
					<th><?php echo date('F jS, Y', strtotime($themedata['loanDueDate']->date));?></th>
				</tr>
			</table>
		</div>
		<div class="dashboard">
			<?php 
				if ($themedata['allowExtension']) : 
					//////////////
					// active loan, can extend
			?>
					<form action="" method="post" id="pdl2-mobile-account-extend-form">
						<input type="submit" name="op" id="edit-extend" value="I want to extend my loan"  class="form-submit btn lrg smtxt" onClick="window.location.href=Drupal.settings.basePath+'extend'; return false;" />
					</form>
					<?php //echo  drupal_get_form('pdl2_mobile_account_extend_form'); ?>
					<p>Would you like to extend your loan? Doing so will give you more time to pay off your loan, easing your financial burden, at a slight increase in cost. To get an extension, you must apply by 4 pm PST the day prior to your due date.</p>
					<p>You can apply for up to three extensions. After the third extension, we require an additional payment of $50.00 toward the principal amount for loans of $600.00 or less until the principal is fully repaid. For cash advances of $600.01 and above, we require an additional payment of $100.00 toward the loan principal until the principal is fully repaid.</p>
					<p>MyCashNow customers who receive their paychecks on a monthly basis are required to make a payment towards the principal on their 2nd extension. Loans of $600.00 or less require a payment of $100.00 more than the fees due until the principal is fully repaid. Loans of $600.01 and above require a payment of $200.00 more than the fees due until the principal is fully repaid.</p>

			<?php 
				else: 
					/////////////
					// active loan, cannot extend
			?>
					<p>Are you familiar with our referral program? If you refer a new customer who hasn't applied at <?php echo $themedata['companyName']; ?>, we'll give you $100 once they're approved for their first loan. Just tell them to apply at MyCashNow.com and use your referral code, <strong><?php echo $themedata['referralCode']; ?></strong>.</p>
			<?php endif; ?>
		<?php endif; //end else pending ?>
	</div>
<?php else: //no active loan ?>

	<div class="dashboard center">
		<?php 
		if( $themedata['autoApproved'] && $themedata['loantype'] != 'personal' ) : 
			////////////////////
			// no active loan, auto-approved
		?>
			<h2>YOU ARE PRE APPROVED FOR UP TO</h2><h1>$<?php echo number_format($themedata['maxLoanOffer'], 2);?></h1>
			<form action="" method="post" id="pdl2-mobile-account-apply-form">
				<input type="submit" name="op" id="edit-apply" value="APPLY NOW"  class="form-submit btn" onClick="window.location.href=Drupal.settings.basePath+'apply'; return false;" />
			</form>
			<?php //echo  drupal_get_form('pdl2_mobile_account_apply_form'); ?>
		<?php
		else: 
			///////////////////
			//no active loan, not auto-approved 
		?>
			<p>At this time, <?php echo $themedata['companyName']; ?>'s mobile application is not yet available to all customers.</p>
		<?php endif; ?>
	</div>
	
<?php endif; ?>
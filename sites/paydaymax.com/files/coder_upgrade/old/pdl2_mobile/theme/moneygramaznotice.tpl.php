------------- IMPORTANT NOTICE FOR ARIZONA MONEYGRAM CUSTOMERS -------------<br />
 <br />
Regulations in Arizona limit the amount of a MoneyGram transaction to $499.99.<br />
 <br />
You have requested an amount over $499.99.<br />
 <br />
You will need to :<br />
- Change the way you receive the funds<br />
- Request less than $499.99<br />
 <br />
Click OK to continue with MoneyGram or Cancel to make a change.<br />
 <br />
------------- IMPORTANT NOTICE FOR ARIZONA MONEYGRAM CUSTOMERS -------------